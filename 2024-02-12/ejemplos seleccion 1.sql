﻿USE empresas;

-- EJERCICIO 1
-- LAS EMPRESAS QUE SON DE SANTANDER Y QUE TENGAN MAS DE 8 DE TRABAJADORES

SELECT 
    * 
  FROM empresas e
  WHERE 
    e.poblacion = 'Santander'
    AND 
    e.trabajadores>8;

-- EJERCICIO 2
-- LAS EMPRESAS QUE SON DE SANTANDER O QUE TENGAN MAS DE 8 DE TRABAJADORES

SELECT 
    * 
  FROM empresas e
  WHERE 
    e.poblacion = 'Santander'
    OR 
    e.trabajadores>8;


-- EJERCICIO 3
-- LAS EMPRESAS QUE TIENEN ENTRE 5 Y 10 TRABAJADORES 
-- (INCLUYENDO EL 5 Y EL 10, INTERVALO CERRADO)

-- OPCION 1
SELECT 
    * 
  FROM empresas e
  WHERE 
    e.trabajadores>=5
    AND
    e.trabajadores<=10;

-- OPCION 2
SELECT 
    * 
  FROM empresas e
  WHERE 
  e.trabajadores BETWEEN 5 AND 10;  


-- EJERCICIO 4
-- LAS EMPRESAS QUE TIENEN MENOS DE 5 TRABAJADORES O MAS DE 8 TRABAJADORES

SELECT 
    * 
  FROM empresas e
  WHERE 
    e.trabajadores<5
    OR
    e.trabajadores>8;

-- EJERCICIO 5
-- LAS EMPRESAS QUE ESTAN SITUADAS EN SANTANDER O EN ISLA O EN TORRELAVEGA

-- opcion 1
SELECT 
    * 
  FROM empresas e
  WHERE 
    e.poblacion='santander'
    OR 
    e.poblacion='isla'
    OR 
    e.poblacion='torrelavega';

-- opcion 2
SELECT 
  *
  FROM empresas e
  WHERE 
  e.poblacion IN ('santander','isla','torrelavega');


-- EJERCICIO 6
-- QUIERO LAS PERSONAS QUE HAN APROBADO LOS TRES EXAMENES (>=5) 
-- LOS EXAMENES SON DATO1, DATO2, DATO3

SELECT 
    * 
  FROM personas p
  WHERE 
    p.dato1>=5
    AND 
    p.dato2>=5
    AND 
    p.dato3>=5;

-- EJERCICIO 7
-- QUIERO LAS PERSONAS QUE HAN APROBADO ALGUNO DE LOS TRES EXAMENES (>=5) 
-- LOS EXAMENES SON DATO1, DATO2, DATO3    
-- OPCION 1
SELECT 
    * 
  FROM personas p
  WHERE 
    p.dato1>=5
    OR 
    p.dato2>=5
    OR
    p.dato3>=5;

-- OPCION 2
-- INDICAME LOS ALUMNOS QUE NO HAN SUSPENDIDO LOS 3 EXAMENES
-- LA MISMA PREGUNTA AL REVES
SELECT 
    * 
  FROM personas p
  WHERE
  NOT( 
    p.dato1<5
    AND  
    p.dato2<5
    AND 
    p.dato3<5);


-- EJERCICIO 8
-- LAS PERSONAS CUYA FECHA DE NACIMIENTO 
-- ESTA ENTRE ENERO Y NOVIEMBRE DEL AÑO 1990

-- opcion 1
SELECT 
    * 
  FROM personas p
  WHERE 
    p.fecha>='1990-1-1'
    AND 
    p.fecha<='1990-11-30';

-- opcion 2
SELECT 
    * 
  FROM personas p
  WHERE 
    p.fecha BETWEEN '1990-1-1' AND '1990-11-30';

-- opcion 3

SELECT 
    * 
  FROM personas p
  WHERE
    MONTH(p.fecha) BETWEEN 1 AND 11
    AND 
    YEAR(p.fecha) = 1990;


-- EJERCICIO 9
-- LISTAR LAS PERSONAS QUE HAN NACIDO EN NOVIEMBRE

SELECT 
    * 
  FROM personas p
  WHERE   
    MONTH(p.fecha)=11;
  

-- EJERCICIO 10
-- SACAR LOS MESES DE NACIMIENTO DE CADA PERSONA JUNTO CON SU NOMBRE

SELECT 
    p.nombre,
    p.fecha,
    MONTH(p.fecha) mes
  FROM personas p;


-- EJERCICIO 11
-- LISTAR EL MES, EL AÑO Y EL DIA JUNTO CON EL NOMBRE DEL MES Y EL NOMBRE DE CADA PERSONA

SELECT 
  p.nombre,
  MONTH(p.fecha) mes,
  YEAR(p.fecha) año,
  DAY(p.fecha) dia,
  MONTHNAME(p.fecha) nombreMes
  FROM personas p;




