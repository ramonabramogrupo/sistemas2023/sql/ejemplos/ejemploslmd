﻿USE empresas;

-- ejercicio 1
-- Listar el nombre y apellidos de las personas

SELECT DISTINCT p.nombre,p.apellidos FROM personas p;


-- Ejercicio 2
-- Listar el id, el nombre y los apellidos de las personas 

SELECT p.id,p.nombre,p.apellidos FROM personas p;

-- Ejercicio 3
-- Listas poblaciones donde vive alguna persona

SELECT DISTINCT p.poblacion FROM personas p;

-- Ejercicio 4
-- Listar el nombre de las personas (renombrar el campo como nombrePersona)

SELECT 
  DISTINCT  -- quito los nombres repetidos
    p.nombre nombrePersona  -- coloco un alias al campo
  FROM 
    personas p; -- coloco un alias a la tabla

-- Ejercicio 5
-- Listar el nombre y poblacion de las empresas

SELECT DISTINCT e.nombre,e.poblacion FROM empresas e;


-- EJERCICIO 6
-- MOSTRAR EL NOMBRE DE LAS EMPRESAS
SELECT DISTINCT e.nombre FROM empresas e;

-- EJERCICIO 7
-- MOSTRAR LA POBLACION DE LAS EMPRESAS
SELECT DISTINCT e.poblacion FROM empresas e;

-- EJERCICIO 8
-- MOSTRAR EL NOMBRE DE LAS PERSONAS
SELECT DISTINCT p.nombre FROM personas p;

-- EJERCICIO 9
-- MOSTRAR LA FECHA Y EL NOMBRE DE LAS PERSONAS
SELECT DISTINCT p.fecha,p.nombre FROM personas p;

-- EJERCICIO 10
-- MOSTRAR EL CODIGO DE LAS EMPRESAS QUE TIENEN TRABAJADORES
SELECT DISTINCT p.empresa FROM personas p;

-- EJERCICIO 11
-- LISTAR EL CODIGO, EL NOMBRE Y LOS APELLIDOS DE LAS PERSONAS QUE SE LLAMAN MIGUEL
SELECT 
    p.id,p.nombre n,p.apellidos  -- campos (proyectar)
  FROM 
    personas p -- tablas (relaciones)
  WHERE 
    p.nombre='miguel'; -- condicion (seleccionar)

-- EJERCICIO 12
-- LISTAR EL ID Y EL NOMBRE DE LAS PERSONAS CUYA FECHA ES CONOCIDA
-- IS NULL (LOS QUE NO TIENEN NADA EN ESE CAMPO)
-- IS NOT NULL (LOS QUE TIENEN ALGO ESCRITO EN EL CAMPO)

SELECT p.id,p.nombre FROM personas p WHERE p.fechaTrabaja IS NOT NULL;

-- EJERCICIO 13
-- codigo, nombre y poblacion de las empresas que tienen mas de 5 trabajadores
-- ordenado por poblacion de forma descendente

SELECT 
    e.codigo,e.nombre,e.poblacion  -- campos (proyecciones)
  FROM 
    empresas e  -- tablas
  WHERE  
    e.trabajadores>5  -- condicion (selecccion)
  ORDER BY 
    e.poblacion DESC; -- campos para ordenar el resultado

-- EJERCICIO 14
-- CALCULAR LA NOTA MEDIA DE CADA PERSONA. Mostrar esa nota con el nombre y los apellidos

SELECT 
    (p.dato1+p.dato2+p.dato3)/3 notaMedia, -- campo calculado (expresion)
    p.nombre,
    p.apellidos
  FROM personas p;









