﻿-- vamos a trabajar con esta base de datos
-- que contiene dos tablas
USE empleadosdepartamentos;

-- vamos a listar la tabla emple

SELECT * FROM emple e;

-- vamos a lista la tabla depart

SELECT * FROM depart d;

-- combinacion interna
-- JOIN
-- INNER JOIN

SELECT 
    * 
  FROM emple e
    JOIN depart d ON e.dept_no = d.dept_no;

-- REALIZAR EL JOIN CON EL PRODUCTO CARTESIANO
SELECT 
    * 
  FROM emple e,depart d 
    WHERE d.dept_no=e.dept_no;

/*
LO QUE ME MUESTRA SON TODOS LOS EMPLEADOS CON LOS DATOS DEL DEPARTAMENTO
PARA EL QUE TRABAJAN.
SOLO SALEN LOS EMPLEADOS CUYO DEPT_NO ESTA EN DEPARTAMENTOS.
SOLO ME SALEN LOS DEPARTAMENTOS QUE TIENEN EMPLEADOS
*/

-- DEMOSTRAR EL PROBLEMA CON EL JOIN

-- LISTAR LOS NOMBRES DE LOS DEPARTAMENTOS CON SU CODIGO

-- hasta ahora
SELECT d.dept_no,d.dnombre FROM depart d;

-- ahora
SELECT DISTINCT 
    d.dept_no,d.dnombre 
  FROM depart d 
    JOIN emple e ON d.dept_no = e.dept_no;

/*
EL PROBLEMA ES QUE EN ESTA CONSULTA NO TENGO EL 
DEPARTAMENTO 40 PORQUE NO TIENE EMPLEADOS
*/


-- COMBINACION EXTERNA
-- LEFT JOIN | LEFT OUTER JOIN
-- RIGHT JOIN | RIGHT OUTER JOIN

-- INDICAME EL DEPARTAMENTO DEL CUAL NO TENGO EMPLEADOS

SELECT 
    * 
  FROM depart d LEFT JOIN emple e ON d.dept_no = e.dept_no
  WHERE e.dept_no IS NULL;

SELECT 
    * 
  FROM emple e RIGHT JOIN depart d ON e.dept_no = d.dept_no
  WHERE e.dept_no is NULL;


USE ciclistas;

-- Indicame el NOMBRE de los ciclistas que han ganado etapas
-- UTILIZAR INNER JOIN

SELECT DISTINCT
    c.nombre 
  FROM ciclista c 
    JOIN etapa e ON c.dorsal = e.dorsal;

-- Indicame el nombre de los ciclistas que NO han ganado etapas
-- UTILIZAR LEFT JOIN
-- ciclistas - etapa

SELECT 
  c.nombre 
  FROM ciclista c 
    LEFT JOIN etapa e ON c.dorsal = e.dorsal
  WHERE e.dorsal is NULL;








