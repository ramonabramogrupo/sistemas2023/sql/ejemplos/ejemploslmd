﻿-- 1 paso
-- listar el dorsal de los ciclistas que han llevado el maillot amarillo

SELECT DISTINCT c.dorsal,c.nombre 
FROM ciclista c 
  JOIN lleva l ON c.dorsal = l.dorsal
  JOIN maillot m ON l.código = m.código
WHERE 
  m.color='amarillo';

-- 2 paso
-- listar el dorsal y el nombre de todos los ciclistas
SELECT c.dorsal,c.nombre 
FROM ciclista c;

-- 3 paso
-- restar todos los ciclistas menos los que han llevado alguna 
-- vez el maillot amarillo
-- ciclistas - c1
SELECT 
  c.dorsal,
  c.nombre 
FROM ciclista c
  LEFT JOIN (
      SELECT DISTINCT c.dorsal,c.nombre 
      FROM ciclista c 
        JOIN lleva l ON c.dorsal = l.dorsal
        JOIN maillot m ON l.código = m.código
      WHERE 
        m.color='amarillo'
  ) c1 using(dorsal)
  WHERE c1.dorsal is null;

-- aprovecha EXCEPT
-- C2-C1
SELECT c.dorsal,c.nombre FROM ciclista c
EXCEPT
SELECT DISTINCT c.dorsal,c.nombre 
FROM ciclista c 
  JOIN lleva l ON c.dorsal = l.dorsal
  JOIN maillot m ON l.código = m.código
WHERE 
  m.color='amarillo';


-- lo mismo pero optimizando el paso1


-- 0 paso
-- sacar el codigo del maillot amarillo
SELECT m.código FROM maillot m WHERE m.color='amarillo';

-- 1 paso
-- listar el dorsal de los ciclistas que han llevado el maillot amarillo

SELECT DISTINCT l.dorsal
FROM lleva l
WHERE 
  l.código IN (SELECT m.código FROM maillot m WHERE m.color='amarillo');

-- 2 paso
-- listar el dorsal y el nombre de todos los ciclistas
SELECT c.dorsal,c.nombre 
FROM ciclista c;

-- 3 paso
-- restar todos los ciclistas menos los que han llevado alguna 
-- vez el maillot amarillo
-- ciclistas - c1
SELECT 
  c.dorsal,
  c.nombre 
FROM ciclista c
  LEFT JOIN (
      SELECT DISTINCT l.dorsal
      FROM lleva l
      WHERE 
        l.código IN (SELECT m.código FROM maillot m WHERE m.color='amarillo')
  ) c1 using(dorsal)
  WHERE c1.dorsal is null;

-- aprovecha EXCEPT
-- C2-C1
SELECT c.dorsal FROM ciclista c
EXCEPT
  SELECT DISTINCT l.dorsal
  FROM lleva l
  WHERE 
    l.código IN (SELECT m.código FROM maillot m WHERE m.color='amarillo');

-- terminar la consulta JOIN
SELECT c.dorsal,c.nombre FROM 
ciclista c JOIN (
SELECT c.dorsal FROM ciclista c
EXCEPT
  SELECT DISTINCT l.dorsal
  FROM lleva l
  WHERE 
    l.código IN (SELECT m.código FROM maillot m WHERE m.color='amarillo')
)c3 using(dorsal);

