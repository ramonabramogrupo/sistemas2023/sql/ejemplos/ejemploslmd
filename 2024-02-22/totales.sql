﻿use empresas;

-- utilizar la funcion ceil
-- devuelve el entero superior
-- utilizar la funcion truncate
-- devuelve el numero TRUNCADO con los decimales que le indico en el segundo argumento
-- utilizar la funcion round
-- devuelve el numero REDONDEADO con los decimales que le indico en el segundo argumento

SELECT CEIL(2.1),CEIL(-2.1),CEIL(12.958);

SELECT TRUNCATE(2.1,0),TRUNCATE(-2.1,0),TRUNCATE(12.958,1);

SELECT ROUND(2.1),ROUND(-2.1,0),ROUND(12.958,1),ROUND(12.958);


-- LISTAME EL NOMBRE DE LA PERSONA CON LA NOTA MEDIA DE LAS TRES NOTAS (DATO1,DATO2,DATO3) 
-- REDONDEADA A 2 DECIMALES

SELECT 
    p.nombre,
    round((p.dato1+p.dato2+p.dato3)/3,2) media 
  FROM personas p;


-- DISTINCT ES PARA QUITAR REPETIDOS DEL RESULTADO
-- listar las poblaciones de las personas

SELECT DISTINCT  
    p.poblacion 
  FROM personas p;

-- Quiero conocer todas las personas que trabajan en la misma empresa que pedro rodriguez

-- Consulta 1
-- Sacar la empresa para la que trabaja Pedro Rodriguez
SELECT 
    p.empresa 
  FROM 
    personas p 
  WHERE 
    p.nombre='pedro' AND p.apellidos='rodriguez';

-- Consulta 2
SELECT 
    * 
  FROM 
    personas p
  WHERE  
    p.empresa=(
                SELECT 
                p.empresa 
              FROM 
                personas p 
              WHERE 
                p.nombre='pedro' AND p.apellidos='rodriguez'
    );


-- Empresas que trabajan en la misma poblacion que la empresa numero 3

-- Consulta 1
-- poblacion en donde trabaja la empresa 3
SELECT e.poblacion FROM empresas e WHERE e.codigo=3;

-- Consulta 2
-- empresas que trabajan en la poblacion de la empresa 3

SELECT 
    * 
  FROM 
    empresas e
  WHERE 
    e.poblacion=(
      SELECT e.poblacion FROM empresas e WHERE e.codigo=3
    );

-- sintaxis de crear vista
-- CREATE VIEW nombreVista AS consulta;

-- Sintaxis opcional
-- CREATE OR REPLACE VIEW nombreVista AS consulta;

-- si quiero probar la vista
-- select * from nombreVista;

-- si quiero eliminar la vista
-- DROP VIEW nombreVista;




-- Empresas que trabajan en la misma poblacion que la empresa numero 3
-- QUIERO REALIZAR LA CONSULTA ANTERIOR CON VISTAS


-- Consulta 1
-- poblacion en donde trabaja la empresa 3
CREATE OR REPLACE VIEW consulta1 AS 
  SELECT e.poblacion FROM empresas e WHERE e.codigo=3;


-- Consulta 2
-- empresas que trabajan en la poblacion de la empresa 3

SELECT 
    * 
  FROM 
    empresas e
  WHERE 
    e.poblacion=(
      SELECT * FROM consulta1 c
    );


SELECT * from personas p;


-- Personas que hayan nacido el mismo mes que miguel garcia

-- Consulta 1
-- Sacar el mes de nacimiento de miguel garcia (11)

-- opcion 1
SELECT MONTH(p.fecha) mes FROM personas p 
  WHERE p.nombre='miguel' AND p.apellidos='garcia';

-- opcion 2
SELECT MONTH(p.fecha) mes FROM personas p 
  WHERE CONCAT_WS(" ",p.nombre,p.apellidos)='miguel garcia';

-- Consulta 2
-- Sacar las personas que han nacido el mismo mes que miguel garcia (11)
-- 10,5,19,6,4

SELECT 
    * 
  FROM personas p 
  WHERE 
    MONTH(p.fecha)=(
      SELECT MONTH(p.fecha) mes FROM personas p 
        WHERE CONCAT_WS(" ",p.nombre,p.apellidos)='miguel garcia'
    );

-- Realizar lo mismo pero con vistas

-- Consulta 1
-- Sacar el mes de nacimiento de miguel garcia (11)
-- Almacenarlo como Consulta1A

CREATE OR REPLACE VIEW consulta1a AS 
  SELECT MONTH(p.fecha) mes FROM personas p 
    WHERE CONCAT_WS(" ",p.nombre,p.apellidos)='miguel garcia';


-- Consulta 2    
-- Sacar las personas que han nacido el mismo mes que miguel garcia (11)
-- 10,5,19,6,4
-- Aprovecho la Consulta1A


SELECT 
    * 
  FROM personas p 
  WHERE 
    MONTH(p.fecha)=(
      SELECT * FROM consulta1a
    );


-- Cuantas personas tengo de cada poblacion

SELECT p.poblacion,count(*) numero 
  FROM personas p 
  GROUP BY p.poblacion;

-- ¿Que poblaciones tienen mas de 1 persona?
-- primero tengo que saber cuantas personas tengo de cada poblacion
-- despues quedarme con las poblaciones que tienen mas de 1 persona

SELECT 
    p.poblacion,
    count(*) numero
  FROM personas p
  GROUP BY p.poblacion
  HAVING numero>1;

-- ¿que poblaciones cuyo nombre comience por L tienen mas de 1 persona?
-- primero tengo que saber que poblaciones comienzan por L
-- quiero saber el numero de personas que tienen esas poblaciones 
-- quiero quedarme solo con las que tienen mas de 1 persona

SELECT 
    p.poblacion,
    count(*) numero 
  FROM personas p
  WHERE 
    left(p.poblacion,1)='l'
  GROUP BY p.poblacion
  HAVING numero>1;


-- contar cuantas poblaciones hay en la tabla personas

-- en mysql
SELECT count(DISTINCT p.poblacion) numero FROM personas p;

-- en access
SELECT count(*) FROM 
  (SELECT DISTINCT p.poblacion FROM personas p) c1


