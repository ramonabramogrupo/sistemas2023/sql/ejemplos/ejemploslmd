﻿USE ejemplocomercios;

-- listado de todos los clientes
SELECT * FROM cliente c;

-- listando los registros
SELECT * FROM registra r;

-- listame los registros con los datos del cliente que se ha registrado

-- con on
SELECT 
    * 
  FROM cliente c 
    JOIN registra r ON c.dni=r.dni;


-- con using
SELECT 
    * 
  FROM cliente c 
    JOIN registra r using(dni);


-- listarme el nombre del fabricante con el listado de todos 
-- los programas que ha desarrollado
-- no necesito los datos de los programas solo su codigo

-- tabla fabricante
SELECT * FROM fabricante f;

-- tabla desarrolla
SELECT * FROM desarrolla d;

-- las combino
SELECT * FROM fabricante f JOIN desarrolla d ON f.id_fab = d.id_fab;
SELECT * FROM fabricante f JOIN desarrolla d using(id_fab);


-- la misma consulta anterior pero ahora quiero los datos
-- de los programas

-- programa
SELECT * FROM programa p;

-- fabricante
SELECT * FROM fabricante f;

-- desarrolla
SELECT * FROM desarrolla d;


-- metodo no le aconsejo
SELECT 
    * 
  FROM programa p 
    JOIN fabricante f 
    JOIN desarrolla d
    ON f.id_fab = d.id_fab AND p.codigo = d.codigo;

-- metodo a utilizar
SELECT 
    * 
  FROM fabricante f 
    JOIN desarrolla d ON f.id_fab = d.id_fab
    JOIN programa p ON d.codigo = p.codigo;

-- puedo utilizar using
SELECT 
    * 
  FROM fabricante f
    JOIN desarrolla d using(id_fab)
    JOIN programa p using(codigo);

