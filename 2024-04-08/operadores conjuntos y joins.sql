﻿USE productostiendas;

-- quiero explicar el except junto con left join

/*
Identifica los codigos de los productos que no se venden en ninguna tienda. 

-- utiliza LEFT JOIN
-- utiliza EXCEPT
*/

-- c1
-- todos los productos
SELECT p.id_producto FROM productos p;

-- c2
-- productos vendidos
SELECT DISTINCT pt.id_producto FROM productos_tiendas pt;

-- Esta consulta en este ejercicio no me hace falta
-- productos vendidos
SELECT 
  DISTINCT pt.id_producto 
FROM productos p 
  JOIN productos_tiendas pt ON p.id_producto = pt.id_producto;

-- Utilizando LEFT JOIN
-- productos no vendidos
-- realizado con left join
SELECT 
  p.id_producto 
FROM productos p 
    LEFT JOIN productos_tiendas pt USING(id_producto)
WHERE pt.id_producto is null;

-- optimizar

SELECT 
  p.id_producto 
FROM productos p
  LEFT JOIN (
    SELECT DISTINCT pt.id_producto FROM productos_tiendas pt
  ) c2 USING(id_producto)
WHERE 
  c2.id_producto is null;

-- Utilizando EXCEPT
-- realizado con except
SELECT p.id_producto FROM productos p
EXCEPT 
SELECT DISTINCT pt.id_producto FROM productos_tiendas pt;

/*
Identifica los nombres de los productos que no se venden en ninguna tienda. 

-- utiliza LEFT JOIN
-- utiliza EXCEPT
*/

-- c1
-- codigo de los productos
SELECT p.id_producto FROM productos p;

-- c2 
-- codigo productos vendidos
SELECT DISTINCT pt.id_producto FROM productos_tiendas pt;

-- LEFT JOIN
-- productos-productos_tiendas

SELECT 
  p.nombre 
FROM  productos p 
  LEFT JOIN productos_tiendas pt ON p.id_producto = pt.id_producto
WHERE pt.id_producto is null;

-- optimizado
-- productos - C2
SELECT 
  p.nombre 
FROM productos p 
  LEFT JOIN (
    SELECT DISTINCT pt.id_producto FROM productos_tiendas pt
  ) c2 using(id_producto)
WHERE c2.id_producto is null;

-- Utilizando EXCEPT
-- C1-C2

-- C3
-- saco primero el codigo
SELECT p.id_producto FROM productos p
EXCEPT 
SELECT DISTINCT pt.id_producto FROM productos_tiendas pt;

-- y ahora combino para sacar el nombre
SELECT 
  p.nombre 
FROM productos p 
  JOIN (
    SELECT p.id_producto FROM productos p
    EXCEPT 
    SELECT DISTINCT pt.id_producto FROM productos_tiendas pt
  ) c3 USING(id_producto);

/* 
Codigo de las tiendas que no han vendido productos
*/

-- c1
SELECT t.id_tienda FROM tiendas t;

-- c2
SELECT DISTINCT pt.id_tienda FROM productos_tiendas pt;

-- left join
SELECT 
  t.id_tienda 
FROM tiendas t 
  LEFT JOIN productos_tiendas pt ON t.id_tienda = pt.id_tienda
WHERE pt.id_tienda IS NULL;

-- optimizar este left join
SELECT 
  t.id_tienda 
FROM tiendas t 
  LEFT JOIN (
    SELECT DISTINCT pt.id_tienda FROM productos_tiendas pt
  ) c2 USING(id_tienda)
WHERE 
  c2.id_tienda IS NULL;

-- except
SELECT t.id_tienda FROM tiendas t
EXCEPT
SELECT DISTINCT pt.id_tienda FROM productos_tiendas pt;

/*
id tiendas que nunca han vendido mas de 20 unidades de un determinado producto
*/

-- esto no vale
SELECT pt.id_tienda FROM productos_tiendas pt WHERE pt.cantidad<=20;

-- c1
SELECT t.id_tienda FROM tiendas t;

-- c2
SELECT 
  DISTINCT pt.id_tienda 
FROM productos_tiendas pt 
  WHERE pt.cantidad>20;

-- left join
SELECT 
  t.id_tienda 
FROM tiendas t 
  LEFT JOIN (
      SELECT 
        DISTINCT pt.id_tienda
      FROM productos_tiendas pt 
        WHERE pt.cantidad>20
  ) c2 ON c2.id_tienda=t.id_tienda
WHERE 
  c2.id_tienda IS NULL;


-- except
SELECT t.id_tienda FROM tiendas t
EXCEPT 
SELECT 
  DISTINCT pt.id_tienda 
FROM productos_tiendas pt 
  WHERE pt.cantidad>20;


-- quiero explicar el intersect y compararla con NATURAL JOIN

/*
quiero saber el id de las tiendas que han vendido el producto 1 y el producto 2
*/

-- esto no vale
SELECT * FROM productos_tiendas pt 
WHERE pt.id_producto=1 AND pt.id_producto=2;

-- c1
-- id tiendas que han vendido el producto 1
SELECT 
  DISTINCT pt.id_tienda 
FROM productos_tiendas pt 
WHERE pt.id_producto=1;

-- c2
-- id tiendas que han vendido el producto 2
SELECT 
  DISTINCT pt.id_tienda 
FROM productos_tiendas pt 
WHERE pt.id_producto=2;

-- NATURAL JOIN
-- C1 NATURAL JOIN C2
SELECT 
  * 
FROM (
  SELECT 
    DISTINCT pt.id_tienda 
  FROM productos_tiendas pt 
  WHERE pt.id_producto=1
) C1 NATURAL JOIN (
  SELECT 
      DISTINCT pt.id_tienda 
  FROM productos_tiendas pt 
  WHERE pt.id_producto=2
) C2;


-- INTERSECT 
-- C1 INTERSECT C2
SELECT 
  DISTINCT pt.id_tienda 
FROM productos_tiendas pt 
WHERE pt.id_producto=1
INTERSECT 
SELECT 
  DISTINCT pt.id_tienda 
FROM productos_tiendas pt 
WHERE pt.id_producto=2;



/*
quiero saber las tiendas que han vendido el producto 1 o el producto 2
*/

-- c1
-- id tiendas que han vendido el producto 1
SELECT 
  DISTINCT pt.id_tienda 
FROM productos_tiendas pt 
WHERE pt.id_producto=1;

-- c2
-- id tiendas que han vendido el producto 2
SELECT 
  DISTINCT pt.id_tienda 
FROM productos_tiendas pt 
WHERE pt.id_producto=2;

-- con el operador UNION

SELECT 
  DISTINCT pt.id_tienda 
FROM productos_tiendas pt 
WHERE pt.id_producto=1
UNION 
SELECT 
  DISTINCT pt.id_tienda 
FROM productos_tiendas pt 
WHERE pt.id_producto=2;

-- utilizando OR

SELECT 
  DISTINCT pt.id_tienda 
FROM productos_tiendas pt 
WHERE 
  pt.id_producto=1 OR pt.id_producto=2;







