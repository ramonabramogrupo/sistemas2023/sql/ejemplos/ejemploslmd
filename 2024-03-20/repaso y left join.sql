﻿USE spj;

-- HASTA AHORA
-- JOIN
-- WHERE IN
-- UNION
-- INTERSECT 

-- AÑADIR
-- VIEWS
-- OUTER JOIN
-- EXCEPT
-- WHERE EXISTS  ==> SEMIJOIN

-- SUBCONSULTAS
-- CONSULTA INTRODUCIDO EN OTRA
-- SELECT SUBCONSULTA ==> SUBCONSULTA ESCALAR (SOLO PUEDE DEVOLVER UN VALOR)
-- FROM SUBCONSULTA ==> ES OBLIGATORIO ALIAS y PARENTESIS
-- WHERE SUBCONSULTA ==> NO SE COLOCA ALIAS Y DEPENDIENDO DE LA PREGUNTA Y EL OPERADOR LA SUBCONSULTA PUEDE DEVOLVER UNO O VARIOS VALORES
-- EN EL WHERE
-- OPERADOR IN (1 SOLO CAMPO Y VARIOS VALORES)
-- OPERADOR =,>,>=,<,....
-- OPERADOR EXISTS (SEMIJOIN Y ANTISEMIJOIN)
-- OPERADOR ALL,SOME,ANY


-- NOMBRE Y EL COLOR DE LAS PIEZAS QUE HAN SIDO SUMINISTRADAS EN ALGUNA OCASION

-- JOIN
SELECT
DISTINCT
  nomp,
  color
FROM spj
  JOIN p
    ON spj.p = p.P;

-- where in
-- sacar las p que han sido sumnistradas alguna vez
SELECT DISTINCT
  p
FROM spj;
-- termino la consulta
SELECT
  nomp,
  color
FROM p
WHERE P IN (SELECT DISTINCT
    p
  FROM spj);


-- nombre, color de las piezas han sido suministradas junto con la cantidad de piezas suministradas a alguna proyecto.

-- join
SELECT DISTINCT
  p.nomp,
  p.color,
  spj.cant
FROM p
  JOIN spj
    ON p.P = spj.p;

-- where in (no vale)


-- como estoy hasta las narices de los problemas con los NOMBRES DE LOS campos
-- y LOS NOMBRES DE las tablas
-- VOY A CREAR VISTAS
-- UNA VISTA ES UNA CONSULTA DE SELECCION DE DATOS ALMACENADA EN EL SERVIDOR
-- las vistas se utilizan como una tabla
-- SINTAXIS CREACION

-- CREATE VIEW nombreVista AS select .....

-- SINTAXIS DE BORRADO

-- DROP VIEW nombreVista;

-- SINTAXIS ALTERNATIVA
-- CREATE OR REPLACE VIEW nombreVista AS select ...


-- crear una vista para tener la tabla s modificada 
-- s ==> Distribuidores
-- campo s
-- campo noms
-- campo estado
-- campo ciudad

CREATE OR REPLACE VIEW distribuidores
AS
SELECT
  s codigoDistribuidor,
  noms nombreDistribuidor,
  estado,
  ciudad
FROM s;

-- listar la tabla s

SELECT
  *
FROM distribuidores d; -- a traves de la vista
SELECT
  *
FROM s; -- directamente desde la tabla

-- crear una vista para la tabla p
-- p ==> piezas
-- campo P ==> p
-- el resto de campos como estan

CREATE OR REPLACE VIEW piezas
AS
SELECT
  P codigoPieza,
  nomp nombrePieza,
  color,
  peso,
  ciudad
FROM p;

-- crear una vista para la tabla j
-- tabla j ==> proyectos

CREATE OR REPLACE VIEW proyectos
AS
SELECT
  j codigoProyecto,
  nomj nombreProyecto,
  ciudad
FROM j;

-- creo una vista para la tabla spj
-- tabla spj ==> suministrados

CREATE OR REPLACE VIEW suministros
AS
SELECT
  s codigoSuministrador,
  p codigoPieza,
  j codigoProyecto,
  cant
FROM spj;

-- NOMBRE DE LOS PROYECTOS QUE HAN RECIBIDO ALGUN SUMINISTRO (QUE ESTAN EN SPJ)
-- UTILIZAR SI QUEREIS LAS VISTAS

-- JOIN

SELECT DISTINCT
  p.nombreProyecto
FROM proyectos p
  JOIN suministros s USING (codigoProyecto);

-- sin la vista
SELECT j.nomj FROM j JOIN spj ON using(j);

-- IN
-- todos los proyectos que estan en suministros
SELECT DISTINCT
  s.codigoProyecto
FROM suministros s;
-- termino
SELECT
  p.nombreProyecto
FROM proyectos p
WHERE p.codigoProyecto IN (SELECT DISTINCT s.codigoProyecto FROM suministros s);


-- NOMBRE DE LOS PROYECTOS QUE HAN RECIBIDO ALGUN SUMINISTRO JUNTO CON EL CODIGO Y CANTIDAD DE LAS PIEZAS SUMINISTRADAS
-- JOIN
SELECT DISTINCT 
  p.nombreProyecto,
  s.cant,
  s.codigoPieza
FROM suministros s 
JOIN proyectos p USING(codigoProyecto);

-- IN (no se puede)

-- SOLO SACO EL NOMBRE DEL PROYECTO
SELECT p.nombreProyecto FROM proyectos p WHERE p.codigoProyecto IN (
  SELECT DISTINCT s.codigoProyecto FROM suministros s
);

-- NOMBRE DE LOS DISTRIBUIDORES QUE HAN SUMINISTRADO LA PIEZA P1 A ALGUN PROYECTO
-- JOIN
SELECT DISTINCT 
  d.nombreDistribuidor 
FROM distribuidores d 
  JOIN suministros s ON d.codigoDistribuidor=s.codigoSuministrador
WHERE s.codigoPieza='P1';

-- IN
SELECT 
  d.nombreDistribuidor 
FROM distribuidores d
WHERE d.codigoDistribuidor IN (
  SELECT DISTINCT s.codigoSuministrador 
  FROM suministros s WHERE s.codigoPieza='P1'
);


/***
  COMBINACIONES EXTERNAS
**/

-- CODIGO DE LAS PIEZAS QUE NO SE HAN SUMINISTRADO NUNCA
-- LEFT JOIN

-- PIENSO
-- PIEZAS - SUMINISTROS
-- PRIMERA TABLA (PIEZAS) LEFT JOIN SEGUNDA TABLA(SUMINISTROS) WHERE NULL(SUMINISTROS)
SELECT 
  p.codigoPieza 
FROM  piezas p LEFT JOIN suministros s USING(codigoPieza)
WHERE s.codigoSuministrador is NULL;


-- NOT IN
SELECT 
  p.codigoPieza 
FROM piezas p
WHERE p.codigoPieza NOT IN (
  SELECT DISTINCT s.codigoPieza FROM suministros s
);


-- EXCEPT
-- codigoPieza(piezas) - codigoPieza(suministros)

SELECT p.codigoPieza FROM piezas p  -- minuendo
EXCEPT 
SELECT s.codigoPieza FROM suministros s; -- sustraendo


-- PROYECTOS A LOS QUE NUNCA SE LES HA SUMINISTRADO PIEZAS
-- LEFT JOIN

-- PIENSO
-- PROYECTOS - SUMINISTROS
SELECT 
  * 
FROM proyectos p LEFT JOIN suministros s USING(codigoProyecto)
WHERE s.codigoSuministrador IS NULL;


-- NOT IN
SELECT 
  * 
FROM proyectos p 
where p.codigoProyecto NOT IN (
  SELECT DISTINCT p.codigoProyecto FROM suministros s
);

-- EXCEPT
SELECT p.codigoProyecto FROM proyectos p
EXCEPT 
SELECT DISTINCT s.codigoProyecto FROM suministros s;


-- SUMINISTRADORES QUE NO HAN SUMINISTRADO NUNCA PIEZAS
-- LEFT JOIN

-- PIENSO
-- DISTRIBUIDORES - SUMINISTROS
SELECT 
  * 
FROM distribuidores d 
  LEFT JOIN suministros s ON d.codigoDistribuidor=s.codigoSuministrador
WHERE s.codigoSuministrador is null;

-- NOT IN
SELECT 
  * 
FROM distribuidores d
WHERE d.codigoDistribuidor NOT IN (
  SELECT distinct s.codigoSuministrador FROM suministros s
);

-- EXCEPT

SELECT d.codigoDistribuidor FROM distribuidores d
EXCEPT 
SELECT DISTINCT s.codigoSuministrador FROM suministros s;

-- si quiero algo que no sea el codigo del distribuidor
-- por ejemplo el nombre necesito un JOIN mas
SELECT 
* 
FROM distribuidores d JOIN (
  SELECT d.codigoDistribuidor FROM distribuidores d
  EXCEPT 
  SELECT DISTINCT s.codigoSuministrador FROM suministros s
) c1 using(codigoDistribuidor);
