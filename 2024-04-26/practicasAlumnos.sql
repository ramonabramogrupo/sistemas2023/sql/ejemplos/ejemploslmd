﻿DROP DATABASE IF EXISTS CursoPracticas;
CREATE DATABASE IF NOT EXISTS CursoPracticas;
USE CursoPracticas;

-- Creación de la tabla 'alumnos'
CREATE TABLE alumnos (
    alumno_id INT AUTO_INCREMENT PRIMARY KEY,
    nombre VARCHAR(100) NOT NULL,
    apellidos VARCHAR(100) NOT NULL,
    poblacion VARCHAR(100),
    telefono VARCHAR(15),
    email VARCHAR(100),
    practicasEntregadas INT DEFAULT 0 -- Campo calculado, inicializado a 0
);

-- Creación de la tabla 'practicas'
CREATE TABLE practicas (
    practica_id INT AUTO_INCREMENT PRIMARY KEY,
    titulo VARCHAR(255) NOT NULL,
    descripcion TEXT,
    dificultad ENUM('baja', 'media', 'alta'),
    alumnosEntregan INT DEFAULT 0, -- Campo calculado, inicializado a 0
    notaMedia FLOAT(5,2) DEFAULT 0.00,
    notaMax FLOAT(5,2) DEFAULT 0.00,
    notaMin FLOAT(5,2) DEFAULT 0.00,
    CHECK(notaMedia BETWEEN 0 AND 10),
    CHECK(notaMin BETWEEN 0 AND 10),
    CHECK(notaMax BETWEEN 0 AND 10)
);

-- Creación de la tabla 'entregas'
CREATE TABLE entregas (
    entrega_id INT AUTO_INCREMENT PRIMARY KEY,
    alumno INT NOT NULL,
    practica INT NOT NULL,
    nota FLOAT(5,2),
    fechaEntrega DATE,
    CHECK(nota BETWEEN 0 AND 10),
    CONSTRAINT fkEntregasAlumnos FOREIGN KEY (alumno) REFERENCES alumnos(alumno_id) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT fkEntregasPracticas FOREIGN KEY (practica) REFERENCES practicas(practica_id) ON DELETE CASCADE ON UPDATE CASCADE
);


-- Inserción en 'alumnos'
INSERT INTO alumnos (nombre, apellidos, poblacion, telefono, email) VALUES
('Juan', 'Pérez', 'Madrid', '123456789', 'juan.perez@example.com'),
('Ana', 'López', 'Madrid', '987654321', 'ana.lopez@example.com'),
('Carlos', 'García', 'Barcelona', '567891234', 'carlos.garcia@example.com'),
('Laura', 'Martínez', 'Barcelona', '321654987', 'laura.martinez@example.com'),
('Sara', 'Gomez', 'Santander', '654321789', 'sara.gomez@example.com'),
('Pablo', 'Ruiz', 'Santander', '432165798', 'pablo.ruiz@example.com'),
('Nora', 'Díaz', 'Santander', '589123647', 'nora.diaz@example.com'),
('Oscar', 'Vidal', 'Madrid', '698741253', 'oscar.vidal@example.com'),
('Lola', 'Moreno', 'Barcelona', '851276934', 'lola.moreno@example.com'),
('Iker', 'Jimenez', 'Barcelona', '963852741', 'iker.jimenez@example.com');

-- Inserción en 'practicas'
INSERT INTO practicas (titulo, descripcion, dificultad) VALUES
('Introducción a SQL', 'Práctica básica de SQL', 'baja'),
('Consultas Avanzadas', 'Prácticas sobre JOINs y subconsultas', 'media'),
('Optimización de Queries', 'Cómo mejorar el rendimiento de las consultas', 'alta'),
('Triggers en SQL', 'Implementación de triggers', 'alta'),
('Procedimientos Almacenados', 'Creación de procedimientos almacenados', 'media'),
('Seguridad en Bases de Datos', 'Mejores prácticas de seguridad', 'alta'),
('Indexación', 'Cómo y cuándo indexar tablas', 'media'),
('Backup y Restauración', 'Procedimientos para backup', 'baja'),
('Replicación de Datos', 'Cómo configurar la replicación', 'alta'),
('Análisis de Datos', 'Técnicas básicas de análisis de datos', 'media');

-- Inserción en 'entregas'
INSERT INTO entregas (alumno, practica, nota, fechaEntrega) VALUES
(1, 1, 8.5, '2023-04-25'),
(2, 2, 9.0, '2023-04-26'),
(3, 3, 7.5, '2023-04-27'),
(4, 4, 6.0, '2023-04-28'),
(5, 5, 8.0, '2023-04-29'),
(6, 6, 9.5, '2023-04-30'),
(7, 7, 6.5, '2023-05-01'),
(8, 8, 7.0, '2023-05-02'),
(9, 9, 8.5, '2023-05-03'),
(10, 10, 9.0, '2023-05-04');
