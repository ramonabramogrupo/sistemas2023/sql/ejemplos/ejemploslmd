﻿USE productostiendas;

-- leer el valor de la variable de servidor AUTOCOMMIT
SELECT @@autocommit;

-- asignar un valor a la variable de servidor AUTOCOMMIT
set AUTOCOMMIT=0;

INSERT INTO productos 
  (nombre, precio) VALUES 
  ('producto nuevo 1',1);

-- almacena el producto
COMMIT;
-- rechaza el producto
ROLLBACK;

SELECT * FROM productos p;

-- eliminar todos los registros de productos_tiendas;

DELETE FROM productos_tiendas;
SELECT * FROM productos_tiendas pt;

-- eliminar el registro 6
DELETE FROM productos_tiendas WHERE id=6;

COMMIT; -- el registro se elimina definitivamente
ROLLBACK; -- el registro se recupera


-- consuta delete de una tabla
-- eliminar la tienda 1

DELETE FROM tiendas t 
WHERE t.id_tienda=1; -- produce un error porque la tienda esta en productos_tiendas (RESTRICT)


-- consulta de eliminacion de varias tablas
-- eliminar la tienda A y sus ventas

-- elimina de la tienda A todas su ventas
DELETE productos_tiendas -- tabla donde quiero eliminar los registros
FROM tiendas JOIN productos_tiendas using(id_tienda)
WHERE tiendas.nombre='Tienda A';

-- elimina la tienda A
DELETE FROM tiendas WHERE nombre='Tienda A';

SELECT * FROM tiendas t;
SELECT * FROM productos_tiendas pt;

ROLLBACK;


-- eliminar todas las ventas del producto cuyo nombre es 'Producto 1'
-- no quiero eliminar el producto 1 solo sus ventas

-- utilizamos JOIN
-- detras del delete la tabla o tablas donde quieres eliminar los registros
-- detras from todas las tablas que necesitas para realizar la consulta
-- where es la condicion por la que quieres eliminar los registros
DELETE pt -- tabla donde quiero eliminar registros
FROM productos p JOIN productos_tiendas pt using(id_producto)
WHERE p.nombre='Producto 1';

-- utilizamos IN
DELETE 
FROM productos_tiendas pt -- coloco la tabla donde quiero eliminar los registros
WHERE pt.id_producto IN (
  SELECT p.id_producto FROM productos p WHERE p.nombre='Producto 1' -- busco el id del producto a borrar
);

SELECT * FROM productos p;
SELECT * FROM productos_tiendas pt;
rollback;


-- eliminar las ventas de todos los productos cuyo precio exceda los 25
-- sin eliminar los productos

-- join

DELETE pt
FROM productos p JOIN productos_tiendas pt using(id_producto)
WHERE p.precio>25;

SELECT * FROM productos_tiendas pt;
SELECT * FROM productos p;
ROLLBACK;

-- in
DELETE 
FROM productos_tiendas
WHERE id_producto IN (
  SELECT p.id_producto FROM productos p WHERE p.precio>25 
);


/**
CONSULTAS DE CREACION DE TABLA (LMD)
**/

-- QUIERO UNA TABLA DENOMINADA PRODUCTOS VENDIDOS CON EL ID Y EL NOMBRE DEL PRODUCTO VENDIDO

CREATE TABLE productosVendidos AS 
SELECT 
  DISTINCT p.id_producto,p.nombre 
FROM productos_tiendas pt 
  JOIN productos p USING(id_producto);

DROP TABLE if EXISTS productosVendidos;

CREATE TABLE productosVendidos (
  id int AUTO_INCREMENT,
  nombre varchar(200),
  PRIMARY KEY(id)
) AS 
SELECT 
  DISTINCT p.id_producto,p.nombre 
FROM productos_tiendas pt 
  JOIN productos p USING(id_producto);



-- crear una tabla llamada productos no vendidos 
-- donde me coloque el id, el nombre y el precio de los productos no vendidos





 

