﻿USE productostiendas;

-- todos los precios de los productos a 0
UPDATE productos p 
  SET p.precio=0;

SELECT * FROM productos p;

-- colocar el precio a 10 del producto con id 1
UPDATE productos p -- tablas
  SET p.precio=10 -- campos a actualizar
  WHERE p.id_producto=1; -- condiciones

-- sumar 1 a todos los precios
UPDATE productos p
  SET p.precio=p.precio+1;

-- actualiza el precio de todos los productos vendidos a 100

-- listar los productos a actualizar
SELECT * 
  FROM productos_tiendas pt JOIN productos p USING(id_producto);

-- convierto en un update
UPDATE productos_tiendas pt JOIN productos p using(id_producto)
  set p.precio=100;

-- lo realizo con where in
UPDATE productos p
  set p.precio=100
  WHERE p.id_producto IN (SELECT DISTINCT pt.id_producto FROM productos_tiendas pt);

-- actualizar los precios de los productos no vendidos
-- quiero restar al precio de estos productos 10 euros

-- saco los productos no vendidos

-- left join
SELECT * FROM productos p LEFT JOIN productos_tiendas pt using(id_producto) WHERE pt.id_producto is null;
-- not in
SELECT * 
FROM productos p 
WHERE p.id_producto NOT IN(
  SELECT DISTINCT pt.id_producto FROM productos_tiendas pt
  );

-- si quiero actualizar utilizando para seleccionar left join
UPDATE productos p LEFT JOIN productos_tiendas pt using(id_producto) 
set p.precio=p.precio-10
WHERE pt.id_producto is NULL;

-- si quiero actualizar utilizando para seleccionar not in
UPDATE productos p 
set p.precio=p.precio-10
WHERE p.id_producto NOT IN(
  SELECT DISTINCT pt.id_producto FROM productos_tiendas pt
);

-- añadir un campo a la tabla productos que indique las veces que se ha vendido ese producto (el campo le llamados totalventas int)

ALTER TABLE productos ADD COLUMN totalventas int DEFAULT 0;

-- voy a realizar la consulta de totales que permite obtener el dato

SELECT p.id_producto,count(pt.id_producto) totalventas 
FROM productos p 
  LEFT JOIN productos_tiendas pt using(id_producto)
  GROUP BY p.id_producto;

-- esa consulta de totales la tengo que colocar con un join en el update

UPDATE productos p JOIN (
  SELECT p.id_producto,count(pt.id_producto) totalventas 
  FROM productos p 
    LEFT JOIN productos_tiendas pt using(id_producto)
    GROUP BY p.id_producto
) c1 using(id_producto)
set p.totalventas=c1.totalventas; 


-- añadir un campo en tiendas para saber el numero de ventas de esa tienda
-- el campo le llamamos totalventas de tipo int

ALTER TABLE tiendas ADD COLUMN totalventas int DEFAULT 0;

-- 1 paso : realizar la consulta de totales
SELECT t.id_tienda,count(pt.id_tienda) totalventas
FROM tiendas t 
  LEFT JOIN productos_tiendas pt using(id_tienda)
  GROUP BY t.id_tienda;

-- 2 paso: realizar UPDATE colocando la de totales con JOIN
UPDATE tiendas t JOIN (
    SELECT t.id_tienda,count(pt.id_tienda) totalventas
    FROM tiendas t 
      LEFT JOIN productos_tiendas pt using(id_tienda)
      GROUP BY t.id_tienda
) c1 using(id_tienda)
set t.totalventas=c1.totalventas;

-- Añadir un campo en la tabla productos denominado cantidadtotal
-- Ese campo debe tener la suma de todas las cantidades vendidas de ese producto

/**
id,producto_id,tienda_id,cantidad
1,1,1,10
2,1,2,10,
3,2,1,5

producto_id,totalventas,cantidadtotal
1,2,20
2,1,5
**/

-- añadimos el campo a la tabla productos

ALTER TABLE productos  ADD COLUMN cantidadtotal int DEFAULT 0;

-- 1 paso : realizar la consulta de totales
-- producto_id,cantidadtotal (para la cantidad total necesito suma)

SELECT p.id_producto,sum(pt.cantidad) cantidadtotal 
FROM productos p 
  LEFT JOIN productos_tiendas pt using(id_producto)
  GROUP BY p.id_producto;

/*-- no salen los productos no vendidos
SELECT pt.id_producto,sum(pt.cantidad) cantidadtotal 
FROM productos_tiendas pt 
  GROUP BY pt.id_producto;*/



-- 2 paso: realizar el update combinando la tabla productos con la realizada en el paso 1

UPDATE productos p JOIN (
  SELECT p.id_producto,sum(pt.cantidad) cantidadtotal 
  FROM productos p 
    LEFT JOIN productos_tiendas pt using(id_producto)
    GROUP BY p.id_producto
) c1 using(id_producto)
set p.cantidadtotal=IF(c1.cantidadtotal IS NULL,0,c1.cantidadtotal);

UPDATE productos p JOIN (
  SELECT p.id_producto,sum(pt.cantidad) cantidadtotal 
  FROM productos p 
    LEFT JOIN productos_tiendas pt using(id_producto)
    GROUP BY p.id_producto
) c1 using(id_producto)
set p.cantidadtotal=IFNULL(c1.cantidadtotal,0);

SELECT * FROM productos p;

-- añadir un campo en la tabla tiendas que se denomina cantidadtotal
-- ese campo debe tener el numero total de unidades vendidas de esa tienda

ALTER TABLE tiendas ADD COLUMN cantidadtotal int DEFAULT 0;

-- estas dos opciones me permiten cambiar un campo existente 
-- sin afectar a los datos
ALTER TABLE tiendas MODIFY COLUMN cantidadtotal int DEFAULT 0;
ALTER TABLE tiendas CHANGE COLUMN cantidadtotal cantidatotal int DEFAULT 0;

-- 1 paso : realizar la consulta de totales

SELECT t.id_tienda,SUM(pt.cantidad) cantidadtotal 
FROM tiendas t 
  LEFT JOIN productos_tiendas pt USING(id_tienda)
GROUP BY t.id_tienda;

-- 2 paso : realizar al consulta de actualizacion combinando la tabla tiendas con la consulta de totales

UPDATE tiendas t JOIN (
  SELECT t.id_tienda,SUM(pt.cantidad) cantidadtotal
  FROM tiendas t 
    LEFT JOIN productos_tiendas pt USING(id_tienda)
  GROUP BY t.id_tienda
) c1 using(id_tienda)
set t.cantidadtotal=IFNULL(c1.cantidadtotal,0); 

SELECT * FROM tiendas t;

-- añadir un campo en la tabla productos_tiendas denominado total de tipo float
ALTER TABLE productos_tiendas ADD COLUMN total float DEFAULT 0;

-- en ese campo debe calcularse el precio total de la venta teniendo en cuenta que el precio del producto esta en la tabla productos

/**
producto_id, precio
1,10
2,5
3,1

producto_id,tienda_id,cantidad,total
1,1,8,10*8=80
1,2,3,10*3=30
2,1,6,5*6=30

**/

-- 1 paso : realizar la consulta de seleccion

SELECT 
  *,p.precio*pt.cantidad
FROM productos p JOIN productos_tiendas pt using(id_producto);


-- 2 paso : convertirla a consulta de actualizacion
UPDATE 
productos p JOIN productos_tiendas pt using(id_producto)
set pt.total=p.precio*pt.cantidad;

SELECT * FROM productos_tiendas pt;