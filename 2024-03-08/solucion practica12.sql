﻿USE ciclistas;

-- 1	Nombre y edad de los ciclistas que han ganado etapas

-- sin optimizar

# 1 paso 
## necesito etapas
## necesito ciclistas

# 2 paso 
## combinar las dos tablas

-- utilizando on
SELECT DISTINCT 
    c.nombre,c.edad 
  FROM etapa e 
    JOIN ciclista c ON e.dorsal = c.dorsal;

-- lo mismo aprovechando la propiedad conmutativa
SELECT DISTINCT 
    c.nombre,c.edad 
  FROM ciclista c 
    JOIN etapa e ON e.dorsal = c.dorsal;

-- utilizando using
SELECT DISTINCT 
    c.nombre,c.edad 
  FROM ciclista c 
    JOIN etapa e using(dorsal);

-- nunca 
-- solo utilizaremos el producto cartesiano con where en caso
-- de no tener join
SELECT DISTINCT 
    c.nombre,c.edad 
  FROM ciclista c,etapa e 
  WHERE c.dorsal=e.dorsal;


-- optimizadas 

-- C1
-- De la tabla etapa solo necesito el dorsal del ciclista que la ha ganado
SELECT distinct e.dorsal FROM etapa e;

-- Combino la consulta anterior con ciclista
SELECT 
    c.nombre,c.edad 
  FROM (
      SELECT distinct e.dorsal FROM etapa e
    ) c1 JOIN ciclista c using(dorsal);

-- otra opcion seria utilizar where
SELECT 
    c.nombre,c.edad 
  FROM ciclista c 
  WHERE dorsal IN (SELECT distinct e.dorsal FROM etapa e);
 

-- 2	Nombre y edad de los ciclistas que han ganado puertos

-- sin optimizar

SELECT DISTINCT 
    c.nombre,c.edad
  FROM puerto p 
    JOIN ciclista c ON p.dorsal = c.dorsal;

SELECT DISTINCT 
    c.nombre,c.edad
  FROM puerto p 
    JOIN ciclista c using(dorsal);

-- consulta optimizada

-- c1
-- quedarme con el dorsal de los ciclistas que han ganado puertos

SELECT DISTINCT p.dorsal FROM puerto p;

-- combino la consulta anterior con ciclista
SELECT 
  c.nombre,c.edad 
  FROM (
    SELECT DISTINCT p.dorsal FROM puerto p
  )c1 JOIN ciclista c using(dorsal);


-- utilizando where + in

SELECT 
    c.nombre,c.edad 
  FROM ciclista c 
  WHERE c.dorsal IN (SELECT DISTINCT p.dorsal FROM puerto p);


-- 3	Nombre y edad de los ciclistas que han ganado etapas y puertos
  
-- sin optimizar

SELECT DISTINCT 
    c.nombre,c.edad 
  FROM ciclista c 
    JOIN etapa e ON c.dorsal = e.dorsal
    JOIN puerto p ON c.dorsal = p.dorsal;
   

-- optimizada

-- c1 
-- dorsal de los ciclistas que han ganado etapas
SELECT DISTINCT e.dorsal FROM etapa e;

-- c2
-- dorsal de los ciclistas que han ganado puertos
SELECT DISTINCT p.dorsal FROM puerto p;

-- c3
-- ciclistas que han ganado puertos y etapas
SELECT 
    c1.dorsal 
  FROM (
    SELECT DISTINCT e.dorsal FROM etapa e
  ) c1 JOIN (
    SELECT DISTINCT p.dorsal FROM puerto p
  ) c2 using(dorsal);

-- puedo realizarlo con una interseccion
  SELECT DISTINCT e.dorsal FROM etapa e
    INTERSECT 
  SELECT DISTINCT p.dorsal FROM puerto p;

-- con un natural join
SELECT 
    c1.dorsal 
  FROM (
    SELECT DISTINCT e.dorsal FROM etapa e
  ) c1 NATURAL JOIN (
    SELECT DISTINCT p.dorsal FROM puerto p
  ) c2;

-- sacar lo que me pide ya es trivial
-- combino c3 con ciclistas para sacar nombre y edad
SELECT 
    c.nombre,c.edad 
  FROM (
          SELECT 
              c1.dorsal 
            FROM (
              SELECT DISTINCT e.dorsal FROM etapa e
            ) c1 JOIN (
              SELECT DISTINCT p.dorsal FROM puerto p
            ) c2 using(dorsal)
        ) c3 join ciclista c using(dorsal);

-- la puedo realizar con un natural join
SELECT 
    c.nombre,c.edad 
  FROM (
          SELECT 
              c1.dorsal 
            FROM (
              SELECT DISTINCT e.dorsal FROM etapa e
            ) c1 JOIN (
              SELECT DISTINCT p.dorsal FROM puerto p
            ) c2 using(dorsal)
        ) c3 NATURAL join ciclista c;

-- la voy a realizar con where + in
SELECT 
    c.nombre,c.edad 
  FROM ciclista c 
  WHERE c.dorsal IN (
      SELECT 
      c1.dorsal 
    FROM (
      SELECT DISTINCT e.dorsal FROM etapa e
    ) c1 JOIN (
      SELECT DISTINCT p.dorsal FROM puerto p
    ) c2 using(dorsal)
  );


-- 4	Listar el director de los equipos 
--    que tengan ciclistas que hayan ganado alguna etapa

-- sin optimizar
-- necesito equipo,ciclista,etapa

SELECT DISTINCT 
    eq.director
  FROM equipo eq
    JOIN ciclista c ON eq.nomequipo = c.nomequipo
    JOIN etapa e ON c.dorsal = e.dorsal;

-- optimizar
-- c1
-- ciclistas que han ganado etapas

SELECT DISTINCT e.dorsal FROM etapa e;

-- c2 
-- equipos para los que corren estos ciclistas

SELECT DISTINCT 
    c.nomequipo 
  FROM ciclista c JOIN (
    SELECT DISTINCT e.dorsal FROM etapa e
  ) c1 using(dorsal);

-- termino la consulta 

-- opcion 1 
-- combino c2 con equipo

SELECT 
    e.director 
  FROM equipo e JOIN (
    SELECT DISTINCT 
        c.nomequipo 
      FROM ciclista c JOIN (
        SELECT DISTINCT e.dorsal FROM etapa e
      ) c1 using(dorsal)
  ) c2 USING(nomequipo);

-- opcion 2
-- where + in
SELECT 
    e.director 
  FROM equipo e 
  WHERE e.nomequipo IN (
    SELECT DISTINCT 
        c.nomequipo 
      FROM ciclista c JOIN (
        SELECT DISTINCT e.dorsal FROM etapa e
      ) c1 using(dorsal)
  );

-- opcion 3
-- natural join

SELECT 
    e.director 
  FROM equipo e NATURAL JOIN (
    SELECT DISTINCT 
        c.nomequipo 
      FROM ciclista c JOIN (
        SELECT DISTINCT e.dorsal FROM etapa e
      ) c1 using(dorsal)
  ) c2;


 
-- 5	Dorsal y nombre de los ciclistas que hayan llevado algún maillot

-- sin optimizar
-- tablas ciclistas,lleva

SELECT 
    DISTINCT c.dorsal,c.nombre 
  FROM ciclista c 
    JOIN lleva l ON c.dorsal = l.dorsal;

-- optimizar

-- c1
-- sacar los dorsales de los ciclistas que han llevado maillot
SELECT DISTINCT l.dorsal FROM lleva l;

-- termino la consulta

-- opcion 1
-- join

SELECT 
    c.dorsal,c.nombre 
  FROM ciclista c JOIN (
    SELECT DISTINCT l.dorsal FROM lleva l
  ) c1 USING(dorsal);

-- opcion 2
-- where + in
SELECT 
    c.dorsal,c.nombre 
  FROM ciclista c 
  WHERE c.dorsal IN (SELECT DISTINCT l.dorsal FROM lleva l);

-- 6	Dorsal y nombre de los ciclistas que hayan llevado el maillot amarillo

-- sin optimizar
-- tablas maillot,ciclistas, lleva

SELECT DISTINCT
    c.dorsal,c.nombre 
  FROM ciclista c 
    JOIN lleva l ON c.dorsal = l.dorsal 
    JOIN maillot m ON l.código = m.código
  WHERE 
    m.color='amarillo';

-- consulta optimizada

-- c1
-- saca el codigo del maillot amarillo
SELECT m.código FROM maillot m WHERE m.color='amarillo';

-- c2
-- dorsal de los ciclistas que han llevado el maillot amarillo

SELECT DISTINCT 
    l.dorsal 
  FROM lleva l 
  WHERE 
    l.código=(SELECT m.código FROM maillot m WHERE m.color='amarillo');

-- PARA TERMINAR

-- OPCION 1
-- JOIN

 SELECT 
    c.dorsal,c.nombre 
  FROM ciclista c
    JOIN (
          SELECT DISTINCT 
              l.dorsal 
            FROM lleva l 
            WHERE 
              l.código=(SELECT m.código FROM maillot m WHERE m.color='amarillo')
    ) c2 using(dorsal);
  
-- opcion 2
-- where in

SELECT 
    c.dorsal,c.nombre 
  FROM ciclista c 
  WHERE c.dorsal IN (
      SELECT DISTINCT 
        l.dorsal 
      FROM lleva l 
      WHERE 
        l.código=(SELECT m.código FROM maillot m WHERE m.color='amarillo')
  ); 


-- 7	Dorsal de los ciclistas que hayan llevado algún maillot y que han ganado etapas

-- sin optimizar
-- tablas lleva,etapa conectadas por dorsal

SELECT DISTINCT *
  FROM lleva l 
    JOIN etapa e ON l.dorsal = e.dorsal; 

-- optimizado

-- c1 
-- dorsal de los ciclistas que han llevado algun maillot
SELECT DISTINCT l.dorsal FROM lleva l;

-- c2
-- dorsal de los ciclistas que han ganado etapas
SELECT DISTINCT e.dorsal FROM etapa e;

-- terminar

-- opcion 1
-- interseccion c1 y c2

SELECT DISTINCT l.dorsal FROM lleva l
INTERSECT 
SELECT DISTINCT e.dorsal FROM etapa e;

-- opcion 2
-- natural join c1 y c2
SELECT 
    DISTINCT C1.dorsal 
  FROM (
      SELECT DISTINCT l.dorsal FROM lleva l
    ) C1 
    NATURAL JOIN (
      SELECT DISTINCT e.dorsal FROM etapa e
      ) C2;

-- 8	Indicar el numetapa y los km de las etapas que tengan puertos

-- sin optimizar
-- tablas etapa y puerto
SELECT DISTINCT 
  p.numetapa, 
  e.kms
  FROM puerto p 
    JOIN etapa e ON p.numetapa = e.numetapa;

-- optimizada 

-- c1
-- saco el numetapa de las etapas que tienen puerto
SELECT DISTINCT p.numetapa FROM puerto p;

-- terminar

-- opcion 1
-- join etapa y c1

SELECT 
    c1.numetapa,e.kms 
  FROM etapa e JOIN (
    SELECT DISTINCT p.numetapa FROM puerto p
  ) c1 using(numetapa);


-- opcion 2
-- where in

SELECT 
    e.numetapa,e.kms 
  FROM etapa e 
  WHERE e.numetapa IN (SELECT DISTINCT p.numetapa FROM puerto p);

-- 9	Indicar los km de las etapas que hayan ganado ciclistas del Banesto y que tengan puertos

-- sin optimizar
-- tablas etapa,ciclistas,puerto
SELECT DISTINCT  
    e.kms
  FROM ciclista c 
    JOIN etapa e ON c.dorsal = e.dorsal
    JOIN puerto p ON e.numetapa = p.numetapa
  WHERE 
    c.nomequipo='Banesto';


-- optimizada

-- c1
-- dorsal de los ciclistas de banesto
SELECT c.dorsal FROM ciclista c WHERE c.nomequipo='Banesto';


-- c2
-- numetapa de las etapas que tengan puerto

SELECT DISTINCT p.numetapa FROM puerto p;

-- c3
-- dorsal,km de los ciclistas que han ganado etapas que tengan puerto
-- etapa join c2
-- etapa where numetapa in c2

-- opcion join
SELECT 
    e.dorsal,e.kms
  FROM etapa e JOIN (
    SELECT DISTINCT p.numetapa FROM puerto p
  ) c2 using(numetapa);

-- opcion where in
SELECT 
    e.dorsal,e.kms 
  FROM etapa e 
  WHERE e.numetapa IN (SELECT DISTINCT p.numetapa FROM puerto p);

-- terminada
-- c1 join c3
-- c3 where dorsal in c1
-- dorsal y km de los ciclistas de banesto que han ganado etapas con puertos

SELECT 
    c3.kms 
  FROM (
      SELECT c.dorsal FROM ciclista c WHERE c.nomequipo='Banesto'
  ) c1 JOIN (
      SELECT 
          e.dorsal,e.kms 
        FROM etapa e 
        WHERE e.numetapa IN (SELECT DISTINCT p.numetapa FROM puerto p)
  ) c3 USING(dorsal);

-- 10	Listar el número de ciclistas que hayan ganado alguna etapa con puerto

-- sin optimizar
-- tablas etapas,puerto
SELECT DISTINCT e.dorsal FROM etapa e JOIN puerto p ON e.numetapa = p.numetapa;

-- optimizada

-- c1
-- numetapa de las que tienen puerto
SELECT DISTINCT p.numetapa FROM puerto p;

-- terminar
-- c1 join etapa
-- etapa where numetapa in c1

-- opcion 1 
-- join
SELECT 
    DISTINCT e.dorsal
  FROM etapa e JOIN (
    SELECT DISTINCT p.numetapa FROM puerto p
  ) c1 USING(numetapa);

-- opcion 2
-- where 

SELECT 
    distinct e.dorsal 
  FROM etapa e 
  WHERE e.numetapa IN (SELECT DISTINCT p.numetapa FROM puerto p);


-- 11	Indicar el nombre de los puertos que hayan sido ganados por ciclistas de Banesto

-- sin optimizar
-- tablas puertos,ciclistas
SELECT 
    p.nompuerto 
  FROM puerto p 
    JOIN ciclista c ON p.dorsal = c.dorsal
  WHERE c.nomequipo='Banesto';


-- optimizada 

-- c1
-- dorsal de los ciclistas de banesto
SELECT c.dorsal FROM ciclista c WHERE c.nomequipo = 'Banesto';


-- terminar

-- opcion 1
-- puerto join c1

SELECT 
  p.nompuerto 
FROM puerto p 
  JOIN (
    SELECT c.dorsal FROM ciclista c WHERE c.nomequipo = 'Banesto'
    )c1 using(dorsal);

-- opcion 2
--- puerto where dorsal in c1

SELECT 
  p.nompuerto 
FROM puerto p 
WHERE p.dorsal IN (SELECT c.dorsal FROM ciclista c WHERE c.nomequipo = 'Banesto');

-- 12	Listar el número de etapas que tengan puerto que hayan sido ganados por ciclistas de Banesto con mas de 200 km

-- sin optimizar
-- tablas etapas,ciclistas,puertos

SELECT 
  DISTINCT e.numetapa 
FROM etapa e 
  JOIN ciclista c ON e.dorsal = c.dorsal
  JOIN puerto p ON e.numetapa = p.numetapa
WHERE c.nomequipo='Banesto' AND e.kms>=200;


-- optimizada

-- c1
-- numetapa de las que tienen puerto
SELECT DISTINCT p.numetapa FROM puerto p;

-- c2
-- dorsal de los ciclistas de banesto
SELECT c.dorsal FROM ciclista c WHERE c.nomequipo='Banesto';

-- c3
-- numetapa,dorsal de las etapas que tienen mas de 200 km
SELECT e.numetapa,e.dorsal FROM etapa e WHERE e.kms>=200;

-- terminar

-- opcion 1
-- c1 join c3 join c2
SELECT 
  c1.numetapa 
FROM (
  SELECT DISTINCT p.numetapa FROM puerto p
) c1 JOIN (
  SELECT e.numetapa,e.dorsal FROM etapa e WHERE e.kms>=200
) c3 using(numetapa) JOIN (
  SELECT c.dorsal FROM ciclista c WHERE c.nomequipo='Banesto'
) c2 using(dorsal);

-- opcion 2
-- c1 join c3 where dorsal in c2

SELECT 
  c1.numetapa 
FROM (
    SELECT DISTINCT p.numetapa FROM puerto p
  ) c1 JOIN (
    SELECT e.numetapa,e.dorsal FROM etapa e WHERE e.kms>=200
  ) c3 using(numetapa) 
WHERE dorsal IN (SELECT c.dorsal FROM ciclista c WHERE c.nomequipo='Banesto');

-- opcion 3
-- c3 where numetapa in c1 and dorsal in c2

SELECT 
  e.numetapa 
FROM  etapa e
WHERE 
  e.dorsal IN (SELECT c.dorsal FROM ciclista c WHERE c.nomequipo='Banesto')
  AND 
  e.numetapa IN (SELECT DISTINCT p.numetapa FROM puerto p)
  AND 
  e.kms>=200;

