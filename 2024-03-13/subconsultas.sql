﻿USE ejemploComercios;

-- quiero el dni y el nombre de los clientes que se han registrado 
-- en un comercio de Sevilla

-- utilizar join sin optimizar
-- cliente,registra,comercio
SELECT DISTINCT 
    c.dni,c.nombre 
  FROM cliente c 
    JOIN registra r ON c.dni = r.dni
    JOIN comercio c1 ON r.cif = c1.cif
    WHERE c1.ciudad='sevilla';

-- optimizar
-- c1
-- cif de los comercias de sevilla
SELECT c.cif FROM comercio c WHERE c.ciudad='sevilla';

-- c2
-- dni de los clientes que se han registrado en esos comercios

SELECT DISTINCT 
    dni 
  FROM registra r JOIN (
    SELECT c.cif FROM comercio c WHERE c.ciudad='sevilla'
  )c1 USING(cif);

-- terminar
-- opcion 1
-- combinamos c2 con cliente para sacar el dni y el nombre
SELECT 
   c.dni,c.nombre 
  FROM cliente c JOIN (
        SELECT DISTINCT 
        dni 
      FROM registra r JOIN (
        SELECT c.cif FROM comercio c WHERE c.ciudad='sevilla'
      )c1 USING(cif)
  )c2 using(dni);

-- opcion 2
-- utilizar natural join
SELECT 
   c.dni,c.nombre 
  FROM cliente c NATURAL JOIN (
        SELECT DISTINCT 
        dni 
      FROM registra r JOIN (
        SELECT c.cif FROM comercio c WHERE c.ciudad='sevilla'
      )c1 USING(cif)
  )c2;

-- opcion 3
-- utilizar where + in
SELECT 
    c.nombre,c.dni
  FROM cliente c 
    WHERE c.dni IN (
      SELECT DISTINCT 
          dni 
        FROM registra r JOIN (
          SELECT c.cif FROM comercio c WHERE c.ciudad='sevilla'
        )c1 USING(cif)
    );


-- Ejercicio
-- Nombre de los Comercios que estan en sevilla y en madrid

-- mal
SELECT c.cif FROM comercio c WHERE c.ciudad='sevilla' AND c.ciudad='madrid';

-- c1
-- nombres de los comercios de sevilla
SELECT c.nombre FROM comercio c WHERE c.ciudad='sevilla';

-- c2
-- nombres de los comercios de madrid
SELECT c.nombre FROM comercio c WHERE c.ciudad='madrid';


-- terminar
-- combino c1 y c2
SELECT 
    * 
  FROM (
    SELECT c.nombre FROM comercio c WHERE c.ciudad='sevilla'
  ) c1 JOIN (
    SELECT c.nombre FROM comercio c WHERE c.ciudad='madrid'
  ) c2 USING(nombre);

-- utilizando la interseccion

SELECT c.nombre FROM comercio c WHERE c.ciudad='sevilla'
INTERSECT 
SELECT c.nombre FROM comercio c WHERE c.ciudad='madrid'


-- utilizando natural join

SELECT 
    * 
  FROM (
    SELECT c.nombre FROM comercio c WHERE c.ciudad='sevilla'
  ) c1 NATURAL JOIN (
    SELECT c.nombre FROM comercio c WHERE c.ciudad='madrid'
  ) c2;

-- utilizando where + in
    SELECT c.nombre FROM comercio c WHERE c.ciudad='sevilla' AND c.nombre IN (
      SELECT c.nombre FROM comercio c WHERE c.ciudad='madrid'
    );



-- Ejercicio
-- Nombre de los Comercios que estan en sevilla o en madrid

-- OR

SELECT DISTINCT c.nombre FROM comercio c WHERE c.ciudad='sevilla' OR c.ciudad='madrid';

-- c1
-- nombres de los comercios de sevilla
SELECT c.nombre FROM comercio c WHERE c.ciudad='sevilla';

-- c2
-- nombres de los comercios de madrid
SELECT c.nombre FROM comercio c WHERE c.ciudad='madrid';

-- terminar
-- union

SELECT c.nombre FROM comercio c WHERE c.ciudad='sevilla'
UNION ALL
SELECT c.nombre FROM comercio c WHERE c.ciudad='madrid';