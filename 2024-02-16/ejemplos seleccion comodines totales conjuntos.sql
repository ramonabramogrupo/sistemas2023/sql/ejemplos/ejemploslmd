﻿USE empleadosdepartamentos;

/**
Empleados que trabajan en el departamento 10 o 20
**/

-- utilizando un OR
SELECT 
    * 
  FROM emple e
  WHERE 
    e.dept_no=10 OR e.dept_no=20;

-- utilizando IN
SELECT 
    * 
  FROM emple e
  WHERE e.dept_no IN (10,20);

-- union
SELECT * FROM emple e WHERE e.dept_no=10
UNION 
SELECT * FROM emple e WHERE e.dept_no=20;


/**
Lista los empleados cuyo salario es menor que 2000 o salarios mayor que 3000
**/

-- utilizando OR
SELECT 
    * 
  FROM emple e
  WHERE 
    e.salario<2000 OR e.salario>3000;

-- utilizando between
SELECT 
  * 
  FROM emple e
  WHERE 
    e.salario NOT BETWEEN 2000 AND 3000;

-- UTILIZANDO UNION

SELECT * FROM emple e WHERE e.salario<2000
UNION
SELECT * FROM emple e WHERE e.salario>3000;

/**
listar apellidos  de los empleados mas los nombres de los departamentos
**/

SELECT DISTINCT e.apellido FROM emple e
UNION 
SELECT DISTINCT d.dnombre FROM  depart d;


/**
Empleados cuyo salario son 1600 o 1350 0 1040
**/

-- utilizando or
SELECT * FROM emple e 
  WHERE e.salario=1600 OR e.salario=1350 OR e.salario=1040;

-- utilizando in
SELECT * FROM emple e WHERE e.salario IN (1600,1350,1040);

-- utilizando union
  SELECT * FROM emple e 
    WHERE e.salario=1600
UNION 
  SELECT * FROM emple e 
    WHERE e.salario=1350
UNION 
  SELECT * FROM emple e 
    WHERE e.salario=1040;

/**
   Listar los vendedores y empleados (oficios) cuyo salario sea mayor    que 1500 euros
**/

SELECT 
    * 
  FROM emple e
  WHERE 
    e.oficio='vendedor' OR e.oficio='empleado' AND e.salario>1500;

SELECT 
    * 
  FROM emple e
  WHERE 
    e.oficio='vendedor' AND e.salario>1500
    OR 
    e.oficio='empleado' AND e.salario>1500;

SELECT 
    * 
  FROM emple e
  WHERE 
    e.oficio IN ('vendedor','empleado') AND e.salario>1500;

  SELECT 
      * 
    FROM emple e
    WHERE 
      e.oficio='vendedor' AND e.salario>1500
UNION  
  SELECT 
      * 
    FROM emple e
    WHERE 
      e.oficio='empleado' AND e.salario>1500;    

/**
Indicarme los codigos de departamento que tienen empleados y vendedores
**/


SELECT 
    * 
  FROM emple e
  WHERE 
    e.oficio='empleado'
    AND 
    e.oficio='vendedor';

-- necesitamos un operador de conjuntos
SELECT DISTINCT 
    e.dept_no 
  FROM emple e
  WHERE 
    e.oficio='empleado'
INTERSECT 
SELECT DISTINCT   
    e.dept_no 
  FROM emple e
  WHERE 
    e.oficio='vendedor';

-- no estoy muy suelto y utilizo UNION
SELECT DISTINCT 
    e.dept_no 
  FROM emple e
  WHERE 
    e.oficio='empleado'
UNION
SELECT DISTINCT   
    e.dept_no 
  FROM emple e
  WHERE 
   e.oficio='vendedor';


/**
   Listar los vendedores y empleados (oficios) cuyo salario sea mayor que 1500 euros.
REALIZARLA CON INTERSECCION Y UTILIZANDO IN
MOSTRAR LOS CAMPOS EMP_NO Y APELLIDO
**/

SELECT  
    * 
  FROM emple e
  WHERE 
    e.oficio IN ('empleado','vendedor')
INTERSECT 
SELECT  
    * 
  FROM emple e
  WHERE 
    e.salario>1500;


-- empleados cuya fecha de alta es enero

-- con like
SELECT 
  * 
  FROM emple e
  WHERE 
    e.fecha_alt LIKE '%-01-%';

-- con funciones
SELECT 
  * 
  FROM emple e
  WHERE 
   MONTH(e.fecha_alt)=1;



-- empleados cuyo apellido comience por m

-- con like
SELECT 
  * 
  FROM emple e
  WHERE 
    e.apellido LIKE 'm%';

-- con funciones

-- con left
SELECT 
  * 
  FROM emple e
  WHERE 
    LEFT(e.apellido,1)='m';

-- con substring (substr)
SELECT 
    * 
  FROM emple e
  WHERE 
    SUBSTRING(e.apellido,1,1)='m';


-- los empleados que el segundo caracter de su apellido sea una 'o'

-- con like (%,_)
SELECT 
  * 
  FROM emple e
  WHERE e.apellido LIKE '_o%';


-- con funciones 

-- con substring
SELECT 
  * 
  FROM emple e
  WHERE 
    SUBSTRING(e.apellido,2,1)='o';

-- con left y right
SELECT 
  *
  FROM emple e
  WHERE 
    RIGHT(LEFT(e.apellido,2),1)='o'; 
  
    






