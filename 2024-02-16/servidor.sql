-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 16, 2024 at 10:49 AM
-- Server version: 8.2.0
-- PHP Version: 8.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `agenda2023`
--
CREATE DATABASE IF NOT EXISTS `agenda2023` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci;
USE `agenda2023`;

-- --------------------------------------------------------

--
-- Table structure for table `cita`
--

CREATE TABLE `cita` (
  `id` int NOT NULL,
  `nombre` varchar(400) DEFAULT NULL,
  `lugar` varchar(100) DEFAULT NULL,
  `fechaHora` datetime DEFAULT NULL,
  `duracion` int DEFAULT NULL,
  `descripcion` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cita`
--
ALTER TABLE `cita`
  ADD PRIMARY KEY (`id`);
--
-- Database: `alumnos`
--
CREATE DATABASE IF NOT EXISTS `alumnos` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci;
USE `alumnos`;

-- --------------------------------------------------------

--
-- Table structure for table `alumnos`
--

CREATE TABLE `alumnos` (
  `id` int NOT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  `apellidos` varchar(200) DEFAULT NULL,
  `telefono` varchar(20) NOT NULL,
  `fechaNacimiento` date DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `poblacion` varchar(100) DEFAULT 'Santander'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cursos`
--

CREATE TABLE `cursos` (
  `idCurso` int NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `fechaComienzo` date DEFAULT NULL,
  `precio` float DEFAULT '0'
) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `alumnos`
--
ALTER TABLE `alumnos`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `uk1` (`nombre`,`apellidos`),
  ADD KEY `telefono` (`telefono`),
  ADD KEY `poblacion` (`poblacion`);

--
-- Indexes for table `cursos`
--
ALTER TABLE `cursos`
  ADD PRIMARY KEY (`idCurso`),
  ADD UNIQUE KEY `nombre` (`nombre`),
  ADD KEY `fechaComienzo` (`fechaComienzo`),
  ADD KEY `k1` (`fechaComienzo`,`precio`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `alumnos`
--
ALTER TABLE `alumnos`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cursos`
--
ALTER TABLE `cursos`
  MODIFY `idCurso` int NOT NULL AUTO_INCREMENT;
--
-- Database: `aplicacion001`
--
CREATE DATABASE IF NOT EXISTS `aplicacion001` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci;
USE `aplicacion001`;

-- --------------------------------------------------------

--
-- Table structure for table `producto`
--

CREATE TABLE `producto` (
  `idProducto` int NOT NULL,
  `nombreProducto` varchar(200) DEFAULT NULL,
  `precioProducto` float DEFAULT '0',
  `fechaEntrada` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `producto`
--

INSERT INTO `producto` (`idProducto`, `nombreProducto`, `precioProducto`, `fechaEntrada`) VALUES
(1, 'manzanas', 2.45, '2023-12-22'),
(2, 'naranjas', 3, '2023-12-11'),
(3, 'patatas', 1.23, NULL),
(4, 'lechugas', 0.34, '2023-08-01'),
(5, '', 0, NULL),
(6, 'tomates', 15, '2023-12-02'),
(7, 'bolis', 12, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `producto`
--
ALTER TABLE `producto`
  ADD PRIMARY KEY (`idProducto`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `producto`
--
ALTER TABLE `producto`
  MODIFY `idProducto` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- Database: `aulas2023`
--
CREATE DATABASE IF NOT EXISTS `aulas2023` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci;
USE `aulas2023`;

-- --------------------------------------------------------

--
-- Table structure for table `alumnos`
--

CREATE TABLE `alumnos` (
  `id` int NOT NULL,
  `nombre` varchar(300) DEFAULT NULL,
  `apellidos` varchar(400) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `telefono` varchar(20) DEFAULT NULL,
  `foto` varchar(200) DEFAULT NULL,
  `fecha` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `contenido`
--

CREATE TABLE `contenido` (
  `id` int NOT NULL,
  `documento` varchar(200) DEFAULT NULL,
  `foto` varchar(200) DEFAULT NULL,
  `idCurso` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cursos`
--

CREATE TABLE `cursos` (
  `id` int NOT NULL,
  `nombre` varchar(400) DEFAULT NULL,
  `temario` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `matricula`
--

CREATE TABLE `matricula` (
  `id` int NOT NULL,
  `idAlumno` int DEFAULT NULL,
  `idCurso` int DEFAULT NULL,
  `fechaMatricula` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `alumnos`
--
ALTER TABLE `alumnos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contenido`
--
ALTER TABLE `contenido`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fkContenidoCursos` (`idCurso`);

--
-- Indexes for table `cursos`
--
ALTER TABLE `cursos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `matricula`
--
ALTER TABLE `matricula`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `idAlumno` (`idAlumno`,`idCurso`,`fechaMatricula`),
  ADD KEY `fkMatriculaCursos` (`idCurso`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `matricula`
--
ALTER TABLE `matricula`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `contenido`
--
ALTER TABLE `contenido`
  ADD CONSTRAINT `fkContenidoCursos` FOREIGN KEY (`idCurso`) REFERENCES `cursos` (`id`);

--
-- Constraints for table `matricula`
--
ALTER TABLE `matricula`
  ADD CONSTRAINT `fkMatriculaAlumnos` FOREIGN KEY (`idAlumno`) REFERENCES `alumnos` (`id`),
  ADD CONSTRAINT `fkMatriculaCursos` FOREIGN KEY (`idCurso`) REFERENCES `cursos` (`id`);
--
-- Database: `basura`
--
CREATE DATABASE IF NOT EXISTS `basura` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci;
USE `basura`;
--
-- Database: `bucles`
--
CREATE DATABASE IF NOT EXISTS `bucles` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci;
USE `bucles`;
--
-- Database: `ciclistas`
--
CREATE DATABASE IF NOT EXISTS `ciclistas` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `ciclistas`;

-- --------------------------------------------------------

--
-- Table structure for table `ciclista`
--

CREATE TABLE `ciclista` (
  `dorsal` smallint NOT NULL,
  `nombre` varchar(30) NOT NULL,
  `edad` smallint DEFAULT NULL,
  `nomequipo` varchar(25) NOT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=162 DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `ciclista`
--

INSERT INTO `ciclista` (`dorsal`, `nombre`, `edad`, `nomequipo`) VALUES
(1, 'Miguel Induráin', 32, 'Banesto'),
(2, 'Pedro Delgado', 35, 'Banesto'),
(3, 'Alex Zulle', 27, 'ONCE'),
(4, 'Tony Rominger', 30, 'Mapei-Clas'),
(5, 'Gert-Jan Theunisse', 32, 'TVM'),
(6, 'Adriano Baffi', 33, 'Mercatone Uno'),
(7, 'Massimiliano Lelli', 30, 'Mercatone Uno'),
(8, 'Jean Van Poppel', 33, 'Lotus Festina'),
(9, 'Massimo Podenzana', 34, 'Navigare'),
(10, 'Mario Cipollini', 28, 'Mercatone Uno'),
(11, 'Flavio Giupponi', 31, 'Bresciali-Refin'),
(12, 'Alessio Di Basco', 31, 'Amore Vita'),
(13, 'Lale Cubino', 28, 'Seguros Amaya'),
(14, 'Roberto Pagnin', 33, 'Navigare'),
(15, 'Jesper Skibby', 31, 'TVM'),
(16, 'Dimitri Konishev', 29, 'Jolly Club'),
(17, 'Bruno Leali', 37, 'Bresciali-Refin'),
(18, 'Robert Millar', 37, 'TVM'),
(19, 'Julian Gorospe', 34, 'Banesto'),
(20, 'Alfonso Gutiérrez', 29, 'Artiach'),
(21, 'Erwin Nijboer', 31, 'Artiach'),
(22, 'Giorgio Furlan', 32, 'Gewiss'),
(23, 'Lance Armstrong', 27, 'Motorola'),
(24, 'Claudio Chiappucci', 29, 'Carrera'),
(25, 'Gianni Bugno', 32, 'Gatorade'),
(26, 'Mikel Zarrabeitia', 27, 'Banesto'),
(27, 'Laurent Jalabert', 28, 'ONCE'),
(28, 'Jesus Montoya', 33, 'Banesto'),
(29, 'Angel Edo', 28, 'Kelme'),
(30, 'Melchor Mauri', 28, 'Banesto'),
(31, 'Vicente Aparicio', 30, 'Banesto'),
(32, 'Laurent Dufaux', 28, 'ONCE'),
(33, 'Stefano della Santa', 29, 'Mapei-Clas'),
(34, 'Angel Yesid Camargo', 30, 'Kelme'),
(35, 'Erik Dekker', 28, 'Wordperfect'),
(36, 'Gian Matteo Fagnini', 32, 'Mercatone Uno'),
(37, 'Scott Sunderland', 29, 'TVM'),
(38, 'Javier Palacin', 25, 'Euskadi'),
(39, 'Rudy Verdonck', 30, 'Lotus Festina'),
(40, 'Viatceslav Ekimov', 32, 'Wordperfect'),
(41, 'Rolf Aldag', 25, 'Telecom'),
(42, 'Davide Cassani', 29, 'TVM'),
(43, 'Francesco Casagrande', 28, 'Mercatone Uno'),
(44, 'Luca Gelfi', 27, 'Gatorade'),
(45, 'Alberto Elli', 26, 'Artiach'),
(46, 'Agustin Sagasti', 24, 'Euskadi'),
(47, 'Laurent Pillon', 32, 'Gewiss'),
(48, 'Marco Saligari', 29, 'Gewiss'),
(49, 'Eugeni Berzin', 23, 'Gewiss'),
(50, 'Fernando Escartin', 27, 'Mapei-Clas'),
(51, 'Udo Bolts', 30, 'Telecom'),
(52, 'Vladislav Bobrik', 26, 'Gewiss'),
(53, 'Michele Bartoli', 28, 'Mercatone Uno'),
(54, 'Steffen Wesemann', 30, 'Telecom'),
(55, 'Nicola Minali', 28, 'Gewiss'),
(56, 'Andrew Hampsten', 29, 'Banesto'),
(57, 'Stefano Zanini', 28, 'Navigare'),
(58, 'Gerd Audehm', 34, 'Telecom'),
(59, 'Mariano Picolli', 28, 'Mercatone Uno'),
(60, 'Giovanni Lombardi', 28, 'Bresciali-Refin'),
(61, 'Walte Castignola', 26, 'Navigare'),
(62, 'Raul Alcala', 30, 'Motorola'),
(63, 'Alvaro Mejia', 32, 'Motorola'),
(64, 'Giuseppe Petito', 28, 'Mercatone Uno'),
(65, 'Pascal Lino', 29, 'Amore Vita'),
(66, 'Enrico Zaina', 24, 'Gewiss'),
(67, 'Armand de las Cuevas', 28, 'Castorama'),
(68, 'Angel Citracca', 28, 'Navigare'),
(69, 'Eddy Seigneur', 27, 'Castorama'),
(70, 'Sandro Heulot', 29, 'Banesto'),
(71, 'Prudencio Induráin', 27, 'Banesto'),
(72, 'Stefano Colage', 28, 'Bresciali-Refin'),
(73, 'Laurent Fignon', 35, 'Gatorade'),
(74, 'Claudio Chioccioli', 36, 'Amore Vita'),
(75, 'Juan Romero', 32, 'Seguros Amaya'),
(76, 'Marco Giovannetti', 34, 'Gatorade'),
(77, 'Javier Mauleon', 33, 'Mapei-Clas'),
(78, 'Antonio Esparza', 35, 'Kelme'),
(79, 'Johan Bruyneel', 33, 'ONCE'),
(80, 'Federico Echave', 37, 'Mapei-Clas'),
(81, 'Piotr Ugrumov', 33, 'Gewiss'),
(82, 'Edgar Corredor', 30, 'Kelme'),
(83, 'Hernan Buenahora', 32, 'Kelme'),
(84, 'Jon Unzaga', 31, 'Mapei-Clas'),
(85, 'Dimitri Abdoujaparov', 30, 'Carrera'),
(86, 'Juan Martinez Oliver', 32, 'Kelme'),
(87, 'Fernando Mota', 32, 'Artiach'),
(88, 'Angel Camarillo', 28, 'Mapei-Clas'),
(89, 'Stefan Roche', 36, 'Carrera'),
(90, 'Ivan Ivanov', 27, 'Artiach'),
(91, 'Nestor Mora', 28, 'Kelme'),
(92, 'Federico Garcia', 27, 'Artiach'),
(93, 'Bo Hamburger', 29, 'TVM'),
(94, 'Marino Alonso', 30, 'Banesto'),
(95, 'Manuel Guijarro', 31, 'Lotus Festina'),
(96, 'Tom Cordes', 29, 'Wordperfect'),
(97, 'Casimiro Moreda', 28, 'ONCE'),
(98, 'Eleuterio Anguita', 25, 'Artiach'),
(99, 'Per Pedersen', 29, 'Seguros Amaya'),
(100, 'William Palacios', 30, 'Jolly Club'),
(1000, 'prueba', 18, 'Amore Vita');

-- --------------------------------------------------------

--
-- Table structure for table `equipo`
--

CREATE TABLE `equipo` (
  `nomequipo` varchar(25) NOT NULL,
  `director` varchar(30) DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=744 DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `equipo`
--

INSERT INTO `equipo` (`nomequipo`, `director`) VALUES
('Amore Vita', 'Ricardo Padacci'),
('Artiach', 'José Peréz'),
('Banesto', 'Miguel Echevarria'),
('Bresciali-Refin', 'Pietro Armani'),
('Carrera', 'Luigi Petroni'),
('Castorama', 'Jean Philip'),
('Euskadi', 'Pedro Txucaru'),
('Gatorade', 'Gian Luca Pacceli'),
('Gewiss', 'Moreno Argentin'),
('Jolly Club', 'Johan Richard'),
('Kelme', 'Álvaro Pino'),
('Lotus Festina', 'Suarez Cuevas'),
('Mapei-Clas', 'Juan Fernandez'),
('Mercatone Uno', 'Ettore Romano'),
('Motorola', 'John Fidwell'),
('Navigare', 'Lonrenzo Sciacci'),
('ONCE', 'Manuel Sainz'),
('PDM', 'Piet Van Der Kruis'),
('Seguros Amaya', 'Minguez'),
('Telecom', 'Morgan Reikcard'),
('TVM', 'Steveens Henk'),
('Wordperfect', 'Bill Gates');

-- --------------------------------------------------------

--
-- Table structure for table `etapa`
--

CREATE TABLE `etapa` (
  `numetapa` smallint NOT NULL,
  `kms` smallint NOT NULL,
  `salida` varchar(35) NOT NULL,
  `llegada` varchar(35) NOT NULL,
  `dorsal` smallint DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=780 DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `etapa`
--

INSERT INTO `etapa` (`numetapa`, `kms`, `salida`, `llegada`, `dorsal`) VALUES
(1, 9, 'Valladolid', 'Valladolid', 1),
(2, 180, 'Valladolid', 'Salamanca', 36),
(3, 240, 'Salamanca', 'Caceres', 12),
(4, 230, 'Almendralejo', 'Córdoba', 83),
(5, 170, 'Córdoba', 'Granada', 27),
(6, 150, 'Granada', 'Sierra Nevada', 52),
(7, 250, 'Baza', 'Alicante', 22),
(8, 40, 'Benidorm', 'Benidorm', 1),
(9, 150, 'Benidorm', 'Valencia', 35),
(10, 200, 'Igualada', 'Andorra', 2),
(11, 195, 'Andorra', 'Estación de Cerler', 65),
(12, 220, 'Benasque', 'Zaragoza', 12),
(13, 200, 'Zaragoza', 'Pamplona', 93),
(14, 172, 'Pamplona', 'Alto de la Cruz de la Demanda', 86),
(15, 207, 'Santo Domingo de la Calzada', 'Santander', 10),
(16, 160, 'Santander', 'Lagos de Covadonga', 5),
(17, 140, 'Cangas de Onis', 'Alto del Naranco', 4),
(18, 195, 'Ávila', 'Ávila', 8),
(19, 190, 'Ávila', 'Destilerias Dyc', 2),
(20, 52, 'Segovia', 'Destilerias Dyc', 2),
(21, 170, 'Destilerias Dyc', 'Madrid', 27);

-- --------------------------------------------------------

--
-- Table structure for table `lleva`
--

CREATE TABLE `lleva` (
  `dorsal` smallint NOT NULL,
  `numetapa` smallint NOT NULL,
  `código` varchar(3) NOT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=150 DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `lleva`
--

INSERT INTO `lleva` (`dorsal`, `numetapa`, `código`) VALUES
(1, 1, 'MGE'),
(1, 1, 'MMO'),
(1, 1, 'MMV'),
(1, 1, 'MRE'),
(1, 1, 'MSE'),
(1, 2, 'MGE'),
(1, 3, 'MGE'),
(1, 4, 'MGE'),
(1, 16, 'MGE'),
(1, 17, 'MGE'),
(1, 18, 'MGE'),
(1, 19, 'MGE'),
(1, 20, 'MGE'),
(1, 21, 'MGE'),
(2, 5, 'MGE'),
(2, 6, 'MGE'),
(2, 7, 'MGE'),
(2, 21, 'MMO'),
(3, 11, 'MGE'),
(3, 12, 'MGE'),
(4, 8, 'MGE'),
(8, 2, 'MSE'),
(8, 4, 'MSE'),
(10, 18, 'MSE'),
(12, 3, 'MSE'),
(12, 5, 'MSE'),
(12, 6, 'MSE'),
(16, 2, 'MMV'),
(16, 3, 'MMV'),
(16, 5, 'MMV'),
(16, 6, 'MMV'),
(17, 4, 'MMV'),
(20, 6, 'MRE'),
(20, 7, 'MRE'),
(20, 8, 'MRE'),
(20, 9, 'MRE'),
(20, 10, 'MRE'),
(20, 11, 'MRE'),
(20, 12, 'MRE'),
(20, 13, 'MRE'),
(20, 14, 'MRE'),
(20, 15, 'MRE'),
(20, 16, 'MRE'),
(20, 17, 'MRE'),
(20, 18, 'MMV'),
(20, 19, 'MRE'),
(20, 20, 'MRE'),
(20, 21, 'MRE'),
(22, 14, 'MSE'),
(22, 15, 'MSE'),
(22, 16, 'MSE'),
(22, 17, 'MSE'),
(22, 19, 'MSE'),
(22, 20, 'MSE'),
(22, 21, 'MSE'),
(24, 4, 'MMO'),
(25, 2, 'MMO'),
(25, 3, 'MMO'),
(25, 5, 'MMO'),
(26, 6, 'MMO'),
(26, 7, 'MMO'),
(26, 8, 'MMO'),
(26, 9, 'MGE'),
(26, 9, 'MMO'),
(26, 10, 'MGE'),
(26, 18, 'MMO'),
(27, 2, 'MRE'),
(27, 3, 'MRE'),
(27, 4, 'MRE'),
(27, 5, 'MRE'),
(27, 18, 'MRE'),
(28, 14, 'MMO'),
(28, 15, 'MMO'),
(28, 16, 'MMO'),
(28, 17, 'MMO'),
(28, 19, 'MMO'),
(28, 20, 'MMO'),
(30, 10, 'MMO'),
(30, 11, 'MMO'),
(30, 12, 'MMO'),
(30, 13, 'MGE'),
(30, 13, 'MMO'),
(30, 14, 'MGE'),
(30, 15, 'MGE'),
(33, 7, 'MMV'),
(33, 8, 'MMV'),
(42, 14, 'MMV'),
(42, 15, 'MMV'),
(42, 16, 'MMV'),
(42, 17, 'MMV'),
(42, 19, 'MMV'),
(42, 20, 'MMV'),
(42, 21, 'MMV'),
(48, 9, 'MMV'),
(48, 10, 'MMV'),
(48, 11, 'MMV'),
(48, 12, 'MMV'),
(48, 13, 'MMV'),
(67, 1, 'MMS'),
(67, 3, 'MMS'),
(69, 2, 'MMS'),
(69, 4, 'MMS'),
(99, 7, 'MSE'),
(99, 8, 'MSE'),
(99, 9, 'MSE'),
(99, 10, 'MSE'),
(99, 11, 'MSE'),
(99, 12, 'MSE'),
(99, 13, 'MSE');

-- --------------------------------------------------------

--
-- Table structure for table `maillot`
--

CREATE TABLE `maillot` (
  `código` varchar(3) NOT NULL,
  `tipo` varchar(30) NOT NULL,
  `color` varchar(20) NOT NULL,
  `premio` int NOT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=2730 DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `maillot`
--

INSERT INTO `maillot` (`código`, `tipo`, `color`, `premio`) VALUES
('MGE', 'General', 'amarillo', 8000000),
('MMO', 'Montaña', 'blanco y rojo', 2000000),
('MMS', 'Más Sufrido', 'estrellas moradas', 2000000),
('MMV', 'Metas volantes', 'rojo', 2000000),
('MRE', 'Regularidad', 'verde', 2000000),
('MSE', 'Sprints especiales', 'rosa', 2000000);

-- --------------------------------------------------------

--
-- Table structure for table `puerto`
--

CREATE TABLE `puerto` (
  `nompuerto` varchar(35) NOT NULL,
  `altura` smallint NOT NULL,
  `categoria` varchar(1) NOT NULL,
  `pendiente` double(15,5) DEFAULT NULL,
  `numetapa` smallint NOT NULL,
  `dorsal` smallint DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=1170 DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `puerto`
--

INSERT INTO `puerto` (`nompuerto`, `altura`, `categoria`, `pendiente`, `numetapa`, `dorsal`) VALUES
('Alto del Naranco', 565, '1', 6.90000, 10, 30),
('Arcalis', 2230, 'E', 6.50000, 10, 4),
('Cerler-Circo de Ampriu', 2500, 'E', 5.87000, 11, 9),
('Coll de la Comella', 1362, '1', 8.07000, 10, 2),
('Coll de Ordino', 1980, 'E', 5.30000, 10, 7),
('Cruz de la Demanda', 1850, 'E', 7.00000, 11, 20),
('Lagos de Covadonga', 1134, 'E', 6.86000, 16, 42),
('Navacerrada', 1860, '1', 7.50000, 19, 2),
('Puerto de Alisas', 672, '1', 5.80000, 15, 1),
('Puerto de la Morcuera', 1760, '2', 6.50000, 19, 2),
('Puerto de Mijares', 1525, '1', 4.90000, 18, 24),
('Puerto de Navalmoral', 1521, '2', 4.30000, 18, 2),
('Puerto de Pedro Bernardo', 1250, '1', 4.20000, 18, 25),
('Sierra Nevada', 2500, 'E', 6.00000, 2, 26);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ciclista`
--
ALTER TABLE `ciclista`
  ADD PRIMARY KEY (`dorsal`),
  ADD KEY `equipociclista` (`nomequipo`);

--
-- Indexes for table `equipo`
--
ALTER TABLE `equipo`
  ADD PRIMARY KEY (`nomequipo`);

--
-- Indexes for table `etapa`
--
ALTER TABLE `etapa`
  ADD PRIMARY KEY (`numetapa`),
  ADD KEY `ciclistaetapa` (`dorsal`);

--
-- Indexes for table `lleva`
--
ALTER TABLE `lleva`
  ADD PRIMARY KEY (`numetapa`,`código`),
  ADD KEY `ciclistallevar` (`dorsal`),
  ADD KEY `etapallevar` (`numetapa`),
  ADD KEY `maillotllevar` (`código`);

--
-- Indexes for table `maillot`
--
ALTER TABLE `maillot`
  ADD PRIMARY KEY (`código`);

--
-- Indexes for table `puerto`
--
ALTER TABLE `puerto`
  ADD PRIMARY KEY (`nompuerto`),
  ADD KEY `ciclistapuerto` (`dorsal`),
  ADD KEY `etapapuerto` (`numetapa`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `ciclista`
--
ALTER TABLE `ciclista`
  ADD CONSTRAINT `FK_ciclista_equipo_nomequipo` FOREIGN KEY (`nomequipo`) REFERENCES `equipo` (`nomequipo`);

--
-- Constraints for table `etapa`
--
ALTER TABLE `etapa`
  ADD CONSTRAINT `FK_etapa_ciclista_dorsal` FOREIGN KEY (`dorsal`) REFERENCES `ciclista` (`dorsal`);

--
-- Constraints for table `lleva`
--
ALTER TABLE `lleva`
  ADD CONSTRAINT `FK_lleva_ciclista_dorsal` FOREIGN KEY (`dorsal`) REFERENCES `ciclista` (`dorsal`),
  ADD CONSTRAINT `FK_lleva_etapa_numetapa` FOREIGN KEY (`numetapa`) REFERENCES `etapa` (`numetapa`),
  ADD CONSTRAINT `FK_lleva_maillot_código` FOREIGN KEY (`código`) REFERENCES `maillot` (`código`);

--
-- Constraints for table `puerto`
--
ALTER TABLE `puerto`
  ADD CONSTRAINT `FK_puerto_ciclista_dorsal` FOREIGN KEY (`dorsal`) REFERENCES `ciclista` (`dorsal`),
  ADD CONSTRAINT `FK_puerto_etapa_numetapa` FOREIGN KEY (`numetapa`) REFERENCES `etapa` (`numetapa`);
--
-- Database: `clientes2023`
--
CREATE DATABASE IF NOT EXISTS `clientes2023` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci;
USE `clientes2023`;

-- --------------------------------------------------------

--
-- Table structure for table `cliente`
--

CREATE TABLE `cliente` (
  `idcliente` int NOT NULL,
  `nombreCliente` varchar(100) DEFAULT NULL,
  `apellidosCliente` varchar(200) DEFAULT NULL,
  `emailCliente` varchar(100) DEFAULT NULL,
  `fechaNacimientoCliente` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `cliente`
--

INSERT INTO `cliente` (`idcliente`, `nombreCliente`, `apellidosCliente`, `emailCliente`, `fechaNacimientoCliente`) VALUES
(1, 'ramon', 'abramo', 'aaaaa', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`idcliente`);
--
-- Database: `cms`
--
CREATE DATABASE IF NOT EXISTS `cms` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci;
USE `cms`;

-- --------------------------------------------------------

--
-- Table structure for table `entradas`
--

CREATE TABLE `entradas` (
  `id` int NOT NULL,
  `titulo` varchar(100) DEFAULT NULL,
  `texto` text,
  `fecha` datetime DEFAULT CURRENT_TIMESTAMP,
  `foto` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `entradas`
--

INSERT INTO `entradas` (`id`, `titulo`, `texto`, `fecha`, `foto`) VALUES
(1, 'ejemplo de entrada', 'texto de la entrada', '2022-02-01 00:00:00', 'mandarina.png'),
(2, 'noticia 1', 'texto de la noticia', '2023-01-02 00:00:00', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `entradas`
--
ALTER TABLE `entradas`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `entradas`
--
ALTER TABLE `entradas`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Database: `compañias`
--
CREATE DATABASE IF NOT EXISTS `compañias` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `compañias`;

-- --------------------------------------------------------

--
-- Table structure for table `ciudad`
--

CREATE TABLE `ciudad` (
  `nombre` varchar(30) NOT NULL,
  `población` int DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=3276 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ciudad`
--

INSERT INTO `ciudad` (`nombre`, `población`) VALUES
('AB', 70000),
('CR', 60000),
('CU', 50000),
('GU', 75000),
('TO', 80000);

-- --------------------------------------------------------

--
-- Table structure for table `compañia`
--

CREATE TABLE `compañia` (
  `nombre` varchar(30) NOT NULL,
  `ciudad` varchar(30) DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=5461 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `compañia`
--

INSERT INTO `compañia` (`nombre`, `ciudad`) VALUES
('NESTLE', 'AB'),
('INDRA', 'CR'),
('FAGOR', 'TO');

-- --------------------------------------------------------

--
-- Table structure for table `persona`
--

CREATE TABLE `persona` (
  `nombre` varchar(30) NOT NULL,
  `calle` varchar(30) DEFAULT NULL,
  `ciudad` varchar(30) DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=1489 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `persona`
--

INSERT INTO `persona` (`nombre`, `calle`, `ciudad`) VALUES
('Ana', 'Calle5', 'AB'),
('Antonio', 'Calle7', 'CU'),
('Blas', 'Calle6', 'AB'),
('Casimiro', 'Calle11', 'CR'),
('Eva', 'Calle2', 'CR'),
('Jose', 'Calle3', 'TO'),
('Juan', 'Calle1', 'CR'),
('Marga', 'Calle9', 'GU'),
('Maria', 'Calle4', 'TO'),
('Paco', 'Calle9', 'GU'),
('Wendy', 'Calle8', 'CU');

-- --------------------------------------------------------

--
-- Table structure for table `supervisa`
--

CREATE TABLE `supervisa` (
  `supervisor` varchar(30) NOT NULL,
  `persona` varchar(30) NOT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=2340 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `supervisa`
--

INSERT INTO `supervisa` (`supervisor`, `persona`) VALUES
('Maria', 'Ana'),
('Blas', 'Antonio'),
('Marga', 'Blas'),
('Wendy', 'Eva'),
('Wendy', 'Juan'),
('Jose', 'Maria'),
('Marga', 'Paco');

-- --------------------------------------------------------

--
-- Table structure for table `trabaja`
--

CREATE TABLE `trabaja` (
  `persona` varchar(30) NOT NULL,
  `compañia` varchar(30) DEFAULT NULL,
  `salario` int DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=1638 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trabaja`
--

INSERT INTO `trabaja` (`persona`, `compañia`, `salario`) VALUES
('Ana', 'FAGOR', 18000),
('Antonio', 'NESTLE', 15000),
('Blas', 'NESTLE', 25000),
('Eva', 'INDRA', 30000),
('Jose', 'FAGOR', 50000),
('Juan', 'INDRA', 40000),
('Marga', 'NESTLE', 50000),
('Maria', 'FAGOR', 40000),
('Paco', 'NESTLE', 25000),
('Wendy', 'INDRA', 50000);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ciudad`
--
ALTER TABLE `ciudad`
  ADD PRIMARY KEY (`nombre`);

--
-- Indexes for table `compañia`
--
ALTER TABLE `compañia`
  ADD PRIMARY KEY (`nombre`),
  ADD KEY `FK_compañia_ciudad_nombre` (`ciudad`);

--
-- Indexes for table `persona`
--
ALTER TABLE `persona`
  ADD PRIMARY KEY (`nombre`),
  ADD KEY `FK_persona_ciudad_nombre` (`ciudad`);

--
-- Indexes for table `supervisa`
--
ALTER TABLE `supervisa`
  ADD PRIMARY KEY (`supervisor`,`persona`),
  ADD KEY `FK_supervisa_persona_nombre1` (`persona`);

--
-- Indexes for table `trabaja`
--
ALTER TABLE `trabaja`
  ADD PRIMARY KEY (`persona`),
  ADD KEY `FK_trabaja_compañia_nombre` (`compañia`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `compañia`
--
ALTER TABLE `compañia`
  ADD CONSTRAINT `FK_compañia_ciudad_nombre` FOREIGN KEY (`ciudad`) REFERENCES `ciudad` (`nombre`);

--
-- Constraints for table `persona`
--
ALTER TABLE `persona`
  ADD CONSTRAINT `FK_persona_ciudad_nombre` FOREIGN KEY (`ciudad`) REFERENCES `ciudad` (`nombre`);

--
-- Constraints for table `supervisa`
--
ALTER TABLE `supervisa`
  ADD CONSTRAINT `FK_supervisa_persona_nombre` FOREIGN KEY (`supervisor`) REFERENCES `persona` (`nombre`),
  ADD CONSTRAINT `FK_supervisa_persona_nombre1` FOREIGN KEY (`persona`) REFERENCES `persona` (`nombre`);

--
-- Constraints for table `trabaja`
--
ALTER TABLE `trabaja`
  ADD CONSTRAINT `FK_trabaja_compañia_nombre` FOREIGN KEY (`compañia`) REFERENCES `compañia` (`nombre`),
  ADD CONSTRAINT `FK_trabaja_persona_nombre` FOREIGN KEY (`persona`) REFERENCES `persona` (`nombre`);
--
-- Database: `compranp`
--
CREATE DATABASE IF NOT EXISTS `compranp` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `compranp`;

-- --------------------------------------------------------

--
-- Table structure for table `clientes`
--

CREATE TABLE `clientes` (
  `codCli` int NOT NULL,
  `nombre` varchar(50) COLLATE utf8mb3_spanish_ci DEFAULT NULL,
  `apellidos` varchar(50) COLLATE utf8mb3_spanish_ci NOT NULL,
  `fechaNac` date DEFAULT NULL,
  `poblacion` int NOT NULL,
  `numeroProductos` int DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=5461 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_spanish_ci;

--
-- Dumping data for table `clientes`
--

INSERT INTO `clientes` (`codCli`, `nombre`, `apellidos`, `fechaNac`, `poblacion`, `numeroProductos`) VALUES
(1, 'Eva', 'Gomez', '2021-02-11', 1, 10),
(2, 'Ana', 'Lopez', '2021-02-11', 1, 9),
(3, 'Jose', 'Ruiz', '2021-02-11', 2, 1),
(4, 'Roberto', 'Abramo', NULL, 4, 2),
(5, 'Luis', 'Sanz', '2021-02-11', 2, 1),
(6, 'Raul', 'Gutierrez', NULL, 3, 3),
(7, 'Rosa', 'Feijoo', NULL, 8, 3),
(8, 'Ramon', 'Abramo', '2021-02-11', 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `clientes1`
--

CREATE TABLE `clientes1` (
  `codCli` int NOT NULL,
  `nombre` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `clientes1`
--

INSERT INTO `clientes1` (`codCli`, `nombre`) VALUES
(1, 'Eva Gomez'),
(2, 'Ana Lopez'),
(3, 'Jose Ruiz'),
(4, 'Roberto Abramo'),
(5, 'Luis Sanz'),
(6, 'Raul Gutierrez'),
(7, 'Rosa Feijoo'),
(8, 'Ramon Abramo');

-- --------------------------------------------------------

--
-- Table structure for table `compran`
--

CREATE TABLE `compran` (
  `idCompran` int NOT NULL,
  `codCli` int NOT NULL,
  `codPro` int NOT NULL,
  `numeroProductos` int DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=5461 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_spanish_ci;

--
-- Dumping data for table `compran`
--

INSERT INTO `compran` (`idCompran`, `codCli`, `codPro`, `numeroProductos`) VALUES
(1, 1, 1, 122),
(2, 1, 2, 66),
(3, 3, 1, 10),
(4, 4, 1, NULL),
(5, 2, 1, 3),
(6, 5, 6, NULL),
(7, 1, 3, NULL),
(8, 1, 4, 34),
(9, 1, 5, NULL),
(10, 1, 6, NULL),
(11, 1, 7, NULL),
(12, 1, 8, NULL),
(13, 1, 9, NULL),
(14, 1, 10, NULL),
(15, 2, 3, NULL),
(16, 2, 4, NULL),
(17, 2, 5, NULL),
(18, 2, 6, NULL),
(19, 2, 7, NULL),
(20, 2, 8, NULL),
(21, 2, 9, NULL),
(22, 2, 10, NULL),
(39, 6, 3, NULL),
(40, 7, 4, NULL),
(41, 7, 5, NULL),
(42, 6, 7, NULL),
(43, 4, 8, NULL),
(44, 7, 9, NULL),
(45, 6, 10, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `compranp`
--

CREATE TABLE `compranp` (
  `idCompran` int NOT NULL,
  `fecha` date NOT NULL,
  `cantidad` int DEFAULT NULL,
  `total` float DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=4096 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_spanish_ci;

--
-- Dumping data for table `compranp`
--

INSERT INTO `compranp` (`idCompran`, `fecha`, `cantidad`, `total`) VALUES
(1, '2020-01-02', 25, 250),
(1, '2020-01-03', 25, 250),
(1, '2020-01-04', 25, 250),
(1, '2020-01-05', 25, 250),
(1, '2020-01-10', 5, 50),
(1, '2020-11-04', 2, 20),
(1, '2020-11-06', 15, 150),
(2, '2020-01-05', 4, 40),
(2, '2020-01-06', 47, 470),
(2, '2020-01-07', 10, 100),
(2, '2020-11-09', 5, 50),
(3, '2020-11-02', 10, 100),
(5, '2021-01-05', 3, 27),
(8, '2021-02-05', 34, 306);

-- --------------------------------------------------------

--
-- Table structure for table `poblacion`
--

CREATE TABLE `poblacion` (
  `idPob` int NOT NULL,
  `nombre` varchar(50) COLLATE utf8mb3_spanish_ci DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=8192 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_spanish_ci;

--
-- Dumping data for table `poblacion`
--

INSERT INTO `poblacion` (`idPob`, `nombre`) VALUES
(1, 'santander'),
(2, 'Laredo'),
(3, 'Reinosa'),
(4, 'Santoña'),
(5, 'Solares'),
(6, 'Noja'),
(7, 'Torrelavega'),
(8, 'Isla'),
(9, 'Muriedas'),
(10, 'Astillero');

-- --------------------------------------------------------

--
-- Table structure for table `productos`
--

CREATE TABLE `productos` (
  `codPro` int NOT NULL,
  `nombre` varchar(50) COLLATE utf8mb3_spanish_ci DEFAULT NULL,
  `precio` float DEFAULT NULL,
  `numeroClientes` int DEFAULT '0'
) ENGINE=InnoDB AVG_ROW_LENGTH=8192 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_spanish_ci;

--
-- Dumping data for table `productos`
--

INSERT INTO `productos` (`codPro`, `nombre`, `precio`, `numeroClientes`) VALUES
(1, 'chorizo', 100, 0),
(2, 'bolis', 2, 1),
(3, 'goma\r\n', 1, 3),
(4, 'bloc pequeño', 10, 3),
(5, 'bloc grande', 18, 3),
(6, 'bloc anillas', 15, 3),
(7, 'paquete folios 100', 12, 3),
(8, 'paquete folios 500', 20, 3),
(9, 'portaminas', 5, 3),
(10, 'regla', 30, 3),
(101, 'raro', 10, 0),
(102, 'morcilla', 10, 0);

-- --------------------------------------------------------

--
-- Table structure for table `productosnovendidos`
--

CREATE TABLE `productosnovendidos` (
  `codPro` int NOT NULL,
  `nombre` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `productosnovendidos`
--

INSERT INTO `productosnovendidos` (`codPro`, `nombre`) VALUES
(101, 'raro'),
(102, 'morcilla');

-- --------------------------------------------------------

--
-- Table structure for table `productosvendidos`
--

CREATE TABLE `productosvendidos` (
  `codPro` int NOT NULL,
  `nombre` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `productosvendidos`
--

INSERT INTO `productosvendidos` (`codPro`, `nombre`) VALUES
(1, 'lapiz'),
(2, 'bolis'),
(3, 'goma\r\n'),
(4, 'bloc pequeño'),
(5, 'bloc grande'),
(6, 'bloc anillas'),
(7, 'paquete folios 100'),
(8, 'paquete folios 500'),
(9, 'portaminas'),
(10, 'regla');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`codCli`),
  ADD KEY `fkClientesPoblacion` (`poblacion`);

--
-- Indexes for table `clientes1`
--
ALTER TABLE `clientes1`
  ADD PRIMARY KEY (`codCli`);

--
-- Indexes for table `compran`
--
ALTER TABLE `compran`
  ADD PRIMARY KEY (`idCompran`),
  ADD UNIQUE KEY `ukcompran` (`codCli`,`codPro`),
  ADD KEY `fkcompranproductos` (`codPro`);

--
-- Indexes for table `compranp`
--
ALTER TABLE `compranp`
  ADD PRIMARY KEY (`idCompran`,`fecha`);

--
-- Indexes for table `poblacion`
--
ALTER TABLE `poblacion`
  ADD PRIMARY KEY (`idPob`);

--
-- Indexes for table `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`codPro`);

--
-- Indexes for table `productosnovendidos`
--
ALTER TABLE `productosnovendidos`
  ADD PRIMARY KEY (`codPro`);

--
-- Indexes for table `productosvendidos`
--
ALTER TABLE `productosvendidos`
  ADD PRIMARY KEY (`codPro`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `clientes`
--
ALTER TABLE `clientes`
  MODIFY `codCli` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `compran`
--
ALTER TABLE `compran`
  MODIFY `idCompran` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `clientes`
--
ALTER TABLE `clientes`
  ADD CONSTRAINT `fkClientesPoblacion` FOREIGN KEY (`poblacion`) REFERENCES `poblacion` (`idPob`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `compran`
--
ALTER TABLE `compran`
  ADD CONSTRAINT `fkcompranclientes` FOREIGN KEY (`codCli`) REFERENCES `clientes` (`codCli`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fkcompranproductos` FOREIGN KEY (`codPro`) REFERENCES `productos` (`codPro`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `compranp`
--
ALTER TABLE `compranp`
  ADD CONSTRAINT `fkcompranpCompran` FOREIGN KEY (`idCompran`) REFERENCES `compran` (`idCompran`) ON DELETE CASCADE ON UPDATE CASCADE;
--
-- Database: `concesionario`
--
CREATE DATABASE IF NOT EXISTS `concesionario` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci;
USE `concesionario`;

-- --------------------------------------------------------

--
-- Table structure for table `coche`
--

CREATE TABLE `coche` (
  `bastidor` varchar(100) NOT NULL,
  `marca` varchar(100) DEFAULT NULL,
  `modelo` varchar(100) DEFAULT NULL,
  `cilindrada` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `coche`
--

INSERT INTO `coche` (`bastidor`, `marca`, `modelo`, `cilindrada`) VALUES
('b1', 'yamaha', 'uno', 0),
('bastidor coche', 'ford', 'nuevo', 1000),
('bastidor1', 'fiat', 'doblo', 1500),
('bastidor2', '', '', 0),
('bastidor3', '', '', 0),
('bastidor4', 'ford', 'mondeos', 4568),
('bastidor44', 'asd', 'asdasd', 23);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `coche`
--
ALTER TABLE `coche`
  ADD PRIMARY KEY (`bastidor`);
--
-- Database: `concesionariomotos`
--
CREATE DATABASE IF NOT EXISTS `concesionariomotos` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci;
USE `concesionariomotos`;

-- --------------------------------------------------------

--
-- Table structure for table `moto`
--

CREATE TABLE `moto` (
  `id` int NOT NULL,
  `matricula` varchar(10) DEFAULT NULL,
  `marca` varchar(100) DEFAULT NULL,
  `modelo` varchar(100) DEFAULT NULL,
  `precio` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `moto`
--

INSERT INTO `moto` (`id`, `matricula`, `marca`, `modelo`, `precio`) VALUES
(1000, 'dgh1234', 'yamaha', 'tmax', 2000),
(2000, 'm2', 'yamaha', 'm', 1),
(15444, '111111', '111', '111', 111),
(154441111, '11111122', '111', '111', 111);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `moto`
--
ALTER TABLE `moto`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `matricula` (`matricula`);
--
-- Database: `consultas2`
--
CREATE DATABASE IF NOT EXISTS `consultas2` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `consultas2`;

-- --------------------------------------------------------

--
-- Table structure for table `consultas`
--

CREATE TABLE `consultas` (
  `id` int NOT NULL,
  `texto` varchar(255) DEFAULT NULL,
  `tabla` varchar(255) DEFAULT NULL,
  `sql` varchar(255) DEFAULT NULL,
  `resultados` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=468 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `consultas`
--

INSERT INTO `consultas` (`id`, `texto`, `tabla`, `sql`, `resultados`) VALUES
(1, 'Mostrar todos los campos y todos los registros de la tabla empleado.', 'emple', 'select * from emple', NULL),
(2, 'Mostrar todos los campos y todos los registros de la tabla departamento.', 'depart', 'select * from depart', NULL),
(3, 'Mostrar el apellido y oficio de cada empleado.', 'emple', 'SELECT e.apellido,e.oficio FROM emple e', NULL),
(4, 'Mostrar la localización y número de cada departamento.', 'depart', NULL, NULL),
(5, 'Mostrar el número, nombre y localización de cada departamento.', 'depart', NULL, NULL),
(6, 'Indicar el número de empleados que hay.', 'emple', NULL, NULL),
(7, 'Datos de los empleados ordenados por apellido de forma ascendente.', 'emple', NULL, NULL),
(8, 'Datos de los empleados ordenados por apellido de forma descendente.', 'emple', NULL, NULL),
(9, 'Indicar el número de departamentos que hay.', 'depart', NULL, NULL),
(10, 'Datos de los empleados ordenados por número departamento descendentemente.', 'emple', NULL, NULL),
(11, 'Datos de los empleados ordenados por num. depart. descendentemente y por oficio ascendente.', 'emple', NULL, NULL),
(12, 'Datos de los empleados ordenados por num. depart. descendentemente y por apellido ascendente.', 'emple', NULL, NULL),
(13, 'Mostrar los códigos de los empleados cuyo salario sea mayor que 2000.', 'emple', NULL, NULL),
(14, 'Mostrar los códigos y los apellidos de los empleados cuyo salario sea menor que 2000.', 'emple', NULL, NULL),
(15, 'Mostrar los datos de los empleados cuyo salario este entre 1500 y 2500.', 'emple', NULL, NULL),
(16, 'Mostrar los datos de los empleados cuyo oficio sea ANALISTA.', 'emple', NULL, NULL),
(17, 'Mostrar los datos de los empleados cuyo oficio sea ANALISTA y ganen más de 2000.', 'emple', NULL, NULL),
(18, 'Seleccionar el apellido y oficio de los empleados del departamento número 20.', 'emple', NULL, NULL),
(19, 'Contar el número de empleados cuyo oficio sea VENDEDOR.', 'emple', NULL, NULL),
(20, 'Mostrar todos los datos de los empleados cuyos apellidos comiencen por m o por n ordenados por apellidos ascendentemente.', 'emple', NULL, NULL),
(21, 'Seleccionar los empleados cuyo oficio sea VENDEDOR ordenados por apellido de forma ascendente.', 'emple', NULL, NULL),
(22, 'Mostrar los apellidos del empleado que más gana.', 'emple', NULL, NULL),
(23, 'Mostrar los empleados cuyo departamento sea 10 y cuyo oficio sea ANALISTA ordenando por apellido y oficio ascendentemente.', 'emple', NULL, NULL),
(24, 'Listado de los distintos meses en que los empleados se han dado de alta.', 'emple', NULL, NULL),
(25, 'Listado de los distintos años en que los empleados se han dado de alta.', 'emple', NULL, NULL),
(26, 'Listado de los distintos días en que los empleados se han dado de alta.', 'emple', NULL, NULL),
(27, 'e pertenezcan al departamento número 20.', 'emple', NULL, NULL),
(28, 'Listado que nos coloque el apellido del empleado y el nombre del departamento al que pertenece.', 'emple', NULL, NULL),
(29, 'Listado del apellido, el oficioy el nombre del departamento al que pertenece el empleado ordenándolos por apellido de forma descendente.', 'emple', NULL, NULL),
(30, 'Listar el número de empleados por departamento.', 'emple', NULL, NULL),
(31, 'Listar el número de empleados por departamento con nombre departamento.', 'emple', NULL, NULL),
(32, 'Listar el apellido de todos los empleados y ordenadorlos por oficio, y por nombre.', 'emple', NULL, NULL),
(33, 'Seleccionar de la tabla EMPLE los empleados cuyo apellido empiece por A. Listar el apellido de los empleados.', 'emple', NULL, NULL),
(34, 'Seleccionar de la tabla EMPLE los empleados cuyo apellido empiece por A o por M. Listar el apellido de los empleados.', 'emple', NULL, NULL),
(35, ' Listar todos los campos de la tabla empleados  cuyo apellido no termine Z.', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `depart`
--

CREATE TABLE `depart` (
  `dept_no` int NOT NULL,
  `dnombre` varchar(30) DEFAULT NULL,
  `loc` varchar(30) DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=4096 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `depart`
--

INSERT INTO `depart` (`dept_no`, `dnombre`, `loc`) VALUES
(10, 'CONTABILIDAD', 'SEVILLA'),
(20, 'INVESTIGACIÓN', 'MADRID'),
(30, 'VENTAS', 'BARCELONA'),
(40, 'PRODUCCIÓN', 'BILBAO');

-- --------------------------------------------------------

--
-- Table structure for table `emple`
--

CREATE TABLE `emple` (
  `emp_no` int NOT NULL,
  `apellido` varchar(50) NOT NULL,
  `oficio` varchar(30) DEFAULT NULL,
  `dir` int DEFAULT NULL,
  `fecha_alt` date DEFAULT NULL,
  `salario` int DEFAULT NULL,
  `comision` int DEFAULT NULL,
  `dept_no` int DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=1170 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `emple`
--

INSERT INTO `emple` (`emp_no`, `apellido`, `oficio`, `dir`, `fecha_alt`, `salario`, `comision`, `dept_no`) VALUES
(4578, 'abramo', 'profesor', 12, '2050-03-05', 4510, 100, 20),
(7369, 'SÁNCHEZ', 'EMPLEADO', 7902, '1990-12-17', 1040, NULL, 20),
(7499, 'ARROYO', 'VENDEDOR', 7698, '1990-02-20', 1500, 390, 30),
(7521, 'SALA', 'VENDEDOR', 7698, '1991-02-22', 1625, 650, 30),
(7566, 'JIMÉNEZ', 'DIRECTOR', 7839, '1991-04-02', 2900, NULL, 20),
(7654, 'MARTÍN', 'VENDEDOR', 7698, '1991-09-29', 1600, 1020, 30),
(7698, 'NEGRO', 'DIRECTOR', 7839, '1991-05-01', 3005, NULL, 30),
(7782, 'CEREZO', 'DIRECTOR', 7839, '1991-06-09', 2885, NULL, 10),
(7788, 'GIL', 'ANALISTA', 7566, '1991-11-09', 3000, NULL, 20),
(7839, 'REY', 'PRESIDENTE', 1, '1991-11-17', 4100, NULL, 10),
(7844, 'TOVAR', 'VENDEDOR', 7698, '1991-09-08', 1350, 0, 30),
(7876, 'ALONSO', 'EMPLEADO', 7788, '1991-09-23', 1430, NULL, 20),
(7900, 'JIMENO', 'EMPLEADO', 7698, '1991-12-03', 1335, NULL, 30),
(7902, 'FERNÁNDEZ', 'ANALISTA', 7566, '1991-12-03', 3000, NULL, 20),
(7934, 'MUÑOZ', 'EMPLEADO', 7782, '1992-01-23', 1690, NULL, 10);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `consultas`
--
ALTER TABLE `consultas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `depart`
--
ALTER TABLE `depart`
  ADD PRIMARY KEY (`dept_no`);

--
-- Indexes for table `emple`
--
ALTER TABLE `emple`
  ADD PRIMARY KEY (`emp_no`),
  ADD KEY `FKempleDepart` (`dept_no`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `consultas`
--
ALTER TABLE `consultas`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `depart`
--
ALTER TABLE `depart`
  MODIFY `dept_no` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `emple`
--
ALTER TABLE `emple`
  ADD CONSTRAINT `FKempleDepart` FOREIGN KEY (`dept_no`) REFERENCES `depart` (`dept_no`) ON DELETE CASCADE ON UPDATE CASCADE;
--
-- Database: `cursores`
--
CREATE DATABASE IF NOT EXISTS `cursores` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci;
USE `cursores`;

-- --------------------------------------------------------

--
-- Table structure for table `numeros`
--

CREATE TABLE `numeros` (
  `id` int NOT NULL,
  `valor` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `numeros`
--

INSERT INTO `numeros` (`id`, `valor`) VALUES
(1, 2),
(2, 4),
(3, 7),
(4, 20),
(5, 13);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `numeros`
--
ALTER TABLE `numeros`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `numeros`
--
ALTER TABLE `numeros`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- Database: `desarrollo`
--
CREATE DATABASE IF NOT EXISTS `desarrollo` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci;
USE `desarrollo`;

-- --------------------------------------------------------

--
-- Table structure for table `alumnos`
--

CREATE TABLE `alumnos` (
  `codigo` int NOT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  `correo` varchar(100) DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=4096 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `alumnos`
--

INSERT INTO `alumnos` (`codigo`, `nombre`, `correo`) VALUES
(1, 'Eva', 'eva@alpe.es'),
(2, 'Luis', 'luis@alpe.es'),
(3, 'Susana', 'susana@alpe.es'),
(4, 'Lorena', 'lorena@alpe.es'),
(10, 'juan', 'juan@alpe.es'),
(11, 'pedro', 'pedro@alpe.es');

-- --------------------------------------------------------

--
-- Table structure for table `examenes`
--

CREATE TABLE `examenes` (
  `id` int NOT NULL,
  `titulo` varchar(100) DEFAULT NULL,
  `nota` float DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `codigoAlumno` int DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=8192 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `examenes`
--

INSERT INTO `examenes` (`id`, `titulo`, `nota`, `fecha`, `codigoAlumno`) VALUES
(1, 'php', 9, '2023-05-01', 1),
(2, 'php', 8, '2023-04-20', 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `alumnos`
--
ALTER TABLE `alumnos`
  ADD PRIMARY KEY (`codigo`),
  ADD UNIQUE KEY `correo` (`correo`);

--
-- Indexes for table `examenes`
--
ALTER TABLE `examenes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKExamenesAlumnos` (`codigoAlumno`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `examenes`
--
ALTER TABLE `examenes`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `examenes`
--
ALTER TABLE `examenes`
  ADD CONSTRAINT `FKExamenesAlumnos` FOREIGN KEY (`codigoAlumno`) REFERENCES `alumnos` (`codigo`);
--
-- Database: `disparadores`
--
CREATE DATABASE IF NOT EXISTS `disparadores` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci;
USE `disparadores`;

-- --------------------------------------------------------

--
-- Table structure for table `historico`
--

CREATE TABLE `historico` (
  `id` int NOT NULL,
  `numero` int DEFAULT '0',
  `dia` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `historico`
--

INSERT INTO `historico` (`id`, `numero`, `dia`) VALUES
(1, 0, 15),
(2, 0, 4),
(4, 2, 19),
(5, 2, 8),
(6, 1, 12);

-- --------------------------------------------------------

--
-- Table structure for table `noticias`
--

CREATE TABLE `noticias` (
  `id` int NOT NULL,
  `texto` varchar(100) DEFAULT NULL,
  `titulo` varchar(100) DEFAULT NULL,
  `longitud` int DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `dia` int DEFAULT NULL,
  `mes` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `noticias`
--

INSERT INTO `noticias` (`id`, `texto`, `titulo`, `longitud`, `fecha`, `dia`, `mes`) VALUES
(2, 'ejemplo de noticia corta', 'corta', 24, '2022-01-12', 12, 'enero'),
(3, 'EJEMPLO NUEVA', 'nueva', 13, '2022-05-19', 19, 'mayo'),
(4, 'EJEMPLO NUEVA 1', 'nueva 2', 15, '2022-05-08', 8, 'mayo'),
(5, 'EJEMPLO NUEVA', 'nueva', 13, '2022-05-19', 19, 'mayo'),
(6, 'EJEMPLO NUEVA 1', 'nueva 2', 15, '2022-05-08', 8, 'mayo');

--
-- Triggers `noticias`
--
DELIMITER $$
CREATE TRIGGER `noticiasAD` AFTER DELETE ON `noticias` FOR EACH ROW BEGIN
  UPDATE historico h
    SET numero=numero-1
    WHERE 
      dia=OLD.dia;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `noticiasAI` AFTER INSERT ON `noticias` FOR EACH ROW BEGIN
    DECLARE dia int DEFAULT 0;
    DECLARE contador int DEFAULT 0;

    set dia=NEW.dia; -- esta variable almacena el dia calculado con el disparador anterior
   
    -- cuantos registros del mismo dia que la noticia
    -- introducida tengo en historico
    -- si es 1 ese dia ya esta en historico
    -- es es 0 ese dia no esta en historico
    -- opcion 1
    SELECT COUNT(*) INTO contador FROM historico h WHERE h.dia=dia;
    -- opcion 2
    -- SET contador=(SELECT COUNT(*) INTO contador FROM historico h WHERE h.dia=dia);
    
    IF (contador=1) THEN
      -- si contador es 1 indica que en historico ya tenia noticias de ese dia
      -- solamente tengo que sumar 1 a numero
      UPDATE historico h
        SET h.numero=h.numero+1
        WHERE h.dia=dia;
    ELSE 
      -- si contador es 0 indica que no tenia noticias de ese dia 
      -- tengo que insertar un registro nuevo en historico
      INSERT INTO historico (dia, numero)
        VALUES (dia,1);
    END IF;

END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `noticiasAU` AFTER UPDATE ON `noticias` FOR EACH ROW BEGIN

  -- comprobar si tengo que realizar algo
  IF (NEW.dia<>OLD.dia) THEN
    UPDATE historico h
      SET h.numero=h.numero-1
      WHERE h.dia=OLd.dia;
    IF (SELECT COUNT(*) FROM historico h WHERE h.dia=NEW.dia) THEN
      UPDATE historico h
      SET h.numero=h.numero+1
      WHERE h.dia=NEW.dia;
    ELSE 
      INSERT INTO historico (dia, numero) VALUES (NEW.dia, 1);
    END IF;
  END IF;

  
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `noticiasBI` BEFORE INSERT ON `noticias` FOR EACH ROW BEGIN
  SET NEW.longitud=CHAR_LENGTH(NEW.texto);
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `noticiasBI2` BEFORE INSERT ON `noticias` FOR EACH ROW BEGIN
  SET LC_TIME_NAMES='es_es';
  SET NEW.dia=DAY(NEW.fecha), 
      NEW.mes=MONTHNAME(NEW.fecha);
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `noticiasBU` BEFORE UPDATE ON `noticias` FOR EACH ROW BEGIN
  SET LC_TIME_NAMES='es_es';
  SET NEW.dia=DAY(NEW.fecha), 
      NEW.mes=MONTHNAME(NEW.fecha), 
      NEW.longitud=CHAR_LENGTH(NEW.texto);

END
$$
DELIMITER ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `historico`
--
ALTER TABLE `historico`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `noticias`
--
ALTER TABLE `noticias`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `historico`
--
ALTER TABLE `historico`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `noticias`
--
ALTER TABLE `noticias`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- Database: `disparadores1`
--
CREATE DATABASE IF NOT EXISTS `disparadores1` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci;
USE `disparadores1`;

-- --------------------------------------------------------

--
-- Table structure for table `clientes`
--

CREATE TABLE `clientes` (
  `codigo` int NOT NULL,
  `referencia` varchar(20) DEFAULT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  `empresa` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `fechaNacimiento` date DEFAULT NULL,
  `fecha` date DEFAULT (curdate())
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `clientes`
--

INSERT INTO `clientes` (`codigo`, `referencia`, `nombre`, `empresa`, `email`, `fechaNacimiento`, `fecha`) VALUES
(1, 'ramon1', 'ramon', 'alpe', NULL, '1980-01-02', '2023-07-13'),
(2, 'ana2', 'ana', 'seur', NULL, '1978-01-02', '2023-07-13'),
(3, 'cesar3', 'cesar', 'alpe', NULL, '2000-01-02', '2023-07-13'),
(4, 'lolo4', 'lolo', 'alpe', NULL, '2001-02-03', '2023-07-13'),
(5, 'cesar5', 'cesar', 'alpe', NULL, '2000-01-02', '2023-07-13'),
(6, 'lolo6', 'lolo', 'alpe', NULL, '2001-02-03', '2023-07-13'),
(7, 'cesar7', 'cesar', 'alpe', NULL, '2000-01-02', '2023-07-13'),
(8, 'cesar8', 'cesar', 'alpe', NULL, '2000-01-02', '2023-07-13'),
(9, 'cesar9', 'cesar', 'alpe', NULL, '2000-01-02', '2023-07-13'),
(10, 'cesar10', 'cesar', 'alpe', NULL, '2000-01-02', '2023-07-13'),
(11, 'lolo11', 'lolo', 'alpe', NULL, '2001-02-03', '2023-07-13'),
(13, 'lolita13', 'lolita', 'alpe', NULL, '2001-02-03', '2023-07-13'),
(110, 'lorena110', 'lorena', 'alpe', NULL, '2000-01-02', '2023-07-13'),
(120, 'luis120', 'luis', 'alpe', NULL, '2000-01-02', '2023-07-13'),
(410, 'lorena410', 'lorena', 'alpe', NULL, '2000-01-02', '2023-07-13'),
(420, 'luis420', 'luis', 'alpe', NULL, '2000-01-02', '2023-07-13'),
(421, 'cesar421', 'cesar', 'alpe', NULL, '2000-01-02', '2023-07-13');

--
-- Triggers `clientes`
--
DELIMITER $$
CREATE TRIGGER `clientesBI` BEFORE INSERT ON `clientes` FOR EACH ROW BEGIN

DECLARE numero int DEFAULT 0;
SET @@information_schema_stats_expiry=0;

IF (NEW.CODIGO=0) THEN
  SELECT 
      AUTO_INCREMENT
    INTO 
      numero
    FROM INFORMATION_SCHEMA.TABLES
      WHERE 
    table_name = 'clientes' AND TABLE_SCHEMA='disparadores1';
  
    SET NEW.referencia=CONCAT(new.nombre,numero);
ELSE 
    SET NEW.referencia=CONCAT(new.nombre,new.CODIGO);
END IF;


END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `clientesBU` BEFORE UPDATE ON `clientes` FOR EACH ROW BEGIN
  SET NEW.referencia=CONCAT(NEW.nombre,NEW.codigo);
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `empresas`
--

CREATE TABLE `empresas` (
  `empresa` varchar(100) NOT NULL,
  `trabajadores` int DEFAULT '0',
  `fechaUltimo` datetime DEFAULT NULL,
  `fechaEntrada` date DEFAULT NULL,
  `diaSemana` varchar(100) DEFAULT NULL,
  `diaMes` int DEFAULT NULL,
  `mes` varchar(100) DEFAULT NULL,
  `mesNumero` int DEFAULT NULL,
  `anno` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `empresas`
--

INSERT INTO `empresas` (`empresa`, `trabajadores`, `fechaUltimo`, `fechaEntrada`, `diaSemana`, `diaMes`, `mes`, `mesNumero`, `anno`) VALUES
('alpe', 0, '2023-07-13 16:36:14', '2021-08-14', 'sábado', 14, 'agosto', 8, 2021),
('carrefour', 0, NULL, '2022-07-16', 'sábado', 16, 'julio', 7, 2022),
('juguetes norma', 0, NULL, '2021-12-04', 'sábado', 4, 'diciembre', 12, 2021),
('seur', 0, NULL, '2021-11-02', NULL, NULL, NULL, NULL, NULL);

--
-- Triggers `empresas`
--
DELIMITER $$
CREATE TRIGGER `biempresas` BEFORE INSERT ON `empresas` FOR EACH ROW BEGIN
  SET @@lc_time_names = 'es_es'; -- LOS MESES Y DIAS EN CASTELLANO
  SET
  NEW.diaSemana = DAYNAME(NEW.fechaEntrada),
  NEW.diaMes = DAY(NEW.fechaEntrada),
  NEW.mes = MONTHNAME(NEW.fechaEntrada),
  NEW.mesNumero = MONTH(NEW.fechaEntrada),
  NEW.anno = YEAR(NEW.fechaEntrada);
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `buempresas` BEFORE UPDATE ON `empresas` FOR EACH ROW BEGIN
  SET @@lc_time_names = 'es_es'; -- LOS MESES Y DIAS EN CASTELLANO

  IF (NEW.fechaEntrada <> OLD.fechaEntrada) THEN
    SET
    NEW.diaSemana = DAYNAME(NEW.fechaEntrada),
    NEW.diaMes = DAY(NEW.fechaEntrada),
    NEW.mes = MONTHNAME(NEW.fechaEntrada),
    NEW.mesNumero = MONTH(NEW.fechaEntrada),
    NEW.anno = YEAR(NEW.fechaEntrada);
  END IF;

END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `empresasBI` BEFORE INSERT ON `empresas` FOR EACH ROW BEGIN
  SET LC_TIME_NAMES='es_es';
  SET 
    NEW.diaSemana=DAYNAME(NEW.fechaEntrada),
    NEW.mes=MONTHNAME(NEW.fechaEntrada),
    NEW.mesNumero=MONTH(new.fechaEntrada),
    new.diaMes=DAY(new.fechaEntrada),
    new.anno=year(new.fechaEntrada);
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `empresasBU` BEFORE UPDATE ON `empresas` FOR EACH ROW BEGIN
  SET LC_TIME_NAMES='es_es';

  IF (NEW.fechaEntrada<>OLD.fechaEntrada) THEN
    SET 
      NEW.diaSemana=DAYNAME(NEW.fechaEntrada),
      NEW.mes=MONTHNAME(NEW.fechaEntrada),
      NEW.mesNumero=MONTH(new.fechaEntrada),
      new.diaMes=DAY(new.fechaEntrada),
      new.anno=year(new.fechaEntrada);  
  END IF;
  
END
$$
DELIMITER ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `clientesEmpresas` (`empresa`);

--
-- Indexes for table `empresas`
--
ALTER TABLE `empresas`
  ADD PRIMARY KEY (`empresa`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `clientes`
--
ALTER TABLE `clientes`
  MODIFY `codigo` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=423;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `clientes`
--
ALTER TABLE `clientes`
  ADD CONSTRAINT `clientesEmpresas` FOREIGN KEY (`empresa`) REFERENCES `empresas` (`empresa`) ON DELETE RESTRICT ON UPDATE RESTRICT;
--
-- Database: `ejemplo001`
--
CREATE DATABASE IF NOT EXISTS `ejemplo001` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci;
USE `ejemplo001`;

-- --------------------------------------------------------

--
-- Table structure for table `coches`
--

CREATE TABLE `coches` (
  `codigo` int NOT NULL,
  `matricula` char(7) NOT NULL,
  `marca` varchar(100) DEFAULT NULL,
  `modelo` varchar(100) DEFAULT NULL,
  `fecha` date NOT NULL DEFAULT '2024-01-01'
) ;

--
-- Dumping data for table `coches`
--

INSERT INTO `coches` (`codigo`, `matricula`, `marca`, `modelo`, `fecha`) VALUES
(1, '3456dgh', 'ford', 'mondeo', '2024-01-15'),
(2, '3456fgh', 'ford', 'mondeo', '2024-01-01');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `coches`
--
ALTER TABLE `coches`
  ADD PRIMARY KEY (`codigo`),
  ADD UNIQUE KEY `ukMatricula` (`matricula`),
  ADD KEY `kmarcamodelo` (`marca`,`modelo`),
  ADD KEY `kfecha` (`fecha`);
--
-- Database: `ejemplo01aplicacion`
--
CREATE DATABASE IF NOT EXISTS `ejemplo01aplicacion` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci;
USE `ejemplo01aplicacion`;

-- --------------------------------------------------------

--
-- Table structure for table `alquiler`
--

CREATE TABLE `alquiler` (
  `idAlquiler` int NOT NULL,
  `cliente` varchar(10) DEFAULT NULL,
  `coche` varchar(10) DEFAULT NULL,
  `fechaAlquiler` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `alquiler`
--

INSERT INTO `alquiler` (`idAlquiler`, `cliente`, `coche`, `fechaAlquiler`) VALUES
(1, '20211111j', '1', '2023-08-08 00:00:00'),
(3, '20211111j', '2', '2023-08-09 00:00:00'),
(4, '20202020a', '2', NULL),
(5, '20211111j', '2', '2023-08-09 19:36:00');

-- --------------------------------------------------------

--
-- Table structure for table `clientes`
--

CREATE TABLE `clientes` (
  `nif` varchar(10) NOT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  `apellidos` varchar(100) DEFAULT NULL,
  `poblacion` varchar(100) DEFAULT NULL,
  `telefono` varchar(100) NOT NULL,
  `correo` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `clientes`
--

INSERT INTO `clientes` (`nif`, `nombre`, `apellidos`, `poblacion`, `telefono`, `correo`) VALUES
('20202020a', 'eva', '', '', '456456456', 'aaaa@aaa.es'),
('20211111j', 'alberto', 'ruiz', 'santander', '456456456', 'jjj@kkk.es');

-- --------------------------------------------------------

--
-- Table structure for table `coches`
--

CREATE TABLE `coches` (
  `bastidor` varchar(10) NOT NULL,
  `matricula` varchar(8) NOT NULL,
  `modelo` varchar(100) DEFAULT NULL,
  `marca` varchar(100) NOT NULL,
  `cilindrada` int DEFAULT NULL,
  `tipo` enum('turismo','suv','furgoneta','4x4') DEFAULT NULL,
  `fechaCompra` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `coches`
--

INSERT INTO `coches` (`bastidor`, `matricula`, `modelo`, `marca`, `cilindrada`, `tipo`, `fechaCompra`) VALUES
('1', '3452DDD', 'PICASSO', 'CITROEN', 2000, 'turismo', '2023-02-01'),
('2', '1234AAA', 'C3', 'CITROEN', 1100, 'turismo', '2023-08-07');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `alquiler`
--
ALTER TABLE `alquiler`
  ADD PRIMARY KEY (`idAlquiler`),
  ADD UNIQUE KEY `idAlquiler` (`idAlquiler`,`cliente`,`coche`),
  ADD KEY `fkAlquilerCliente` (`cliente`),
  ADD KEY `fkAlquilerCoches` (`coche`);

--
-- Indexes for table `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`nif`),
  ADD UNIQUE KEY `correo` (`correo`);

--
-- Indexes for table `coches`
--
ALTER TABLE `coches`
  ADD PRIMARY KEY (`bastidor`),
  ADD UNIQUE KEY `matricula` (`matricula`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `alquiler`
--
ALTER TABLE `alquiler`
  MODIFY `idAlquiler` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `alquiler`
--
ALTER TABLE `alquiler`
  ADD CONSTRAINT `fkAlquilerCliente` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`nif`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fkAlquilerCoches` FOREIGN KEY (`coche`) REFERENCES `coches` (`bastidor`) ON DELETE CASCADE ON UPDATE CASCADE;
--
-- Database: `ejemplo03aplicacion`
--
CREATE DATABASE IF NOT EXISTS `ejemplo03aplicacion` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci;
USE `ejemplo03aplicacion`;

-- --------------------------------------------------------

--
-- Table structure for table `actividades`
--

CREATE TABLE `actividades` (
  `id` int NOT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  `descripcion` varchar(500) DEFAULT NULL,
  `duracion` int DEFAULT NULL,
  `imagen` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `actividades`
--

INSERT INTO `actividades` (`id`, `nombre`, `descripcion`, `duracion`, `imagen`) VALUES
(1, 'pilates', '', 60, 'pilates1.jpg'),
(2, 'yoga', '', 30, 'yoga.jpg'),
(3, 'estiramientos', '', 45, 'estiramientos.jpg'),
(4, 'mantenimiento', '', 60, 'mantenimiento.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `monitor`
--

CREATE TABLE `monitor` (
  `id` int NOT NULL,
  `nombre` varchar(200) DEFAULT NULL,
  `imagen` varchar(200) DEFAULT NULL,
  `telefono` varchar(20) DEFAULT NULL,
  `correo` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `monitor`
--

INSERT INTO `monitor` (`id`, `nombre`, `imagen`, `telefono`, `correo`) VALUES
(1, 'jose vazquez', 'josevazquez.jpg', '', ''),
(2, 'rosa perez', 'rosaperez.jpg', '', ''),
(3, 'luis pascal', 'luispascal.jpg', '645645645', 'luispascal@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `realizan`
--

CREATE TABLE `realizan` (
  `id` int NOT NULL,
  `actividad` int DEFAULT NULL,
  `sala` int DEFAULT NULL,
  `monitor` int DEFAULT NULL,
  `fechaHora` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `realizan`
--

INSERT INTO `realizan` (`id`, `actividad`, `sala`, `monitor`, `fechaHora`) VALUES
(2, 1, 1, 1, '2023-08-10 19:51:00'),
(11, 1, 1, 1, '2023-09-01 21:30:00'),
(7, 1, 1, 2, '2023-08-31 18:26:00'),
(1, 1, 2, 1, '2023-08-11 22:00:00'),
(4, 1, 2, 1, '2023-08-31 19:10:00'),
(12, 1, 3, 1, '2023-09-01 21:31:00'),
(10, 1, 3, 3, '2023-09-01 20:48:00'),
(5, 2, 1, 1, '2023-08-31 20:36:00'),
(8, 2, 1, 2, '2023-09-01 18:39:00'),
(6, 3, 1, 2, '2023-08-31 20:06:00'),
(13, 3, 3, 2, '2023-09-01 22:34:00'),
(9, 4, 3, 3, '2023-09-01 19:13:00');

-- --------------------------------------------------------

--
-- Table structure for table `salas`
--

CREATE TABLE `salas` (
  `id` int NOT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  `descripcion` varchar(500) DEFAULT NULL,
  `plazas` int DEFAULT NULL,
  `imagen` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `salas`
--

INSERT INTO `salas` (`id`, `nombre`, `descripcion`, `plazas`, `imagen`) VALUES
(1, 'fuerza', '', 100, 'fuerza.jpg'),
(2, 'formacion', '', 100, 'formacion.jpg'),
(3, 'estiramientos', '', 50, 'estiramientos.jpg'),
(4, 'pensar', '', NULL, ''),
(5, 'meditacion', '', NULL, 'meditacion.jpg'),
(6, 'dinamismo', '', NULL, 'dinamismo.jpg');

-- --------------------------------------------------------

--
-- Stand-in structure for view `trabajador`
-- (See below for the actual view)
--
CREATE TABLE `trabajador` (
`monitor` int
,`numero` bigint
);

-- --------------------------------------------------------

--
-- Structure for view `trabajador`
--
DROP TABLE IF EXISTS `trabajador`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `trabajador`  AS SELECT `c2`.`numero` AS `numero`, `c2`.`monitor` AS `monitor` FROM (select count(0) AS `numero`,`r`.`monitor` AS `monitor` from `realizan` `r` group by `r`.`monitor`) AS `c2` WHERE (`c2`.`numero` = (select max(`c1`.`numero`) AS `maximo` from (select count(0) AS `numero`,`r`.`monitor` AS `monitor` from `realizan` `r` group by `r`.`monitor`) `c1`))  ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `actividades`
--
ALTER TABLE `actividades`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `monitor`
--
ALTER TABLE `monitor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `realizan`
--
ALTER TABLE `realizan`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `actividad` (`actividad`,`sala`,`monitor`,`fechaHora`),
  ADD KEY `fkRealizanMonitor` (`monitor`),
  ADD KEY `fkRealizanSalas` (`sala`);

--
-- Indexes for table `salas`
--
ALTER TABLE `salas`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `actividades`
--
ALTER TABLE `actividades`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `monitor`
--
ALTER TABLE `monitor`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `realizan`
--
ALTER TABLE `realizan`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `salas`
--
ALTER TABLE `salas`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `realizan`
--
ALTER TABLE `realizan`
  ADD CONSTRAINT `fkRealizanActividades` FOREIGN KEY (`actividad`) REFERENCES `actividades` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fkRealizanMonitor` FOREIGN KEY (`monitor`) REFERENCES `monitor` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fkRealizanSalas` FOREIGN KEY (`sala`) REFERENCES `salas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
--
-- Database: `ejemplo2aplicacion`
--
CREATE DATABASE IF NOT EXISTS `ejemplo2aplicacion` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci;
USE `ejemplo2aplicacion`;

-- --------------------------------------------------------

--
-- Table structure for table `clientes`
--

CREATE TABLE `clientes` (
  `id` int NOT NULL,
  `nombre` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `clientes`
--

INSERT INTO `clientes` (`id`, `nombre`) VALUES
(1, 'eva lopez'),
(2, 'andres gomez');

-- --------------------------------------------------------

--
-- Table structure for table `compran`
--

CREATE TABLE `compran` (
  `idCompran` int NOT NULL,
  `producto` int DEFAULT NULL,
  `cliente` int DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `cantidad` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `compran`
--

INSERT INTO `compran` (`idCompran`, `producto`, `cliente`, `fecha`, `cantidad`) VALUES
(1, 2, 1, '2023-08-10', 1);

-- --------------------------------------------------------

--
-- Table structure for table `productos`
--

CREATE TABLE `productos` (
  `id` int NOT NULL,
  `nombre` varchar(200) DEFAULT NULL,
  `peso` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `productos`
--

INSERT INTO `productos` (`id`, `nombre`, `peso`) VALUES
(1, 'cazadora cuero', 1),
(2, 'zapatos ante', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `compran`
--
ALTER TABLE `compran`
  ADD PRIMARY KEY (`idCompran`),
  ADD UNIQUE KEY `producto` (`producto`,`cliente`,`fecha`),
  ADD KEY `fkcompranclientes` (`cliente`);

--
-- Indexes for table `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `clientes`
--
ALTER TABLE `clientes`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `compran`
--
ALTER TABLE `compran`
  MODIFY `idCompran` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `productos`
--
ALTER TABLE `productos`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `compran`
--
ALTER TABLE `compran`
  ADD CONSTRAINT `fkcompranclientes` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fkcompranproductos` FOREIGN KEY (`producto`) REFERENCES `productos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
--
-- Database: `ejemploclase`
--
CREATE DATABASE IF NOT EXISTS `ejemploclase` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci;
USE `ejemploclase`;
--
-- Database: `ejemplocomercios`
--
CREATE DATABASE IF NOT EXISTS `ejemplocomercios` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci;
USE `ejemplocomercios`;

-- --------------------------------------------------------

--
-- Table structure for table `cliente`
--

CREATE TABLE `cliente` (
  `dni` int NOT NULL,
  `nombre` varchar(50) DEFAULT NULL,
  `edad` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `comercio`
--

CREATE TABLE `comercio` (
  `cif` int NOT NULL,
  `nombre` varchar(50) DEFAULT NULL,
  `ciudad` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `desarrolla`
--

CREATE TABLE `desarrolla` (
  `id_fab` int NOT NULL,
  `codigo` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `distribuye`
--

CREATE TABLE `distribuye` (
  `cif` int NOT NULL,
  `codigo` int NOT NULL,
  `cantidad` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fabricante`
--

CREATE TABLE `fabricante` (
  `id_fab` int NOT NULL,
  `nombre` varchar(50) DEFAULT NULL,
  `pais` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `programa`
--

CREATE TABLE `programa` (
  `codigo` int NOT NULL,
  `nombre` varchar(50) DEFAULT NULL,
  `version` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `registra`
--

CREATE TABLE `registra` (
  `cif` int NOT NULL,
  `dni` int NOT NULL,
  `codigo` int DEFAULT NULL,
  `medio` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`dni`);

--
-- Indexes for table `comercio`
--
ALTER TABLE `comercio`
  ADD PRIMARY KEY (`cif`);

--
-- Indexes for table `desarrolla`
--
ALTER TABLE `desarrolla`
  ADD PRIMARY KEY (`id_fab`,`codigo`),
  ADD KEY `fkDesarrollaPrograma` (`codigo`);

--
-- Indexes for table `distribuye`
--
ALTER TABLE `distribuye`
  ADD PRIMARY KEY (`cif`,`codigo`),
  ADD KEY `fkDistribuyePrograma` (`codigo`);

--
-- Indexes for table `fabricante`
--
ALTER TABLE `fabricante`
  ADD PRIMARY KEY (`id_fab`);

--
-- Indexes for table `programa`
--
ALTER TABLE `programa`
  ADD PRIMARY KEY (`codigo`);

--
-- Indexes for table `registra`
--
ALTER TABLE `registra`
  ADD PRIMARY KEY (`cif`,`dni`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `desarrolla`
--
ALTER TABLE `desarrolla`
  ADD CONSTRAINT `fkDesarrollaFabricante` FOREIGN KEY (`id_fab`) REFERENCES `fabricante` (`id_fab`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `fkDesarrollaPrograma` FOREIGN KEY (`codigo`) REFERENCES `programa` (`codigo`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Constraints for table `distribuye`
--
ALTER TABLE `distribuye`
  ADD CONSTRAINT `fkDistribuyeComercio` FOREIGN KEY (`cif`) REFERENCES `comercio` (`cif`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `fkDistribuyePrograma` FOREIGN KEY (`codigo`) REFERENCES `programa` (`codigo`) ON DELETE RESTRICT ON UPDATE RESTRICT;
--
-- Database: `ejemplodisparadores`
--
CREATE DATABASE IF NOT EXISTS `ejemplodisparadores` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci;
USE `ejemplodisparadores`;

-- --------------------------------------------------------

--
-- Table structure for table `productos`
--

CREATE TABLE `productos` (
  `producto` varchar(5) NOT NULL,
  `precio` float DEFAULT NULL,
  `cantidad` int DEFAULT '0',
  `codigoEspecial` varchar(20) DEFAULT NULL,
  `inicialProducto` char(1) DEFAULT NULL,
  `fechaUltimaVenta` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `productos`
--

INSERT INTO `productos` (`producto`, `precio`, `cantidad`, `codigoEspecial`, `inicialProducto`, `fechaUltimaVenta`) VALUES
('p1', 10, 0, NULL, NULL, NULL),
('p2', 1, 0, NULL, NULL, NULL),
('p3', 2, 0, NULL, NULL, NULL),
('p4', 22, 0, NULL, NULL, NULL),
('p5', 8, 0, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ventas`
--

CREATE TABLE `ventas` (
  `id` int NOT NULL,
  `producto` varchar(5) DEFAULT NULL,
  `unidades` int NOT NULL,
  `total` int DEFAULT '0',
  `fechaVenta` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `ventas`
--

INSERT INTO `ventas` (`id`, `producto`, `unidades`, `total`, `fechaVenta`) VALUES
(1, 'p1', 10, 100, '2022-12-30 10:39:09'),
(2, 'p2', 20, 20, '2022-12-30 10:39:09'),
(3, 'p1', 70, 700, '2022-12-30 10:39:09'),
(4, 'p3', 210, 420, '2022-12-30 10:39:09'),
(5, 'p1', 80, 800, '2022-12-30 10:39:09'),
(6, 'p3', 30, 60, '2022-12-30 10:39:09'),
(7, 'p4', 16, 352, '2022-12-30 10:39:09'),
(8, 'p5', 10, 80, '2022-12-30 10:39:09');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`producto`);

--
-- Indexes for table `ventas`
--
ALTER TABLE `ventas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fkventasproductos` (`producto`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ventas`
--
ALTER TABLE `ventas`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `ventas`
--
ALTER TABLE `ventas`
  ADD CONSTRAINT `fkventasproductos` FOREIGN KEY (`producto`) REFERENCES `productos` (`producto`) ON DELETE RESTRICT ON UPDATE RESTRICT;
--
-- Database: `ejemplofk`
--
CREATE DATABASE IF NOT EXISTS `ejemplofk` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci;
USE `ejemplofk`;

-- --------------------------------------------------------

--
-- Table structure for table `ciudades`
--

CREATE TABLE `ciudades` (
  `ciudad_id` int NOT NULL,
  `ciudad_nom` varchar(50) DEFAULT NULL,
  `provincia` varchar(50) NOT NULL DEFAULT 'Cantabria'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `ciudades`
--

INSERT INTO `ciudades` (`ciudad_id`, `ciudad_nom`, `provincia`) VALUES
(3, 'Laredo', 'Cantabria'),
(2, 'Potes', 'cantabria'),
(4, 'Reinosa', 'Cantabria'),
(1, 'santander', 'cantabria'),
(5, 'Torrelavega', 'Cantabria');

-- --------------------------------------------------------

--
-- Table structure for table `personas`
--

CREATE TABLE `personas` (
  `persona_id` int NOT NULL,
  `persona_nom` varchar(50) NOT NULL,
  `ciudad_id` int DEFAULT '2',
  `correo` varchar(50) DEFAULT NULL
) ;

--
-- Dumping data for table `personas`
--

INSERT INTO `personas` (`persona_id`, `persona_nom`, `ciudad_id`, `correo`) VALUES
(1, 'Jose', 1, NULL),
(2, 'Ana', 1, NULL),
(3, 'silvia', 2, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ciudades`
--
ALTER TABLE `ciudades`
  ADD PRIMARY KEY (`ciudad_id`),
  ADD KEY `indice1` (`ciudad_nom`,`provincia`);

--
-- Indexes for table `personas`
--
ALTER TABLE `personas`
  ADD PRIMARY KEY (`persona_id`),
  ADD UNIQUE KEY `uk1` (`correo`),
  ADD KEY `indice1` (`persona_nom`),
  ADD KEY `fkPersonasCiudades` (`ciudad_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ciudades`
--
ALTER TABLE `ciudades`
  MODIFY `ciudad_id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `personas`
--
ALTER TABLE `personas`
  MODIFY `persona_id` int NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `personas`
--
ALTER TABLE `personas`
  ADD CONSTRAINT `fkPersonasCiudades` FOREIGN KEY (`ciudad_id`) REFERENCES `ciudades` (`ciudad_id`) ON DELETE SET;
--
-- Database: `ejemploprogramacion`
--
CREATE DATABASE IF NOT EXISTS `ejemploprogramacion` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci;
USE `ejemploprogramacion`;

-- --------------------------------------------------------

--
-- Table structure for table `numeros`
--

CREATE TABLE `numeros` (
  `id` int NOT NULL,
  `suma` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `numeros`
--

INSERT INTO `numeros` (`id`, `suma`) VALUES
(1, 1),
(2, 3),
(3, 6),
(4, 10),
(5, 15);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `numeros`
--
ALTER TABLE `numeros`
  ADD PRIMARY KEY (`id`);
--
-- Database: `ejemploshospital`
--
CREATE DATABASE IF NOT EXISTS `ejemploshospital` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci;
USE `ejemploshospital`;

-- --------------------------------------------------------

--
-- Table structure for table `personas`
--

CREATE TABLE `personas` (
  `cod_hospital` int DEFAULT NULL,
  `dni` int NOT NULL,
  `apellidos` varchar(50) DEFAULT NULL,
  `funcion` varchar(30) DEFAULT NULL,
  `salario` int DEFAULT NULL,
  `localidad` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `personas`
--

INSERT INTO `personas` (`cod_hospital`, `dni`, `apellidos`, `funcion`, `salario`, `localidad`) VALUES
(1, 12345678, 'García Hernández, Eladio', 'CONSERJE', 1200, 'LORCA'),
(4, 22233311, 'Martínez Molina, Gloria', 'MEDICO', 1600, 'MURCIA'),
(2, 22233322, 'Tristán García, Ana', 'MEDICO', 1900, 'MURCIA'),
(2, 22233333, 'Martínez Molina, Andrés', 'MEDICO', 1600, 'CARTAGENA'),
(4, 33222111, 'Mesa del Castillo, Juan', 'MEDICO', 2200, 'LORCA'),
(3, 55544411, 'Ruiz Hernández, Caridad', 'MEDICO', 1900, 'LORCA'),
(4, 55544412, 'Jiménez Jiménez, Dolores', 'CONSERJE', 1200, 'MURCIA'),
(2, 55544433, 'González Marín, Alicia', 'CONSERJE', 1200, 'MURCIA'),
(1, 66655544, 'Castillo Montes, Pedro', 'MEDICO', 1700, 'MURCIA'),
(1, 87654321, 'Fuentes Bermejo, Carlos', 'DIRECTOR', 2000, 'MURCIA'),
(3, 99988333, 'Serrano Díaz, Alejandro', 'DIRECTOR', 2400, 'CARTAGENA');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `personas`
--
ALTER TABLE `personas`
  ADD PRIMARY KEY (`dni`);
--
-- Database: `empleados`
--
CREATE DATABASE IF NOT EXISTS `empleados` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci;
USE `empleados`;

-- --------------------------------------------------------

--
-- Table structure for table `empleados`
--

CREATE TABLE `empleados` (
  `Nro` int NOT NULL,
  `Nombre` varchar(25) DEFAULT NULL,
  `Fecha` datetime DEFAULT NULL,
  `Barrio` varchar(12) DEFAULT NULL,
  `TipoTrabajo` varchar(12) DEFAULT NULL,
  `HorasTrabajadas` int NOT NULL,
  `DiasTrabajados` int NOT NULL,
  `oficios` varchar(25) DEFAULT NULL,
  `salario` float DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `empleados`
--

INSERT INTO `empleados` (`Nro`, `Nombre`, `Fecha`, `Barrio`, `TipoTrabajo`, `HorasTrabajadas`, `DiasTrabajados`, `oficios`, `salario`) VALUES
(4, 'SUÁREZ LUIS', '1956-08-10 00:00:00', 'Cordon', 'Jornalero', 276, 0, 'Chofer', 6900),
(7, 'PÉREZ EMA', '1961-04-09 00:00:00', 'Aguada', 'Efectivo', 0, 22, 'Azafata', 5500),
(14, 'PEREIRA ANA', '1979-06-04 00:00:00', 'Aguada', 'Efectivo', 0, 21, 'Administración', 5250),
(20, 'GUTIÉRREZ ADOLFO', '1965-07-14 00:00:00', 'Aguada', 'Jornalero', 128, 0, 'Chofer', 3200),
(26, 'LÓPEZ SOFÍA', '1966-12-21 00:00:00', 'Unión', 'Jornalero', 490, 0, 'Azafata', 12250),
(31, 'ALFONSO RODRIGO', '1982-08-11 00:00:00', 'Cordon', 'Efectivo', 0, 22, 'Chofer', 5500),
(32, 'PEÑA BEATRIZ', '1974-10-07 00:00:00', 'Unión', 'Efectivo', 0, 22, 'Ventas', 5500),
(33, 'ACOSTA PEDRO', '1978-07-30 00:00:00', 'Unión', 'Efectivo', 0, 18, 'Chofer', 4500),
(38, 'COSTELA MARTHA', '1959-05-05 00:00:00', 'Cordon', 'Jornalero', 260, 0, 'Azafata', 6500),
(40, 'PERERA LEONCIO', '1970-09-18 00:00:00', 'Unión', 'Efectivo', 0, 22, 'Administración', 5500),
(43, 'BACCIO DANIEL', '1976-08-13 00:00:00', 'Blanqueada', 'Jornalero', 168, 0, 'Chofer', 4200),
(46, 'LÓPEZ LORENA', '1962-12-08 00:00:00', 'Unión', 'Efectivo', 0, 22, 'Chofer', 5500),
(47, 'HERNÁNDEZ JULIO', '1969-07-19 00:00:00', 'Blanqueada', 'Efectivo', 0, 16, 'Ventas', 4000),
(52, 'GARCÍA PEDRO', '1949-08-05 00:00:00', 'Blanqueada', 'Efectivo', 0, 18, 'Taller', 4500),
(57, 'COSTA JULIAN', '1959-01-26 00:00:00', 'Blanqueada', 'Jornalero', 450, 0, 'Chofer', 11250),
(59, 'MARTÍNEZ SOÑA', '1969-02-28 00:00:00', 'Aguada', 'Efectivo', 0, 26, 'Administración', 6500),
(61, 'GARCÍA PABLO', '1977-01-20 00:00:00', 'Unión', 'Efectivo', 0, 15, 'Taller', 3750),
(69, 'FERREIRA FEDERICO', '1968-09-05 00:00:00', 'Aguada', 'Efectivo', 0, 20, 'Chofer', 5000),
(72, 'VICENTE MARIA', '1973-05-09 00:00:00', 'Blanqueada', 'Jornalero', 120, 0, 'Azafata', 3000),
(78, 'PEÑA IRMA', '1980-09-03 00:00:00', 'Unión', 'Efectivo', 0, 22, 'Ventas', 5500),
(81, 'GALO RODOLFO', '1968-11-10 00:00:00', 'Unión', 'Efectivo', 0, 22, 'Chofer', 5500),
(84, 'HERRERA SANTIAGO', '1960-08-06 00:00:00', 'Centro', 'Jornalero', 176, 0, 'Chofer', 4400),
(85, 'GIMÉNEZ ELBIO', '1978-12-27 00:00:00', 'Unión', 'Jornalero', 168, 0, 'Chofer', 4200),
(86, 'BRESIA JULIA', '1982-01-06 00:00:00', 'Blanqueada', 'Efectivo', 0, 19, 'Administración', 4750),
(95, 'BARCIA JOUAQUIN', '1976-06-04 00:00:00', 'Blanqueada', 'Efectivo', 0, 10, 'Chofer', 2500),
(98, 'PALACIOS ESTELA', '1969-05-25 00:00:00', 'Unión', 'Efectivo', 0, 21, 'Azafata', 5250),
(105, 'RODAS LEONARDO', '1975-04-18 00:00:00', 'Centro', 'Jornalero', 265, 0, 'Chofer', 6625),
(113, 'ANGELUCCI FABRICIO', '1981-12-30 00:00:00', 'Blanqueada', 'Efectivo', 0, 22, 'Administración', 5500),
(123, 'DE LOS SANTOS JACINTO', '1945-04-16 00:00:00', 'Aguada', 'Efectivo', 0, 22, 'Chofer', 5500),
(999, 'RAMON', NULL, NULL, 'Jornalero', 20, 0, NULL, 0);

--
-- Triggers `empleados`
--
DELIMITER $$
CREATE TRIGGER `biempleados` BEFORE INSERT ON `empleados` FOR EACH ROW BEGIN
  SET NEW.Nombre=UCASE(NEW.nombre);
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `buempleados` BEFORE UPDATE ON `empleados` FOR EACH ROW BEGIN
  SET NEW.Nombre=UCASE(NEW.nombre);
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `importes`
--

CREATE TABLE `importes` (
  `tipo` varchar(30) NOT NULL,
  `valor` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `importes`
--

INSERT INTO `importes` (`tipo`, `valor`) VALUES
('efectivo', 250),
('jornalero', 25);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `empleados`
--
ALTER TABLE `empleados`
  ADD PRIMARY KEY (`Nro`);

--
-- Indexes for table `importes`
--
ALTER TABLE `importes`
  ADD PRIMARY KEY (`tipo`);
--
-- Database: `empleados2023`
--
CREATE DATABASE IF NOT EXISTS `empleados2023` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci;
USE `empleados2023`;

-- --------------------------------------------------------

--
-- Table structure for table `departamento`
--

CREATE TABLE `departamento` (
  `id` int UNSIGNED NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `presupuesto` double UNSIGNED NOT NULL,
  `gastos` double UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `departamento`
--

INSERT INTO `departamento` (`id`, `nombre`, `presupuesto`, `gastos`) VALUES
(1, 'Desarrollo', 120000, 6000),
(2, 'Sistemas', 150000, 21000),
(3, 'Recursos Humanos', 280000, 25000),
(4, 'Contabilidad', 110000, 3000),
(5, 'I+D', 375000, 380000),
(6, 'Proyectos', 0, 0),
(7, 'Publicidad', 0, 1000);

-- --------------------------------------------------------

--
-- Table structure for table `empleado`
--

CREATE TABLE `empleado` (
  `id` int UNSIGNED NOT NULL,
  `nif` varchar(9) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `apellido1` varchar(100) NOT NULL,
  `apellido2` varchar(100) DEFAULT NULL,
  `id_departamento` int UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `empleado`
--

INSERT INTO `empleado` (`id`, `nif`, `nombre`, `apellido1`, `apellido2`, `id_departamento`) VALUES
(1, '32481596F', 'Aarón', 'Rivero', 'Gómez', 1),
(2, 'Y5575632D', 'Adela', 'Salas', 'Díaz', 2),
(3, 'R6970642B', 'Adolfo', 'Rubio', 'Flores', 3),
(4, '77705545E', 'Adrián', 'Suárez', NULL, 4),
(5, '17087203C', 'Marcos', 'Loyola', 'Méndez', 5),
(6, '38382980M', 'María', 'Santana', 'Moreno', 1),
(7, '80576669X', 'Pilar', 'Ruiz', NULL, 2),
(8, '71651431Z', 'Pepe', 'Ruiz', 'Santana', 3),
(9, '56399183D', 'Juan', 'Gómez', 'López', 2),
(10, '46384486H', 'Diego', 'Flores', 'Salas', 5),
(11, '67389283A', 'Marta', 'Herrera', 'Gil', 1),
(12, '41234836R', 'Irene', 'Salas', 'Flores', NULL),
(13, '82635162B', 'Juan Antonio', 'Sáez', 'Guerrero', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `departamento`
--
ALTER TABLE `departamento`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `empleado`
--
ALTER TABLE `empleado`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nif` (`nif`),
  ADD KEY `id_departamento` (`id_departamento`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `departamento`
--
ALTER TABLE `departamento`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `empleado`
--
ALTER TABLE `empleado`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `empleado`
--
ALTER TABLE `empleado`
  ADD CONSTRAINT `empleado_ibfk_1` FOREIGN KEY (`id_departamento`) REFERENCES `departamento` (`id`);
--
-- Database: `empleadosalpe`
--
CREATE DATABASE IF NOT EXISTS `empleadosalpe` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci;
USE `empleadosalpe`;

-- --------------------------------------------------------

--
-- Table structure for table `empleados`
--

CREATE TABLE `empleados` (
  `id` int UNSIGNED NOT NULL,
  `nombre` varchar(30) NOT NULL,
  `apellidos` varchar(30) NOT NULL,
  `edad` int NOT NULL,
  `poblacion` varchar(30) NOT NULL,
  `codigoPostal` varchar(5) NOT NULL,
  `fechaNacimiento` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `empleados`
--

INSERT INTO `empleados` (`id`, `nombre`, `apellidos`, `edad`, `poblacion`, `codigoPostal`, `fechaNacimiento`) VALUES
(2, 'eva', 'lopez', 40, 'Santander', '39009', '1999-02-01'),
(3, 'ramon', 'abramo', 0, 'Potes', '39005', '1980-01-01'),
(4, 'eva', 'lopez', 40, 'Santander', '39009', '1999-02-01'),
(5, 'eva', 'lopez', 40, 'Santander', '39009', '1999-02-01'),
(6, 'eva', 'lopez', 40, 'Santander', '39009', '1999-02-01'),
(7, 'eva', 'lopez', 40, 'Santander', '39009', '1999-02-01'),
(8, 'eva', 'lopez', 40, 'Santander', '39009', '1999-02-01'),
(9, 'eva', 'lopez', 40, 'Santander', '39009', '1999-02-01'),
(10, 'eva', 'lopez', 40, 'Santander', '39009', '1999-02-01'),
(11, 'eva', 'lopez', 40, 'Santander', '39009', '1999-02-01'),
(12, 'eva', 'lopez', 40, 'Santander', '39009', '1999-02-01'),
(13, 'eva', 'lopez', 40, 'Santander', '39009', '1999-02-01');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `empleados`
--
ALTER TABLE `empleados`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `empleados`
--
ALTER TABLE `empleados`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- Database: `empleadosdepartamentos`
--
CREATE DATABASE IF NOT EXISTS `empleadosdepartamentos` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci;
USE `empleadosdepartamentos`;

-- --------------------------------------------------------

--
-- Table structure for table `depart`
--

CREATE TABLE `depart` (
  `dept_no` int NOT NULL,
  `dnombre` varchar(30) DEFAULT NULL,
  `loc` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `depart`
--

INSERT INTO `depart` (`dept_no`, `dnombre`, `loc`) VALUES
(10, 'CONTABILIDAD', 'SEVILLA'),
(20, 'INVESTIGACIÓN', 'MADRID'),
(30, 'VENTAS', 'BARCELONA'),
(40, 'PRODUCCIÓN', 'BILBAO');

-- --------------------------------------------------------

--
-- Table structure for table `emple`
--

CREATE TABLE `emple` (
  `emp_no` int NOT NULL,
  `apellido` varchar(50) NOT NULL,
  `oficio` varchar(30) DEFAULT NULL,
  `dir` int DEFAULT NULL,
  `fecha_alt` date DEFAULT NULL,
  `salario` int DEFAULT NULL,
  `comision` int DEFAULT NULL,
  `dept_no` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `emple`
--

INSERT INTO `emple` (`emp_no`, `apellido`, `oficio`, `dir`, `fecha_alt`, `salario`, `comision`, `dept_no`) VALUES
(7369, 'SÁNCHEZ', 'EMPLEADO', 7902, '1990-12-17', 1040, NULL, 20),
(7499, 'ARROYO', 'VENDEDOR', 7698, '1990-02-20', 1500, 390, 30),
(7521, 'SALA', 'VENDEDOR', 7698, '1991-02-22', 1625, 650, 30),
(7566, 'JIMÉNEZ', 'DIRECTOR', 7839, '1991-04-02', 2900, NULL, 20),
(7654, 'MARTÍN', 'VENDEDOR', 7698, '1991-09-29', 1600, 1020, 30),
(7698, 'NEGRO', 'DIRECTOR', 7839, '1991-05-01', 3005, NULL, 30),
(7782, 'CEREZO', 'DIRECTOR', 7839, '1991-06-09', 2885, NULL, 10),
(7788, 'GIL', 'ANALISTA', 7566, '1991-11-09', 3000, NULL, 20),
(7839, 'REY', 'PRESIDENTE', NULL, '1991-11-17', 4100, NULL, 10),
(7844, 'TOVAR', 'VENDEDOR', 7698, '1991-09-08', 1350, 0, 30),
(7876, 'ALONSO', 'EMPLEADO', 7788, '1991-09-23', 1430, NULL, 20),
(7900, 'JIMENO', 'EMPLEADO', 7698, '1991-12-03', 1335, NULL, 30),
(7902, 'FERNÁNDEZ', 'ANALISTA', 7566, '1991-12-03', 3000, NULL, 20),
(7934, 'MUÑOZ', 'EMPLEADO', 7782, '1992-01-23', 1690, NULL, 10);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `depart`
--
ALTER TABLE `depart`
  ADD PRIMARY KEY (`dept_no`);

--
-- Indexes for table `emple`
--
ALTER TABLE `emple`
  ADD PRIMARY KEY (`emp_no`),
  ADD KEY `FK_emple_depart_dept_no` (`dept_no`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `emple`
--
ALTER TABLE `emple`
  ADD CONSTRAINT `FK_emple_depart_dept_no` FOREIGN KEY (`dept_no`) REFERENCES `depart` (`dept_no`);
--
-- Database: `empresas`
--
CREATE DATABASE IF NOT EXISTS `empresas` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `empresas`;

-- --------------------------------------------------------

--
-- Table structure for table `empresas`
--

CREATE TABLE `empresas` (
  `codigo` int NOT NULL,
  `nombre` varchar(50) DEFAULT NULL,
  `poblacion` varchar(50) DEFAULT NULL,
  `cp` varchar(5) DEFAULT NULL,
  `trabajadores` int DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=98 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `empresas`
--

INSERT INTO `empresas` (`codigo`, `nombre`, `poblacion`, `cp`, `trabajadores`) VALUES
(1, 'WorldWide Optics Corporation', 'Liencres', '39006', 3),
(2, 'Advanced Research Corp.', 'Isla', NULL, 10),
(3, 'National Space Explore Co.', 'Corrales', '39049', 7),
(4, 'Smart Mining Corporation', 'Santander', '39011', 5),
(5, 'Global High-Technologies Corp.', 'Torrelavega', '39004', 10),
(6, 'First 2G Wireless Corporation', 'Santoña', NULL, NULL),
(7, 'Future Industry Corporation', 'Noja', '39038', 6),
(8, 'West I-Mobile Group', 'Arnuero', '39056', 2),
(9, NULL, 'Loredo', '39049', 7),
(10, 'United Logics Group', 'Laredo', '39025', 1),
(11, 'Home Space Research Inc.', 'Potes', '39082', 1),
(12, 'Canadian Space Research Group', 'Miengo', '39022', 4),
(13, 'Australian W-Mobile Inc.', 'Isla', '39000', 5),
(14, 'East High-Technologies Inc.', 'Torrelavega', '39044', 4),
(15, 'General High-Technologies Group', 'Santander', '39097', 9),
(16, 'Creative Fossil Fuel Power Group', 'Miengo', '39053', 7),
(17, 'Creative Laboratories Corp.', 'Corrales', '39022', 7),
(18, 'Domestic J-Mobile Inc.', 'Liencres', '39033', 7),
(19, 'City Q-Mobile Group', 'Noja', '39050', 1),
(20, NULL, 'Santoña', '39084', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `personas`
--

CREATE TABLE `personas` (
  `id` int NOT NULL,
  `nombre` varchar(50) DEFAULT NULL,
  `apellidos` varchar(50) DEFAULT NULL,
  `poblacion` varchar(50) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `dato1` int DEFAULT NULL,
  `dato2` int DEFAULT NULL,
  `dato3` int DEFAULT NULL,
  `fechaTrabaja` date DEFAULT NULL,
  `empresa` int DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=98 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `personas`
--

INSERT INTO `personas` (`id`, `nombre`, `apellidos`, `poblacion`, `fecha`, `dato1`, `dato2`, `dato3`, `fechaTrabaja`, `empresa`) VALUES
(1, 'Daniel', 'Loma', 'Liencres', '1990-09-16', 5, 6, 6, '2014-10-25', 13),
(2, 'Miguel', 'Gil', 'Arnuero', '1999-12-18', 6, 6, 3, '2017-09-28', 3),
(3, 'Ramon', 'Fernandez', 'Corrales', '1986-05-26', 10, 8, 0, '2000-03-31', 18),
(4, 'Chelo', 'Moreno', 'Noja', '1996-11-11', 8, 9, 6, '2015-09-28', 2),
(5, 'Miguel', 'Garcia', 'Santoña', '1988-11-13', 2, 1, 7, '2011-11-26', 20),
(6, 'Asun', 'Diaz', 'Loredo', '1992-11-05', 4, 10, 1, '2007-12-28', 16),
(7, 'Roberto', 'Vazquez', 'Santander', '1986-10-29', 4, 5, 4, '2005-06-01', 10),
(8, 'Joaquin', 'Castro', 'Torrelavega', '1996-03-24', 6, 8, 5, '2016-02-05', 8),
(9, 'Guillermo', 'Torres', 'Laredo', '1987-07-01', 8, 8, 6, '2007-06-25', 9),
(10, 'Sergio', 'Muñoz', 'Potes', '1986-11-04', 4, 1, 0, '2006-02-13', 7),
(11, 'Rosa', 'Fresno', 'Miengo', '1995-02-07', 5, 8, 0, '2012-07-27', 9),
(12, 'Pedro', 'Rodriguez', 'Isla', '1995-05-16', 0, 3, 5, '2016-05-03', 3),
(13, 'Ana', 'Perez', 'Arnuero', '1988-04-06', 6, 0, 6, '2005-01-24', 7),
(14, 'Alberto', 'Molina', 'Torrelavega', '1986-01-20', NULL, 3, 1, '2006-03-26', NULL),
(15, 'Marta', 'Ramirez', 'Miengo', '1990-03-31', 5, 5, 9, '2007-09-14', 20),
(16, 'Carmen', 'Gonzalez', 'Santander', '1990-10-06', 7, 4, 1, '2012-11-19', 9),
(17, 'Ramon', 'Gutierrez', 'Isla', '1987-06-10', 8, 2, 1, '2007-03-28', 2),
(18, 'Daniel', 'Morales', 'Laredo', '1994-04-13', 10, 7, 0, '2015-12-12', 14),
(19, 'Miguel', 'Sanchez', 'Corrales', '1989-11-22', 7, NULL, NULL, NULL, 19),
(20, 'Miguel', 'Lopez', 'Liencres', '1993-06-23', NULL, NULL, NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `empresas`
--
ALTER TABLE `empresas`
  ADD PRIMARY KEY (`codigo`);

--
-- Indexes for table `personas`
--
ALTER TABLE `personas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKPersonasEmpresas` (`empresa`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `empresas`
--
ALTER TABLE `empresas`
  MODIFY `codigo` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `personas`
--
ALTER TABLE `personas`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `personas`
--
ALTER TABLE `personas`
  ADD CONSTRAINT `FKPersonasEmpresas` FOREIGN KEY (`empresa`) REFERENCES `empresas` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE;
--
-- Database: `errores`
--
CREATE DATABASE IF NOT EXISTS `errores` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci;
USE `errores`;

-- --------------------------------------------------------

--
-- Table structure for table `numeros`
--

CREATE TABLE `numeros` (
  `id` int NOT NULL,
  `valor` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `numeros`
--

INSERT INTO `numeros` (`id`, `valor`) VALUES
(1, 2),
(2, 4),
(3, 7),
(4, 20),
(5, 13);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `numeros`
--
ALTER TABLE `numeros`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `numeros`
--
ALTER TABLE `numeros`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- Database: `eventos2023`
--
CREATE DATABASE IF NOT EXISTS `eventos2023` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci;
USE `eventos2023`;

-- --------------------------------------------------------

--
-- Table structure for table `evento`
--

CREATE TABLE `evento` (
  `id` int NOT NULL,
  `codigoReserva` varchar(100) NOT NULL,
  `nombre` varchar(300) DEFAULT NULL,
  `descripcion` text,
  `fechaHora` datetime DEFAULT NULL,
  `fechaRelease` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sala`
--

CREATE TABLE `sala` (
  `id` int NOT NULL,
  `nombre` varchar(400) DEFAULT NULL,
  `descripcion` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tienelugar`
--

CREATE TABLE `tienelugar` (
  `id` int NOT NULL,
  `idEvento` int DEFAULT NULL,
  `idSala` int DEFAULT NULL,
  `montajeSala` varchar(400) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `evento`
--
ALTER TABLE `evento`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `codigoReserva` (`codigoReserva`);

--
-- Indexes for table `sala`
--
ALTER TABLE `sala`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tienelugar`
--
ALTER TABLE `tienelugar`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `idEvento` (`idEvento`,`idSala`),
  ADD KEY `fkTieneSala` (`idSala`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tienelugar`
--
ALTER TABLE `tienelugar`
  ADD CONSTRAINT `fkTieneEvento` FOREIGN KEY (`idEvento`) REFERENCES `evento` (`id`),
  ADD CONSTRAINT `fkTieneSala` FOREIGN KEY (`idSala`) REFERENCES `sala` (`id`);
--
-- Database: `excepciones`
--
CREATE DATABASE IF NOT EXISTS `excepciones` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci;
USE `excepciones`;

-- --------------------------------------------------------

--
-- Table structure for table `errores`
--

CREATE TABLE `errores` (
  `id` int NOT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  `fecha` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `errores`
--

INSERT INTO `errores` (`id`, `nombre`, `fecha`) VALUES
(1, 'error 1', '2023-08-03 15:36:29'),
(2, 'error 1', '2023-08-03 15:37:00'),
(3, 'error 1', '2023-08-03 15:37:03'),
(4, 'error 1', '2023-08-03 15:38:00'),
(5, 'error 1', '2023-08-03 15:38:05'),
(6, 'error 1', '2023-08-03 15:38:34'),
(7, 'error en el procedimiento errores 2', '2023-08-03 15:48:11'),
(8, 'error en el procedimiento errores 2', '2023-08-03 15:51:28'),
(9, 'error en el procedimiento errores 3', '2023-08-03 16:00:54'),
(10, 'error en el procedimiento errores 3', '2023-08-03 16:01:27'),
(11, 'no puedo introducir registros en la tabla numeros sin los campos requeridos', '2023-08-03 16:18:11'),
(12, 'no puedo introducir registros en la tabla numeros sin los campos requeridos', '2023-08-03 16:25:03'),
(13, 'no puedo introducir registros en la tabla numeros sin los campos requeridos', '2023-08-03 16:27:49'),
(14, 'no puedo introducir registros en la tabla numeros sin los campos requeridos', '2023-08-03 16:27:49'),
(15, 'no podemos meter datos repetidos en la tabla numeros', '2023-08-03 16:45:13'),
(16, 'no podemos meter datos repetidos en la tabla numeros', '2023-08-03 16:47:22'),
(17, 'no puedo introducir registros en la tabla numeros sin los campos requeridos', '2023-08-03 16:47:22'),
(18, 'no podemos meter datos repetidos en la tabla numeros', '2023-08-03 16:53:37'),
(19, 'no puedo introducir registros en la tabla numeros sin los campos requeridos', '2023-08-03 16:53:37'),
(20, 'no podemos meter datos repetidos en la tabla numeros', '2023-08-03 17:01:04');

-- --------------------------------------------------------

--
-- Table structure for table `numeros`
--

CREATE TABLE `numeros` (
  `id` int NOT NULL,
  `valor` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `numeros`
--

INSERT INTO `numeros` (`id`, `valor`) VALUES
(1, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `errores`
--
ALTER TABLE `errores`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `numeros`
--
ALTER TABLE `numeros`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `valor` (`valor`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `errores`
--
ALTER TABLE `errores`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- Database: `facturas`
--
CREATE DATABASE IF NOT EXISTS `facturas` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci;
USE `facturas`;

-- --------------------------------------------------------

--
-- Table structure for table `factura`
--

CREATE TABLE `factura` (
  `id` int NOT NULL,
  `cliente` varchar(100) DEFAULT NULL,
  `telefono` varchar(100) DEFAULT NULL,
  `correo` varchar(100) DEFAULT NULL,
  `total` float DEFAULT NULL,
  `iva` float DEFAULT NULL,
  `totalIva` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `factura`
--

INSERT INTO `factura` (`id`, `cliente`, `telefono`, `correo`, `total`, `iva`, `totalIva`) VALUES
(1, 'jose', '123123123', 'jose@alpe.es', 20, 2, 22),
(2, 'eva', '345345345', 'eva@alpe.es', 10, 2, 12),
(3, 'sonia', '123123123', 'sonia@alpe.es', 25, 7.5, 32.5),
(4, 'joselito', '123123123', 'joselito@alpe.es', 300, 20, 320);

-- --------------------------------------------------------

--
-- Table structure for table `grupos`
--

CREATE TABLE `grupos` (
  `id` int NOT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  `iva` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `grupos`
--

INSERT INTO `grupos` (`id`, `nombre`, `iva`) VALUES
(1, 'verduras', 0.1),
(2, 'frutas', 0.2),
(3, 'carnes', 0.3),
(4, 'tuberculos', 0.05);

-- --------------------------------------------------------

--
-- Table structure for table `lineas`
--

CREATE TABLE `lineas` (
  `id` int NOT NULL,
  `producto` int DEFAULT NULL,
  `nombreProducto` varchar(100) DEFAULT NULL,
  `precio` float DEFAULT NULL,
  `cantidad` int DEFAULT NULL,
  `total` float DEFAULT NULL,
  `factura` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `lineas`
--

INSERT INTO `lineas` (`id`, `producto`, `nombreProducto`, `precio`, `cantidad`, `total`, `factura`) VALUES
(1, 1, 'lechuga', 1, 10, 10, 1),
(2, 2, 'patatas', 2, 5, 10, 1),
(3, 4, 'melocotones', 1, 10, 10, 2),
(4, 3, 'filete ternera', 5, 5, 25, 3),
(5, 1, 'lechuga', 1, 100, 100, 4),
(6, 2, 'patatas', 2, 100, 200, 4);

--
-- Triggers `lineas`
--
DELIMITER $$
CREATE TRIGGER `lineasAD` AFTER DELETE ON `lineas` FOR EACH ROW BEGIN
  CALL actualizarFactura(OLD.factura);
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `lineasAI` AFTER INSERT ON `lineas` FOR EACH ROW BEGIN
  CALL actualizarFactura(NEW.factura);
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `lineasAU` AFTER UPDATE ON `lineas` FOR EACH ROW BEGIN
  CALL actualizarFactura(NEW.factura);

  IF (NEW.factura<>OLD.factura) THEN
    CALL actualizarFactura(OLD.factura);
  END IF;
  
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `lineasBI` BEFORE INSERT ON `lineas` FOR EACH ROW BEGIN
  -- variables
  DECLARE vNombre varchar(100) DEFAULT NULL;
  DECLARE vPrecio float DEFAULT 0;
  DECLARE vTotal float DEFAULT 0;

  -- leo los valores
  SELECT 
      p.nombre,p.precio INTO vNombre,vPrecio 
    FROM productos p 
    WHERE id=NEw.producto;

  -- asigno los valores a los campos
  SET 
    NEW.nombreProducto=vNombre,
    NEW.precio=vPrecio,
    NEW.total=NEW.cantidad*vPrecio;
  
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `lineasBU` BEFORE UPDATE ON `lineas` FOR EACH ROW BEGIN
  SET NEW.nombreProducto=(SELECT p.nombre FROM productos p WHERE id=NEW.producto);
  SET NEW.precio=(SELECT p.precio FROM productos p WHERE id=NEW.producto);
  SET NEW.total=NEW.cantidad*NEW.precio;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `productos`
--

CREATE TABLE `productos` (
  `id` int NOT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  `precio` float DEFAULT NULL,
  `grupo` int DEFAULT NULL,
  `nombreGrupo` varchar(100) DEFAULT NULL,
  `ivaGrupo` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `productos`
--

INSERT INTO `productos` (`id`, `nombre`, `precio`, `grupo`, `nombreGrupo`, `ivaGrupo`) VALUES
(1, 'lechuga', 1, 1, 'verduras', 0.1),
(2, 'patatas', 2, 4, 'tuberculos', 0.05),
(3, 'filete ternera', 5, 3, 'carnes', 0.3),
(4, 'melocotones', 1, 2, 'frutas', 0.2),
(5, 'platanos', 1, 2, 'frutas', 0.2),
(6, 'mandarinas', 0.5, 2, 'frutas', 0.2);

--
-- Triggers `productos`
--
DELIMITER $$
CREATE TRIGGER `productosBI` BEFORE INSERT ON `productos` FOR EACH ROW BEGIN
  -- variables
  DECLARE vNombre varchar(100) DEFAULT NULL;
  DECLARE vIva float DEFAULT 0;
  
  -- leo los valores
  SELECT 
      nombre,iva INTO vNombre,vIva 
    FROM grupos WHERE id=NEW.grupo;

  -- los almaceno en los campos
  SET NEW.nombreGrupo=vNombre,NEW.ivaGrupo=vIva;

END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `productosBU` BEFORE UPDATE ON `productos` FOR EACH ROW BEGIN
   SET 
    NEW.nombreGrupo=(SELECT nombre FROM grupos WHERE id=NEW.grupo),
    NEW.ivaGrupo=(SELECT iva FROM grupos WHERE id=NEW.grupo);
END
$$
DELIMITER ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `factura`
--
ALTER TABLE `factura`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `grupos`
--
ALTER TABLE `grupos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lineas`
--
ALTER TABLE `lineas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fklineasFacturas` (`factura`),
  ADD KEY `fklineasProductos` (`producto`);

--
-- Indexes for table `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fkproductosGrupos` (`grupo`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `factura`
--
ALTER TABLE `factura`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `grupos`
--
ALTER TABLE `grupos`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `lineas`
--
ALTER TABLE `lineas`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `productos`
--
ALTER TABLE `productos`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `lineas`
--
ALTER TABLE `lineas`
  ADD CONSTRAINT `fklineasFacturas` FOREIGN KEY (`factura`) REFERENCES `factura` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fklineasProductos` FOREIGN KEY (`producto`) REFERENCES `productos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `productos`
--
ALTER TABLE `productos`
  ADD CONSTRAINT `fkproductosGrupos` FOREIGN KEY (`grupo`) REFERENCES `grupos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
--
-- Database: `formularios`
--
CREATE DATABASE IF NOT EXISTS `formularios` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci;
USE `formularios`;

-- --------------------------------------------------------

--
-- Table structure for table `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1686564886);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);
--
-- Database: `futbol2023`
--
CREATE DATABASE IF NOT EXISTS `futbol2023` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci;
USE `futbol2023`;

-- --------------------------------------------------------

--
-- Table structure for table `alquileres`
--

CREATE TABLE `alquileres` (
  `id` int NOT NULL,
  `idSocio` int DEFAULT NULL,
  `idCampo` int DEFAULT NULL,
  `fechaHora` timestamp NULL DEFAULT NULL,
  `horas` int DEFAULT NULL,
  `personas` int DEFAULT NULL,
  `precioTotal` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `campos`
--

CREATE TABLE `campos` (
  `id` int NOT NULL,
  `nombre` varchar(300) DEFAULT NULL,
  `aforo` int DEFAULT NULL,
  `precio` float DEFAULT NULL,
  `direccion` varchar(400) DEFAULT NULL,
  `telefono` varchar(20) DEFAULT NULL,
  `tipo` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `socios`
--

CREATE TABLE `socios` (
  `id` int NOT NULL,
  `nombre` varchar(200) DEFAULT NULL,
  `apellidos` varchar(400) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `telefono` varchar(20) DEFAULT NULL,
  `fechahora` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `password` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `alquileres`
--
ALTER TABLE `alquileres`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `idSocio` (`idSocio`,`idCampo`,`fechaHora`),
  ADD KEY `fkAlquileresCampos` (`idCampo`);

--
-- Indexes for table `campos`
--
ALTER TABLE `campos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `socios`
--
ALTER TABLE `socios`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `alquileres`
--
ALTER TABLE `alquileres`
  ADD CONSTRAINT `fkAlquileresCampos` FOREIGN KEY (`idCampo`) REFERENCES `campos` (`id`),
  ADD CONSTRAINT `fkAlquileresSocios` FOREIGN KEY (`idSocio`) REFERENCES `socios` (`id`);
--
-- Database: `laravel1`
--
CREATE DATABASE IF NOT EXISTS `laravel1` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci;
USE `laravel1`;
--
-- Database: `libros`
--
CREATE DATABASE IF NOT EXISTS `libros` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci;
USE `libros`;

-- --------------------------------------------------------

--
-- Table structure for table `autores`
--

CREATE TABLE `autores` (
  `idAutor` int NOT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  `poblacion` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `autores`
--

INSERT INTO `autores` (`idAutor`, `nombre`, `poblacion`) VALUES
(1, 'Autor 1', 'Santander'),
(2, 'Autor 22', 'Laredo'),
(3, 'jorge', 'santander'),
(4, 'ana', 'santander'),
(5, 'pepe', 'reinosa');

-- --------------------------------------------------------

--
-- Table structure for table `libros`
--

CREATE TABLE `libros` (
  `idLibro` int NOT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  `idAutor` int DEFAULT NULL,
  `paginas` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `libros`
--

INSERT INTO `libros` (`idLibro`, `nombre`, `idAutor`, `paginas`) VALUES
(1, 'Libro 1', 1, 200),
(2, 'Libro bonito', 2, 100),
(3, 'Libro feo', 3, 200),
(4, 'Libro mediano', 1, 800),
(5, 'Libro mediano', 2, 800);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `autores`
--
ALTER TABLE `autores`
  ADD PRIMARY KEY (`idAutor`);

--
-- Indexes for table `libros`
--
ALTER TABLE `libros`
  ADD PRIMARY KEY (`idLibro`),
  ADD KEY `FK_libros_autores_idAutor` (`idAutor`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `autores`
--
ALTER TABLE `autores`
  MODIFY `idAutor` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `libros`
--
ALTER TABLE `libros`
  MODIFY `idLibro` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `libros`
--
ALTER TABLE `libros`
  ADD CONSTRAINT `FK_libros_autores_idAutor` FOREIGN KEY (`idAutor`) REFERENCES `autores` (`idAutor`);
--
-- Database: `mvc`
--
CREATE DATABASE IF NOT EXISTS `mvc` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci;
USE `mvc`;

-- --------------------------------------------------------

--
-- Table structure for table `entradas`
--

CREATE TABLE `entradas` (
  `id` int NOT NULL,
  `titulo` varchar(255) COLLATE utf8mb3_spanish_ci DEFAULT NULL,
  `texto` varchar(255) COLLATE utf8mb3_spanish_ci DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=8192 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_spanish_ci;

--
-- Dumping data for table `entradas`
--

INSERT INTO `entradas` (`id`, `titulo`, `texto`) VALUES
(1, 'Entrada 1', 'Esta es la primera entrada'),
(2, 'Entrada 2', 'Segunda entrada');

-- --------------------------------------------------------

--
-- Table structure for table `noticias`
--

CREATE TABLE `noticias` (
  `id` int NOT NULL,
  `Titulo` varchar(255) COLLATE utf8mb3_spanish_ci DEFAULT NULL,
  `TextoCorto` varchar(255) COLLATE utf8mb3_spanish_ci DEFAULT NULL,
  `TextoLargo` varchar(255) COLLATE utf8mb3_spanish_ci DEFAULT NULL,
  `Fecha` date DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=5461 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_spanish_ci;

--
-- Dumping data for table `noticias`
--

INSERT INTO `noticias` (`id`, `Titulo`, `TextoCorto`, `TextoLargo`, `Fecha`) VALUES
(1, 'Noticia 1', NULL, 'Esta es la noticia que comienza la lista', '2017-12-13'),
(2, 'Noticia 2', NULL, 'Esta es la noticia segunda. No tiene mas que lo que ves', '2017-12-05'),
(3, 'titulo', '...', 'eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee', '0000-00-00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `entradas`
--
ALTER TABLE `entradas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `noticias`
--
ALTER TABLE `noticias`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `entradas`
--
ALTER TABLE `entradas`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `noticias`
--
ALTER TABLE `noticias`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Database: `mvc4`
--
CREATE DATABASE IF NOT EXISTS `mvc4` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `mvc4`;

-- --------------------------------------------------------

--
-- Table structure for table `entradas`
--

CREATE TABLE `entradas` (
  `id` int NOT NULL,
  `Titulo` varchar(255) DEFAULT NULL,
  `Texto` varchar(255) DEFAULT NULL,
  `Foto` varchar(255) DEFAULT NULL,
  `portada` bit(1) DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=16384 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `entradas`
--

INSERT INTO `entradas` (`id`, `Titulo`, `Texto`, `Foto`, `portada`) VALUES
(1, 'Estamos abiertos', 'Ya hemos comenzado a preparar ', 'entrada1.jpg', b'1');

-- --------------------------------------------------------

--
-- Table structure for table `noticias`
--

CREATE TABLE `noticias` (
  `id` int NOT NULL,
  `Titulo` varchar(255) DEFAULT NULL,
  `Texto` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=8192 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `noticias`
--

INSERT INTO `noticias` (`id`, `Titulo`, `Texto`) VALUES
(1, 'Noticia 1', 'Esta es la noticia 1'),
(2, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int NOT NULL,
  `Nombre` varchar(255) DEFAULT NULL,
  `Login` varchar(255) DEFAULT NULL,
  `Password` varchar(255) DEFAULT NULL,
  `direccion` varchar(255) DEFAULT NULL,
  `foto` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=16384 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `usuarios`
--

INSERT INTO `usuarios` (`id`, `Nombre`, `Login`, `Password`, `direccion`, `foto`) VALUES
(1, 'Ramon', 'RamonAbramo', 'facil', 'Ruiz de Alda', 'usuario1.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `entradas`
--
ALTER TABLE `entradas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `noticias`
--
ALTER TABLE `noticias`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `entradas`
--
ALTER TABLE `entradas`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `noticias`
--
ALTER TABLE `noticias`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Database: `noticias`
--
CREATE DATABASE IF NOT EXISTS `noticias` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `noticias`;

-- --------------------------------------------------------

--
-- Table structure for table `autores`
--

CREATE TABLE `autores` (
  `idAutor` int NOT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  `edad` int DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=1638 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `autores`
--

INSERT INTO `autores` (`idAutor`, `nombre`, `edad`) VALUES
(1, 'Alberto', 54),
(2, 'Marta', 46),
(3, 'Daniel', 48),
(4, 'Chelo', 34),
(5, 'Roberto', 64),
(6, 'Rosa', 45),
(7, 'Ana', 29),
(8, 'Ramon', 63),
(9, 'Joaquin', NULL),
(10, 'Carmen', 48);

-- --------------------------------------------------------

--
-- Table structure for table `noticias`
--

CREATE TABLE `noticias` (
  `idNoticia` int NOT NULL,
  `titulo` varchar(50) DEFAULT NULL,
  `idAutor` int DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=1638 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `noticias`
--

INSERT INTO `noticias` (`idNoticia`, `titulo`, `idAutor`) VALUES
(1, 'La ciudad de los puertos', 1),
(2, 'Otra pelicula de accion', NULL),
(3, 'Esta terminando el ocaso', 10),
(4, 'El mar de las mareas', NULL),
(5, 'El partido del siglo', 3),
(6, 'Uno mas de los nuestros', NULL),
(7, 'La nueva red social', 8),
(8, 'El pueblo de pescadores', 3),
(9, 'El virus del 2020', 2),
(10, 'La lucha continua', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `autores`
--
ALTER TABLE `autores`
  ADD PRIMARY KEY (`idAutor`);

--
-- Indexes for table `noticias`
--
ALTER TABLE `noticias`
  ADD PRIMARY KEY (`idNoticia`),
  ADD KEY `fknoticiasautores` (`idAutor`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `autores`
--
ALTER TABLE `autores`
  MODIFY `idAutor` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `noticias`
--
ALTER TABLE `noticias`
  MODIFY `idNoticia` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `noticias`
--
ALTER TABLE `noticias`
  ADD CONSTRAINT `fknoticiasautores` FOREIGN KEY (`idAutor`) REFERENCES `autores` (`idAutor`);
--
-- Database: `noticias2023`
--
CREATE DATABASE IF NOT EXISTS `noticias2023` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci;
USE `noticias2023`;

-- --------------------------------------------------------

--
-- Table structure for table `autor`
--

CREATE TABLE `autor` (
  `id` int NOT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  `foto` varchar(100) DEFAULT NULL,
  `fechaNacimiento` date DEFAULT NULL,
  `correo` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `autor`
--

INSERT INTO `autor` (`id`, `nombre`, `foto`, `fechaNacimiento`, `correo`) VALUES
(1, 'ramon', '1autor3.jpg', '2023-06-22', 'ramon@alpe.es'),
(2, 'daniel', '2anonimo.png', NULL, ''),
(9, 'daniel', '91312307.png', '2023-06-14', 'daniel@alpe.es'),
(18, 'Autor nuevo', '18autor1.jpg', '2023-06-09', 'autor1@alpe.es');

-- --------------------------------------------------------

--
-- Table structure for table `noticia`
--

CREATE TABLE `noticia` (
  `idNoticia` int NOT NULL,
  `titular` varchar(255) DEFAULT NULL,
  `textoCorto` varchar(800) DEFAULT NULL,
  `textoLargo` longtext,
  `portada` tinyint(1) DEFAULT NULL,
  `seccion` int DEFAULT NULL,
  `fecha` datetime DEFAULT NULL,
  `foto` varchar(255) DEFAULT NULL,
  `autor` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `noticia`
--

INSERT INTO `noticia` (`idNoticia`, `titular`, `textoCorto`, `textoLargo`, `portada`, `seccion`, `fecha`, `foto`, `autor`) VALUES
(1, 'Php', 'Php', 'php', 1, 2, '2023-06-22 04:02:00', '1lechugas.png', 1),
(2, 'no es importante', 'aa', 'aa', 0, 2, '2023-06-23 00:00:00', '2mandarina.png', 1),
(3, 'Las novedades del mvc', 'aaa', 'aaaaa', 1, 2, '2023-06-15 00:00:00', '3lechugas.png', 2),
(5, 'sin portada', 'asasdas', 'dasdasdasdasd', 0, 1, NULL, '5melon.png', 1),
(26, 'Noticia nueva', 'asdasdasa', 'asdasdasda', 1, 5, '2023-06-22 15:54:00', '26esparragos.png', 1),
(45, 'noticia nueva', 'dfdfsdf', 'sdfsdfs', 1, 1, '2023-06-16 16:37:00', '452023-03-21_12-35-28.png', 1),
(46, 'asdasd', 'asdasdas', 'asdasd', 1, 1, '2023-06-11 19:25:00', '461312307.png', 1),
(98, 'aasdfsdfs', 'fsdfsdfsdf', 'sdfsdfsd', 0, 1, NULL, '982023-03-21_12-35-28.png', 1);

-- --------------------------------------------------------

--
-- Table structure for table `seccion`
--

CREATE TABLE `seccion` (
  `id` int NOT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  `foto` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `seccion`
--

INSERT INTO `seccion` (`id`, `nombre`, `foto`) VALUES
(1, 'Deportes', '1melon.png'),
(2, 'Nacional', '21312307.png'),
(3, 'Cultura', '3esparragos.png'),
(5, 'Local', '5descarga.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `autor`
--
ALTER TABLE `autor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `noticia`
--
ALTER TABLE `noticia`
  ADD PRIMARY KEY (`idNoticia`),
  ADD KEY `fkNoticiaSeccion` (`seccion`),
  ADD KEY `fkNoticiaAutor` (`autor`);

--
-- Indexes for table `seccion`
--
ALTER TABLE `seccion`
  ADD PRIMARY KEY (`id`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `noticia`
--
ALTER TABLE `noticia`
  ADD CONSTRAINT `fkNoticiaAutor` FOREIGN KEY (`autor`) REFERENCES `autor` (`id`),
  ADD CONSTRAINT `fkNoticiaSeccion` FOREIGN KEY (`seccion`) REFERENCES `seccion` (`id`);
--
-- Database: `noticiasfotos2023`
--
CREATE DATABASE IF NOT EXISTS `noticiasfotos2023` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci;
USE `noticiasfotos2023`;

-- --------------------------------------------------------

--
-- Table structure for table `fotos`
--

CREATE TABLE `fotos` (
  `id` int NOT NULL,
  `nombre` varchar(200) DEFAULT NULL,
  `noticia` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `fotos`
--

INSERT INTO `fotos` (`id`, `nombre`, `noticia`) VALUES
(1, '1_94654691_hi037872495.jpg', 1),
(2, '2816jSd4e+fL.jpg', 1),
(3, '31312307.png', 2),
(4, '4descarga (2).jpg', 3);

-- --------------------------------------------------------

--
-- Table structure for table `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1688373012),
('m230629_212102_create_noticias_table', 1688373062),
('m230630_040253_create_fotos_table', 1688373062),
('m230701_134952_create_user_table', 1688373062);

-- --------------------------------------------------------

--
-- Table structure for table `noticias`
--

CREATE TABLE `noticias` (
  `id` int NOT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  `descripcion` text,
  `fecha` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `noticias`
--

INSERT INTO `noticias` (`id`, `nombre`, `descripcion`, `fecha`) VALUES
(1, 'noticia1', 'asdasdas', '2023-07-03 09:14:28'),
(2, 'sdfsdf', 'sdfsdfsdf', '2023-07-03 09:14:35'),
(3, 'dsdasda', 'sdasdasdasda', '2023-07-03 09:16:39');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int NOT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `apellidos` varchar(255) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `username` varchar(100) DEFAULT NULL,
  `authKey` varchar(255) DEFAULT NULL,
  `activo` tinyint(1) DEFAULT '1',
  `password` varchar(255) DEFAULT NULL,
  `accessToken` varchar(255) DEFAULT NULL,
  `administrador` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `nombre`, `apellidos`, `email`, `username`, `authKey`, `activo`, `password`, `accessToken`, `administrador`) VALUES
(1, 'abramo', NULL, 'ramon@alpe.es', 'ramonabramo', 'hOiQ949TqRWshFiY1SwhbL9Gi6jn3umZ', 1, '$2y$13$msgMrwMyQJX5nZqNlX0kx.zPtrZDlsxlxF9JmlHcsH55Fu002Os6K', NULL, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `fotos`
--
ALTER TABLE `fotos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fkfotosNoticias` (`noticia`);

--
-- Indexes for table `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `noticias`
--
ALTER TABLE `noticias`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `fotos`
--
ALTER TABLE `fotos`
  ADD CONSTRAINT `fkfotosNoticias` FOREIGN KEY (`noticia`) REFERENCES `noticias` (`id`);
--
-- Database: `p0e2u2m3`
--
CREATE DATABASE IF NOT EXISTS `p0e2u2m3` DEFAULT CHARACTER SET utf8mb3 COLLATE utf8mb3_spanish_ci;
USE `p0e2u2m3`;

-- --------------------------------------------------------

--
-- Table structure for table `clientes`
--

CREATE TABLE `clientes` (
  `cod` int NOT NULL,
  `nombre` varchar(50) COLLATE utf8mb3_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_spanish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `coches`
--

CREATE TABLE `coches` (
  `id` int NOT NULL,
  `marca` varchar(50) COLLATE utf8mb3_spanish_ci DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `precio` float DEFAULT NULL,
  `cod-cliente` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_spanish_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`cod`);

--
-- Indexes for table `coches`
--
ALTER TABLE `coches`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_coches_clientes_idx` (`cod-cliente`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `coches`
--
ALTER TABLE `coches`
  ADD CONSTRAINT `fk_coches_clientes` FOREIGN KEY (`cod-cliente`) REFERENCES `clientes` (`cod`);
--
-- Database: `p0e9u2m3`
--
CREATE DATABASE IF NOT EXISTS `p0e9u2m3` DEFAULT CHARACTER SET utf8mb3 COLLATE utf8mb3_spanish_ci;
USE `p0e9u2m3`;

-- --------------------------------------------------------

--
-- Table structure for table `clientes`
--

CREATE TABLE `clientes` (
  `cod` int NOT NULL,
  `nombre` varchar(50) COLLATE utf8mb3_spanish_ci DEFAULT NULL,
  `id-coche-alquilado` int DEFAULT NULL,
  `fecha-alquiler` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_spanish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `coches`
--

CREATE TABLE `coches` (
  `id` int NOT NULL,
  `marca` varchar(255) COLLATE utf8mb3_spanish_ci DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `precio` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_spanish_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`cod`),
  ADD UNIQUE KEY `UK_clientes_id-coche-alquilado` (`id-coche-alquilado`);

--
-- Indexes for table `coches`
--
ALTER TABLE `coches`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `clientes`
--
ALTER TABLE `clientes`
  MODIFY `cod` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `coches`
--
ALTER TABLE `coches`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `clientes`
--
ALTER TABLE `clientes`
  ADD CONSTRAINT `FK_clientes_coches` FOREIGN KEY (`id-coche-alquilado`) REFERENCES `coches` (`id`);
--
-- Database: `personas`
--
CREATE DATABASE IF NOT EXISTS `personas` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci;
USE `personas`;

-- --------------------------------------------------------

--
-- Table structure for table `cliente`
--

CREATE TABLE `cliente` (
  `idCliente` int NOT NULL,
  `nombre` varchar(200) DEFAULT NULL,
  `edad` int DEFAULT NULL,
  `nombreEmpresa` varchar(200) DEFAULT NULL,
  `telefono` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `cliente`
--

INSERT INTO `cliente` (`idCliente`, `nombre`, `edad`, `nombreEmpresa`, `telefono`) VALUES
(1, 'Luis', 50, 'Alpe', '942942942'),
(2, 'Ana', 20, 'Alpe', '456456456');

-- --------------------------------------------------------

--
-- Table structure for table `persona`
--

CREATE TABLE `persona` (
  `idPersona` int NOT NULL,
  `nombre` varchar(200) DEFAULT NULL,
  `edad` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `persona`
--

INSERT INTO `persona` (`idPersona`, `nombre`, `edad`) VALUES
(1, 'persona1', 18),
(2, 'persona2', 45);

-- --------------------------------------------------------

--
-- Table structure for table `personas`
--

CREATE TABLE `personas` (
  `id` int UNSIGNED NOT NULL,
  `nombre` varchar(30) NOT NULL,
  `apellidos` varchar(30) NOT NULL,
  `edad` int NOT NULL,
  `poblacion` varchar(30) NOT NULL,
  `codigoPostal` varchar(5) NOT NULL,
  `fechaNacimiento` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`idCliente`);

--
-- Indexes for table `persona`
--
ALTER TABLE `persona`
  ADD PRIMARY KEY (`idPersona`);

--
-- Indexes for table `personas`
--
ALTER TABLE `personas`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cliente`
--
ALTER TABLE `cliente`
  MODIFY `idCliente` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `persona`
--
ALTER TABLE `persona`
  MODIFY `idPersona` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `personas`
--
ALTER TABLE `personas`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Database: `personas1`
--
CREATE DATABASE IF NOT EXISTS `personas1` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci;
USE `personas1`;

-- --------------------------------------------------------

--
-- Table structure for table `personas`
--

CREATE TABLE `personas` (
  `id` int NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `apellidos` varchar(100) NOT NULL,
  `edad` int DEFAULT NULL,
  `fechaNacimiento` date DEFAULT NULL,
  `poblacion` varchar(50) DEFAULT NULL,
  `codigoPostal` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `personas`
--

INSERT INTO `personas` (`id`, `nombre`, `apellidos`, `edad`, `fechaNacimiento`, `poblacion`, `codigoPostal`) VALUES
(1, 'Juan', 'García Pérez', 30, '1992-05-15', 'Santander', '39001'),
(2, 'María', 'López Fernández', 25, '1997-09-20', 'Torrelavega', '39300'),
(3, 'Pedro', 'Martínez Ruiz', 28, '1994-03-10', 'Laredo', '39770'),
(4, 'Ana', 'Fernández Montero', 22, '1999-07-05', 'Castro Urdiales', '39700'),
(5, 'Luis', 'Sánchez García', 35, '1986-12-12', 'Reinosa', '39200'),
(6, 'Elena', 'Pérez Martín', 42, '1979-04-03', 'Colindres', '39750'),
(7, 'Carlos', 'Rodríguez Gómez', 31, '1990-08-18', 'Los Corrales de Buelna', '39400'),
(8, 'Laura', 'Ruiz Díaz', 27, '1994-05-22', 'Santoña', '39740'),
(9, 'Javier', 'González Castro', 38, '1983-11-07', 'San Vicente de la Barquera', '39540'),
(10, 'Isabel', 'Fernández Vega', 29, '1992-02-14', 'Comillas', '39520'),
(11, 'Miguel', 'Pérez Soto', 33, '1988-06-28', 'Cabezón de la Sal', '39500'),
(12, 'Carmen', 'Martín Iglesias', 40, '1981-12-09', 'Puente San Miguel', '39478'),
(13, 'Antonio', 'Ruiz Alonso', 45, '1976-03-25', 'Toranzo', '39699'),
(14, 'Sara', 'Gómez Ortega', 24, '1997-10-11', 'Polanco', '39470'),
(15, 'Daniel', 'Sánchez Pérez', 26, '1995-07-19', 'Santa María de Cayón', '39690');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `personas`
--
ALTER TABLE `personas`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `personas`
--
ALTER TABLE `personas`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- Database: `piezas`
--
CREATE DATABASE IF NOT EXISTS `piezas` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `piezas`;

-- --------------------------------------------------------

--
-- Table structure for table `piezas`
--

CREATE TABLE `piezas` (
  `P` varchar(9) NOT NULL,
  `nomp` varchar(45) DEFAULT NULL,
  `color` varchar(45) DEFAULT NULL,
  `peso` varchar(45) DEFAULT NULL,
  `ciudad` varchar(45) DEFAULT NULL,
  `piezasPorColor` int DEFAULT '0',
  `piezasPorCiudad` int DEFAULT '0'
) ENGINE=InnoDB AVG_ROW_LENGTH=2730 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `piezas`
--

INSERT INTO `piezas` (`P`, `nomp`, `color`, `peso`, `ciudad`, `piezasPorColor`, `piezasPorCiudad`) VALUES
('p1', 'MESA', 'ROJO', '12', 'SANTANDER', 0, 0),
('p2', 'SILLA', 'BLANCA', '17', 'PARIS', 100, 100),
('p3', 'ARMARIO', 'GRIS', '17', 'ROMA', 3510, 3510),
('p4', 'ARCHIVADOR', 'ROJO', '14', 'LONDRES', 1600, 2200),
('p5', 'PUERTA ', 'BLANCA', '12', 'PARIS', 100, 100),
('p6', 'LAMPARA', 'AMARILLA', '19', 'LONDRES', 600, 2200);

-- --------------------------------------------------------

--
-- Table structure for table `proveedores`
--

CREATE TABLE `proveedores` (
  `s` varchar(9) NOT NULL,
  `noms` varchar(45) DEFAULT NULL,
  `estado` varchar(45) DEFAULT NULL,
  `ciudad` varchar(45) DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=3276 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `proveedores`
--

INSERT INTO `proveedores` (`s`, `noms`, `estado`, `ciudad`) VALUES
('s1', 'Smith', '20', 'Londres'),
('s2', 'Jones', '10', 'París'),
('s3', 'Blake', '30', 'París'),
('s4', 'Clark', '20', 'Londres'),
('s5', 'Adams', '30', 'Atenas');

-- --------------------------------------------------------

--
-- Table structure for table `proyectos`
--

CREATE TABLE `proyectos` (
  `j` varchar(9) NOT NULL,
  `nomj` varchar(45) DEFAULT NULL,
  `ciudad` varchar(45) DEFAULT NULL,
  `piezasTotales` int DEFAULT '0'
) ENGINE=InnoDB AVG_ROW_LENGTH=2340 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `proyectos`
--

INSERT INTO `proyectos` (`j`, `nomj`, `ciudad`, `piezasTotales`) VALUES
('j1', 'Edificio1', 'París', 0),
('j2', 'Edificio2', 'Roma', 0),
('j3', 'Edificio3', 'Atenas', 0),
('j4', 'Edificio4', 'Atenas', 0),
('j5', 'Edificio5', 'Londres', 0),
('j6', 'Edificio6', 'Madrid', 0),
('j7', 'Edificio7', 'Londres', 0);

-- --------------------------------------------------------

--
-- Table structure for table `spj`
--

CREATE TABLE `spj` (
  `s` varchar(9) NOT NULL,
  `p` varchar(9) NOT NULL,
  `j` varchar(9) NOT NULL,
  `cant` int DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=819 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `spj`
--

INSERT INTO `spj` (`s`, `p`, `j`, `cant`) VALUES
('s1', 'p3', 'j2', 20),
('s1', 'p3', 'j3', 30),
('s1', 'p3', 'j4', 40),
('s1', 'p3', 'j5', 50),
('s1', 'p3', 'j6', 400),
('s1', 'p3', 'j7', 70),
('s2', 'p3', 'j1', 400),
('s2', 'p3', 'j2', 200),
('s2', 'p3', 'j3', 200),
('s2', 'p3', 'j4', 500),
('s2', 'p3', 'j5', 600),
('s2', 'p3', 'j7', 800),
('s2', 'p5', 'j2', 100),
('s3', 'p3', 'j1', 200),
('s3', 'p4', 'j2', 500),
('s4', 'p6', 'j3', 300),
('s4', 'p6', 'j7', 300);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `piezas`
--
ALTER TABLE `piezas`
  ADD PRIMARY KEY (`P`);

--
-- Indexes for table `proveedores`
--
ALTER TABLE `proveedores`
  ADD PRIMARY KEY (`s`);

--
-- Indexes for table `proyectos`
--
ALTER TABLE `proyectos`
  ADD PRIMARY KEY (`j`);

--
-- Indexes for table `spj`
--
ALTER TABLE `spj`
  ADD PRIMARY KEY (`s`,`p`,`j`),
  ADD KEY `jspj` (`j`),
  ADD KEY `pspj` (`p`),
  ADD KEY `sspj` (`s`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `spj`
--
ALTER TABLE `spj`
  ADD CONSTRAINT `FK_spj_j_j` FOREIGN KEY (`j`) REFERENCES `proyectos` (`j`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_spj_p_P` FOREIGN KEY (`p`) REFERENCES `piezas` (`P`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_spj_s_s` FOREIGN KEY (`s`) REFERENCES `proveedores` (`s`) ON DELETE CASCADE ON UPDATE CASCADE;
--
-- Database: `piscina2023`
--
CREATE DATABASE IF NOT EXISTS `piscina2023` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci;
USE `piscina2023`;

-- --------------------------------------------------------

--
-- Table structure for table `cliente`
--

CREATE TABLE `cliente` (
  `id` int NOT NULL,
  `nombre` varchar(300) DEFAULT NULL,
  `apellidos` varchar(300) DEFAULT NULL,
  `correo` varchar(200) NOT NULL,
  `telefono` varchar(100) NOT NULL,
  `fechaHora` datetime DEFAULT NULL,
  `contrasena` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `piscina`
--

CREATE TABLE `piscina` (
  `id` int NOT NULL,
  `nombre` varchar(300) DEFAULT NULL,
  `descripcion` text,
  `aforo` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `reserva`
--

CREATE TABLE `reserva` (
  `id` int NOT NULL,
  `idCliente` int DEFAULT NULL,
  `idPiscina` int DEFAULT NULL,
  `fechaHora` datetime DEFAULT NULL,
  `duracion` int DEFAULT NULL,
  `precio` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `correo` (`correo`);

--
-- Indexes for table `piscina`
--
ALTER TABLE `piscina`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reserva`
--
ALTER TABLE `reserva`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `idCliente` (`idCliente`,`idPiscina`,`fechaHora`),
  ADD KEY `fkReservaPiscina` (`idPiscina`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `reserva`
--
ALTER TABLE `reserva`
  ADD CONSTRAINT `fkReservaCliente` FOREIGN KEY (`idCliente`) REFERENCES `cliente` (`id`),
  ADD CONSTRAINT `fkReservaPiscina` FOREIGN KEY (`idPiscina`) REFERENCES `piscina` (`id`);
--
-- Database: `portal`
--
CREATE DATABASE IF NOT EXISTS `portal` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci;
USE `portal`;

-- --------------------------------------------------------

--
-- Table structure for table `nivel`
--

CREATE TABLE `nivel` (
  `id` int NOT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  `cantidad` int DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `nivel`
--

INSERT INTO `nivel` (`id`, `nombre`, `cantidad`) VALUES
(1, 'basico', 0),
(2, 'normal', 0),
(3, 'alto', 0),
(4, 'muy alto', 0),
(5, 'locura', 0);

-- --------------------------------------------------------

--
-- Table structure for table `pregunta`
--

CREATE TABLE `pregunta` (
  `idPregunta` int NOT NULL,
  `numero` int DEFAULT NULL,
  `test` int DEFAULT NULL,
  `enunciado` varchar(600) DEFAULT NULL,
  `nivel` int DEFAULT NULL,
  `foto` varchar(200) DEFAULT NULL,
  `explicacion` text,
  `fotoExplicacion` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `respuesta`
--

CREATE TABLE `respuesta` (
  `idRespuesta` int NOT NULL,
  `test` int DEFAULT NULL,
  `texto` varchar(600) DEFAULT NULL,
  `correcta` tinyint(1) DEFAULT NULL,
  `foto` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `test`
--

CREATE TABLE `test` (
  `idTest` int NOT NULL,
  `titulo` varchar(200) DEFAULT NULL,
  `descripcion` text,
  `duracion` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `test`
--

INSERT INTO `test` (`idTest`, `titulo`, `descripcion`, `duracion`) VALUES
(1, 'Informatica 1', '<p>Este test es de informatica</p>', 28),
(2, 'Informatica 2', '<p>Ejemplo de test</p>', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `nivel`
--
ALTER TABLE `nivel`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pregunta`
--
ALTER TABLE `pregunta`
  ADD PRIMARY KEY (`idPregunta`),
  ADD UNIQUE KEY `numero` (`numero`,`test`),
  ADD KEY `fkpreguntatest` (`test`),
  ADD KEY `fkpreguntanivel` (`nivel`);

--
-- Indexes for table `respuesta`
--
ALTER TABLE `respuesta`
  ADD PRIMARY KEY (`idRespuesta`),
  ADD KEY `fkrespuestatest` (`test`);

--
-- Indexes for table `test`
--
ALTER TABLE `test`
  ADD PRIMARY KEY (`idTest`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `nivel`
--
ALTER TABLE `nivel`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `pregunta`
--
ALTER TABLE `pregunta`
  MODIFY `idPregunta` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `respuesta`
--
ALTER TABLE `respuesta`
  MODIFY `idRespuesta` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `test`
--
ALTER TABLE `test`
  MODIFY `idTest` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `pregunta`
--
ALTER TABLE `pregunta`
  ADD CONSTRAINT `fkpreguntanivel` FOREIGN KEY (`nivel`) REFERENCES `nivel` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fkpreguntatest` FOREIGN KEY (`test`) REFERENCES `test` (`idTest`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `respuesta`
--
ALTER TABLE `respuesta`
  ADD CONSTRAINT `fkrespuestatest` FOREIGN KEY (`test`) REFERENCES `test` (`idTest`) ON DELETE CASCADE ON UPDATE CASCADE;
--
-- Database: `practica1`
--
CREATE DATABASE IF NOT EXISTS `practica1` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci;
USE `practica1`;

-- --------------------------------------------------------

--
-- Table structure for table `clientes`
--

CREATE TABLE `clientes` (
  `cod` int NOT NULL,
  `nombre` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='Tabla para introducir los clientes de la empresa';

--
-- Dumping data for table `clientes`
--

INSERT INTO `clientes` (`cod`, `nombre`) VALUES
(1, 'Ana'),
(2, 'Jose'),
(3, 'Luis');

-- --------------------------------------------------------

--
-- Table structure for table `coches`
--

CREATE TABLE `coches` (
  `id` int NOT NULL,
  `marca` varchar(50) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `precio` float DEFAULT NULL,
  `cod-cliente` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='Nos permite almacenar los coches de nuestro garaje';

--
-- Dumping data for table `coches`
--

INSERT INTO `coches` (`id`, `marca`, `fecha`, `precio`, `cod-cliente`) VALUES
(1, 'Ford', '2021-01-12', 100, 1),
(2, 'Renault', '2021-11-11', 110, 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`cod`);

--
-- Indexes for table `coches`
--
ALTER TABLE `coches`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_coches_clientes` (`cod-cliente`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `clientes`
--
ALTER TABLE `clientes`
  MODIFY `cod` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `coches`
--
ALTER TABLE `coches`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `coches`
--
ALTER TABLE `coches`
  ADD CONSTRAINT `fk_coches_clientes` FOREIGN KEY (`cod-cliente`) REFERENCES `clientes` (`cod`);
--
-- Database: `practica2`
--
CREATE DATABASE IF NOT EXISTS `practica2` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci;
USE `practica2`;

-- --------------------------------------------------------

--
-- Table structure for table `clientes`
--

CREATE TABLE `clientes` (
  `cod` int NOT NULL,
  `nombre` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='Clientes de la empresa';

--
-- Dumping data for table `clientes`
--

INSERT INTO `clientes` (`cod`, `nombre`) VALUES
(1, 'Ana'),
(2, 'Jose'),
(3, 'Luis');

-- --------------------------------------------------------

--
-- Table structure for table `coches`
--

CREATE TABLE `coches` (
  `id` int NOT NULL,
  `marca` varchar(50) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `precio` float DEFAULT NULL COMMENT 'precio base de compra'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='coches de la empresa';

--
-- Dumping data for table `coches`
--

INSERT INTO `coches` (`id`, `marca`, `fecha`, `precio`) VALUES
(1, 'Ford', '2021-01-12', 100),
(2, 'Renault', '2021-11-11', 110);

-- --------------------------------------------------------

--
-- Table structure for table `comprados`
--

CREATE TABLE `comprados` (
  `idCoches` int NOT NULL,
  `codClientes` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='coches comprados por los clientes';

--
-- Dumping data for table `comprados`
--

INSERT INTO `comprados` (`idCoches`, `codClientes`) VALUES
(1, 1),
(2, 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`cod`);

--
-- Indexes for table `coches`
--
ALTER TABLE `coches`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comprados`
--
ALTER TABLE `comprados`
  ADD PRIMARY KEY (`idCoches`,`codClientes`),
  ADD UNIQUE KEY `idCoches` (`idCoches`),
  ADD KEY `fkCompradosClientes` (`codClientes`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `clientes`
--
ALTER TABLE `clientes`
  MODIFY `cod` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `coches`
--
ALTER TABLE `coches`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `comprados`
--
ALTER TABLE `comprados`
  ADD CONSTRAINT `fkCompradosClientes` FOREIGN KEY (`codClientes`) REFERENCES `clientes` (`cod`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `fkCompradosCoches` FOREIGN KEY (`idCoches`) REFERENCES `coches` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
--
-- Database: `practica3`
--
CREATE DATABASE IF NOT EXISTS `practica3` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci;
USE `practica3`;

-- --------------------------------------------------------

--
-- Table structure for table `clientes`
--

CREATE TABLE `clientes` (
  `codigo` int NOT NULL,
  `nombre` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `clientes`
--

INSERT INTO `clientes` (`codigo`, `nombre`) VALUES
(1, 'Jose'),
(2, 'Ana');

-- --------------------------------------------------------

--
-- Table structure for table `coches`
--

CREATE TABLE `coches` (
  `matricula` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `coches`
--

INSERT INTO `coches` (`matricula`) VALUES
('DDD1111'),
('fgd1234');

-- --------------------------------------------------------

--
-- Table structure for table `poblacion`
--

CREATE TABLE `poblacion` (
  `codPob` int NOT NULL,
  `nombre` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `poblacion`
--

INSERT INTO `poblacion` (`codPob`, `nombre`) VALUES
(1, 'Santander'),
(2, 'Laredo');

-- --------------------------------------------------------

--
-- Table structure for table `relacion`
--

CREATE TABLE `relacion` (
  `codCliente` int NOT NULL,
  `codPob` int NOT NULL,
  `matricula` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `relacion`
--

INSERT INTO `relacion` (`codCliente`, `codPob`, `matricula`) VALUES
(1, 1, 'fgd1234'),
(1, 2, 'DDD1111');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`codigo`);

--
-- Indexes for table `coches`
--
ALTER TABLE `coches`
  ADD PRIMARY KEY (`matricula`);

--
-- Indexes for table `poblacion`
--
ALTER TABLE `poblacion`
  ADD PRIMARY KEY (`codPob`);

--
-- Indexes for table `relacion`
--
ALTER TABLE `relacion`
  ADD PRIMARY KEY (`codCliente`,`codPob`,`matricula`),
  ADD KEY `fkRelacionPoblacion` (`codPob`),
  ADD KEY `fkRelacionCoches` (`matricula`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `clientes`
--
ALTER TABLE `clientes`
  MODIFY `codigo` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `poblacion`
--
ALTER TABLE `poblacion`
  MODIFY `codPob` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `relacion`
--
ALTER TABLE `relacion`
  ADD CONSTRAINT `fkRelacionCliente` FOREIGN KEY (`codCliente`) REFERENCES `clientes` (`codigo`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `fkRelacionCoches` FOREIGN KEY (`matricula`) REFERENCES `coches` (`matricula`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `fkRelacionPoblacion` FOREIGN KEY (`codPob`) REFERENCES `poblacion` (`codPob`) ON DELETE RESTRICT ON UPDATE RESTRICT;
--
-- Database: `practica4`
--
CREATE DATABASE IF NOT EXISTS `practica4` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci;
USE `practica4`;

-- --------------------------------------------------------

--
-- Table structure for table `departamento`
--

CREATE TABLE `departamento` (
  `nombreD` varchar(100) NOT NULL,
  `numeroD` int NOT NULL,
  `numDeEmpleados` int DEFAULT NULL,
  `nssEmpleadoDirige` varchar(10) DEFAULT NULL,
  `fechaInicioJefe` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `departamento`
--

INSERT INTO `departamento` (`nombreD`, `numeroD`, `numDeEmpleados`, `nssEmpleadoDirige`, `fechaInicioJefe`) VALUES
('comercial', 1, NULL, '5', NULL),
('comercial', 2, NULL, '6', NULL),
('programacion', 1, NULL, '1', NULL),
('programacion', 2, NULL, '2', NULL),
('tecnico', 1, NULL, '3', NULL),
('tecnico', 2, NULL, '8', NULL),
('tecnico', 3, NULL, '4', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `dependiente`
--

CREATE TABLE `dependiente` (
  `nombreDependiente` varchar(100) NOT NULL,
  `nssEmpleado` varchar(10) NOT NULL,
  `sexo` varchar(20) DEFAULT NULL,
  `fechaNcto` date DEFAULT NULL,
  `parentesco` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `dependiente`
--

INSERT INTO `dependiente` (`nombreDependiente`, `nssEmpleado`, `sexo`, `fechaNcto`, `parentesco`) VALUES
('jorge', '1', NULL, NULL, 'hijo'),
('loreto', '2', NULL, NULL, 'hija');

-- --------------------------------------------------------

--
-- Table structure for table `empleado`
--

CREATE TABLE `empleado` (
  `nssEmpleado` varchar(10) NOT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  `apellido` varchar(200) DEFAULT NULL,
  `iniciales` varchar(4) DEFAULT NULL,
  `fechaNcto` date DEFAULT NULL,
  `sexo` varchar(20) DEFAULT NULL,
  `direccion` varchar(400) DEFAULT NULL,
  `salario` float DEFAULT NULL,
  `nombreDPertenece` varchar(100) DEFAULT NULL,
  `numeroDPertenece` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `empleado`
--

INSERT INTO `empleado` (`nssEmpleado`, `nombre`, `apellido`, `iniciales`, `fechaNcto`, `sexo`, `direccion`, `salario`, `nombreDPertenece`, `numeroDPertenece`) VALUES
('1', 'rosa', NULL, NULL, NULL, NULL, NULL, NULL, 'comercial', 1),
('2', 'eva', NULL, NULL, NULL, NULL, NULL, NULL, 'comercial', 2),
('3', 'jose', NULL, NULL, NULL, NULL, NULL, NULL, 'tecnico', 1),
('4', 'luis', NULL, NULL, NULL, NULL, NULL, NULL, 'tecnico', 1),
('5', 'cesar', NULL, NULL, NULL, NULL, NULL, NULL, 'tecnico', 1),
('6', 'cesar', NULL, NULL, NULL, NULL, NULL, NULL, 'tecnico', 1),
('7', 'eva', NULL, NULL, NULL, NULL, NULL, NULL, 'tecnico', 1),
('8', 'susana', NULL, NULL, NULL, NULL, NULL, NULL, 'tecnico', 1);

-- --------------------------------------------------------

--
-- Table structure for table `localizaciones`
--

CREATE TABLE `localizaciones` (
  `nombreD` varchar(100) NOT NULL,
  `numeroD` int NOT NULL,
  `localizacionDept` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `localizaciones`
--

INSERT INTO `localizaciones` (`nombreD`, `numeroD`, `localizacionDept`) VALUES
('comercial', 1, 'santander'),
('comercial', 2, 'loredo'),
('programacion', 1, 'santander'),
('programacion', 2, 'santander'),
('tecnico', 1, 'laredo'),
('tecnico', 2, 'laredo'),
('tecnico', 3, 'torrelavega');

-- --------------------------------------------------------

--
-- Table structure for table `proyecto`
--

CREATE TABLE `proyecto` (
  `numeroP` int NOT NULL,
  `nombreP` varchar(100) NOT NULL,
  `localizacion` varchar(200) DEFAULT NULL,
  `nombreDControla` varchar(100) DEFAULT NULL,
  `numeroDControla` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `proyecto`
--

INSERT INTO `proyecto` (`numeroP`, `nombreP`, `localizacion`, `nombreDControla`, `numeroDControla`) VALUES
(1, 'proyecto', 'santander', 'programacion', 1),
(2, 'proyecto2', 'isla', 'programacion', 1),
(3, 'proyecto3', 'noja', 'tecnico', 1);

-- --------------------------------------------------------

--
-- Table structure for table `supervisa`
--

CREATE TABLE `supervisa` (
  `nssEmpleado` varchar(10) NOT NULL,
  `nssSupervisor` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `supervisa`
--

INSERT INTO `supervisa` (`nssEmpleado`, `nssSupervisor`) VALUES
('1', '2'),
('2', '4'),
('3', '2');

-- --------------------------------------------------------

--
-- Table structure for table `trabajeen`
--

CREATE TABLE `trabajeen` (
  `nssEmpleado` varchar(10) NOT NULL,
  `nombreP` varchar(100) NOT NULL,
  `numeroP` int NOT NULL,
  `horas` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `trabajeen`
--

INSERT INTO `trabajeen` (`nssEmpleado`, `nombreP`, `numeroP`, `horas`) VALUES
('1', 'proyecto', 1, 3),
('2', 'proyecto2', 2, 4),
('2', 'proyecto3', 3, 5);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `departamento`
--
ALTER TABLE `departamento`
  ADD PRIMARY KEY (`nombreD`,`numeroD`),
  ADD KEY `fkDepartamentoEmpleado` (`nssEmpleadoDirige`);

--
-- Indexes for table `dependiente`
--
ALTER TABLE `dependiente`
  ADD PRIMARY KEY (`nombreDependiente`,`nssEmpleado`),
  ADD KEY `fkDependienteEmpleado` (`nssEmpleado`);

--
-- Indexes for table `empleado`
--
ALTER TABLE `empleado`
  ADD PRIMARY KEY (`nssEmpleado`),
  ADD KEY `fkEmpleadoDepartamento` (`nombreDPertenece`,`numeroDPertenece`);

--
-- Indexes for table `localizaciones`
--
ALTER TABLE `localizaciones`
  ADD PRIMARY KEY (`nombreD`,`numeroD`,`localizacionDept`);

--
-- Indexes for table `proyecto`
--
ALTER TABLE `proyecto`
  ADD PRIMARY KEY (`numeroP`,`nombreP`),
  ADD KEY `fkProyectoDepartamento` (`nombreDControla`,`numeroDControla`);

--
-- Indexes for table `supervisa`
--
ALTER TABLE `supervisa`
  ADD PRIMARY KEY (`nssEmpleado`,`nssSupervisor`),
  ADD UNIQUE KEY `uk1` (`nssEmpleado`),
  ADD KEY `fkSupervisaEmpleadoSupervisa` (`nssSupervisor`);

--
-- Indexes for table `trabajeen`
--
ALTER TABLE `trabajeen`
  ADD PRIMARY KEY (`nssEmpleado`,`nombreP`,`numeroP`),
  ADD KEY `fkTrabajaEnProyecto` (`numeroP`,`nombreP`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `departamento`
--
ALTER TABLE `departamento`
  ADD CONSTRAINT `fkDepartamentoEmpleado` FOREIGN KEY (`nssEmpleadoDirige`) REFERENCES `empleado` (`nssEmpleado`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Constraints for table `dependiente`
--
ALTER TABLE `dependiente`
  ADD CONSTRAINT `fkDependienteEmpleado` FOREIGN KEY (`nssEmpleado`) REFERENCES `empleado` (`nssEmpleado`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Constraints for table `empleado`
--
ALTER TABLE `empleado`
  ADD CONSTRAINT `fkEmpleadoDepartamento` FOREIGN KEY (`nombreDPertenece`,`numeroDPertenece`) REFERENCES `departamento` (`nombreD`, `numeroD`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Constraints for table `localizaciones`
--
ALTER TABLE `localizaciones`
  ADD CONSTRAINT `fkLocalizacionesDepartamento` FOREIGN KEY (`nombreD`,`numeroD`) REFERENCES `departamento` (`nombreD`, `numeroD`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Constraints for table `proyecto`
--
ALTER TABLE `proyecto`
  ADD CONSTRAINT `fkProyectoDepartamento` FOREIGN KEY (`nombreDControla`,`numeroDControla`) REFERENCES `departamento` (`nombreD`, `numeroD`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Constraints for table `supervisa`
--
ALTER TABLE `supervisa`
  ADD CONSTRAINT `fkSupervisaEmpleado` FOREIGN KEY (`nssEmpleado`) REFERENCES `empleado` (`nssEmpleado`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `fkSupervisaEmpleadoSupervisa` FOREIGN KEY (`nssSupervisor`) REFERENCES `empleado` (`nssEmpleado`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Constraints for table `trabajeen`
--
ALTER TABLE `trabajeen`
  ADD CONSTRAINT `fkTrabajaEnEmpleado` FOREIGN KEY (`nssEmpleado`) REFERENCES `empleado` (`nssEmpleado`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `fkTrabajaEnProyecto` FOREIGN KEY (`numeroP`,`nombreP`) REFERENCES `proyecto` (`numeroP`, `nombreP`) ON DELETE RESTRICT ON UPDATE RESTRICT;
--
-- Database: `practica5`
--
CREATE DATABASE IF NOT EXISTS `practica5` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci;
USE `practica5`;

-- --------------------------------------------------------

--
-- Table structure for table `ejercicio1`
--

CREATE TABLE `ejercicio1` (
  `numero` int NOT NULL,
  `lado` float DEFAULT NULL,
  `area` float DEFAULT NULL,
  `perimetro` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `ejercicio1`
--

INSERT INTO `ejercicio1` (`numero`, `lado`, `area`, `perimetro`) VALUES
(1, 2, 4, 8),
(2, 3, 9, 12);

-- --------------------------------------------------------

--
-- Table structure for table `ejercicio2`
--

CREATE TABLE `ejercicio2` (
  `id` int NOT NULL,
  `ladoCampo` float DEFAULT NULL,
  `volumenCampo` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `ejercicio2`
--

INSERT INTO `ejercicio2` (`id`, `ladoCampo`, `volumenCampo`) VALUES
(1, 2, 8),
(2, 3, 27);

-- --------------------------------------------------------

--
-- Table structure for table `ejercicio3`
--

CREATE TABLE `ejercicio3` (
  `id` int NOT NULL,
  `ladoCampo` float DEFAULT NULL,
  `volumenCampo` float DEFAULT NULL,
  `areaTotalCampo` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `ejercicio3`
--

INSERT INTO `ejercicio3` (`id`, `ladoCampo`, `volumenCampo`, `areaTotalCampo`) VALUES
(1, 2, 8, 24),
(2, 3, 27, 54);

-- --------------------------------------------------------

--
-- Table structure for table `ejercicio6`
--

CREATE TABLE `ejercicio6` (
  `id` int NOT NULL,
  `ladoCampo` float DEFAULT NULL,
  `volumenCampo` float DEFAULT NULL,
  `areaTotalCampo` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `ejercicio6`
--

INSERT INTO `ejercicio6` (`id`, `ladoCampo`, `volumenCampo`, `areaTotalCampo`) VALUES
(1, 2, 8, 24),
(2, 3, 27, 54);

-- --------------------------------------------------------

--
-- Table structure for table `ejercicio10`
--

CREATE TABLE `ejercicio10` (
  `id` int NOT NULL,
  `radioCampo` float DEFAULT NULL,
  `alturaCampo` float DEFAULT NULL,
  `areaLateralCampo` float DEFAULT NULL,
  `areaTotalCampo` float DEFAULT NULL,
  `volumenCampo` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `ejercicio10`
--

INSERT INTO `ejercicio10` (`id`, `radioCampo`, `alturaCampo`, `areaLateralCampo`, `areaTotalCampo`, `volumenCampo`) VALUES
(1, 2, 5, 62.8319, 87.9646, 62.8319);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ejercicio1`
--
ALTER TABLE `ejercicio1`
  ADD PRIMARY KEY (`numero`);

--
-- Indexes for table `ejercicio2`
--
ALTER TABLE `ejercicio2`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ejercicio3`
--
ALTER TABLE `ejercicio3`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ejercicio6`
--
ALTER TABLE `ejercicio6`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ejercicio10`
--
ALTER TABLE `ejercicio10`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ejercicio1`
--
ALTER TABLE `ejercicio1`
  MODIFY `numero` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `ejercicio2`
--
ALTER TABLE `ejercicio2`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `ejercicio3`
--
ALTER TABLE `ejercicio3`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `ejercicio6`
--
ALTER TABLE `ejercicio6`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `ejercicio10`
--
ALTER TABLE `ejercicio10`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Database: `practica8`
--
CREATE DATABASE IF NOT EXISTS `practica8` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci;
USE `practica8`;

-- --------------------------------------------------------

--
-- Table structure for table `ordenadores`
--

CREATE TABLE `ordenadores` (
  `id` int NOT NULL,
  `descripcion` varchar(800) DEFAULT NULL,
  `procesador` varchar(255) DEFAULT NULL,
  `memoria` varchar(255) DEFAULT NULL,
  `discoduro` varchar(255) DEFAULT NULL,
  `ethernet` tinyint(1) DEFAULT NULL,
  `wifi` tinyint(1) DEFAULT NULL,
  `video` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `ordenadores`
--

INSERT INTO `ordenadores` (`id`, `descripcion`, `procesador`, `memoria`, `discoduro`, `ethernet`, `wifi`, `video`) VALUES
(1, 'prueba', '', '', '', 0, 0, ''),
(2, '', '', '', '', 0, 1, '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ordenadores`
--
ALTER TABLE `ordenadores`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ordenadores`
--
ALTER TABLE `ordenadores`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Database: `practica10`
--
CREATE DATABASE IF NOT EXISTS `practica10` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci;
USE `practica10`;
--
-- Database: `practicadisparadores`
--
CREATE DATABASE IF NOT EXISTS `practicadisparadores` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci;
USE `practicadisparadores`;

-- --------------------------------------------------------

--
-- Table structure for table `productos`
--

CREATE TABLE `productos` (
  `producto` varchar(5) NOT NULL,
  `cantidad` int DEFAULT '0' COMMENT 'beneficio total por este producto'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `productos`
--

INSERT INTO `productos` (`producto`, `cantidad`) VALUES
('p1', 530),
('p2', 540),
('p3', 660),
('p4', 840),
('p5', 510);

-- --------------------------------------------------------

--
-- Table structure for table `ventas`
--

CREATE TABLE `ventas` (
  `id` int NOT NULL,
  `producto` varchar(5) DEFAULT NULL,
  `precio` int NOT NULL,
  `unidades` int NOT NULL,
  `total` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `ventas`
--

INSERT INTO `ventas` (`id`, `producto`, `precio`, `unidades`, `total`) VALUES
(1, 'p1', 10, 5, 50),
(2, 'p2', 20, 3, 60),
(3, 'p1', 10, 7, 70),
(4, 'p3', 30, 7, 210),
(5, 'p1', 10, 8, 80),
(6, 'p1', 10, 9, 90),
(7, 'p2', 20, 10, 200),
(8, 'p1', 10, 11, 110),
(9, 'p4', 30, 12, 360),
(10, 'p1', 10, 13, 130),
(11, 'p2', 20, 14, 280),
(12, 'p3', 30, 15, 450),
(13, 'p4', 30, 16, 480),
(14, 'p5', 30, 17, 510);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`producto`);

--
-- Indexes for table `ventas`
--
ALTER TABLE `ventas`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ventas`
--
ALTER TABLE `ventas`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- Database: `practicas`
--
CREATE DATABASE IF NOT EXISTS `practicas` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci;
USE `practicas`;

-- --------------------------------------------------------

--
-- Table structure for table `alumno`
--

CREATE TABLE `alumno` (
  `matricula` int NOT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  `grupo` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `alumno`
--

INSERT INTO `alumno` (`matricula`, `nombre`, `grupo`) VALUES
(1, 'ANA', 'GRUPO A'),
(2, 'JOSE', 'GRUPO A'),
(4, 'LUISA', 'GRUPO B'),
(8, 'FERNANDO', 'GRUPO C');

-- --------------------------------------------------------

--
-- Table structure for table `practica`
--

CREATE TABLE `practica` (
  `codigo` int NOT NULL,
  `titulo` varchar(100) DEFAULT NULL,
  `dificultad` varchar(100) DEFAULT NULL,
  `alumno` int DEFAULT NULL,
  `nota` float DEFAULT NULL,
  `fecha` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `practica`
--

INSERT INTO `practica` (`codigo`, `titulo`, `dificultad`, `alumno`, `nota`, `fecha`) VALUES
(1, 'IMPLEMENTACION ESQUEMA 1_1', 'BASICA', 1, 8, '2024-01-22'),
(2, 'IMPLEMENTACION ESQUEMA 1_N', 'BASICA', 1, 9, '2024-01-23'),
(3, 'IMPLEMENTACION ESQUEMA N_N', 'AVANZADO', 1, 10, '2024-01-24');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `alumno`
--
ALTER TABLE `alumno`
  ADD PRIMARY KEY (`matricula`);

--
-- Indexes for table `practica`
--
ALTER TABLE `practica`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `fkPracticaAlumno` (`alumno`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `practica`
--
ALTER TABLE `practica`
  ADD CONSTRAINT `fkPracticaAlumno` FOREIGN KEY (`alumno`) REFERENCES `alumno` (`matricula`) ON DELETE RESTRICT ON UPDATE RESTRICT;
--
-- Database: `procedimientos1`
--
CREATE DATABASE IF NOT EXISTS `procedimientos1` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci;
USE `procedimientos1`;

-- --------------------------------------------------------

--
-- Table structure for table `ciudades`
--

CREATE TABLE `ciudades` (
  `id` int NOT NULL,
  `nombre` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `personas`
--

CREATE TABLE `personas` (
  `id` int NOT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  `edad` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ciudades`
--
ALTER TABLE `ciudades`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `personas`
--
ALTER TABLE `personas`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ciudades`
--
ALTER TABLE `ciudades`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `personas`
--
ALTER TABLE `personas`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;
--
-- Database: `productos`
--
CREATE DATABASE IF NOT EXISTS `productos` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `productos`;

-- --------------------------------------------------------

--
-- Table structure for table `mensajes`
--

CREATE TABLE `mensajes` (
  `idmensaje` int NOT NULL,
  `descripcion` varchar(512) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `fechaPublicacion` date DEFAULT NULL,
  `hora` time DEFAULT NULL,
  `producto` int DEFAULT NULL,
  `usuario` int DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=8192 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `mensajes`
--

INSERT INTO `mensajes` (`idmensaje`, `descripcion`, `fechaPublicacion`, `hora`, `producto`, `usuario`) VALUES
(1, 'Estamos probando', '2021-07-09', '07:30:00', 2, 1),
(2, 'otro mensaje', '2021-07-09', '07:30:00', 2, 3);

-- --------------------------------------------------------

--
-- Table structure for table `productos`
--

CREATE TABLE `productos` (
  `idproducto` int NOT NULL,
  `nombre` varchar(100) COLLATE utf8mb4_general_ci NOT NULL,
  `precioBase` float DEFAULT NULL,
  `fechaPublicacion` date DEFAULT NULL,
  `foto` varchar(100) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `vendido` tinyint(1) DEFAULT NULL,
  `fechaVenta` date DEFAULT NULL,
  `usuarioPublica` int DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=8192 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `productos`
--

INSERT INTO `productos` (`idproducto`, `nombre`, `precioBase`, `fechaPublicacion`, `foto`, `vendido`, `fechaVenta`, `usuarioPublica`) VALUES
(1, 'pizarra', 600, '2020-07-09', '1pizarra.png', 0, NULL, 1),
(2, 'pizarra1', 1000, NULL, '2pizarra.jpg', NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int NOT NULL,
  `nombre` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `apellidos` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `email` varchar(50) COLLATE utf8mb4_general_ci NOT NULL,
  `username` varchar(55) COLLATE utf8mb4_general_ci NOT NULL,
  `authKey` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `activo` int NOT NULL DEFAULT '0',
  `password` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `accessToken` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `role` int NOT NULL DEFAULT '1'
) ENGINE=InnoDB AVG_ROW_LENGTH=8192 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `usuarios`
--

INSERT INTO `usuarios` (`id`, `nombre`, `apellidos`, `email`, `username`, `authKey`, `activo`, `password`, `accessToken`, `role`) VALUES
(1, 'ramon', 'abramo', 'RAMONABRAMO@GMAIL.COM', 'ramonabramo', '', 1, '$2y$13$IV5m6RXdTjO8l0f30FtChe0PKG3yQWqTaT5kNIhibqlx7UJ/FE0j6', '', 1),
(3, 'ana', NULL, 'clasealpeformacion2021@gmail.com', 'ana', 'IutZo6GIIkTMJgymUnwZG2Bg6a6vZGyv', 1, '$2y$13$8ILS9FdQHB4yLt.dZ7aULeJGkn85fsJPnGQvhepMysrQUClmBnA/O', NULL, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `mensajes`
--
ALTER TABLE `mensajes`
  ADD PRIMARY KEY (`idmensaje`),
  ADD KEY `fkmensajesproductos` (`producto`),
  ADD KEY `fkmensajesusuarios` (`usuario`);

--
-- Indexes for table `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`idproducto`),
  ADD KEY `fkproductosusuarios` (`usuarioPublica`);

--
-- Indexes for table `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UK_usuarios_email` (`email`),
  ADD UNIQUE KEY `UK_usuarios_username` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `mensajes`
--
ALTER TABLE `mensajes`
  MODIFY `idmensaje` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `productos`
--
ALTER TABLE `productos`
  MODIFY `idproducto` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `mensajes`
--
ALTER TABLE `mensajes`
  ADD CONSTRAINT `fkmensajesproductos` FOREIGN KEY (`producto`) REFERENCES `productos` (`idproducto`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fkmensajesusuarios` FOREIGN KEY (`usuario`) REFERENCES `usuarios` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `productos`
--
ALTER TABLE `productos`
  ADD CONSTRAINT `fkproductosusuarios` FOREIGN KEY (`usuarioPublica`) REFERENCES `usuarios` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
--
-- Database: `productosclientes`
--
CREATE DATABASE IF NOT EXISTS `productosclientes` DEFAULT CHARACTER SET utf8mb3 COLLATE utf8mb3_spanish_ci;
USE `productosclientes`;

-- --------------------------------------------------------

--
-- Table structure for table `clientes`
--

CREATE TABLE `clientes` (
  `CÓDIGO CLIENTE` varchar(4) COLLATE utf8mb3_spanish_ci NOT NULL,
  `EMPRESA` varchar(25) COLLATE utf8mb3_spanish_ci DEFAULT NULL,
  `DIRECCIÓN` varchar(20) COLLATE utf8mb3_spanish_ci DEFAULT NULL,
  `POBLACIÓN` varchar(10) COLLATE utf8mb3_spanish_ci DEFAULT NULL,
  `TELÉFONO` varchar(9) COLLATE utf8mb3_spanish_ci DEFAULT NULL,
  `RESPONSABLE` varchar(40) COLLATE utf8mb3_spanish_ci DEFAULT NULL,
  `HISTORIAL` longtext COLLATE utf8mb3_spanish_ci
) ENGINE=InnoDB AVG_ROW_LENGTH=431 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_spanish_ci;

--
-- Dumping data for table `clientes`
--

INSERT INTO `clientes` (`CÓDIGO CLIENTE`, `EMPRESA`, `DIRECCIÓN`, `POBLACIÓN`, `TELÉFONO`, `RESPONSABLE`, `HISTORIAL`) VALUES
('CT01', 'BELTRÁN E HIJOS', 'LAS FUENTES 78', 'MADRID', '914456435', 'ANGEL MARTÍNEZ', NULL),
('CT02', 'LA MODERNA', 'LA PALOMA 123', 'OVIEDO', '985323434', 'JUAN GARCÍA', NULL),
('CT03', 'EL ESPAÑOLITO', 'MOTORES 34', 'BARCELONA', '934565343', 'ANA FERNÁNDEZ', NULL),
('CT04', 'EXPORTASA', 'VALLECAS 34', 'MADRID', '913452378', 'ELVIRA GÓMEZ', NULL),
('CT06', 'CONFECCIONES AMPARO', 'LOS MOROS 23', 'GIJÓN', '985754332', 'LUÍS ÁLVAREZ', NULL),
('CT07', 'LA CASA DEL JUGUETE', 'AMÉRICA 45', 'MADRID', '912649987', 'ELÍAS PÉREZ', NULL),
('CT08', 'JUGUETERÍA SUÁREZ', 'PARIS 123', 'BARCELONA', '933457866', 'JUAN GARCÍA', NULL),
('CT09', 'ALMACÉN POPULAR', 'LAS FUENTES 124', 'BILBAO', '942347127', 'JOSÉ ÁLVAREZ', NULL),
('CT10', 'FERETERÍA EL CLAVO', 'PASEO DE ÁLAMOS 78', 'MADRID', '914354866', 'MANUEL MENÉNDEZ', NULL),
('CT11', 'JUGUETES MARTÍNEZ', 'VIA LAYETANA 245', 'BARCELONA', '936628554', 'FRANCISCO CUEVAS', NULL),
('CT12', 'FERNÁNDEZ SL', 'PASEO DEL MAR 45', 'SANTANDER', '942049586', 'ELISA COLLADO', NULL),
('CT13', 'CONFECCIONES ARTÍMEZ', 'GENERAL PERÓN 45', 'A CORUÑA', '981345239', 'ESTEBAN PASCUAL', NULL),
('CT14', 'DEPORTES GARCÍA', 'GUZMÁN EL BUENO 45', 'MADRID', '913299475', 'ANA JIMÉNEZ', NULL),
('CT15', 'EXCLUSIVAS FERNÁNDEZ', 'LLOBREGAT 250', 'BARCELONA', '939558365', 'LUISA FERNÁNDEZ', NULL),
('CT16', 'DEPORTES MORÁN', 'AUTONOMÍA 45', 'LUGO', '982986944', 'JOSÉ MANZANO', NULL),
('CT17', 'BAZAR FRANCISCO', 'CARMEN 45', 'ZAMORA', '980495288', 'CARLOS BELTRÁN', NULL),
('CT18', 'JUGUETES LA SONRISA', 'LA BAÑEZA 67', 'LEÓN', '987945368', 'FAUSTINO PÉREZ', NULL),
('CT19', 'CONFECCIONES GALÁN', 'FUENCARRAL 78', 'MADRID', '913859234', 'JUAN GARCÍA', NULL),
('CT20', 'LA CURTIDORA', 'OLIVARES 3', 'MÁLAGA', '953756259', 'MARÍA GÓMEZ', NULL),
('CT21', 'LÍNEA JOVEN', 'SIERPES 78', 'SEVILLA', '953452567', 'ASUNCIÓN SALADO', NULL),
('CT22', 'BAZAR EL BARAT', 'DIAGONAL 56', 'BARCELONA', '936692866', 'ELISA DAPENA', NULL),
('CT23', 'EL PALACIO DE LA MODA', 'ORTEGA Y GASSET 129', 'MADRID', '927785235', 'LAURA CARRASCO', NULL),
('CT24', 'SÁEZ Y CÍA', 'INFANTA MERCEDS 23', 'SEVILLA', '954869234', 'MANUEL GUERRA', NULL),
('CT25', 'DEPORTES EL MADRILEÑO', 'CASTILLA 345', 'ZARAGOZA', '976388934', 'CARLOS GONZÁLEZ', NULL),
('CT26', 'FERRETERÍA LA ESCOBA', 'ORENSE 7', 'MADRID', '918459346', 'JOSÉ GARCÍA', NULL),
('CT27', 'JUGUETES EL BARATO', 'VÍA AUGUSTA 245', 'BARCELONA', '933486984', 'ELVIRA IGLESIAS', NULL),
('CT28', 'CONFECCIONES HERMINIA', 'CORRIDA 345', 'GIJÓN', '985597315', 'ABEL GONZÁLEZ', NULL),
('CT30', 'BAZAR EL ARGENTINO', 'ATOCHA 55', 'MADRID', '912495973', 'ADRIÁN ÁLVAREZ', NULL),
('CT31', 'LA TIENDA ELEGANTE', 'EL COMENDADOR 67', 'ZARAGOZA', '975694035', 'JOSÉ PASCUAL', NULL),
('CT32', 'DEPORTES NAUTICOS GARCÍA', 'JUAN FERNÁNDEZ 89', 'ÁVILA', '920268648', 'JUAN CONRADO', NULL),
('CT33', 'CONFECCIONES RUIZ', 'LLOBREGAT 345', 'BARCELONA', '934587615', 'CARLOS SANZ', NULL),
('CT34', 'BAZAR LA FARAONA', 'CASTILLA Y LEÓN 34', 'MADRID', '915483627', 'ANGEL SANTAMARÍA', NULL),
('CT35', 'FERRETERÍA EL MARTILLO', 'CASTELLANOS 205', 'SALAMANCA', '923548965', 'JOAQUÍN FERNANDEZ', NULL),
('CT36', 'JUGUETES EDUCATIVOS SANZ', 'ORENSE 89', 'MADRID', '916872354', 'PEDRO IGLESIAS', NULL),
('CT37', 'ALMACENES FERNANDEZ', 'ANTÓN 67', 'TERUEL', '978564025', 'MARIA ARDANZA', NULL),
('CT38', 'CONFECCIONES MÓNICA', 'MOTORES 67', 'BARCELONA', '935681245', 'PEDRO SERRANO', NULL),
('CT39', 'FERRETERÍA LIMA', 'VALLECAS 45', 'MADRID', '913532785', 'LUIS GARCÍA', NULL),
('CT40', 'DEPORTES EL BRASILEÑO', 'ABEL MARTÍNEZ 67', 'SALAMANCA', '921548762', 'CARLOS GÓMEZ', NULL);

-- --------------------------------------------------------

--
-- Stand-in structure for view `consulta1`
-- (See below for the actual view)
--
CREATE TABLE `consulta1` (
`CÓDIGO ARTÍCULO` varchar(4)
,`PRECIO` decimal(19,4)
,`SECCIÓN` varchar(12)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `consulta2`
-- (See below for the actual view)
--
CREATE TABLE `consulta2` (
`CÓDIGO ARTÍCULO` varchar(4)
,`PRECIO` decimal(19,4)
,`SECCIÓN` varchar(12)
);

-- --------------------------------------------------------

--
-- Table structure for table `pedidos`
--

CREATE TABLE `pedidos` (
  `NÚMERO DE PEDIDO` int NOT NULL,
  `CÓDIGO CLIENTE` varchar(4) COLLATE utf8mb3_spanish_ci DEFAULT NULL,
  `FECHA DE PEDIDO` datetime DEFAULT NULL,
  `FORMA DE PAGO` varchar(8) COLLATE utf8mb3_spanish_ci DEFAULT NULL,
  `DESCUENTO` decimal(19,4) DEFAULT NULL,
  `ENVIADO` tinyint(1) NOT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=315 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_spanish_ci;

--
-- Dumping data for table `pedidos`
--

INSERT INTO `pedidos` (`NÚMERO DE PEDIDO`, `CÓDIGO CLIENTE`, `FECHA DE PEDIDO`, `FORMA DE PAGO`, `DESCUENTO`, `ENVIADO`) VALUES
(1, 'CT01', '2000-03-11 00:00:00', 'CONTADO', '0.0200', 1),
(3, 'CT23', '2000-03-18 00:00:00', 'APLAZADO', '0.0600', 0),
(5, 'CT25', '2000-03-31 00:00:00', 'CONTADO', '0.0900', 0),
(7, 'CT12', '2000-04-12 00:00:00', 'CONTADO', '0.0700', 0),
(8, 'CT01', '2000-04-15 00:00:00', 'TARJETA', '0.0200', 1),
(9, 'CT21', '2000-04-21 00:00:00', 'CONTADO', '0.0400', 0),
(11, 'CT04', '2001-05-01 00:00:00', 'CONTADO', '0.0800', 1),
(12, 'CT06', '2001-05-19 00:00:00', 'CONTADO', '0.0900', 1),
(13, 'CT13', '2000-04-30 00:00:00', 'APLAZADO', '0.0300', 0),
(16, 'CT25', '2001-05-11 00:00:00', 'CONTADO', '0.1200', 0),
(19, 'CT10', '2002-05-22 00:00:00', 'CONTADO', '0.0700', 1),
(21, 'CT16', '2001-05-28 00:00:00', 'CONTADO', '0.0300', 0),
(22, 'CT07', '2000-05-31 00:00:00', 'TARJETA', '0.0500', 1),
(25, 'CT18', '2000-06-02 00:00:00', 'CONTADO', '0.0600', 0),
(26, 'CT09', '2001-06-04 00:00:00', 'APLAZADO', '0.0700', 1),
(27, 'CT34', '2000-06-06 00:00:00', 'CONTADO', '0.0400', 0),
(28, 'CT28', '2000-06-08 00:00:00', 'APLAZADO', '0.0800', 0),
(29, 'CT30', '2001-04-02 00:00:00', 'TARJETA', '0.0600', 0),
(30, 'CT02', '2000-08-15 00:00:00', 'CONTADO', '0.0600', 1),
(31, 'CT30', '2000-06-08 00:00:00', 'TARJETA', '0.0500', 1),
(32, 'CT14', '2001-06-20 00:00:00', 'APLAZADO', '0.0600', 0),
(34, 'CT26', '2002-06-23 00:00:00', 'TARJETA', '0.0500', 0),
(35, 'CT26', '2001-06-30 00:00:00', 'CONTADO', '0.0600', 0),
(37, 'CT24', '2001-07-02 00:00:00', 'TARJETA', '0.0300', 1),
(39, 'CT20', '2001-07-08 00:00:00', 'TARJETA', '0.0600', 1),
(40, 'CT04', '2002-07-12 00:00:00', 'CONTADO', '0.1200', 0),
(42, 'CT34', '2002-07-15 00:00:00', 'APLAZADO', '0.0700', 1),
(43, 'CT09', '2001-07-18 00:00:00', 'CONTADO', '0.0700', 0),
(44, 'CT34', '2002-07-20 00:00:00', 'APLAZADO', '0.0400', 0),
(45, 'CT30', '2002-07-22 00:00:00', 'TARJETA', '0.0700', 0),
(46, 'CT31', '2002-07-25 00:00:00', 'CONTADO', '0.0600', 0),
(47, 'CT34', '2000-07-31 00:00:00', 'APLAZADO', '0.0800', 0),
(48, 'CT18', '2002-08-30 00:00:00', 'CONTADO', '0.0300', 0),
(49, 'CT28', '2002-09-02 00:00:00', 'CONTADO', '0.0300', 0),
(50, 'CT09', '2002-09-05 00:00:00', 'APLAZADO', '0.0800', 0),
(51, 'CT09', '2002-09-05 00:00:00', 'CONTADO', '0.0500', 1),
(63, 'CT28', '2000-09-10 00:00:00', 'CONTADO', '0.0900', 0),
(72, 'CT01', '2002-08-18 00:00:00', 'CONTADO', '0.0500', 1),
(73, 'CT01', '2001-08-02 00:00:00', 'CONTADO', '0.0700', 0),
(74, 'CT01', '2002-09-17 00:00:00', 'APLAZADO', '0.0800', 0),
(75, 'CT01', '2002-09-30 00:00:00', 'TARJETA', '0.1200', 0),
(76, 'CT01', '2002-10-19 00:00:00', 'CONTADO', '0.0400', 1),
(77, 'CT01', '2000-10-28 00:00:00', 'CONTADO', '0.0500', 0),
(79, 'CT34', '2000-12-12 00:00:00', 'CONTADO', '0.0500', 0),
(85, 'CT04', '2002-12-23 00:00:00', 'TARJETA', '0.0400', 0),
(86, 'CT09', '2001-12-24 00:00:00', 'APLAZADO', '0.0300', 0),
(98, 'CT01', '2001-12-27 00:00:00', 'CONTADO', '0.0800', 1),
(102, 'CT06', '2001-01-12 00:00:00', 'CONTADO', '0.0700', 1),
(103, 'CT02', '2001-01-24 00:00:00', 'CONTADO', '0.0400', 0),
(105, 'CT30', '2001-01-01 00:00:00', 'APLAZADO', '0.0900', 0),
(5005, 'CT30', '2002-08-10 00:00:00', 'TARJETA', '0.0000', 1),
(5050, 'CT30', '2002-03-27 00:00:00', 'TARJETA', '0.0000', 1);

-- --------------------------------------------------------

--
-- Table structure for table `productos`
--

CREATE TABLE `productos` (
  `CÓDIGO ARTÍCULO` varchar(4) COLLATE utf8mb3_spanish_ci NOT NULL,
  `SECCIÓN` varchar(12) COLLATE utf8mb3_spanish_ci DEFAULT NULL,
  `NOMBRE ARTÍCULO` varchar(255) COLLATE utf8mb3_spanish_ci DEFAULT NULL,
  `PRECIO` decimal(19,4) DEFAULT NULL,
  `FECHA` datetime DEFAULT NULL,
  `IMPORTADO` tinyint(1) NOT NULL DEFAULT '0',
  `PAÍS DE ORIGEN` varchar(255) COLLATE utf8mb3_spanish_ci DEFAULT NULL,
  `FOTO` longblob
) ENGINE=InnoDB AVG_ROW_LENGTH=409 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_spanish_ci;

--
-- Dumping data for table `productos`
--

INSERT INTO `productos` (`CÓDIGO ARTÍCULO`, `SECCIÓN`, `NOMBRE ARTÍCULO`, `PRECIO`, `FECHA`, `IMPORTADO`, `PAÍS DE ORIGEN`, `FOTO`) VALUES
('AR01', 'FERRETERÍA', 'DESTORNILLADOR', '6.6280', '2000-10-22 00:00:00', 0, 'ESPAÑA', ''),
('AR02', 'CONFECCIÓN', 'TRAJE CABALLERO', '284.5769', '2002-03-11 00:00:00', 1, 'ITALIA', ''),
('AR03', 'JUGUETERÍA', 'COCHE TELEDIRIGIDO', '159.4462', '2002-05-26 00:00:00', 1, 'MARRUECOS', ''),
('AR04', 'DEPORTES', 'RAQUETA TENIS', '93.4694', '2000-03-20 00:00:00', 1, 'USA', ''),
('AR06', 'DEPORTES', 'MANCUERNAS', '60.0000', '2000-09-13 00:00:00', 1, 'USA', ''),
('AR07', 'CONFECCIÓN', 'SERRUCHO', '30.2045', '2001-03-23 00:00:00', 1, 'FRANCIA', ''),
('AR08', 'JUGUETERÍA', 'CORREPASILLOS', '103.3356', '2000-04-11 00:00:00', 1, 'JAPÓN', ''),
('AR09', 'CONFECCIÓN', 'PANTALÓN SEÑORA', '174.2310', '2000-01-10 00:00:00', 1, 'MARRUECOS', ''),
('AR10', 'JUGUETERÍA', 'CONSOLA VIDEO', '442.5444', '2002-09-24 00:00:00', 1, 'USA', ''),
('AR11', 'CERÁMICA', 'TUBOS', '168.4253', '2000-02-04 00:00:00', 1, 'CHINA', ''),
('AR12', 'FERRETERÍA', 'LLAVE INGLESA', '24.3986', '2001-05-23 00:00:00', 1, 'USA', ''),
('AR13', 'CONFECCIÓN', 'CAMISA CABALLERO', '67.1306', '2002-08-11 00:00:00', 0, 'ESPAÑA', ''),
('AR14', 'JUGUETERÍA', 'TREN ELÉCTRICO', '1505.3766', '2001-07-03 00:00:00', 1, 'JAPÓN', ''),
('AR15', 'CERÁMICA', 'PLATO DECORATIVO', '54.0911', '2000-06-07 00:00:00', 1, 'CHINA', ''),
('AR16', 'FERRETERÍA', 'ALICATES', '6.7362', '2000-04-17 00:00:00', 1, 'ITALIA', ''),
('AR17', 'JUGUETERÍA', 'MUÑECA ANDADORA', '105.0593', '2001-01-04 00:00:00', 0, 'ESPAÑA', ''),
('AR18', 'DEPORTES', 'PISTOLA OLÍMPICA', '46.7347', '2001-02-02 00:00:00', 1, 'SUECIA', ''),
('AR19', 'CONFECCIÓN', 'BLUSA SRA.', '101.0566', '2000-03-18 00:00:00', 1, 'CHINA', ''),
('AR20', 'CERÁMICA', 'JUEGO DE TE', '43.2728', '2001-01-15 00:00:00', 1, 'CHINA', ''),
('AR21', 'CERÁMICA', 'CENICERO', '19.7468', '2001-07-02 00:00:00', 1, 'JAPÓN', ''),
('AR22', 'FERRETERÍA', 'MARTILLO', '11.3952', '2001-09-04 00:00:00', 0, 'ESPAÑA', ''),
('AR23', 'CONFECCIÓN', 'CAZADORA PIEL', '522.6930', '2001-07-10 00:00:00', 1, 'ITALIA', ''),
('AR24', 'DEPORTES', 'BALÓN RUGBY', '111.6440', '2000-11-11 00:00:00', 1, 'USA', ''),
('AR25', 'DEPORTES', 'BALÓN BALONCESTO', '75.2731', '2001-06-25 00:00:00', 1, 'JAPÓN', ''),
('AR26', 'JUGUETERÍA', 'FUERTE DE SOLDADOS', '143.7020', '2000-11-25 00:00:00', 1, 'JAPÓN', ''),
('AR27', 'CONFECCIÓN', 'ABRIGO CABALLERO', '500000.0000', '2002-04-05 00:00:00', 1, 'ITALIA', ''),
('AR28', 'DEPORTES', 'BALÓN FÚTBOL', '43.9147', '2002-07-04 00:00:00', 0, 'ESPAÑA', ''),
('AR29', 'CONFECCIÓN', 'ABRIGO SRA', '360.0736', '2001-05-03 00:00:00', 1, 'MARRUECOS', ''),
('AR30', 'FERRETERÍA', 'DESTORNILLADOR', '9.0584', '2002-02-20 00:00:00', 1, 'FRANCIA', ''),
('AR31', 'JUGUETERÍA', 'PISTOLA CON SONIDOS', '57.2500', '2001-04-15 00:00:00', 0, 'ESPAÑA', ''),
('AR32', 'DEPORTES', 'CRONÓMETRO', '439.1764', '2002-01-03 00:00:00', 1, 'USA', ''),
('AR33', 'CERÁMICA', 'MACETA', '29.0434', '2000-02-23 00:00:00', 0, 'ESPAÑA', ''),
('AR34', 'OFICINA', 'PIE DE LÁMPARA', '39.7606', '2001-05-27 00:00:00', 1, 'TURQUÍA', ''),
('AR35', 'FERRETERÍA', 'LIMA GRANDE', '22.0692', '2002-08-10 00:00:00', 0, 'ESPAÑA', ''),
('AR36', 'FERRETERÍA', 'JUEGO DE BROCAS', '15.0950', '2002-07-04 00:00:00', 1, 'TAIWÁN', ''),
('AR37', 'CONFECCIÓN', 'CINTURÓN DE PIEL', '4.3273', '2002-05-12 00:00:00', 0, 'ESPAÑA', ''),
('AR38', 'DEPORTES', 'CAÑA DE PESCA', '270.0000', '2000-02-14 00:00:00', 1, 'USA', ''),
('AR39', 'CERÁMICA', 'JARRA CHINA', '127.7704', '2002-09-02 00:00:00', 1, 'CHINA', ''),
('AR40', 'DEPORTES', 'BOTA ALPINISMO', '144.0000', '2002-05-05 00:00:00', 0, 'ESPAÑA', ''),
('AR41', 'DEPORTES', 'PALAS DE PING PONG', '21.6000', '2002-02-02 00:00:00', 0, 'ESPAÑA', ''),
('AR94', 'DEPORTES', NULL, '200.0000', NULL, 0, NULL, NULL),
('AR96', 'DEPORTES', NULL, '5.0000', NULL, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `productos- pedidos`
--

CREATE TABLE `productos- pedidos` (
  `NÚMERO DE PEDIDO` int NOT NULL,
  `CÓDIGO ARTÍCULO` varchar(4) COLLATE utf8mb3_spanish_ci NOT NULL,
  `UNIDADES` double(15,5) DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=297 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_spanish_ci;

--
-- Dumping data for table `productos- pedidos`
--

INSERT INTO `productos- pedidos` (`NÚMERO DE PEDIDO`, `CÓDIGO ARTÍCULO`, `UNIDADES`) VALUES
(1, 'AR01', 11.00000),
(1, 'AR04', 10.00000),
(1, 'AR15', 4.00000),
(1, 'AR22', 18.00000),
(3, 'AR02', 20.00000),
(3, 'AR22', 3.00000),
(5, 'AR04', 16.00000),
(7, 'AR06', 16.00000),
(8, 'AR02', 6.00000),
(8, 'AR06', 5.00000),
(8, 'AR07', 6.00000),
(8, 'AR10', 2.00000),
(8, 'AR12', 30.00000),
(8, 'AR15', 15.00000),
(8, 'AR18', 20.00000),
(8, 'AR19', 18.00000),
(8, 'AR25', 5.00000),
(8, 'AR32', 15.00000),
(8, 'AR33', 18.00000),
(8, 'AR34', 5.00000),
(8, 'AR35', 24.00000),
(9, 'AR06', 14.00000),
(11, 'AR08', 1.00000),
(12, 'AR08', 12.00000),
(13, 'AR08', 8.00000),
(16, 'AR10', 17.00000),
(19, 'AR13', 4.00000),
(21, 'AR15', 11.00000),
(22, 'AR17', 6.00000),
(22, 'AR26', 4.00000),
(22, 'AR28', 21.00000),
(25, 'AR19', 12.00000),
(26, 'AR19', 12.00000),
(27, 'AR21', 11.00000),
(28, 'AR21', 22.00000),
(29, 'AR22', 12.00000),
(30, 'AR23', 33.00000),
(31, 'AR24', 31.00000),
(32, 'AR25', 11.00000),
(34, 'AR22', 7.00000),
(34, 'AR27', 3.00000),
(35, 'AR22', 9.00000),
(35, 'AR27', 12.00000),
(37, 'AR27', 11.00000),
(39, 'AR29', 22.00000),
(40, 'AR30', 1.00000),
(42, 'AR31', 21.00000),
(43, 'AR32', 3.00000),
(44, 'AR22', 22.00000),
(45, 'AR36', 21.00000),
(46, 'AR37', 8.00000),
(47, 'AR38', 12.00000),
(48, 'AR38', 13.00000),
(49, 'AR39', 13.00000),
(50, 'AR39', 1.00000);

-- --------------------------------------------------------

--
-- Structure for view `consulta1`
--
DROP TABLE IF EXISTS `consulta1`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `consulta1`  AS SELECT `p`.`CÓDIGO ARTÍCULO` AS `CÓDIGO ARTÍCULO`, `p`.`SECCIÓN` AS `SECCIÓN`, `p`.`PRECIO` AS `PRECIO` FROM `productos` AS `p` WHERE (`p`.`PRECIO` > 100) ORDER BY `p`.`PRECIO` AS `DESCdesc` ASC  ;

-- --------------------------------------------------------

--
-- Structure for view `consulta2`
--
DROP TABLE IF EXISTS `consulta2`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `consulta2`  AS SELECT `c`.`CÓDIGO ARTÍCULO` AS `CÓDIGO ARTÍCULO`, `c`.`SECCIÓN` AS `SECCIÓN`, `c`.`PRECIO` AS `PRECIO` FROM `consulta1` AS `c` WHERE (`c`.`SECCIÓN` = 'deportes') WITH CASCADED CHECK OPTION  ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`CÓDIGO CLIENTE`);

--
-- Indexes for table `pedidos`
--
ALTER TABLE `pedidos`
  ADD PRIMARY KEY (`NÚMERO DE PEDIDO`),
  ADD KEY `{BC8A638D-F24F-4855-8C3D-726EBF9F62EA}` (`CÓDIGO CLIENTE`);

--
-- Indexes for table `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`CÓDIGO ARTÍCULO`);

--
-- Indexes for table `productos- pedidos`
--
ALTER TABLE `productos- pedidos`
  ADD PRIMARY KEY (`NÚMERO DE PEDIDO`,`CÓDIGO ARTÍCULO`),
  ADD KEY `{610433C3-701F-41EF-A644-338F18850D0D}` (`CÓDIGO ARTÍCULO`),
  ADD KEY `PEDIDOSPRODUCTOS- PEDIDOS` (`NÚMERO DE PEDIDO`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `pedidos`
--
ALTER TABLE `pedidos`
  ADD CONSTRAINT `FK_pedidos_clientes_CÓDIGO CLIENTE` FOREIGN KEY (`CÓDIGO CLIENTE`) REFERENCES `clientes` (`CÓDIGO CLIENTE`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `productos- pedidos`
--
ALTER TABLE `productos- pedidos`
  ADD CONSTRAINT `FK_productos- pedidos_pedidos_NÚMERO DE PEDIDO` FOREIGN KEY (`NÚMERO DE PEDIDO`) REFERENCES `pedidos` (`NÚMERO DE PEDIDO`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_productos- pedidos_productos_CÓDIGO ARTÍCULO` FOREIGN KEY (`CÓDIGO ARTÍCULO`) REFERENCES `productos` (`CÓDIGO ARTÍCULO`) ON DELETE CASCADE ON UPDATE CASCADE;
--
-- Database: `reglas`
--
CREATE DATABASE IF NOT EXISTS `reglas` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci;
USE `reglas`;

-- --------------------------------------------------------

--
-- Table structure for table `practicas`
--

CREATE TABLE `practicas` (
  `id` int NOT NULL,
  `nota` float DEFAULT NULL
) ;

--
-- Dumping data for table `practicas`
--

INSERT INTO `practicas` (`id`, `nota`) VALUES
(1, 8),
(2, 7),
(4, 8),
(5, 7);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `practicas`
--
ALTER TABLE `practicas`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `practicas`
--
ALTER TABLE `practicas`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;
--
-- Database: `rubros`
--
CREATE DATABASE IF NOT EXISTS `rubros` DEFAULT CHARACTER SET utf8mb3 COLLATE utf8mb3_spanish_ci;
USE `rubros`;

-- --------------------------------------------------------

--
-- Table structure for table `gestion`
--

CREATE TABLE `gestion` (
  `gestion_id` int NOT NULL,
  `FECHA` datetime DEFAULT NULL,
  `COD-PRODUCTO` int NOT NULL,
  `COD-MARCA` int NOT NULL,
  `COD-ARTICULO` char(10) COLLATE utf8mb3_spanish_ci DEFAULT NULL,
  `CANTIDAD` double DEFAULT NULL,
  `PRECIO_UNIT.` float DEFAULT '0',
  `IMPORTE` float DEFAULT NULL,
  `DESCUENTO` float DEFAULT '0',
  `SUBTOTAL` float DEFAULT '0',
  `IVA` float DEFAULT '0',
  `TOTAL` float DEFAULT '0',
  `DESC-PRODUCTO` varchar(255) COLLATE utf8mb3_spanish_ci DEFAULT NULL,
  `DESC-RUBRO` varchar(255) COLLATE utf8mb3_spanish_ci DEFAULT NULL,
  `DESC-MARCA` varchar(255) COLLATE utf8mb3_spanish_ci DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=356 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_spanish_ci;

--
-- Dumping data for table `gestion`
--

INSERT INTO `gestion` (`gestion_id`, `FECHA`, `COD-PRODUCTO`, `COD-MARCA`, `COD-ARTICULO`, `CANTIDAD`, `PRECIO_UNIT.`, `IMPORTE`, `DESCUENTO`, `SUBTOTAL`, `IVA`, `TOTAL`, `DESC-PRODUCTO`, `DESC-RUBRO`, `DESC-MARCA`) VALUES
(1, '2005-03-05 00:00:00', 4, 2, '4p2m', 0.3, 165, 49.5, 0, 49.5, 0.23, 60.885, 'jamon', 'fiambres', 'CATTIVELLI'),
(2, '2005-03-05 00:00:00', 8, 9, '8p9m', 0.3, 79, 23.7, 0, 23.7, 0.14, 27.018, 'queso sandwich', 'lacteos', 'PARMALAT'),
(3, '2005-03-05 00:00:00', 11, 11, '11p11m', 2, 12, 24, 0, 24, 0.14, 27.36, 'leche entera', 'lacteos', 'CAPROLET'),
(4, '2005-03-05 00:00:00', 23, 18, '23p18m', 0.5, 32, 16, 0, 16, 0, 16, 'pan porteño', 'panaderia', 'PROPIO'),
(5, '2005-03-03 00:00:00', 5, 6, '5p6m', 0.1, 78, 7.8, 0, 7.8, 0.23, 9.594, 'leonesa', 'fiambres', 'CENTENARIO'),
(6, '2005-03-03 00:00:00', 4, 8, '4p8m', 0.3, 121, 36.3, 0, 36.3, 0.23, 44.649, 'jamon', 'fiambres', 'SHNECK'),
(7, '2005-03-03 00:00:00', 13, 11, '13p11m', 1, 112, 112, 0, 112, 0.14, 127.68, 'queso colonia', 'lacteos', 'CAPROLET'),
(8, '2005-03-03 00:00:00', 22, 18, '22p18m', 0.5, 30, 15, 0, 15, 0, 15, 'pan flauta', 'panaderia', 'PROPIO'),
(9, '2005-03-06 00:00:00', 17, 17, '17p17m', 6, 4, 24, 0, 24, 0, 24, 'pan viena', 'panaderia', 'FARGO'),
(10, '2005-03-06 00:00:00', 11, 14, '11p14m', 3, 10, 30, 0, 30, 0.14, 34.2, 'leche entera', 'lacteos', 'LACTERIA'),
(11, '2005-03-06 00:00:00', 8, 10, '8p10m', 0.4, 81, 32.4, 0, 32.4, 0.14, 36.936, 'queso sandwich', 'lacteos', 'CONAPROLE'),
(12, '2005-03-06 00:00:00', 12, 13, '12p13m', 2, 15, 30, 0, 30, 0.14, 34.2, 'yogurt', 'lacteos', 'MILKY'),
(13, '2005-03-06 00:00:00', 13, 11, '13p11m', 1.3, 112, 145.6, 0, 145.6, 0.14, 165.984, 'queso colonia', 'lacteos', 'CAPROLET'),
(14, '2005-03-06 00:00:00', 3, 4, '3p4m', 0.25, 104, 26, 0, 26, 0.23, 31.98, 'paleta', 'fiambres', 'SARUBI'),
(15, '2005-03-07 00:00:00', 5, 7, '5p7m', 0.3, 82, 24.6, 0.1, 22.14, 0.23, 27.2322, 'leonesa', 'fiambres', 'KALI'),
(16, '2005-03-07 00:00:00', 19, 18, '19p18m', 0.5, 35, 17.5, 0, 17.5, 0, 17.5, 'pan felipe', 'panaderia', 'PROPIO'),
(17, '2005-03-07 00:00:00', 16, 11, '16p11m', 3, 11, 33, 0, 33, 0.14, 37.62, 'leche descremada', 'lacteos', 'CAPROLET'),
(18, '2005-03-07 00:00:00', 14, 12, '14p12m', 0.8, 70.5, 56.4, 0, 56.4, 0.14, 64.296, 'queso semiduro', 'lacteos', 'CLALDY'),
(19, '2005-03-07 00:00:00', 6, 8, '6p8m', 1.5, 72, 108, 0.1, 97.2, 0.23, 119.556, 'panchos', 'fiambres', 'SHNECK'),
(20, '2005-03-07 00:00:00', 4, 3, '4p3m', 0.4, 162, 64.8, 0.1, 58.32, 0.23, 71.7336, 'jamon', 'fiambres', 'OTTONELO'),
(21, '2005-03-07 00:00:00', 18, 17, '18p17m', 0.3, 45, 13.5, 0, 13.5, 0, 13.5, 'pan sandwich', 'panaderia', 'FARGO'),
(22, '2005-03-07 00:00:00', 1, 2, '1p2m', 0.3, 145, 43.5, 0.1, 39.15, 0.23, 48.1545, 'salame', 'fiambres', 'CATTIVELLI'),
(23, '2005-03-07 00:00:00', 3, 7, '3p7m', 0.7, 78, 54.6, 0.1, 49.14, 0.23, 60.4422, 'paleta', 'fiambres', 'KALI'),
(24, '2005-03-07 00:00:00', 9, 9, '9p9m', 0.2, 110, 22, 0, 22, 0.14, 25.08, 'queso dambo', 'lacteos', 'PARMALAT'),
(25, '2005-03-04 00:00:00', 10, 11, '10p11m', 0.1, 117, 11.7, 0, 11.7, 0.14, 13.338, 'queso rallado', 'lacteos', 'CAPROLET'),
(26, '2005-03-04 00:00:00', 12, 14, '12p14m', 2, 19, 38, 0, 38, 0.14, 43.32, 'yogurt', 'lacteos', 'LACTERIA'),
(27, '2005-03-04 00:00:00', 7, 13, '7p13m', 0.3, 60, 18, 0, 18, 0.14, 20.52, 'queso muzarela', 'lacteos', 'MILKY'),
(28, '2005-03-04 00:00:00', 15, 10, '15p10m', 0.2, 122, 24.4, 0, 24.4, 0.14, 27.816, 'manteca', 'lacteos', 'CONAPROLE'),
(29, '2005-03-04 00:00:00', 16, 11, '16p11m', 1, 11, 11, 0, 11, 0.14, 12.54, 'leche descremada', 'lacteos', 'CAPROLET'),
(30, '2005-03-04 00:00:00', 12, 10, '12p10m', 3, 21, 63, 0, 63, 0.14, 71.82, 'yogurt', 'lacteos', 'CONAPROLE'),
(31, '2005-03-04 00:00:00', 3, 4, '3p4m', 0.5, 104, 52, 0, 52, 0.23, 63.96, 'paleta', 'fiambres', 'SARUBI'),
(32, '2005-03-04 00:00:00', 5, 7, '5p7m', 0.3, 82, 24.6, 0, 24.6, 0.23, 30.258, 'leonesa', 'fiambres', 'KALI'),
(33, '2005-03-04 00:00:00', 8, 14, '8p14m', 0.1, 65, 6.5, 0, 6.5, 0.14, 7.41, 'queso sandwich', 'lacteos', 'LACTERIA'),
(34, '2005-03-04 00:00:00', 17, 16, '17p16m', 0.5, 4, 2, 0.15, 1.7, 0, 1.7, 'pan viena', 'panaderia', 'MALLORQUINA'),
(35, '2005-03-01 00:00:00', 21, 17, '21p17m', 10, 4, 40, 0, 40, 0, 40, 'pan tortuga', 'panaderia', 'FARGO'),
(36, '2005-03-01 00:00:00', 22, 18, '22p18m', 2, 30, 60, 0, 60, 0, 60, 'pan flauta', 'panaderia', 'PROPIO'),
(37, '2005-03-01 00:00:00', 20, 18, '20p18m', 1, 45, 45, 0, 45, 0, 45, 'pan manteca', 'panaderia', 'PROPIO'),
(38, '2005-03-01 00:00:00', 11, 9, '11p9m', 4, 11, 44, 0, 44, 0.14, 50.16, 'leche entera', 'lacteos', 'PARMALAT'),
(39, '2005-03-01 00:00:00', 6, 3, '6p3m', 2, 58, 116, 0, 116, 0.23, 142.68, 'panchos', 'fiambres', 'OTTONELO'),
(40, '2005-03-01 00:00:00', 4, 5, '4p5m', 0.25, 117, 29.25, 0, 29.25, 0.23, 35.9775, 'jamon', 'fiambres', 'RAUSA'),
(41, '2005-03-01 00:00:00', 10, 12, '10p12m', 0.35, 125, 43.75, 0, 43.75, 0.14, 49.875, 'queso rallado', 'lacteos', 'CLALDY'),
(42, '2005-03-02 00:00:00', 12, 12, '12p12m', 2, 15.5, 31, 0.05, 29.45, 0.14, 33.573, 'yogurt', 'lacteos', 'CLALDY'),
(43, '2005-03-02 00:00:00', 22, 18, '22p18m', 1.5, 30, 45, 0, 45, 0, 45, 'pan flauta', 'panaderia', 'PROPIO'),
(44, '2005-03-02 00:00:00', 2, 5, '2p5m', 0.8, 90, 72, 0, 72, 0.23, 88.56, 'mortadela', 'fiambres', 'RAUSA'),
(45, '2005-03-02 00:00:00', 8, 10, '8p10m', 0.45, 81, 36.45, 0.05, 34.6275, 0.14, 39.4754, 'queso sandwich', 'lacteos', 'CONAPROLE'),
(46, '2005-03-02 00:00:00', 13, 11, '13p11m', 1.45, 112, 162.4, 0.05, 154.28, 0.14, 175.879, 'queso colonia', 'lacteos', 'CAPROLET');

-- --------------------------------------------------------

--
-- Table structure for table `marcas`
--

CREATE TABLE `marcas` (
  `COD-MARCA` int NOT NULL,
  `DESC-MARCA` varchar(12) COLLATE utf8mb3_spanish_ci DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=910 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_spanish_ci;

--
-- Dumping data for table `marcas`
--

INSERT INTO `marcas` (`COD-MARCA`, `DESC-MARCA`) VALUES
(1, 'CONSTANCIA'),
(2, 'CATTIVELLI'),
(3, 'OTTONELO'),
(4, 'SARUBI'),
(5, 'RAUSA'),
(6, 'CENTENARIO'),
(7, 'KALI'),
(8, 'SHNECK'),
(9, 'PARMALAT'),
(10, 'CONAPROLE'),
(11, 'CAPROLET'),
(12, 'CLALDY'),
(13, 'MILKY'),
(14, 'LACTERIA'),
(15, 'SORCHANTES'),
(16, 'MALLORQUINA'),
(17, 'FARGO'),
(18, 'PROPIO');

-- --------------------------------------------------------

--
-- Table structure for table `precios`
--

CREATE TABLE `precios` (
  `precios_id` int NOT NULL,
  `COD-PRODUCTO` int NOT NULL,
  `DESC-PRODUCTO` varchar(255) COLLATE utf8mb3_spanish_ci DEFAULT NULL,
  `COD-RUBRO` int DEFAULT NULL,
  `DESC-RUBRO` varchar(255) COLLATE utf8mb3_spanish_ci DEFAULT NULL,
  `UNIDAD` varchar(12) COLLATE utf8mb3_spanish_ci DEFAULT NULL,
  `COD-MARCA` int NOT NULL,
  `DESC-MARCA` varchar(255) COLLATE utf8mb3_spanish_ci DEFAULT NULL,
  `COD-ARTICULO` char(8) COLLATE utf8mb3_spanish_ci DEFAULT NULL,
  `IMPORTE` double DEFAULT NULL,
  `IVA_Incl.` float DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=227 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_spanish_ci;

--
-- Dumping data for table `precios`
--

INSERT INTO `precios` (`precios_id`, `COD-PRODUCTO`, `DESC-PRODUCTO`, `COD-RUBRO`, `DESC-RUBRO`, `UNIDAD`, `COD-MARCA`, `DESC-MARCA`, `COD-ARTICULO`, `IMPORTE`, `IVA_Incl.`) VALUES
(1, 1, 'salame', 1, 'fiambres', 'kilo', 4, 'SARUBI', '1P4M', 140, 32.2),
(2, 1, 'salame', 1, 'fiambres', 'kilo', 2, 'CATTIVELLI', '1P2M', 145, 33.35),
(3, 1, 'salame', 1, 'fiambres', 'kilo', 3, 'OTTONELO', '1P3M', 139, 31.97),
(4, 2, 'mortadela', 1, 'fiambres', 'kilo', 2, 'CATTIVELLI', '2P2M', 88, 20.24),
(5, 2, 'mortadela', 1, 'fiambres', 'kilo', 3, 'OTTONELO', '2P3M', 90, 20.7),
(6, 2, 'mortadela', 1, 'fiambres', 'kilo', 6, 'CENTENARIO', '2P6M', 110, 25.3),
(7, 2, 'mortadela', 1, 'fiambres', 'kilo', 5, 'RAUSA', '2P5M', 90, 20.7),
(8, 2, 'mortadela', 1, 'fiambres', 'kilo', 8, 'SHNECK', '2P8M', 92, 21.16),
(9, 3, 'paleta', 1, 'fiambres', 'kilo', 7, 'KALI', '3P7M', 78, 17.94),
(10, 3, 'paleta', 1, 'fiambres', 'kilo', 8, 'SHNECK', '3P8M', 117, 26.91),
(11, 3, 'paleta', 1, 'fiambres', 'kilo', 4, 'SARUBI', '3P4M', 104, 23.92),
(12, 4, 'jamon', 1, 'fiambres', 'kilo', 2, 'CATTIVELLI', '4P2M', 165, 37.95),
(13, 4, 'jamon', 1, 'fiambres', 'kilo', 3, 'OTTONELO', '4P3M', 162, 37.26),
(14, 4, 'jamon', 1, 'fiambres', 'kilo', 6, 'CENTENARIO', '4P6M', 143, 32.89),
(15, 4, 'jamon', 1, 'fiambres', 'kilo', 8, 'SHNECK', '4P8M', 121, 27.83),
(16, 4, 'jamon', 1, 'fiambres', 'kilo', 5, 'RAUSA', '4P5M', 117, 26.91),
(17, 5, 'leonesa', 1, 'fiambres', 'kilo', 6, 'CENTENARIO', '5P6M', 78, 17.94),
(18, 5, 'leonesa', 1, 'fiambres', 'kilo', 7, 'KALI', '5P7M', 82, 18.86),
(19, 5, 'leonesa', 1, 'fiambres', 'kilo', 5, 'RAUSA', '5P5M', 67, 15.41),
(20, 6, 'panchos', 1, 'fiambres', 'kilo', 3, 'OTTONELO', '6P3M', 58, 13.34),
(21, 6, 'panchos', 1, 'fiambres', 'kilo', 8, 'SHNECK', '6P8M', 72, 16.56),
(22, 7, 'queso muzarela', 2, 'lacteos', 'kilo', 9, 'PARMALAT', '7P9M', 74, 10.36),
(23, 7, 'queso muzarela', 2, 'lacteos', 'kilo', 10, 'CONAPROLE', '7P10M', 84, 11.76),
(24, 7, 'queso muzarela', 2, 'lacteos', 'kilo', 13, 'MILKY', '7P13M', 60, 8.4),
(25, 7, 'queso muzarela', 2, 'lacteos', 'kilo', 11, 'CAPROLET', '7P11M', 92, 12.88),
(26, 8, 'queso sandwich', 2, 'lacteos', 'kilo', 9, 'PARMALAT', '8P9M', 79, 11.06),
(27, 8, 'queso sandwich', 2, 'lacteos', 'kilo', 10, 'CONAPROLE', '8P10M', 81, 11.34),
(28, 8, 'queso sandwich', 2, 'lacteos', 'kilo', 13, 'MILKY', '8P13M', 77, 10.78),
(29, 8, 'queso sandwich', 2, 'lacteos', 'kilo', 14, 'LACTERIA', '8P14M', 65, 9.1),
(30, 9, 'queso dambo', 2, 'lacteos', 'kilo', 9, 'PARMALAT', '9P9M', 110, 15.4),
(31, 9, 'queso dambo', 2, 'lacteos', 'kilo', 10, 'CONAPROLE', '9P10M', 112, 15.68),
(32, 9, 'queso dambo', 2, 'lacteos', 'kilo', 13, 'MILKY', '9P13M', 98, 13.72),
(33, 10, 'queso rallado', 2, 'lacteos', 'kilo', 12, 'CLALDY', '10P12M', 125, 17.5),
(34, 10, 'queso rallado', 2, 'lacteos', 'kilo', 11, 'CAPROLET', '10P11M', 117, 16.38),
(35, 10, 'queso rallado', 2, 'lacteos', 'kilo', 14, 'LACTERIA', '10P14M', 109, 15.26),
(36, 11, 'leche entera', 2, 'lacteos', 'litro', 9, 'PARMALAT', '11P9M', 11, 1.54),
(37, 11, 'leche entera', 2, 'lacteos', 'litro', 10, 'CONAPROLE', '11P10M', 11.5, 1.61),
(38, 11, 'leche entera', 2, 'lacteos', 'litro', 11, 'CAPROLET', '11P11M', 12, 1.68),
(39, 11, 'leche entera', 2, 'lacteos', 'litro', 14, 'LACTERIA', '11P14M', 10, 1.4),
(40, 12, 'yogurt', 2, 'lacteos', 'litro', 9, 'PARMALAT', '12P9M', 18, 2.52),
(41, 12, 'yogurt', 2, 'lacteos', 'litro', 14, 'LACTERIA', '12P14M', 19, 2.66),
(42, 12, 'yogurt', 2, 'lacteos', 'litro', 13, 'MILKY', '12P13M', 15, 2.1),
(43, 12, 'yogurt', 2, 'lacteos', 'litro', 12, 'CLALDY', '12P12M', 15.5, 2.17),
(44, 12, 'yogurt', 2, 'lacteos', 'litro', 11, 'CAPROLET', '12P11M', 16, 2.24),
(45, 12, 'yogurt', 2, 'lacteos', 'litro', 10, 'CONAPROLE', '12P10M', 21, 2.94),
(46, 13, 'queso colonia', 2, 'lacteos', 'kilo', 13, 'MILKY', '13P13M', 70, 9.8),
(47, 13, 'queso colonia', 2, 'lacteos', 'kilo', 14, 'LACTERIA', '13P14M', 76, 10.64),
(48, 13, 'queso colonia', 2, 'lacteos', 'kilo', 12, 'CLALDY', '13P12M', 89, 12.46),
(49, 13, 'queso colonia', 2, 'lacteos', 'kilo', 11, 'CAPROLET', '13P11M', 112, 15.68),
(50, 14, 'queso semiduro', 2, 'lacteos', 'kilo', 13, 'MILKY', '14P13M', 98, 13.72),
(51, 14, 'queso semiduro', 2, 'lacteos', 'kilo', 14, 'LACTERIA', '14P14M', 95, 13.3),
(52, 14, 'queso semiduro', 2, 'lacteos', 'kilo', 12, 'CLALDY', '14P12M', 70.5, 9.87),
(53, 14, 'queso semiduro', 2, 'lacteos', 'kilo', 11, 'CAPROLET', '14P11M', 104, 14.56),
(54, 15, 'manteca', 2, 'lacteos', 'kilo', 10, 'CONAPROLE', '15P10M', 122, 17.08),
(55, 15, 'manteca', 2, 'lacteos', 'kilo', 9, 'PARMALAT', '15P9M', 119, 16.66),
(56, 16, 'leche descremada', 2, 'lacteos', 'litro', 9, 'PARMALAT', '16P9M', 12, 1.68),
(57, 16, 'leche descremada', 2, 'lacteos', 'litro', 10, 'CONAPROLE', '16P10M', 11.5, 1.61),
(58, 16, 'leche descremada', 2, 'lacteos', 'litro', 11, 'CAPROLET', '16P11M', 11, 1.54),
(59, 16, 'leche descremada', 2, 'lacteos', 'litro', 14, 'LACTERIA', '16P14M', 10.5, 1.47),
(60, 17, 'pan viena', 3, 'panaderia', 'unidad', 17, 'FARGO', '17P17M', 4, 0),
(61, 17, 'pan viena', 3, 'panaderia', 'unidad', 16, 'MALLORQUINA', '17P16M', 4, 0),
(62, 17, 'pan viena', 3, 'panaderia', 'unidad', 15, 'SORCHANTES', '17P15M', 5, 0),
(63, 18, 'pan sandwich', 3, 'panaderia', 'kilo', 15, 'SORCHANTES', '18P15M', 40, 0),
(64, 18, 'pan sandwich', 3, 'panaderia', 'kilo', 17, 'FARGO', '18P17M', 45, 0),
(65, 18, 'pan sandwich', 3, 'panaderia', 'kilo', 16, 'MALLORQUINA', '18P16M', 42, 0),
(66, 19, 'pan felipe', 3, 'panaderia', 'kilo', 18, 'PROPIO', '19P18M', 35, 0),
(67, 20, 'pan manteca', 3, 'panaderia', 'kilo', 18, 'PROPIO', '20P18M', 45, 0),
(68, 21, 'pan tortuga', 3, 'panaderia', 'unidad', 17, 'FARGO', '21P17M', 4, 0),
(69, 21, 'pan tortuga', 3, 'panaderia', 'unidad', 16, 'MALLORQUINA', '21P16M', 4, 0),
(70, 21, 'pan tortuga', 3, 'panaderia', 'unidad', 15, 'SORCHANTES', '21P15M', 5, 0),
(71, 22, 'pan flauta', 3, 'panaderia', 'kilo', 18, 'PROPIO', '22P18M', 30, 0),
(72, 23, 'pan porteño', 3, 'panaderia', 'kilo', 18, 'PROPIO', '23P18M', 32, 0);

-- --------------------------------------------------------

--
-- Table structure for table `productos`
--

CREATE TABLE `productos` (
  `COD-PRODUCTO` int NOT NULL,
  `DESC-PRODUCTO` varchar(25) COLLATE utf8mb3_spanish_ci DEFAULT NULL,
  `COD-RUBRO` int NOT NULL,
  `DESC-RUBRO` varchar(12) COLLATE utf8mb3_spanish_ci DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=712 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_spanish_ci;

--
-- Dumping data for table `productos`
--

INSERT INTO `productos` (`COD-PRODUCTO`, `DESC-PRODUCTO`, `COD-RUBRO`, `DESC-RUBRO`) VALUES
(1, 'salame', 1, 'fiambres'),
(2, 'mortadela', 1, 'fiambres'),
(3, 'paleta', 1, 'fiambres'),
(4, 'jamon', 1, 'fiambres'),
(5, 'leonesa', 1, 'fiambres'),
(6, 'panchos', 1, 'fiambres'),
(7, 'queso muzarela', 2, 'lacteos'),
(8, 'queso sandwich', 2, 'lacteos'),
(9, 'queso dambo', 2, 'lacteos'),
(10, 'queso rallado', 2, 'lacteos'),
(11, 'leche entera', 2, 'lacteos'),
(12, 'yogurt', 2, 'lacteos'),
(13, 'queso colonia', 2, 'lacteos'),
(14, 'queso semiduro', 2, 'lacteos'),
(15, 'manteca', 2, 'lacteos'),
(16, 'leche descremada', 2, 'lacteos'),
(17, 'pan viena', 3, 'panaderia'),
(18, 'pan sandwich', 3, 'panaderia'),
(19, 'pan felipe', 3, 'panaderia'),
(20, 'pan manteca', 3, 'panaderia'),
(21, 'pan tortuga', 3, 'panaderia'),
(22, 'pan flauta', 3, 'panaderia'),
(23, 'pan porteño', 3, 'panaderia');

-- --------------------------------------------------------

--
-- Table structure for table `rubros`
--

CREATE TABLE `rubros` (
  `COD-RUBRO` int NOT NULL,
  `DESC-RUBRO` varchar(12) COLLATE utf8mb3_spanish_ci DEFAULT NULL,
  `IVA` double DEFAULT NULL,
  `DESCUENTO` double DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=5461 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_spanish_ci;

--
-- Dumping data for table `rubros`
--

INSERT INTO `rubros` (`COD-RUBRO`, `DESC-RUBRO`, `IVA`, `DESCUENTO`) VALUES
(1, 'fiambres', 0.23, 0.1),
(2, 'lacteos', 0.14, 0.05),
(3, 'panaderia', 0, 0.15);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `gestion`
--
ALTER TABLE `gestion`
  ADD PRIMARY KEY (`gestion_id`),
  ADD KEY `COD-ARTICULO_index` (`COD-ARTICULO`),
  ADD KEY `COD-MARCA_index` (`COD-MARCA`),
  ADD KEY `COD-PRODUCTO_index` (`COD-PRODUCTO`);

--
-- Indexes for table `marcas`
--
ALTER TABLE `marcas`
  ADD PRIMARY KEY (`COD-MARCA`);

--
-- Indexes for table `precios`
--
ALTER TABLE `precios`
  ADD PRIMARY KEY (`precios_id`),
  ADD KEY `COD-ARTICULO_index` (`COD-ARTICULO`),
  ADD KEY `COD-MARCA_index` (`COD-MARCA`),
  ADD KEY `COD-RUBRO_index` (`COD-RUBRO`);

--
-- Indexes for table `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`COD-PRODUCTO`),
  ADD KEY `COD-RUBRO_index` (`COD-RUBRO`);

--
-- Indexes for table `rubros`
--
ALTER TABLE `rubros`
  ADD PRIMARY KEY (`COD-RUBRO`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `gestion`
--
ALTER TABLE `gestion`
  MODIFY `gestion_id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT for table `precios`
--
ALTER TABLE `precios`
  MODIFY `precios_id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;
--
-- Database: `seleccion`
--
CREATE DATABASE IF NOT EXISTS `seleccion` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci;
USE `seleccion`;

-- --------------------------------------------------------

--
-- Table structure for table `fechas`
--

CREATE TABLE `fechas` (
  `id` int NOT NULL,
  `fechaEntrada` date DEFAULT NULL,
  `fechaSalida` date DEFAULT NULL,
  `dias` int DEFAULT NULL COMMENT 'dias de diferencia entre fecha de entrada y fecha de salida',
  `mesFechaEntrada` varchar(20) DEFAULT NULL COMMENT 'mes de la fecha de entrada como texto',
  `diaFechaEntrada` varchar(20) DEFAULT NULL COMMENT 'dia de la semana de la fecha de entrada como texto'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `fechas`
--

INSERT INTO `fechas` (`id`, `fechaEntrada`, `fechaSalida`, `dias`, `mesFechaEntrada`, `diaFechaEntrada`) VALUES
(1, '2022-12-01', '2022-12-05', 4, 'Diciembre', 'jueves'),
(2, '2022-01-01', '2022-07-05', 185, 'Enero', 'sábado');

-- --------------------------------------------------------

--
-- Table structure for table `meses`
--

CREATE TABLE `meses` (
  `id` int NOT NULL,
  `nombre` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `meses`
--

INSERT INTO `meses` (`id`, `nombre`) VALUES
(1, 'Enero'),
(2, 'Febrero'),
(3, 'Marzo'),
(4, 'Abril'),
(5, 'Mayo'),
(6, 'Junio'),
(7, 'Julio'),
(8, 'Agosto'),
(9, 'Septiembre'),
(10, 'Octubre'),
(11, 'Noviembre'),
(12, 'Diciembre'),
(13, 'Enero'),
(14, 'Febrero'),
(15, 'Marzo'),
(16, 'Abril'),
(17, 'Mayo'),
(18, 'Junio'),
(19, 'Julio'),
(20, 'Agosto'),
(21, 'Septiembre'),
(22, 'Octubre'),
(23, 'Noviembre'),
(24, 'Diciembre');

-- --------------------------------------------------------

--
-- Table structure for table `numeros`
--

CREATE TABLE `numeros` (
  `id` int NOT NULL,
  `valor` int DEFAULT NULL,
  `texto` varchar(20) DEFAULT NULL
) ;

--
-- Dumping data for table `numeros`
--

INSERT INTO `numeros` (`id`, `valor`, `texto`) VALUES
(1, 2, 'dos'),
(2, 3, 'tres'),
(3, 5, 'cinco'),
(4, 2, 'dos'),
(5, 0, 'cero');

-- --------------------------------------------------------

--
-- Table structure for table `vocales`
--

CREATE TABLE `vocales` (
  `id` int NOT NULL,
  `nombre` char(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `vocales`
--

INSERT INTO `vocales` (`id`, `nombre`) VALUES
(1, 'a'),
(2, 'e'),
(3, 'i'),
(4, 'o'),
(5, 'u');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `fechas`
--
ALTER TABLE `fechas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `meses`
--
ALTER TABLE `meses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `numeros`
--
ALTER TABLE `numeros`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vocales`
--
ALTER TABLE `vocales`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `fechas`
--
ALTER TABLE `fechas`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `meses`
--
ALTER TABLE `meses`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `numeros`
--
ALTER TABLE `numeros`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `vocales`
--
ALTER TABLE `vocales`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- Database: `sp`
--
CREATE DATABASE IF NOT EXISTS `sp` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci;
USE `sp`;

-- --------------------------------------------------------

--
-- Table structure for table `p`
--

CREATE TABLE `p` (
  `p` varchar(2) NOT NULL,
  `pnombre` varchar(50) DEFAULT NULL,
  `color` varchar(20) DEFAULT NULL,
  `peso` smallint DEFAULT NULL,
  `ciudad` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `p`
--

INSERT INTO `p` (`p`, `pnombre`, `color`, `peso`, `ciudad`) VALUES
('P1', 'tuerca', 'verde', 12, 'París'),
('P2', 'perno', 'rojo', 17, 'Londres'),
('P3', 'birlo', 'azul', 17, 'Roma'),
('P4', 'birlo', 'rojo', 14, 'Londres'),
('P5', 'leva', 'azul', 12, 'París'),
('P6', 'engrane', 'rojo', 19, 'París');

-- --------------------------------------------------------

--
-- Table structure for table `s`
--

CREATE TABLE `s` (
  `s` varchar(2) NOT NULL,
  `snombre` varchar(50) DEFAULT NULL,
  `estado` smallint DEFAULT NULL,
  `ciudad` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `s`
--

INSERT INTO `s` (`s`, `snombre`, `estado`, `ciudad`) VALUES
('S1', 'Salazar', 20, 'Londres'),
('S2', 'Jaimes', 10, 'París'),
('S3', 'Bernal', 30, 'París'),
('S4', 'Corona', 20, 'Londres'),
('S5', 'Aldana', 30, 'Atenas');

-- --------------------------------------------------------

--
-- Table structure for table `sp`
--

CREATE TABLE `sp` (
  `s` varchar(2) NOT NULL,
  `p` varchar(2) NOT NULL,
  `cant` smallint DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sp`
--

INSERT INTO `sp` (`s`, `p`, `cant`) VALUES
('S1', 'P1', 300),
('S1', 'P2', 200),
('S1', 'P3', 400),
('S1', 'P4', 200),
('S1', 'P5', 100),
('S1', 'P6', 100),
('S2', 'P1', 300),
('S2', 'P2', 400),
('S3', 'P2', 200),
('S4', 'P2', 200),
('S4', 'P4', 300),
('S4', 'P5', 400);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `p`
--
ALTER TABLE `p`
  ADD PRIMARY KEY (`p`);

--
-- Indexes for table `s`
--
ALTER TABLE `s`
  ADD PRIMARY KEY (`s`);

--
-- Indexes for table `sp`
--
ALTER TABLE `sp`
  ADD PRIMARY KEY (`s`,`p`),
  ADD KEY `PSP` (`p`),
  ADD KEY `SSP` (`s`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `sp`
--
ALTER TABLE `sp`
  ADD CONSTRAINT `FK_sp_1` FOREIGN KEY (`s`) REFERENCES `s` (`s`),
  ADD CONSTRAINT `fkSpP` FOREIGN KEY (`p`) REFERENCES `p` (`p`);
--
-- Database: `spj`
--
CREATE DATABASE IF NOT EXISTS `spj` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci;
USE `spj`;

-- --------------------------------------------------------

--
-- Table structure for table `j`
--

CREATE TABLE `j` (
  `j` varchar(9) CHARACTER SET utf8mb3 COLLATE utf8mb3_spanish_ci NOT NULL,
  `nomj` varchar(45) CHARACTER SET utf8mb3 COLLATE utf8mb3_spanish_ci DEFAULT NULL,
  `ciudad` varchar(45) CHARACTER SET utf8mb3 COLLATE utf8mb3_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_spanish_ci;

--
-- Dumping data for table `j`
--

INSERT INTO `j` (`j`, `nomj`, `ciudad`) VALUES
('J1', 'Edificio1', 'París'),
('J2', 'Edificio2', 'Roma'),
('J3', 'Edificio3', 'Atenas'),
('J4', 'Edificio4', 'Atenas'),
('J5', 'Edificio5', 'Londres'),
('J6', 'Edificio6', 'Madrid'),
('J7', 'Edificio7', 'Londres');

-- --------------------------------------------------------

--
-- Table structure for table `p`
--

CREATE TABLE `p` (
  `P` varchar(9) CHARACTER SET utf8mb3 COLLATE utf8mb3_spanish_ci NOT NULL,
  `nomp` varchar(45) CHARACTER SET utf8mb3 COLLATE utf8mb3_spanish_ci DEFAULT NULL,
  `color` varchar(45) CHARACTER SET utf8mb3 COLLATE utf8mb3_spanish_ci DEFAULT NULL,
  `peso` varchar(45) CHARACTER SET utf8mb3 COLLATE utf8mb3_spanish_ci DEFAULT NULL,
  `ciudad` varchar(45) CHARACTER SET utf8mb3 COLLATE utf8mb3_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_spanish_ci;

--
-- Dumping data for table `p`
--

INSERT INTO `p` (`P`, `nomp`, `color`, `peso`, `ciudad`) VALUES
('P1', 'MESA', 'ROJO', '12', 'LONDRES'),
('P2', 'SILLA', 'BLANCA', '17', 'PARIS'),
('P3', 'ARMARIO', 'GRIS', '17', 'ROMA'),
('P4', 'ARCHIVADOR', 'ROJO', '14', 'LONDRES'),
('P5', 'PUERTA ', 'BLANCA', '12', 'PARIS'),
('P6', 'LAMPARA', 'AMARILLA', '19', 'LONDRES');

-- --------------------------------------------------------

--
-- Table structure for table `s`
--

CREATE TABLE `s` (
  `s` varchar(9) CHARACTER SET utf8mb3 COLLATE utf8mb3_spanish_ci NOT NULL,
  `noms` varchar(45) CHARACTER SET utf8mb3 COLLATE utf8mb3_spanish_ci DEFAULT NULL,
  `estado` varchar(45) CHARACTER SET utf8mb3 COLLATE utf8mb3_spanish_ci DEFAULT NULL,
  `ciudad` varchar(45) CHARACTER SET utf8mb3 COLLATE utf8mb3_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_spanish_ci;

--
-- Dumping data for table `s`
--

INSERT INTO `s` (`s`, `noms`, `estado`, `ciudad`) VALUES
('S1', 'Smith', '20', 'Londres'),
('S2', 'Jones', '10', 'París'),
('S3', 'Blake', '30', 'París'),
('S4', 'Clark', '20', 'Londres'),
('S5', 'Adams', '30', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `spj`
--

CREATE TABLE `spj` (
  `s` varchar(9) CHARACTER SET utf8mb3 COLLATE utf8mb3_spanish_ci NOT NULL,
  `p` varchar(9) CHARACTER SET utf8mb3 COLLATE utf8mb3_spanish_ci NOT NULL,
  `j` varchar(9) CHARACTER SET utf8mb3 COLLATE utf8mb3_spanish_ci NOT NULL,
  `cant` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_spanish_ci;

--
-- Dumping data for table `spj`
--

INSERT INTO `spj` (`s`, `p`, `j`, `cant`) VALUES
('s1', 'p1', 'j1', 200),
('s1', 'p1', 'j4', 200),
('s1', 'p1', 'j5', 700),
('s1', 'p3', 'j2', 20),
('s1', 'p3', 'j3', 30),
('s1', 'p3', 'j4', 40),
('s1', 'p3', 'j5', 50),
('s1', 'p3', 'j6', 400),
('s1', 'p3', 'j7', 70),
('s2', 'p3', 'j1', 400),
('s2', 'p3', 'j2', 200),
('s2', 'p3', 'j3', 200),
('s2', 'p3', 'j4', 500),
('s2', 'p3', 'j5', 600),
('s2', 'p3', 'j7', 800),
('s2', 'p5', 'j2', 100),
('s3', 'p3', 'j1', 200),
('s3', 'p4', 'j2', 500),
('s4', 'p6', 'j3', 300),
('s4', 'p6', 'j7', 300);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `j`
--
ALTER TABLE `j`
  ADD PRIMARY KEY (`j`),
  ADD UNIQUE KEY `j_UNIQUE` (`j`);

--
-- Indexes for table `p`
--
ALTER TABLE `p`
  ADD PRIMARY KEY (`P`),
  ADD UNIQUE KEY `P_UNIQUE` (`P`);

--
-- Indexes for table `s`
--
ALTER TABLE `s`
  ADD PRIMARY KEY (`s`),
  ADD UNIQUE KEY `s_UNIQUE` (`s`);

--
-- Indexes for table `spj`
--
ALTER TABLE `spj`
  ADD PRIMARY KEY (`s`,`p`,`j`),
  ADD KEY `fkSpjP` (`p`),
  ADD KEY `fkSpjJ` (`j`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `spj`
--
ALTER TABLE `spj`
  ADD CONSTRAINT `fkSpjJ` FOREIGN KEY (`j`) REFERENCES `j` (`j`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `fkSpjP` FOREIGN KEY (`p`) REFERENCES `p` (`P`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `fkSpjS` FOREIGN KEY (`s`) REFERENCES `s` (`s`) ON DELETE RESTRICT ON UPDATE RESTRICT;
--
-- Database: `symfony1`
--
CREATE DATABASE IF NOT EXISTS `symfony1` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci;
USE `symfony1`;
--
-- Database: `tienda`
--
CREATE DATABASE IF NOT EXISTS `tienda` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci;
USE `tienda`;

-- --------------------------------------------------------

--
-- Table structure for table `fabricante`
--

CREATE TABLE `fabricante` (
  `codigo` int UNSIGNED NOT NULL,
  `nombre` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `fabricante`
--

INSERT INTO `fabricante` (`codigo`, `nombre`) VALUES
(1, 'Asus'),
(2, 'Lenovo'),
(3, 'Hewlett-Packard'),
(4, 'Samsung'),
(5, 'Seagate'),
(6, 'Crucial'),
(7, 'Gigabyte'),
(8, 'Huawei'),
(9, 'Xiaomi');

-- --------------------------------------------------------

--
-- Table structure for table `producto`
--

CREATE TABLE `producto` (
  `codigo` int UNSIGNED NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `precio` double NOT NULL,
  `codigo_fabricante` int UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `producto`
--

INSERT INTO `producto` (`codigo`, `nombre`, `precio`, `codigo_fabricante`) VALUES
(1, 'Disco duro SATA3 1TB', 86.99, 5),
(2, 'Memoria RAM DDR4 8GB', 120, 6),
(3, 'Disco SSD 1 TB', 150.99, 4),
(4, 'GeForce GTX 1050Ti', 185, 7),
(5, 'GeForce GTX 1080 Xtreme', 755, 6),
(6, 'Monitor 24 LED Full HD', 202, 1),
(7, 'Monitor 27 LED Full HD', 245.99, 1),
(8, 'Portátil Yoga 520', 559, 2),
(9, 'Portátil Ideapd 320', 444, 2),
(10, 'Impresora HP Deskjet 3720', 59.99, 3),
(11, 'Impresora HP Laserjet Pro M26nw', 180, 3),
(12, 'nuevo producto', 12, 1),
(13, 'nuevo', 234, 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `fabricante`
--
ALTER TABLE `fabricante`
  ADD PRIMARY KEY (`codigo`);

--
-- Indexes for table `producto`
--
ALTER TABLE `producto`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `codigo_fabricante` (`codigo_fabricante`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `fabricante`
--
ALTER TABLE `fabricante`
  MODIFY `codigo` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `producto`
--
ALTER TABLE `producto`
  MODIFY `codigo` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `producto`
--
ALTER TABLE `producto`
  ADD CONSTRAINT `producto_ibfk_1` FOREIGN KEY (`codigo_fabricante`) REFERENCES `fabricante` (`codigo`);
--
-- Database: `ventas`
--
CREATE DATABASE IF NOT EXISTS `ventas` DEFAULT CHARACTER SET utf8mb3 COLLATE utf8mb3_spanish_ci;
USE `ventas`;

-- --------------------------------------------------------

--
-- Table structure for table `cliente`
--

CREATE TABLE `cliente` (
  `id` int UNSIGNED NOT NULL,
  `nombre` varchar(100) COLLATE utf8mb3_spanish_ci NOT NULL,
  `apellido1` varchar(100) COLLATE utf8mb3_spanish_ci NOT NULL,
  `apellido2` varchar(100) COLLATE utf8mb3_spanish_ci DEFAULT NULL,
  `ciudad` varchar(100) COLLATE utf8mb3_spanish_ci DEFAULT NULL,
  `categoría` int UNSIGNED DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=1638 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_spanish_ci;

--
-- Dumping data for table `cliente`
--

INSERT INTO `cliente` (`id`, `nombre`, `apellido1`, `apellido2`, `ciudad`, `categoría`) VALUES
(1, 'Aarón', 'Rivero', 'Gómez', 'Almería', 100),
(2, 'Adela', 'Salas', 'Díaz', 'Granada', 200),
(3, 'Adolfo', 'Rubio', 'Flores', 'Sevilla', NULL),
(4, 'Adrián', 'Suárez', NULL, 'Jaén', 300),
(5, 'Marcos', 'Loyola', 'Méndez', 'Almería', 200),
(6, 'María', 'Santana', 'Moreno', 'Cádiz', 100),
(7, 'Pilar', 'Ruiz', NULL, 'Sevilla', 300),
(8, 'Pepe', 'Ruiz', 'Santana', 'Huelva', 200),
(9, 'Guillermo', 'López', 'Gómez', 'Granada', 225),
(10, 'Daniel', 'Santana', 'Loyola', 'Sevilla', 1250);

-- --------------------------------------------------------

--
-- Table structure for table `comercial`
--

CREATE TABLE `comercial` (
  `id` int UNSIGNED NOT NULL,
  `nombre` varchar(100) COLLATE utf8mb3_spanish_ci NOT NULL,
  `apellido1` varchar(100) COLLATE utf8mb3_spanish_ci NOT NULL,
  `apellido2` varchar(100) COLLATE utf8mb3_spanish_ci DEFAULT NULL,
  `comisión` float DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=2048 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_spanish_ci;

--
-- Dumping data for table `comercial`
--

INSERT INTO `comercial` (`id`, `nombre`, `apellido1`, `apellido2`, `comisión`) VALUES
(1, 'Daniel', 'Sáez', 'Vega', 0.15),
(2, 'Juan', 'Gómez', 'López', 0.13),
(3, 'Diego', 'Flores', 'Salas', 0.11),
(4, 'Marta', 'Herrera', 'Gil', 0.14),
(5, 'Antonio', 'Carretero', 'Ortega', 0.12),
(6, 'Manuel', 'Domínguez', 'Hernández', 0.13),
(7, 'Antonio', 'Vega', 'Hernández', 0.11),
(8, 'Alfredo', 'Ruiz', 'Flores', 0.05);

-- --------------------------------------------------------

--
-- Table structure for table `pedido`
--

CREATE TABLE `pedido` (
  `id` int UNSIGNED NOT NULL,
  `total` double NOT NULL,
  `fecha` date DEFAULT NULL,
  `id_cliente` int UNSIGNED NOT NULL,
  `id_comercial` int UNSIGNED NOT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=1024 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_spanish_ci;

--
-- Dumping data for table `pedido`
--

INSERT INTO `pedido` (`id`, `total`, `fecha`, `id_cliente`, `id_comercial`) VALUES
(1, 150.5, '2017-10-05', 5, 2),
(2, 270.65, '2016-09-10', 1, 5),
(3, 65.26, '2017-10-05', 2, 1),
(4, 110.5, '2016-08-17', 8, 3),
(5, 948.5, '2017-09-10', 5, 2),
(6, 2400.6, '2016-07-27', 7, 1),
(7, 5760, '2015-09-10', 2, 1),
(8, 1983.43, '2017-10-10', 4, 6),
(9, 2480.4, '2016-10-10', 8, 3),
(10, 250.45, '2015-06-27', 8, 2),
(11, 75.29, '2016-08-17', 3, 7),
(12, 3045.6, '2017-04-25', 2, 1),
(13, 545.75, '2019-01-25', 6, 1),
(14, 145.82, '2017-02-02', 6, 1),
(15, 370.85, '2019-03-11', 1, 5),
(16, 2389.23, '2019-03-11', 1, 5),
(17, 145, '2022-11-01', 5, 3),
(18, 1222, '2022-12-15', 8, 3);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comercial`
--
ALTER TABLE `comercial`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pedido`
--
ALTER TABLE `pedido`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pedido_ibfk_1` (`id_cliente`),
  ADD KEY `pedido_ibfk_2` (`id_comercial`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cliente`
--
ALTER TABLE `cliente`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `comercial`
--
ALTER TABLE `comercial`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `pedido`
--
ALTER TABLE `pedido`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `pedido`
--
ALTER TABLE `pedido`
  ADD CONSTRAINT `pedido_ibfk_1` FOREIGN KEY (`id_cliente`) REFERENCES `cliente` (`id`),
  ADD CONSTRAINT `pedido_ibfk_2` FOREIGN KEY (`id_comercial`) REFERENCES `comercial` (`id`);
--
-- Database: `ventas1`
--
CREATE DATABASE IF NOT EXISTS `ventas1` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `ventas1`;

-- --------------------------------------------------------

--
-- Table structure for table `grupos`
--

CREATE TABLE `grupos` (
  `IdGrupo` int NOT NULL,
  `NombreGrupo` varchar(50) DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=5461 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `grupos`
--

INSERT INTO `grupos` (`IdGrupo`, `NombreGrupo`) VALUES
(1, 'Frutas'),
(2, 'Verduras'),
(3, 'Hortalizas');

-- --------------------------------------------------------

--
-- Table structure for table `productos`
--

CREATE TABLE `productos` (
  `IdProducto` int NOT NULL,
  `NomProducto` varchar(50) DEFAULT NULL,
  `IdGrupo` int DEFAULT NULL,
  `Precio` double DEFAULT NULL,
  `foto` varchar(100) DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=1092 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `productos`
--

INSERT INTO `productos` (`IdProducto`, `NomProducto`, `IdGrupo`, `Precio`, `foto`) VALUES
(1, 'Mandarinas', 1, 3, 'mandarina.png'),
(2, 'Lechugas', 2, 1, 'lechugas.png'),
(3, 'Melones', 1, 1, 'melon.png'),
(4, 'Coles', 2, 0, NULL),
(5, 'Berenjenas', 3, 2, NULL),
(6, 'Platanos', 1, 2, NULL),
(7, 'Tomates', 2, 0, NULL),
(8, 'Uvas', 1, 3, NULL),
(9, 'Esparragos', 3, 1, NULL),
(10, 'Zanaorias', 3, 0, NULL),
(11, 'Naranjas', 1, 1, NULL),
(12, 'Malocoton', 1, 2, NULL),
(13, 'Pimientos', 3, 0, ''),
(14, 'Manzana', 1, 3, NULL),
(15, 'Patatas', 3, 2, NULL),
(16, 'nada', NULL, NULL, '');

-- --------------------------------------------------------

--
-- Table structure for table `productosvendidos`
--

CREATE TABLE `productosvendidos` (
  `id` int NOT NULL,
  `nombreGrupo` varchar(100) DEFAULT NULL,
  `numeroProductosVendidos` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `productosvendidos`
--

INSERT INTO `productosvendidos` (`id`, `nombreGrupo`, `numeroProductosVendidos`) VALUES
(1, 'Frutas', 2524),
(2, 'Verduras', 1069),
(3, 'Hortalizas', 1405);

-- --------------------------------------------------------

--
-- Table structure for table `vendedores`
--

CREATE TABLE `vendedores` (
  `IdVendedor` int NOT NULL,
  `NombreVendedor` varchar(50) DEFAULT NULL,
  `FechaAlta` datetime DEFAULT NULL,
  `NIF` varchar(9) DEFAULT NULL,
  `FechaNac` datetime DEFAULT NULL,
  `Direccion` varchar(50) DEFAULT NULL,
  `Poblacion` varchar(50) DEFAULT NULL,
  `CodPostal` varchar(5) DEFAULT NULL,
  `Telefon` varchar(50) DEFAULT NULL,
  `EstalCivil` varchar(15) DEFAULT NULL,
  `activo` bit(1) DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=1170 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vendedores`
--

INSERT INTO `vendedores` (`IdVendedor`, `NombreVendedor`, `FechaAlta`, `NIF`, `FechaNac`, `Direccion`, `Poblacion`, `CodPostal`, `Telefon`, `EstalCivil`, `activo`) VALUES
(1, 'Pepito', '2004-03-15 00:00:00', '32456645D', '2019-05-15 00:00:00', 'cvbmcvbmcvb', 'Barcelona', '08782', '937745214', 'Soltero', b'1'),
(2, 'Carmen', '2004-04-12 00:00:00', '12121213G', '1951-11-15 00:00:00', 'jkkhjkjhkhjk', 'Madrid', '28257', '914556565', 'Separado', b'0'),
(3, 'Rosa', '2003-12-23 00:00:00', '11313155O', '1977-11-12 00:00:00', 'jhjhgjhgjhgjg', 'Martorell', '13131', '937754585', 'Casado', b'1'),
(4, 'Gloria', '2001-01-01 00:00:00', '13131333E', '1960-01-13 00:00:00', 'dfsdgdfgdfg', 'badalona', '15344', '464646466', 'Divorciado', b'0'),
(5, 'Fran', '2000-12-12 00:00:00', '11213123O', '1955-02-15 00:00:00', 'ghfghgfh', 'Barcelona', '23131', '13123123123', 'Viudo', b'1'),
(6, 'Monica', '2000-02-12 00:00:00', '13131313O', '1970-11-12 00:00:00', 'hfghfghfghfg', 'malaga', '13131', '4454564646', 'Arrejuntado', b'0'),
(7, 'Quima', '2002-12-12 00:00:00', '46464646F', '1944-04-12 00:00:00', 'jghjghjghjghjgh', 'Madrid', '45456', '464646456', 'Separado', b'1'),
(8, 'Ramon', '2002-12-12 00:00:00', '12113133G', '1958-02-12 00:00:00', NULL, 'Sant Esteve sesrovires', '32311', '231313131', 'Divorciado', b'1'),
(9, 'Carlos', '2004-02-12 00:00:00', '13131313F', '1980-02-11 00:00:00', 'lkljkljkljkljkljkl', 'Madrid', '43434', '464646464', 'Arrejuntado', b'0'),
(10, 'Antonio', '1931-02-03 00:00:00', '13131113G', '1967-02-12 00:00:00', 'fghfghfghfghfgh', 'Sant Esteve sesrovires', '12121', NULL, 'Separado', b'1'),
(11, 'Enrique', '2001-11-12 00:00:00', '31113131G', '1970-12-12 00:00:00', 'dfgdfgdfgdfgdfgd', 'Sant Esteve sesrovires', '12111', NULL, 'Divorciado', b'0'),
(12, 'Carla', '2001-11-12 00:00:00', '31311313E', '1969-01-12 00:00:00', 'sfsdfsdfsdfsdf', 'La Beguda Alta', '31331', '434464646', 'Arrejuntado', b'1'),
(13, 'Federico', '1999-02-21 00:00:00', '11313131C', '1966-01-31 00:00:00', 'xcvxcvxcvxcv', 'Sant Esteve sesrovires', '21545', '131131311', 'Casado', b'1'),
(14, 'Amadeu', '2002-02-20 00:00:00', '46811136H', '1977-09-25 00:00:00', 'asdasdasdasdas', 'Sant Esteve sesrovires', '08635', '1465464646', 'Casado', b'1'),
(15, 'lorena', NULL, '12121212a', NULL, '', '', '', '', '', b'0');

-- --------------------------------------------------------

--
-- Table structure for table `ventas`
--

CREATE TABLE `ventas` (
  `idventas` int NOT NULL,
  `Cod Vendedor` int NOT NULL,
  `Cod Producto` int NOT NULL,
  `Fecha` datetime NOT NULL,
  `Kilos` double DEFAULT NULL,
  `total` float DEFAULT '0'
) ENGINE=InnoDB AVG_ROW_LENGTH=55 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ventas`
--

INSERT INTO `ventas` (`idventas`, `Cod Vendedor`, `Cod Producto`, `Fecha`, `Kilos`, `total`) VALUES
(1, 7, 1, '1998-07-02 00:00:00', 2089, 6267),
(2, 7, 1, '1998-07-12 00:00:00', 1322, 3966),
(3, 7, 1, '1998-01-21 00:00:00', 1176, 3528),
(5, 7, 1, '1998-05-28 00:00:00', 290, 870),
(6, 7, 8, '1998-12-02 00:00:00', 2000, 6000),
(7, 7, 8, '1998-10-31 00:00:00', 1218, 3654),
(8, 7, 8, '1998-06-22 00:00:00', 951, 2853),
(9, 7, 8, '1998-01-28 00:00:00', 536, 1608),
(12, 7, 6, '1998-07-11 00:00:00', 2026, 4052),
(13, 7, 6, '1998-12-26 00:00:00', 1609, 3218),
(14, 7, 6, '1998-10-04 00:00:00', 1036, 2072),
(15, 7, 6, '1998-10-08 00:00:00', 770, 1540),
(16, 7, 6, '1998-03-12 00:00:00', 659, 1318),
(17, 7, 13, '1998-04-12 00:00:00', 2394, 0),
(18, 7, 13, '1998-08-14 00:00:00', 1643, 0),
(19, 7, 13, '1998-05-04 00:00:00', 1538, 0),
(20, 7, 13, '1998-08-08 00:00:00', 1376, 0),
(21, 7, 13, '1998-09-23 00:00:00', 1190, 0),
(22, 7, 13, '1998-04-29 00:00:00', 830, 0),
(23, 7, 13, '1998-01-09 00:00:00', 697, 0),
(24, 7, 13, '1998-03-20 00:00:00', 260, 0),
(29, 7, 10, '1998-10-24 00:00:00', 2463, 0),
(30, 7, 10, '1998-04-06 00:00:00', 2382, 0),
(31, 7, 10, '1998-10-17 00:00:00', 2274, 0),
(32, 7, 10, '1998-04-12 00:00:00', 1603, 0),
(33, 7, 10, '1998-07-05 00:00:00', 693, 0),
(34, 7, 14, '1998-05-09 00:00:00', 2307, 6921),
(35, 7, 14, '1998-05-16 00:00:00', 1794, 5382),
(36, 7, 14, '1998-04-03 00:00:00', 1724, 5172),
(37, 7, 14, '1998-07-18 00:00:00', 1299, 3897),
(38, 7, 4, '1998-08-03 00:00:00', 1580, 0),
(39, 7, 4, '1998-11-22 00:00:00', 1404, 0),
(40, 7, 4, '1998-01-16 00:00:00', 1143, 0),
(41, 7, 5, '1998-12-05 00:00:00', 1328, 2656),
(42, 3, 11, '1998-08-02 00:00:00', 1776, 1776),
(43, 3, 11, '1998-08-18 00:00:00', 1518, 1518),
(44, 3, 11, '1998-01-05 00:00:00', 854, 854),
(45, 3, 11, '1998-03-05 00:00:00', 704, 704),
(46, 3, 12, '1998-12-31 00:00:00', 1772, 3544),
(47, 3, 12, '1998-01-28 00:00:00', 1617, 3234),
(48, 3, 12, '1998-01-08 00:00:00', 1369, 2738),
(49, 3, 12, '1998-11-05 00:00:00', 1308, 2616),
(50, 3, 12, '1998-03-16 00:00:00', 1130, 2260),
(51, 3, 12, '1998-10-23 00:00:00', 738, 1476),
(52, 3, 12, '1998-03-28 00:00:00', 709, 1418),
(53, 3, 9, '1998-05-12 00:00:00', 2244, 2244),
(54, 3, 9, '1998-08-14 00:00:00', 1330, 1330),
(57, 3, 9, '1998-01-30 00:00:00', 268, 268),
(58, 3, 3, '1998-09-19 00:00:00', 1743, 1743),
(59, 3, 3, '1998-06-28 00:00:00', 1467, 1467),
(60, 3, 3, '1998-07-28 00:00:00', 928, 928),
(61, 3, 1, '1998-10-21 00:00:00', 2298, 6894),
(62, 3, 1, '1998-09-12 00:00:00', 1151, 3453),
(63, 3, 1, '1998-06-25 00:00:00', 804, 2412),
(64, 3, 8, '1998-05-30 00:00:00', 1993, 5979),
(65, 3, 8, '1998-11-15 00:00:00', 1637, 4911),
(66, 3, 8, '1998-04-26 00:00:00', 1345, 4035),
(67, 14, 11, '1998-01-02 00:00:00', 1701, 1701),
(68, 11, 11, '1998-04-22 00:00:00', 1646, 1646),
(69, 11, 11, '1998-12-30 00:00:00', 1230, 1230),
(70, 11, 12, '1998-05-04 00:00:00', 2188, 4376),
(71, 11, 12, '1998-05-30 00:00:00', 1731, 3462),
(72, 11, 12, '1998-07-31 00:00:00', 501, 1002),
(73, 11, 9, '1998-08-10 00:00:00', 2429, 2429),
(74, 11, 9, '1998-11-22 00:00:00', 2410, 2410),
(75, 11, 9, '1998-11-11 00:00:00', 2104, 2104),
(76, 11, 9, '1998-11-12 00:00:00', 2037, 2037),
(77, 11, 9, '1998-03-19 00:00:00', 1098, 1098),
(78, 11, 9, '1998-04-13 00:00:00', 898, 898),
(79, 11, 7, '1998-12-16 00:00:00', 1861, 0),
(80, 11, 7, '1998-01-18 00:00:00', 1592, 0),
(81, 11, 7, '1998-11-30 00:00:00', 798, 0),
(85, 11, 3, '1998-06-02 00:00:00', 1944, 1944),
(86, 11, 1, '1998-11-27 00:00:00', 2456, 7368),
(87, 11, 1, '1998-05-25 00:00:00', 2058, 6174),
(88, 11, 1, '1998-01-29 00:00:00', 1167, 3501),
(91, 11, 8, '1998-09-07 00:00:00', 2439, 7317),
(92, 11, 8, '1998-02-23 00:00:00', 2254, 6762),
(93, 11, 8, '1998-02-05 00:00:00', 2199, 6597),
(94, 11, 8, '1998-12-15 00:00:00', 1288, 3864),
(95, 11, 8, '1998-09-30 00:00:00', 723, 2169),
(97, 11, 6, '1998-01-06 00:00:00', 2062, 4124),
(98, 11, 6, '1998-05-08 00:00:00', 877, 1754),
(100, 11, 13, '1998-02-10 00:00:00', 2038, 0),
(101, 11, 13, '1998-09-22 00:00:00', 948, 0),
(108, 11, 10, '1998-11-10 00:00:00', 2073, 0),
(109, 11, 10, '1998-04-30 00:00:00', 824, 0),
(110, 11, 10, '1998-02-10 00:00:00', 691, 0),
(111, 11, 14, '1998-02-11 00:00:00', 2361, 7083),
(112, 11, 14, '1998-07-21 00:00:00', 2187, 6561),
(113, 11, 14, '1998-09-17 00:00:00', 1876, 5628),
(114, 11, 14, '1998-05-06 00:00:00', 613, 1839),
(116, 11, 4, '1998-03-03 00:00:00', 2043, 0),
(117, 11, 4, '1998-07-05 00:00:00', 1914, 0),
(118, 11, 4, '1998-03-10 00:00:00', 1315, 0),
(119, 11, 4, '1998-07-27 00:00:00', 1265, 0),
(120, 11, 4, '1998-07-09 00:00:00', 282, 0),
(121, 11, 5, '1998-12-16 00:00:00', 873, 1746),
(122, 11, 5, '1998-05-28 00:00:00', 868, 1736),
(124, 6, 11, '1998-01-25 00:00:00', 2205, 2205),
(125, 6, 11, '1998-06-10 00:00:00', 1528, 1528),
(126, 6, 11, '1998-11-16 00:00:00', 870, 870),
(127, 6, 11, '1998-02-26 00:00:00', 530, 530),
(128, 6, 12, '1998-10-23 00:00:00', 2480, 4960),
(129, 6, 12, '1998-08-19 00:00:00', 2377, 4754),
(130, 6, 12, '1998-07-05 00:00:00', 1827, 3654),
(131, 6, 12, '1998-01-04 00:00:00', 1518, 3036),
(132, 6, 12, '1998-01-04 00:00:00', 1214, 2428),
(133, 6, 12, '1998-09-23 00:00:00', 1200, 2400),
(134, 6, 12, '1998-12-08 00:00:00', 914, 1828),
(137, 6, 9, '1998-04-26 00:00:00', 1441, 1441),
(138, 6, 9, '1998-01-11 00:00:00', 896, 896),
(139, 6, 7, '1998-05-05 00:00:00', 1864, 0),
(140, 6, 7, '1998-05-07 00:00:00', 1379, 0),
(141, 6, 7, '1998-03-12 00:00:00', 1339, 0),
(142, 6, 7, '1998-01-20 00:00:00', 1312, 0),
(143, 6, 7, '1998-04-17 00:00:00', 285, 0),
(144, 6, 3, '1998-10-17 00:00:00', 2493, 2493),
(145, 6, 3, '1998-12-27 00:00:00', 2289, 2289),
(146, 6, 3, '1998-02-26 00:00:00', 2227, 2227),
(148, 6, 1, '1998-07-05 00:00:00', 2298, 6894),
(149, 6, 1, '1998-07-06 00:00:00', 2002, 6006),
(150, 6, 1, '1998-01-12 00:00:00', 1804, 5412),
(151, 6, 1, '1998-06-10 00:00:00', 1611, 4833),
(152, 6, 1, '1998-12-30 00:00:00', 878, 2634),
(153, 6, 1, '1998-02-03 00:00:00', 794, 2382),
(154, 6, 1, '1998-07-18 00:00:00', 677, 2031),
(155, 6, 1, '1998-10-26 00:00:00', 509, 1527),
(156, 6, 8, '1998-05-27 00:00:00', 1775, 5325),
(157, 6, 8, '1998-11-03 00:00:00', 501, 1503),
(159, 6, 6, '1998-11-03 00:00:00', 1935, 3870),
(160, 6, 6, '1998-04-09 00:00:00', 905, 1810),
(161, 6, 6, '1998-09-29 00:00:00', 828, 1656),
(162, 6, 6, '1998-10-01 00:00:00', 674, 1348),
(164, 6, 13, '1998-01-12 00:00:00', 2264, 0),
(165, 6, 13, '1998-05-17 00:00:00', 1592, 0),
(170, 6, 10, '1998-02-19 00:00:00', 2254, 0),
(171, 6, 10, '1998-06-13 00:00:00', 1945, 0),
(172, 6, 10, '1998-08-03 00:00:00', 1227, 0),
(173, 6, 10, '1998-03-15 00:00:00', 872, 0),
(174, 6, 10, '1998-01-30 00:00:00', 730, 0),
(176, 6, 14, '1998-08-02 00:00:00', 2490, 7470),
(177, 6, 14, '1998-03-28 00:00:00', 2466, 7398),
(178, 6, 14, '1998-12-18 00:00:00', 1734, 5202),
(179, 6, 14, '1998-03-06 00:00:00', 1653, 4959),
(180, 6, 14, '1998-12-06 00:00:00', 1643, 4929),
(181, 6, 14, '1998-09-16 00:00:00', 999, 2997),
(182, 6, 14, '1998-12-27 00:00:00', 899, 2697),
(183, 6, 14, '1998-08-25 00:00:00', 727, 2181),
(185, 6, 4, '1998-01-24 00:00:00', 2351, 0),
(186, 6, 4, '1998-02-26 00:00:00', 1864, 0),
(187, 6, 4, '1998-07-27 00:00:00', 1395, 0),
(188, 6, 4, '1998-07-20 00:00:00', 1351, 0),
(189, 6, 4, '1998-11-09 00:00:00', 1271, 0),
(190, 6, 4, '1998-08-21 00:00:00', 1188, 0),
(191, 6, 4, '1998-05-11 00:00:00', 1106, 0),
(192, 6, 4, '1998-11-15 00:00:00', 1081, 0),
(193, 6, 5, '1998-03-18 00:00:00', 2487, 4974),
(194, 6, 5, '1998-03-23 00:00:00', 2414, 4828),
(195, 6, 5, '1998-12-29 00:00:00', 1943, 3886),
(196, 6, 5, '1998-03-15 00:00:00', 673, 1346),
(197, 1, 11, '1998-06-03 00:00:00', 2319, 2319),
(198, 1, 11, '1998-10-21 00:00:00', 2143, 2143),
(199, 1, 11, '1998-05-23 00:00:00', 265, 265),
(200, 1, 12, '1998-03-30 00:00:00', 1999, 3998),
(201, 1, 12, '1998-09-26 00:00:00', 1508, 3016),
(202, 1, 12, '1998-12-12 00:00:00', 974, 1948),
(203, 1, 12, '1998-04-12 00:00:00', 833, 1666),
(204, 1, 9, '1998-07-18 00:00:00', 2437, 2437),
(205, 1, 9, '1998-10-12 00:00:00', 2086, 2086),
(206, 1, 9, '1998-11-16 00:00:00', 1422, 1422),
(207, 1, 9, '1998-06-02 00:00:00', 1315, 1315),
(209, 1, 3, '1998-09-16 00:00:00', 2477, 2477),
(210, 1, 3, '1998-12-13 00:00:00', 2182, 2182),
(211, 1, 3, '1998-03-31 00:00:00', 1741, 1741),
(212, 1, 3, '1998-10-18 00:00:00', 1568, 1568),
(213, 1, 3, '1998-04-12 00:00:00', 1493, 1493),
(214, 1, 3, '1998-01-28 00:00:00', 1327, 1327),
(215, 1, 3, '1998-03-14 00:00:00', 933, 933),
(216, 1, 3, '1998-03-23 00:00:00', 744, 744),
(217, 1, 3, '1998-06-25 00:00:00', 735, 735),
(218, 1, 3, '1998-09-21 00:00:00', 731, 731),
(219, 1, 3, '1998-01-09 00:00:00', 277, 277),
(220, 1, 1, '1998-07-18 00:00:00', 2066, 6198),
(221, 1, 1, '1998-05-05 00:00:00', 1945, 5835),
(222, 1, 1, '1998-12-09 00:00:00', 1838, 5514),
(223, 1, 1, '1998-09-06 00:00:00', 1400, 4200),
(224, 1, 1, '1998-06-18 00:00:00', 774, 2322),
(225, 1, 8, '1998-09-28 00:00:00', 2064, 6192),
(226, 1, 8, '1998-09-14 00:00:00', 1298, 3894),
(227, 1, 8, '1998-11-08 00:00:00', 1164, 3492),
(228, 1, 8, '1998-01-11 00:00:00', 547, 1641),
(229, 1, 6, '1998-01-15 00:00:00', 1928, 3856),
(230, 1, 6, '1998-08-16 00:00:00', 1533, 3066),
(231, 1, 6, '1998-02-01 00:00:00', 1516, 3032),
(232, 1, 6, '1998-05-11 00:00:00', 1459, 2918),
(233, 1, 13, '1998-03-13 00:00:00', 2021, 0),
(234, 1, 13, '1998-04-07 00:00:00', 1911, 0),
(235, 1, 13, '1998-03-06 00:00:00', 1128, 0),
(243, 1, 10, '1998-01-02 00:00:00', 2308, 0),
(244, 1, 10, '1998-07-18 00:00:00', 1446, 0),
(245, 1, 10, '1998-11-11 00:00:00', 1210, 0),
(246, 1, 10, '1998-02-16 00:00:00', 645, 0),
(247, 1, 10, '1998-01-06 00:00:00', 616, 0),
(248, 1, 14, '1998-04-28 00:00:00', 2495, 7485),
(249, 1, 14, '1998-08-25 00:00:00', 2240, 6720),
(250, 1, 14, '1998-03-25 00:00:00', 2010, 6030),
(251, 1, 14, '1998-07-11 00:00:00', 1963, 5889),
(252, 1, 14, '1998-11-19 00:00:00', 1697, 5091),
(253, 1, 14, '1998-11-12 00:00:00', 1608, 4824),
(254, 1, 14, '1998-11-03 00:00:00', 1531, 4593),
(255, 1, 14, '1998-03-15 00:00:00', 1439, 4317),
(256, 1, 14, '1998-09-20 00:00:00', 932, 2796),
(257, 1, 4, '1998-10-27 00:00:00', 2350, 0),
(258, 1, 4, '1998-08-20 00:00:00', 931, 0),
(259, 1, 4, '1998-02-09 00:00:00', 695, 0),
(260, 1, 4, '1998-09-25 00:00:00', 562, 0),
(263, 1, 5, '1998-10-12 00:00:00', 1633, 3266),
(264, 1, 5, '1998-10-14 00:00:00', 1412, 2824),
(265, 1, 5, '1998-04-05 00:00:00', 1070, 2140),
(266, 1, 5, '1998-07-27 00:00:00', 915, 1830),
(267, 1, 5, '1998-03-13 00:00:00', 744, 1488),
(268, 1, 5, '1998-01-09 00:00:00', 574, 1148),
(269, 7, 11, '1998-05-01 00:00:00', 2447, 2447),
(270, 7, 11, '1998-12-15 00:00:00', 2206, 2206),
(271, 7, 11, '1998-05-09 00:00:00', 2006, 2006),
(272, 7, 11, '1998-06-24 00:00:00', 1802, 1802),
(273, 7, 11, '1998-11-05 00:00:00', 1234, 1234),
(274, 7, 11, '1998-08-29 00:00:00', 1045, 1045),
(275, 7, 11, '1998-08-02 00:00:00', 593, 593),
(276, 7, 11, '1998-03-14 00:00:00', 263, 263),
(277, 7, 12, '1998-02-17 00:00:00', 2393, 4786),
(278, 7, 12, '1998-12-27 00:00:00', 889, 1778),
(279, 7, 12, '1998-10-04 00:00:00', 550, 1100),
(280, 7, 9, '1998-03-21 00:00:00', 2206, 2206),
(281, 7, 9, '1998-03-22 00:00:00', 2129, 2129),
(282, 7, 9, '1998-04-02 00:00:00', 1906, 1906),
(283, 7, 9, '1998-07-27 00:00:00', 1767, 1767),
(284, 7, 9, '1998-11-30 00:00:00', 971, 971),
(285, 7, 9, '1998-11-08 00:00:00', 592, 592),
(286, 7, 7, '1998-01-06 00:00:00', 2329, 0),
(287, 7, 7, '1998-12-30 00:00:00', 1736, 0),
(288, 7, 7, '1998-06-22 00:00:00', 941, 0),
(289, 7, 3, '1998-01-07 00:00:00', 2337, 2337),
(290, 7, 3, '1998-05-28 00:00:00', 1728, 1728),
(291, 7, 3, '1998-12-10 00:00:00', 1668, 1668),
(292, 7, 3, '1998-09-24 00:00:00', 1486, 1486),
(293, 3, 8, '1998-02-04 00:00:00', 1199, 3597),
(294, 3, 8, '1998-03-29 00:00:00', 1142, 3426),
(295, 3, 8, '1998-02-01 00:00:00', 869, 2607),
(298, 3, 6, '1998-10-02 00:00:00', 1493, 2986),
(299, 3, 6, '1998-06-07 00:00:00', 806, 1612),
(300, 3, 6, '1998-01-11 00:00:00', 625, 1250),
(301, 3, 13, '1998-01-31 00:00:00', 1693, 0),
(309, 3, 10, '1998-12-09 00:00:00', 1998, 0),
(310, 3, 10, '1998-02-23 00:00:00', 1325, 0),
(311, 3, 10, '1998-09-14 00:00:00', 937, 0),
(312, 3, 10, '1998-04-04 00:00:00', 750, 0),
(313, 3, 10, '1998-12-17 00:00:00', 646, 0),
(314, 3, 14, '1998-02-24 00:00:00', 2039, 6117),
(315, 3, 14, '1998-06-23 00:00:00', 1733, 5199),
(316, 3, 14, '1998-04-16 00:00:00', 1357, 4071),
(317, 3, 14, '1998-03-20 00:00:00', 1323, 3969),
(318, 3, 14, '1998-12-24 00:00:00', 976, 2928),
(319, 3, 14, '1998-12-04 00:00:00', 657, 1971),
(320, 3, 4, '1998-01-03 00:00:00', 1987, 0),
(321, 3, 4, '1998-09-15 00:00:00', 1693, 0),
(322, 3, 4, '1998-05-15 00:00:00', 1593, 0),
(323, 3, 4, '1998-11-08 00:00:00', 1023, 0),
(324, 3, 4, '1998-10-24 00:00:00', 846, 0),
(325, 3, 4, '1998-08-22 00:00:00', 764, 0),
(327, 7, 5, '1998-01-02 00:00:00', 2260, 4520),
(328, 3, 5, '1998-02-06 00:00:00', 2256, 4512),
(329, 3, 5, '1998-03-20 00:00:00', 1886, 3772),
(330, 3, 5, '1998-08-04 00:00:00', 1210, 2420),
(331, 3, 5, '1998-12-03 00:00:00', 1108, 2216),
(332, 3, 5, '1998-12-08 00:00:00', 935, 1870),
(334, 9, 11, '1998-03-31 00:00:00', 1722, 1722),
(335, 9, 12, '1998-04-28 00:00:00', 1495, 2990),
(336, 9, 12, '1998-05-31 00:00:00', 1316, 2632),
(337, 9, 12, '1998-06-07 00:00:00', 1306, 2612),
(338, 9, 9, '1998-12-07 00:00:00', 2477, 2477),
(339, 9, 9, '1998-04-15 00:00:00', 1922, 1922),
(340, 9, 9, '1998-04-27 00:00:00', 735, 735),
(342, 9, 7, '1998-09-03 00:00:00', 2180, 0),
(343, 9, 7, '1998-12-01 00:00:00', 1642, 0),
(344, 9, 7, '1998-01-29 00:00:00', 1426, 0),
(345, 9, 7, '1998-05-28 00:00:00', 1125, 0),
(346, 9, 7, '1998-09-15 00:00:00', 575, 0),
(347, 9, 3, '1998-08-02 00:00:00', 1405, 1405),
(348, 9, 3, '1998-02-27 00:00:00', 1278, 1278),
(349, 9, 3, '1998-12-07 00:00:00', 1025, 1025),
(350, 9, 3, '1998-12-28 00:00:00', 967, 967),
(351, 9, 3, '1998-01-27 00:00:00', 895, 895),
(352, 9, 3, '1998-08-11 00:00:00', 686, 686),
(353, 9, 3, '1998-05-09 00:00:00', 646, 646),
(354, 9, 1, '1998-09-22 00:00:00', 2155, 6465),
(355, 9, 1, '1998-01-29 00:00:00', 1459, 4377),
(356, 9, 1, '1998-10-08 00:00:00', 1340, 4020),
(358, 9, 8, '1998-11-16 00:00:00', 2463, 7389),
(359, 9, 8, '1998-07-12 00:00:00', 1126, 3378),
(360, 9, 8, '1998-02-06 00:00:00', 816, 2448),
(361, 9, 6, '1998-09-17 00:00:00', 2463, 4926),
(362, 9, 6, '1998-01-09 00:00:00', 2443, 4886),
(363, 9, 6, '1998-09-18 00:00:00', 1869, 3738),
(364, 9, 6, '1998-12-23 00:00:00', 1719, 3438),
(365, 9, 6, '1998-10-21 00:00:00', 1688, 3376),
(366, 9, 6, '1998-10-25 00:00:00', 1537, 3074),
(367, 9, 6, '1998-05-14 00:00:00', 1196, 2392),
(368, 9, 6, '1998-12-24 00:00:00', 1182, 2364),
(369, 9, 6, '1998-05-05 00:00:00', 1096, 2192),
(370, 9, 6, '1998-11-20 00:00:00', 885, 1770),
(371, 9, 6, '1998-01-22 00:00:00', 832, 1664),
(372, 9, 13, '1998-11-30 00:00:00', 1124, 0),
(375, 9, 10, '1998-07-22 00:00:00', 2257, 0),
(376, 9, 10, '1998-05-20 00:00:00', 1454, 0),
(377, 9, 10, '1998-06-01 00:00:00', 1221, 0),
(379, 9, 14, '1998-03-27 00:00:00', 1798, 5394),
(380, 9, 14, '1998-03-11 00:00:00', 1524, 4572),
(381, 9, 14, '1998-07-24 00:00:00', 1383, 4149),
(382, 9, 4, '1998-09-29 00:00:00', 2294, 0),
(383, 9, 4, '1998-03-27 00:00:00', 2282, 0),
(384, 9, 4, '1998-02-28 00:00:00', 2026, 0),
(385, 9, 4, '1998-01-10 00:00:00', 1936, 0),
(386, 9, 4, '1998-12-11 00:00:00', 691, 0),
(387, 9, 5, '1998-11-07 00:00:00', 1880, 3760),
(388, 9, 5, '1998-12-11 00:00:00', 677, 1354),
(389, 9, 5, '1998-11-05 00:00:00', 516, 1032),
(390, 4, 11, '1998-03-16 00:00:00', 2408, 2408),
(391, 4, 11, '1998-01-31 00:00:00', 2254, 2254),
(392, 4, 11, '1998-11-15 00:00:00', 2054, 2054),
(393, 4, 11, '1998-04-07 00:00:00', 2026, 2026),
(394, 4, 11, '1998-10-16 00:00:00', 1322, 1322),
(395, 4, 11, '1998-02-02 00:00:00', 1234, 1234),
(396, 4, 11, '1998-06-22 00:00:00', 930, 930),
(397, 4, 11, '1998-10-31 00:00:00', 636, 636),
(398, 4, 12, '1998-08-16 00:00:00', 2013, 4026),
(399, 4, 12, '1998-11-09 00:00:00', 1693, 3386),
(400, 4, 12, '1998-04-17 00:00:00', 1446, 2892),
(401, 4, 12, '1998-05-18 00:00:00', 1362, 2724),
(402, 4, 12, '1998-12-10 00:00:00', 1272, 2544),
(403, 4, 12, '1998-10-11 00:00:00', 939, 1878),
(404, 4, 12, '1998-04-24 00:00:00', 502, 1004),
(405, 4, 9, '1998-01-18 00:00:00', 2148, 2148),
(406, 4, 9, '1998-04-01 00:00:00', 1511, 1511),
(407, 4, 9, '1998-02-23 00:00:00', 1387, 1387),
(408, 4, 7, '1998-06-29 00:00:00', 2076, 0),
(409, 4, 7, '1998-11-08 00:00:00', 1896, 0),
(410, 4, 7, '1998-01-19 00:00:00', 1573, 0),
(411, 4, 7, '1998-09-07 00:00:00', 727, 0),
(412, 4, 7, '1998-07-14 00:00:00', 612, 0),
(414, 4, 7, '1998-06-13 00:00:00', 274, 0),
(415, 4, 3, '1998-07-31 00:00:00', 2250, 2250),
(416, 4, 3, '1998-09-18 00:00:00', 2188, 2188),
(417, 4, 3, '1998-06-15 00:00:00', 1917, 1917),
(418, 4, 3, '1998-03-05 00:00:00', 1857, 1857),
(419, 4, 3, '1998-12-24 00:00:00', 1311, 1311),
(420, 4, 3, '1998-11-12 00:00:00', 1162, 1162),
(421, 4, 1, '1998-10-31 00:00:00', 2467, 7401),
(422, 4, 1, '1998-01-26 00:00:00', 2412, 7236),
(423, 4, 1, '1998-06-26 00:00:00', 2363, 7089),
(424, 4, 1, '1998-03-10 00:00:00', 2205, 6615),
(425, 4, 1, '1998-12-25 00:00:00', 2182, 6546),
(426, 4, 1, '1998-12-21 00:00:00', 1317, 3951),
(427, 4, 1, '1998-05-25 00:00:00', 1166, 3498),
(428, 4, 1, '1998-08-27 00:00:00', 750, 2250),
(429, 4, 8, '1998-08-20 00:00:00', 1510, 4530),
(430, 4, 8, '1998-04-02 00:00:00', 953, 2859),
(432, 4, 6, '1998-09-13 00:00:00', 2421, 4842),
(433, 4, 6, '1998-03-17 00:00:00', 1640, 3280),
(434, 4, 6, '1998-09-14 00:00:00', 1142, 2284),
(435, 4, 6, '1998-09-17 00:00:00', 1031, 2062),
(436, 4, 13, '1998-10-16 00:00:00', 2333, 0),
(437, 4, 13, '1998-01-08 00:00:00', 2209, 0),
(438, 4, 13, '1998-06-09 00:00:00', 1971, 0),
(439, 4, 13, '1998-01-28 00:00:00', 1004, 0),
(440, 4, 13, '1998-12-05 00:00:00', 738, 0),
(441, 4, 13, '1998-10-01 00:00:00', 718, 0),
(442, 4, 13, '1998-05-06 00:00:00', 588, 0),
(452, 4, 10, '1998-10-12 00:00:00', 2332, 0),
(453, 4, 10, '1998-07-04 00:00:00', 2071, 0),
(454, 4, 10, '1998-11-18 00:00:00', 1994, 0),
(455, 4, 10, '1998-12-14 00:00:00', 1428, 0),
(456, 4, 10, '1998-01-27 00:00:00', 1222, 0),
(457, 4, 14, '1998-01-19 00:00:00', 1461, 4383),
(458, 4, 14, '1998-01-11 00:00:00', 1398, 4194),
(459, 4, 14, '1998-02-25 00:00:00', 1373, 4119),
(460, 4, 14, '1998-12-23 00:00:00', 1292, 3876),
(461, 4, 14, '1998-10-04 00:00:00', 1242, 3726),
(462, 4, 14, '1998-06-05 00:00:00', 1167, 3501),
(463, 4, 14, '1998-03-21 00:00:00', 946, 2838),
(464, 4, 4, '1998-11-08 00:00:00', 2261, 0),
(465, 4, 4, '1998-06-12 00:00:00', 2228, 0),
(466, 4, 4, '1998-10-19 00:00:00', 2056, 0),
(467, 4, 4, '1998-05-27 00:00:00', 1885, 0),
(468, 4, 4, '1998-10-05 00:00:00', 1176, 0),
(469, 4, 4, '1998-01-11 00:00:00', 507, 0),
(470, 4, 5, '1998-11-19 00:00:00', 2423, 4846),
(471, 4, 5, '1998-08-18 00:00:00', 2145, 4290),
(472, 4, 5, '1998-01-12 00:00:00', 1636, 3272),
(473, 4, 5, '1998-09-02 00:00:00', 1090, 2180),
(474, 4, 5, '1998-02-02 00:00:00', 651, 1302),
(475, 4, 5, '1998-03-06 00:00:00', 622, 1244),
(478, 2, 11, '1998-09-07 00:00:00', 2039, 2039),
(479, 2, 11, '1998-05-14 00:00:00', 1559, 1559),
(480, 2, 11, '1998-06-03 00:00:00', 1238, 1238),
(481, 2, 11, '1998-06-16 00:00:00', 1158, 1158),
(482, 2, 12, '1998-12-15 00:00:00', 1733, 3466),
(483, 2, 12, '1998-07-27 00:00:00', 1197, 2394),
(484, 2, 12, '1998-01-02 00:00:00', 1012, 2024),
(485, 2, 12, '1998-09-09 00:00:00', 817, 1634),
(486, 2, 9, '1998-10-15 00:00:00', 1922, 1922),
(487, 2, 9, '1998-10-20 00:00:00', 1654, 1654),
(488, 2, 9, '1998-02-04 00:00:00', 1527, 1527),
(489, 2, 9, '1998-10-24 00:00:00', 1131, 1131),
(490, 2, 9, '1998-01-08 00:00:00', 1101, 1101),
(491, 2, 9, '1998-08-11 00:00:00', 1062, 1062),
(492, 2, 7, '1998-09-06 00:00:00', 2188, 0),
(493, 2, 7, '1998-08-21 00:00:00', 1476, 0),
(494, 2, 7, '1998-07-21 00:00:00', 969, 0),
(495, 2, 7, '1998-08-28 00:00:00', 786, 0),
(496, 2, 3, '1998-06-18 00:00:00', 1780, 1780),
(497, 2, 3, '1998-08-29 00:00:00', 1620, 1620),
(498, 2, 3, '1998-10-19 00:00:00', 783, 783),
(499, 2, 1, '1998-07-04 00:00:00', 2213, 6639),
(500, 2, 1, '1998-11-11 00:00:00', 1351, 4053),
(501, 2, 1, '1998-07-01 00:00:00', 1304, 3912),
(502, 2, 1, '1998-02-11 00:00:00', 647, 1941),
(503, 2, 8, '1998-08-05 00:00:00', 2252, 6756),
(504, 2, 8, '1998-11-10 00:00:00', 2248, 6744),
(505, 2, 8, '1998-11-07 00:00:00', 1883, 5649),
(506, 2, 8, '1998-12-19 00:00:00', 1794, 5382),
(507, 2, 8, '1998-11-24 00:00:00', 1737, 5211),
(508, 2, 8, '1998-09-30 00:00:00', 510, 1530),
(509, 2, 6, '1998-11-01 00:00:00', 2449, 4898),
(510, 2, 6, '1998-06-02 00:00:00', 1784, 3568),
(511, 2, 6, '1998-12-09 00:00:00', 1109, 2218),
(512, 2, 6, '1998-09-13 00:00:00', 676, 1352),
(513, 2, 6, '1998-01-07 00:00:00', 558, 1116),
(514, 2, 13, '1998-04-03 00:00:00', 2371, 0),
(515, 2, 13, '1998-09-30 00:00:00', 2204, 0),
(516, 2, 13, '1998-08-14 00:00:00', 1926, 0),
(517, 2, 13, '1998-11-04 00:00:00', 1925, 0),
(518, 2, 13, '1998-08-18 00:00:00', 1470, 0),
(519, 2, 13, '1998-09-23 00:00:00', 706, 0),
(520, 2, 13, '1998-01-02 00:00:00', 595, 0),
(526, 2, 10, '1998-09-25 00:00:00', 1949, 0),
(527, 2, 10, '1998-09-11 00:00:00', 1674, 0),
(528, 2, 10, '1998-12-14 00:00:00', 1434, 0),
(529, 2, 10, '1998-06-14 00:00:00', 1309, 0),
(530, 2, 10, '1998-01-07 00:00:00', 995, 0),
(532, 2, 14, '1998-05-28 00:00:00', 2306, 6918),
(533, 2, 14, '1998-08-16 00:00:00', 762, 2286),
(534, 2, 4, '1998-02-24 00:00:00', 2275, 0),
(535, 2, 4, '1998-11-09 00:00:00', 2241, 0),
(536, 2, 4, '1998-01-23 00:00:00', 1477, 0),
(537, 2, 4, '1998-05-19 00:00:00', 670, 0),
(539, 2, 5, '1998-08-27 00:00:00', 2153, 4306),
(540, 2, 5, '1998-07-11 00:00:00', 1521, 3042),
(541, 2, 5, '1998-05-16 00:00:00', 1232, 2464),
(542, 2, 5, '1998-03-16 00:00:00', 839, 1678),
(543, 2, 5, '1998-05-02 00:00:00', 579, 1158),
(544, 8, 11, '1998-12-02 00:00:00', 2373, 2373),
(545, 8, 11, '1998-09-23 00:00:00', 699, 699),
(547, 8, 12, '1998-04-24 00:00:00', 2301, 4602),
(548, 8, 12, '1998-02-07 00:00:00', 1482, 2964),
(549, 8, 12, '1998-08-08 00:00:00', 1308, 2616),
(550, 8, 9, '1998-01-02 00:00:00', 2296, 2296),
(551, 8, 9, '1998-01-16 00:00:00', 1915, 1915),
(552, 8, 9, '1998-08-03 00:00:00', 1762, 1762),
(553, 8, 9, '1998-12-20 00:00:00', 1397, 1397),
(554, 8, 9, '1998-06-11 00:00:00', 1307, 1307),
(555, 8, 9, '1998-08-21 00:00:00', 643, 643),
(556, 8, 7, '1998-04-18 00:00:00', 1729, 0),
(557, 8, 3, '1998-04-16 00:00:00', 2082, 2082),
(558, 8, 3, '1998-08-13 00:00:00', 1934, 1934),
(559, 8, 3, '1998-12-27 00:00:00', 751, 751),
(560, 8, 3, '1998-06-15 00:00:00', 556, 556),
(561, 8, 1, '1998-07-18 00:00:00', 1594, 4782),
(562, 8, 1, '1998-05-08 00:00:00', 1564, 4692),
(563, 8, 1, '1998-08-25 00:00:00', 1491, 4473),
(564, 8, 1, '1998-10-14 00:00:00', 1344, 4032),
(565, 8, 1, '1998-10-24 00:00:00', 1005, 3015),
(566, 8, 1, '1998-10-23 00:00:00', 836, 2508),
(567, 8, 1, '1998-08-20 00:00:00', 525, 1575),
(568, 8, 8, '1998-07-30 00:00:00', 2272, 6816),
(569, 8, 8, '1998-10-29 00:00:00', 1810, 5430),
(570, 8, 8, '1998-06-04 00:00:00', 1594, 4782),
(571, 8, 8, '1998-03-02 00:00:00', 506, 1518),
(572, 8, 6, '1998-06-09 00:00:00', 2252, 4504),
(573, 8, 6, '1998-03-28 00:00:00', 1469, 2938),
(574, 8, 6, '1998-12-14 00:00:00', 832, 1664),
(575, 8, 6, '1998-09-03 00:00:00', 659, 1318),
(576, 8, 13, '1998-04-17 00:00:00', 1697, 0),
(577, 8, 13, '1998-07-07 00:00:00', 1454, 0),
(578, 8, 13, '1998-02-24 00:00:00', 1316, 0),
(582, 8, 10, '1998-10-23 00:00:00', 2048, 0),
(583, 8, 10, '1998-01-08 00:00:00', 1784, 0),
(584, 8, 10, '1998-06-03 00:00:00', 1439, 0),
(585, 8, 10, '1998-07-05 00:00:00', 697, 0),
(586, 8, 10, '1998-01-28 00:00:00', 512, 0),
(587, 8, 14, '1998-10-05 00:00:00', 2477, 7431),
(588, 8, 14, '1998-04-14 00:00:00', 1912, 5736),
(589, 8, 14, '1998-10-02 00:00:00', 1869, 5607),
(590, 8, 14, '1998-10-30 00:00:00', 1287, 3861),
(591, 8, 14, '1998-04-20 00:00:00', 1089, 3267),
(592, 8, 14, '1998-07-27 00:00:00', 641, 1923),
(593, 8, 14, '1998-02-13 00:00:00', 624, 1872),
(594, 8, 4, '1998-09-23 00:00:00', 2077, 0),
(595, 8, 4, '1998-01-28 00:00:00', 1747, 0),
(596, 8, 4, '1998-01-29 00:00:00', 1693, 0),
(597, 8, 4, '1998-01-24 00:00:00', 877, 0),
(598, 8, 4, '1998-03-27 00:00:00', 828, 0),
(599, 8, 4, '1998-12-06 00:00:00', 269, 0),
(600, 8, 5, '1998-10-09 00:00:00', 2167, 4334),
(601, 8, 5, '1998-03-02 00:00:00', 1385, 2770),
(602, 8, 5, '1998-01-04 00:00:00', 1031, 2062),
(603, 8, 5, '1998-11-09 00:00:00', 682, 1364),
(604, 10, 11, '1998-04-07 00:00:00', 2492, 2492),
(605, 10, 11, '1998-07-17 00:00:00', 2312, 2312),
(606, 10, 11, '1998-02-13 00:00:00', 2136, 2136),
(607, 10, 12, '1998-04-12 00:00:00', 2360, 4720),
(608, 10, 12, '1998-04-17 00:00:00', 2118, 4236),
(609, 10, 12, '1998-10-23 00:00:00', 1767, 3534),
(610, 10, 12, '1998-11-16 00:00:00', 501, 1002),
(611, 10, 9, '1998-02-10 00:00:00', 2373, 2373),
(612, 10, 9, '1998-11-21 00:00:00', 2224, 2224),
(613, 10, 9, '1998-09-19 00:00:00', 1689, 1689),
(614, 10, 9, '1998-07-11 00:00:00', 1575, 1575),
(615, 10, 9, '1998-11-29 00:00:00', 567, 567),
(616, 10, 9, '1998-04-07 00:00:00', 519, 519),
(617, 10, 7, '1998-03-27 00:00:00', 2078, 0),
(618, 10, 7, '1998-05-23 00:00:00', 1823, 0),
(619, 10, 7, '1998-04-23 00:00:00', 1004, 0),
(620, 10, 7, '1998-02-14 00:00:00', 955, 0),
(621, 10, 7, '1998-10-01 00:00:00', 700, 0),
(622, 10, 3, '1998-10-24 00:00:00', 1824, 1824),
(623, 10, 3, '1998-10-21 00:00:00', 1803, 1803),
(624, 10, 3, '1998-04-22 00:00:00', 795, 795),
(626, 10, 8, '1998-09-13 00:00:00', 2296, 6888),
(627, 10, 8, '1998-06-08 00:00:00', 1439, 4317),
(628, 10, 8, '1998-03-31 00:00:00', 1178, 3534),
(629, 10, 8, '1998-01-13 00:00:00', 699, 2097),
(631, 10, 6, '1998-06-27 00:00:00', 2288, 4576),
(632, 10, 6, '1998-03-30 00:00:00', 1643, 3286),
(633, 10, 6, '1998-01-18 00:00:00', 789, 1578),
(639, 10, 13, '1998-04-13 00:00:00', 2469, 0),
(645, 10, 10, '1998-02-13 00:00:00', 2387, 0),
(646, 10, 10, '1998-06-29 00:00:00', 1952, 0),
(647, 10, 10, '1998-01-19 00:00:00', 1285, 0),
(648, 10, 10, '1998-09-30 00:00:00', 1202, 0),
(649, 10, 10, '1998-05-23 00:00:00', 720, 0),
(650, 10, 10, '1998-06-18 00:00:00', 293, 0),
(651, 10, 14, '1998-02-23 00:00:00', 1592, 4776),
(652, 10, 4, '1998-10-18 00:00:00', 2275, 0),
(653, 10, 4, '1998-01-06 00:00:00', 824, 0),
(654, 10, 4, '1998-06-30 00:00:00', 591, 0),
(655, 10, 4, '1998-05-23 00:00:00', 567, 0),
(656, 10, 5, '1998-01-31 00:00:00', 2456, 4912),
(657, 10, 5, '1998-04-08 00:00:00', 1773, 3546),
(658, 10, 5, '1998-06-24 00:00:00', 1239, 2478),
(659, 10, 5, '1998-08-17 00:00:00', 756, 1512),
(660, 10, 5, '1998-01-20 00:00:00', 752, 1504),
(661, 10, 5, '1998-07-06 00:00:00', 555, 1110),
(662, 5, 11, '1998-10-19 00:00:00', 2396, 2396),
(663, 5, 11, '1998-04-04 00:00:00', 2150, 2150),
(664, 5, 11, '1998-02-20 00:00:00', 1216, 1216),
(666, 5, 12, '1998-02-18 00:00:00', 2387, 4774),
(667, 5, 12, '1998-07-03 00:00:00', 2075, 4150),
(668, 5, 12, '1998-11-28 00:00:00', 1351, 2702),
(669, 5, 9, '1998-02-03 00:00:00', 1842, 1842),
(670, 5, 9, '1998-07-16 00:00:00', 1813, 1813),
(671, 5, 9, '1998-12-11 00:00:00', 1462, 1462),
(672, 5, 9, '1998-03-17 00:00:00', 1189, 1189),
(673, 5, 9, '1998-04-01 00:00:00', 756, 756),
(675, 5, 7, '1998-01-09 00:00:00', 1930, 0),
(676, 5, 7, '1998-03-14 00:00:00', 1095, 0),
(677, 5, 3, '1998-12-14 00:00:00', 1631, 1631),
(678, 5, 3, '1998-08-23 00:00:00', 1456, 1456),
(679, 5, 3, '1998-03-20 00:00:00', 1339, 1339),
(680, 5, 3, '1998-09-22 00:00:00', 1144, 1144),
(681, 5, 1, '1998-01-28 00:00:00', 1887, 5661),
(682, 5, 1, '1998-04-02 00:00:00', 1505, 4515),
(683, 5, 1, '1998-07-15 00:00:00', 1200, 3600),
(684, 5, 1, '1998-01-26 00:00:00', 989, 2967),
(685, 5, 1, '1998-05-07 00:00:00', 983, 2949),
(686, 5, 1, '1998-11-25 00:00:00', 654, 1962),
(687, 5, 1, '1998-07-29 00:00:00', 642, 1926),
(688, 5, 8, '1998-12-16 00:00:00', 1326, 3978),
(689, 5, 8, '1998-05-26 00:00:00', 728, 2184),
(690, 5, 8, '1998-04-12 00:00:00', 587, 1761),
(691, 5, 6, '1998-08-08 00:00:00', 1835, 3670),
(692, 5, 6, '1998-08-15 00:00:00', 840, 1680),
(693, 5, 6, '1998-04-12 00:00:00', 573, 1146),
(694, 5, 13, '1998-11-08 00:00:00', 923, 0),
(695, 5, 13, '1998-03-31 00:00:00', 867, 0),
(696, 5, 13, '1998-04-28 00:00:00', 851, 0),
(697, 5, 13, '1998-01-04 00:00:00', 556, 0),
(701, 5, 10, '1998-05-05 00:00:00', 2188, 0),
(702, 5, 10, '1998-07-03 00:00:00', 1338, 0),
(703, 5, 14, '1998-03-17 00:00:00', 2052, 6156),
(704, 5, 14, '1998-06-24 00:00:00', 1966, 5898),
(705, 5, 14, '1998-05-03 00:00:00', 1053, 3159),
(707, 5, 4, '1998-05-30 00:00:00', 2306, 0),
(708, 5, 4, '1998-01-26 00:00:00', 2101, 0),
(709, 5, 4, '1998-03-05 00:00:00', 1676, 0),
(710, 5, 4, '1998-12-07 00:00:00', 1313, 0),
(711, 5, 4, '1998-02-18 00:00:00', 1190, 0),
(712, 5, 4, '1998-08-26 00:00:00', 1048, 0),
(713, 5, 4, '1998-07-02 00:00:00', 1029, 0),
(714, 5, 4, '1998-04-20 00:00:00', 686, 0),
(715, 5, 5, '1998-09-26 00:00:00', 1867, 3734),
(716, 5, 5, '1998-07-05 00:00:00', 1219, 2438),
(717, 5, 5, '1998-10-20 00:00:00', 663, 1326),
(718, 5, 5, '1998-07-12 00:00:00', 286, 572),
(719, 12, 11, '1998-08-13 00:00:00', 2387, 2387),
(720, 12, 11, '1998-08-12 00:00:00', 2187, 2187),
(721, 12, 11, '1998-02-10 00:00:00', 1399, 1399),
(722, 12, 11, '1998-07-06 00:00:00', 1089, 1089),
(723, 12, 12, '1998-09-25 00:00:00', 2369, 4738),
(724, 12, 12, '1998-07-31 00:00:00', 1862, 3724),
(725, 12, 12, '1998-08-10 00:00:00', 1546, 3092),
(726, 12, 12, '1998-06-17 00:00:00', 1318, 2636),
(728, 12, 9, '1998-08-22 00:00:00', 2476, 2476),
(729, 12, 9, '1998-08-30 00:00:00', 2339, 2339),
(730, 12, 9, '1998-02-25 00:00:00', 970, 970),
(731, 12, 7, '1998-05-24 00:00:00', 2254, 0),
(732, 12, 7, '1998-11-12 00:00:00', 1737, 0),
(733, 12, 7, '1998-06-03 00:00:00', 1507, 0),
(734, 12, 7, '1998-11-10 00:00:00', 1284, 0),
(735, 12, 7, '1998-06-22 00:00:00', 1176, 0),
(736, 12, 7, '1998-04-17 00:00:00', 819, 0),
(737, 12, 3, '1998-08-17 00:00:00', 1866, 1866),
(738, 12, 3, '1998-09-04 00:00:00', 1847, 1847),
(739, 12, 3, '1998-06-30 00:00:00', 1441, 1441),
(740, 12, 3, '1998-06-22 00:00:00', 1321, 1321),
(741, 12, 3, '1998-06-25 00:00:00', 1000, 1000),
(742, 12, 3, '1998-08-21 00:00:00', 518, 518),
(743, 12, 1, '1998-05-07 00:00:00', 2329, 6987),
(744, 12, 1, '1998-07-23 00:00:00', 874, 2622),
(745, 12, 8, '1998-12-29 00:00:00', 2288, 6864),
(746, 12, 8, '1998-06-29 00:00:00', 1566, 4698),
(747, 12, 8, '1998-05-17 00:00:00', 858, 2574),
(748, 12, 6, '1998-01-28 00:00:00', 2342, 4684),
(749, 12, 6, '1998-03-12 00:00:00', 1845, 3690),
(750, 12, 6, '1998-01-19 00:00:00', 1660, 3320),
(751, 12, 6, '1998-06-24 00:00:00', 1347, 2694),
(752, 12, 6, '1998-03-18 00:00:00', 596, 1192),
(754, 12, 13, '1998-09-09 00:00:00', 2297, 0),
(755, 12, 13, '1998-06-11 00:00:00', 2238, 0),
(756, 12, 13, '1998-12-09 00:00:00', 1949, 0),
(757, 12, 13, '1998-06-03 00:00:00', 1921, 0),
(758, 12, 13, '1998-06-16 00:00:00', 1594, 0),
(759, 12, 13, '1998-12-12 00:00:00', 1021, 0),
(763, 12, 10, '1998-11-19 00:00:00', 1208, 0),
(764, 12, 10, '1998-09-22 00:00:00', 942, 0),
(765, 12, 10, '1998-01-27 00:00:00', 928, 0),
(766, 12, 10, '1998-06-25 00:00:00', 645, 0),
(769, 12, 10, '1998-06-16 00:00:00', 298, 0),
(770, 12, 14, '1998-07-16 00:00:00', 1758, 5274),
(771, 12, 14, '1998-07-27 00:00:00', 1457, 4371),
(772, 12, 14, '1998-06-29 00:00:00', 534, 1602),
(773, 12, 4, '1998-02-14 00:00:00', 1212, 0),
(774, 12, 4, '1998-11-03 00:00:00', 920, 0),
(775, 12, 4, '1998-11-24 00:00:00', 802, 0),
(776, 12, 4, '1998-09-08 00:00:00', 799, 0),
(777, 12, 4, '1998-12-11 00:00:00', 731, 0),
(779, 12, 5, '1998-08-11 00:00:00', 2321, 4642),
(780, 12, 5, '1998-09-29 00:00:00', 1008, 2016),
(781, 12, 5, '1998-11-26 00:00:00', 727, 1454),
(783, 12, 5, '1998-12-31 00:00:00', 258, 516),
(784, 13, 11, '1998-09-26 00:00:00', 1517, 1517),
(785, 13, 11, '1998-03-05 00:00:00', 1508, 1508),
(786, 13, 11, '1998-02-27 00:00:00', 786, 786),
(787, 13, 12, '1998-07-22 00:00:00', 2480, 4960),
(788, 13, 12, '1998-07-01 00:00:00', 2203, 4406),
(789, 13, 12, '1998-04-12 00:00:00', 1629, 3258),
(790, 13, 12, '1998-05-23 00:00:00', 1433, 2866),
(791, 13, 12, '1998-08-25 00:00:00', 1277, 2554),
(792, 13, 12, '1998-07-02 00:00:00', 819, 1638),
(793, 13, 12, '1998-12-27 00:00:00', 817, 1634),
(794, 13, 12, '1998-11-25 00:00:00', 525, 1050),
(797, 13, 9, '1998-05-31 00:00:00', 2136, 2136),
(798, 13, 9, '1998-02-01 00:00:00', 1737, 1737),
(799, 13, 9, '1998-04-15 00:00:00', 1288, 1288),
(800, 13, 9, '1998-09-23 00:00:00', 1026, 1026),
(801, 13, 7, '1998-09-13 00:00:00', 2138, 0),
(802, 13, 7, '1998-03-09 00:00:00', 2132, 0),
(803, 13, 7, '1998-04-29 00:00:00', 1879, 0),
(804, 13, 7, '1998-02-20 00:00:00', 957, 0),
(805, 13, 3, '1998-06-07 00:00:00', 1275, 1275),
(806, 13, 3, '1998-12-04 00:00:00', 1210, 1210),
(807, 13, 1, '1998-03-10 00:00:00', 2388, 7164),
(808, 13, 1, '1998-04-04 00:00:00', 2102, 6306),
(809, 13, 1, '1998-08-13 00:00:00', 1575, 4725),
(810, 13, 1, '1998-01-10 00:00:00', 1409, 4227),
(811, 13, 1, '1998-04-28 00:00:00', 982, 2946),
(812, 13, 1, '1998-12-10 00:00:00', 604, 1812),
(814, 13, 1, '1998-08-25 00:00:00', 255, 765),
(815, 13, 8, '1998-07-08 00:00:00', 2082, 6246),
(816, 13, 8, '1998-05-28 00:00:00', 1528, 4584),
(817, 13, 6, '1998-12-29 00:00:00', 1270, 2540),
(818, 13, 6, '1998-02-10 00:00:00', 948, 1896),
(819, 13, 6, '1998-07-26 00:00:00', 806, 1612),
(820, 13, 13, '1998-06-27 00:00:00', 2164, 0),
(821, 13, 13, '1998-02-09 00:00:00', 2128, 0),
(822, 13, 13, '1998-09-23 00:00:00', 1197, 0),
(823, 13, 13, '1998-09-22 00:00:00', 1009, 0),
(824, 13, 13, '1998-06-20 00:00:00', 661, 0),
(831, 13, 10, '1998-11-21 00:00:00', 1621, 0),
(832, 13, 10, '1998-07-11 00:00:00', 1129, 0),
(833, 13, 10, '1998-06-20 00:00:00', 1033, 0),
(834, 13, 14, '1998-08-05 00:00:00', 2465, 7395),
(835, 13, 14, '1998-09-10 00:00:00', 1978, 5934),
(836, 13, 14, '1998-02-24 00:00:00', 1397, 4191),
(837, 13, 14, '1998-01-28 00:00:00', 266, 798),
(838, 13, 4, '1998-02-23 00:00:00', 1871, 0),
(839, 13, 4, '1998-03-29 00:00:00', 1385, 0),
(840, 13, 5, '1998-12-12 00:00:00', 2475, 4950),
(841, 13, 5, '1998-07-14 00:00:00', 2333, 4666),
(842, 13, 5, '1998-10-26 00:00:00', 2313, 4626),
(843, 13, 5, '1998-09-12 00:00:00', 1898, 3796),
(844, 13, 5, '1998-08-23 00:00:00', 710, 1420),
(846, 13, 5, '1998-09-07 00:00:00', 291, 582),
(847, 13, 5, '1998-08-18 00:00:00', 279, 558),
(848, 11, 11, '1999-03-04 00:00:00', 2362, 2362),
(849, 11, 11, '1999-06-06 00:00:00', 1490, 1490),
(850, 11, 11, '1999-12-09 00:00:00', 1153, 1153),
(851, 11, 12, '1999-08-02 00:00:00', 2087, 4174),
(852, 11, 12, '1999-04-06 00:00:00', 1705, 3410),
(853, 11, 12, '1999-07-16 00:00:00', 1339, 2678),
(854, 11, 12, '1999-01-10 00:00:00', 721, 1442),
(855, 11, 9, '1999-03-19 00:00:00', 2090, 2090),
(856, 11, 9, '1999-04-03 00:00:00', 2027, 2027),
(857, 11, 9, '1999-04-26 00:00:00', 1735, 1735),
(858, 11, 9, '1999-09-28 00:00:00', 856, 856),
(859, 11, 9, '1999-10-06 00:00:00', 649, 649),
(860, 11, 7, '1999-08-07 00:00:00', 2100, 0),
(861, 11, 7, '1999-02-09 00:00:00', 1805, 0),
(862, 11, 7, '1999-12-17 00:00:00', 1618, 0),
(863, 11, 7, '1999-03-10 00:00:00', 1430, 0),
(864, 11, 7, '1999-05-07 00:00:00', 1150, 0),
(865, 11, 7, '1999-07-08 00:00:00', 1096, 0),
(867, 11, 3, '1999-09-04 00:00:00', 1718, 1718),
(868, 11, 3, '1999-07-07 00:00:00', 1064, 1064),
(869, 11, 3, '1999-02-24 00:00:00', 1057, 1057),
(870, 11, 3, '1999-06-19 00:00:00', 768, 768),
(871, 11, 1, '1999-01-17 00:00:00', 2414, 7242),
(872, 11, 1, '1999-07-29 00:00:00', 2302, 6906),
(873, 11, 1, '1999-11-13 00:00:00', 1943, 5829),
(874, 11, 1, '1999-12-25 00:00:00', 1123, 3369),
(875, 11, 1, '1999-10-26 00:00:00', 628, 1884),
(877, 11, 8, '1999-09-11 00:00:00', 2420, 7260),
(878, 11, 8, '1999-07-13 00:00:00', 2119, 6357),
(879, 11, 8, '1999-02-18 00:00:00', 1404, 4212),
(880, 11, 8, '1999-06-22 00:00:00', 616, 1848),
(881, 11, 6, '1999-06-11 00:00:00', 2368, 4736),
(882, 11, 6, '1999-06-21 00:00:00', 2048, 4096),
(883, 11, 6, '1999-01-09 00:00:00', 1637, 3274),
(884, 11, 6, '1999-01-12 00:00:00', 1390, 2780),
(885, 11, 6, '1999-08-13 00:00:00', 1369, 2738),
(886, 11, 6, '1999-07-31 00:00:00', 1104, 2208),
(887, 11, 6, '1999-08-25 00:00:00', 816, 1632),
(888, 11, 6, '1999-10-12 00:00:00', 611, 1222),
(890, 11, 13, '1999-05-01 00:00:00', 2443, 0),
(891, 11, 13, '1999-04-13 00:00:00', 1886, 0),
(892, 11, 13, '1999-07-20 00:00:00', 1477, 0),
(893, 11, 13, '1999-01-10 00:00:00', 1451, 0),
(894, 11, 13, '1999-06-03 00:00:00', 1290, 0),
(903, 11, 10, '1999-01-07 00:00:00', 1314, 0),
(905, 11, 14, '1999-12-06 00:00:00', 2107, 6321),
(906, 11, 14, '1999-12-16 00:00:00', 2089, 6267),
(907, 11, 14, '1999-03-15 00:00:00', 2054, 6162),
(908, 11, 14, '1999-08-03 00:00:00', 1350, 4050),
(909, 11, 14, '1999-10-29 00:00:00', 889, 2667),
(910, 11, 4, '1999-10-22 00:00:00', 2285, 0),
(911, 11, 4, '1999-08-08 00:00:00', 1402, 0),
(912, 11, 4, '1999-04-03 00:00:00', 1270, 0),
(913, 11, 5, '1999-11-03 00:00:00', 1946, 3892),
(914, 11, 5, '1999-03-18 00:00:00', 1365, 2730),
(915, 11, 5, '1999-01-15 00:00:00', 918, 1836),
(917, 6, 11, '1999-06-16 00:00:00', 2190, 2190),
(918, 6, 11, '1999-10-08 00:00:00', 1746, 1746),
(919, 6, 11, '1999-07-04 00:00:00', 1716, 1716),
(921, 6, 12, '1999-09-21 00:00:00', 2430, 4860),
(922, 6, 12, '1999-09-30 00:00:00', 2197, 4394),
(923, 6, 12, '1999-11-25 00:00:00', 1328, 2656),
(924, 6, 12, '1999-11-02 00:00:00', 743, 1486),
(926, 6, 12, '1999-12-12 00:00:00', 263, 526),
(927, 6, 9, '1999-08-14 00:00:00', 2469, 2469),
(928, 6, 9, '1999-02-23 00:00:00', 2346, 2346),
(929, 6, 9, '1999-12-27 00:00:00', 1901, 1901),
(930, 6, 9, '1999-03-01 00:00:00', 1097, 1097),
(931, 6, 7, '1999-05-10 00:00:00', 1877, 0),
(932, 6, 7, '1999-10-04 00:00:00', 1591, 0),
(933, 6, 7, '1999-04-03 00:00:00', 955, 0),
(934, 6, 7, '1999-04-12 00:00:00', 802, 0),
(935, 6, 7, '1999-12-10 00:00:00', 512, 0),
(936, 6, 3, '1999-08-11 00:00:00', 1835, 1835),
(937, 6, 3, '1999-08-12 00:00:00', 1660, 1660),
(938, 6, 3, '1999-06-06 00:00:00', 1226, 1226),
(939, 6, 3, '1999-08-07 00:00:00', 1129, 1129),
(940, 6, 1, '1999-02-03 00:00:00', 1930, 5790),
(941, 6, 1, '1999-02-24 00:00:00', 1914, 5742),
(942, 6, 1, '1999-08-29 00:00:00', 1443, 4329),
(943, 6, 1, '1999-10-20 00:00:00', 524, 1572),
(944, 6, 8, '1999-07-11 00:00:00', 1961, 5883),
(945, 6, 8, '1999-02-22 00:00:00', 1185, 3555),
(946, 6, 8, '1999-01-28 00:00:00', 1065, 3195),
(947, 6, 8, '1999-12-01 00:00:00', 502, 1506),
(950, 6, 6, '1999-12-02 00:00:00', 2401, 4802),
(951, 6, 6, '1999-10-07 00:00:00', 1551, 3102),
(953, 6, 13, '1999-11-25 00:00:00', 2359, 0),
(954, 6, 13, '1999-01-02 00:00:00', 2071, 0),
(955, 6, 13, '1999-10-04 00:00:00', 1974, 0),
(956, 6, 13, '1999-08-22 00:00:00', 280, 0),
(959, 6, 10, '1999-03-09 00:00:00', 1879, 0),
(960, 6, 10, '1999-11-24 00:00:00', 1112, 0),
(961, 6, 10, '1999-02-01 00:00:00', 934, 0),
(962, 6, 14, '1999-07-16 00:00:00', 2433, 7299),
(963, 6, 14, '1999-01-11 00:00:00', 2382, 7146),
(964, 6, 14, '1999-12-05 00:00:00', 2106, 6318),
(966, 6, 4, '1999-02-25 00:00:00', 2136, 0),
(967, 6, 4, '1999-05-18 00:00:00', 2078, 0),
(968, 6, 5, '1999-03-29 00:00:00', 1495, 2990),
(969, 6, 5, '1999-05-23 00:00:00', 1423, 2846),
(970, 6, 5, '1999-12-15 00:00:00', 588, 1176),
(971, 1, 11, '1999-10-04 00:00:00', 2088, 2088),
(972, 1, 11, '1999-10-20 00:00:00', 1273, 1273),
(973, 1, 11, '1999-11-29 00:00:00', 701, 701),
(975, 1, 12, '1999-06-25 00:00:00', 2342, 4684),
(976, 1, 12, '1999-02-08 00:00:00', 1854, 3708),
(977, 1, 12, '1999-10-13 00:00:00', 1149, 2298),
(978, 1, 12, '1999-04-10 00:00:00', 1003, 2006),
(979, 1, 12, '1999-10-20 00:00:00', 898, 1796),
(981, 1, 12, '1999-02-07 00:00:00', 259, 518),
(982, 1, 12, '1999-06-22 00:00:00', 251, 502),
(983, 1, 9, '1999-03-13 00:00:00', 2109, 2109),
(984, 1, 7, '1999-06-05 00:00:00', 2453, 0),
(985, 1, 7, '1999-01-21 00:00:00', 2259, 0),
(986, 1, 7, '1999-02-25 00:00:00', 1798, 0),
(987, 1, 7, '1999-06-08 00:00:00', 1789, 0),
(988, 1, 7, '1999-11-14 00:00:00', 1606, 0),
(989, 1, 7, '1999-12-25 00:00:00', 1331, 0),
(990, 1, 7, '1999-05-29 00:00:00', 708, 0),
(992, 1, 3, '1999-04-23 00:00:00', 2148, 2148),
(993, 1, 3, '1999-06-04 00:00:00', 1625, 1625),
(994, 1, 3, '1999-08-20 00:00:00', 646, 646),
(996, 1, 1, '1999-07-24 00:00:00', 1553, 4659),
(997, 1, 1, '1999-12-20 00:00:00', 1206, 3618),
(998, 1, 8, '1999-10-08 00:00:00', 2391, 7173),
(999, 1, 8, '1999-03-15 00:00:00', 2173, 6519),
(1000, 1, 8, '1999-10-10 00:00:00', 1430, 4290),
(1001, 1, 8, '1999-08-23 00:00:00', 859, 2577),
(1002, 1, 8, '1999-12-03 00:00:00', 802, 2406),
(1004, 1, 6, '1999-02-26 00:00:00', 740, 1480),
(1005, 1, 6, '1999-04-14 00:00:00', 657, 1314),
(1006, 1, 13, '1999-05-20 00:00:00', 2408, 0),
(1007, 1, 13, '1999-09-05 00:00:00', 1985, 0),
(1014, 1, 10, '1999-02-04 00:00:00', 2356, 0),
(1015, 1, 10, '1999-04-21 00:00:00', 2198, 0),
(1016, 1, 10, '1999-01-17 00:00:00', 1421, 0),
(1017, 1, 10, '1999-09-08 00:00:00', 1255, 0),
(1018, 1, 10, '1999-03-21 00:00:00', 1167, 0),
(1019, 1, 10, '1999-10-14 00:00:00', 857, 0),
(1020, 1, 10, '1999-06-09 00:00:00', 545, 0),
(1022, 1, 10, '1999-10-14 00:00:00', 284, 0),
(1023, 1, 14, '1999-08-18 00:00:00', 2398, 7194),
(1024, 1, 14, '1999-09-20 00:00:00', 1491, 4473),
(1025, 1, 14, '1999-03-06 00:00:00', 1170, 3510),
(1026, 1, 14, '1999-07-10 00:00:00', 846, 2538),
(1027, 1, 4, '1999-11-05 00:00:00', 2125, 0),
(1028, 1, 4, '1999-12-02 00:00:00', 1766, 0),
(1029, 1, 4, '1999-08-17 00:00:00', 1065, 0),
(1030, 1, 5, '1999-12-07 00:00:00', 590, 1180),
(1034, 7, 11, '1999-12-20 00:00:00', 2150, 2150),
(1035, 7, 11, '1999-08-09 00:00:00', 2064, 2064),
(1036, 7, 11, '1999-11-24 00:00:00', 1420, 1420),
(1038, 7, 12, '1999-01-06 00:00:00', 2500, 5000),
(1039, 7, 12, '1999-05-14 00:00:00', 2432, 4864),
(1040, 7, 12, '1999-02-06 00:00:00', 1805, 3610),
(1041, 7, 12, '1999-06-28 00:00:00', 1387, 2774),
(1042, 7, 12, '1999-11-10 00:00:00', 1354, 2708),
(1043, 7, 12, '1999-07-27 00:00:00', 578, 1156),
(1044, 7, 12, '1999-04-22 00:00:00', 568, 1136),
(1045, 7, 9, '1999-04-21 00:00:00', 1708, 1708),
(1046, 7, 9, '1999-09-26 00:00:00', 1704, 1704),
(1047, 7, 9, '1999-05-07 00:00:00', 1349, 1349),
(1048, 7, 9, '1999-01-02 00:00:00', 1105, 1105),
(1051, 7, 7, '1999-07-05 00:00:00', 1996, 0),
(1052, 7, 7, '1999-07-20 00:00:00', 1834, 0),
(1053, 7, 7, '1999-11-14 00:00:00', 271, 0),
(1054, 7, 3, '1999-03-17 00:00:00', 2387, 2387),
(1055, 7, 3, '1999-12-26 00:00:00', 1784, 1784),
(1056, 7, 3, '1999-06-09 00:00:00', 1760, 1760),
(1057, 7, 3, '1999-06-14 00:00:00', 1308, 1308),
(1058, 7, 3, '1999-06-09 00:00:00', 1207, 1207),
(1059, 7, 3, '1999-02-10 00:00:00', 988, 988),
(1060, 7, 3, '1999-03-21 00:00:00', 677, 677),
(1061, 7, 1, '1999-10-31 00:00:00', 838, 2514),
(1062, 7, 1, '1999-07-08 00:00:00', 635, 1905),
(1063, 7, 8, '1999-07-15 00:00:00', 1804, 5412),
(1064, 7, 8, '1999-11-30 00:00:00', 1454, 4362),
(1065, 7, 8, '1999-01-26 00:00:00', 1015, 3045),
(1066, 7, 8, '1999-11-02 00:00:00', 559, 1677),
(1068, 7, 6, '1999-12-17 00:00:00', 1471, 2942),
(1069, 7, 13, '1999-07-19 00:00:00', 2333, 0),
(1070, 7, 13, '1999-04-04 00:00:00', 2114, 0),
(1071, 7, 13, '1999-10-15 00:00:00', 1421, 0),
(1072, 7, 13, '1999-09-13 00:00:00', 1043, 0),
(1073, 7, 13, '1999-12-20 00:00:00', 706, 0),
(1074, 7, 13, '1999-05-29 00:00:00', 658, 0),
(1080, 7, 10, '1999-05-10 00:00:00', 2066, 0),
(1081, 7, 10, '1999-10-28 00:00:00', 1927, 0),
(1082, 7, 10, '1999-11-05 00:00:00', 1765, 0),
(1083, 7, 10, '1999-07-13 00:00:00', 1429, 0),
(1085, 7, 14, '1999-09-04 00:00:00', 1346, 4038),
(1086, 7, 14, '1999-03-22 00:00:00', 1148, 3444),
(1087, 7, 14, '1999-05-15 00:00:00', 782, 2346),
(1088, 7, 14, '1999-07-09 00:00:00', 735, 2205),
(1090, 7, 4, '1999-05-21 00:00:00', 2290, 0),
(1091, 7, 4, '1999-09-27 00:00:00', 1722, 0),
(1092, 7, 4, '1999-10-24 00:00:00', 1526, 0),
(1093, 7, 4, '1999-04-28 00:00:00', 1509, 0),
(1094, 7, 4, '1999-02-01 00:00:00', 1414, 0),
(1095, 7, 5, '1999-09-18 00:00:00', 2453, 4906),
(1096, 7, 5, '1999-03-16 00:00:00', 2155, 4310),
(1097, 7, 5, '1999-11-16 00:00:00', 1841, 3682),
(1098, 7, 5, '1999-03-31 00:00:00', 1064, 2128),
(1099, 7, 5, '1999-07-23 00:00:00', 803, 1606),
(1100, 3, 11, '1999-12-16 00:00:00', 2247, 2247),
(1101, 3, 11, '1999-04-04 00:00:00', 1937, 1937),
(1102, 3, 11, '1999-11-04 00:00:00', 1317, 1317),
(1103, 3, 11, '1999-01-31 00:00:00', 1023, 1023),
(1104, 3, 11, '1999-11-13 00:00:00', 672, 672),
(1105, 3, 11, '1999-12-12 00:00:00', 257, 257),
(1106, 3, 12, '1999-07-16 00:00:00', 1901, 3802),
(1107, 3, 12, '1999-03-30 00:00:00', 1515, 3030),
(1108, 3, 12, '1999-05-08 00:00:00', 1287, 2574),
(1109, 3, 12, '1999-01-03 00:00:00', 1253, 2506),
(1110, 3, 12, '1999-06-16 00:00:00', 1173, 2346),
(1111, 3, 12, '1999-11-13 00:00:00', 1025, 2050),
(1112, 3, 12, '1999-08-20 00:00:00', 1024, 2048),
(1113, 3, 12, '1999-01-15 00:00:00', 970, 1940),
(1114, 3, 9, '1999-03-26 00:00:00', 2428, 2428),
(1115, 3, 9, '1999-04-15 00:00:00', 1770, 1770),
(1116, 3, 9, '1999-10-12 00:00:00', 1453, 1453),
(1117, 3, 9, '1999-01-20 00:00:00', 1162, 1162),
(1118, 3, 7, '1999-04-27 00:00:00', 2412, 0),
(1119, 3, 7, '1999-10-30 00:00:00', 1552, 0),
(1120, 3, 3, '1999-06-13 00:00:00', 2351, 2351),
(1121, 3, 3, '1999-06-04 00:00:00', 1602, 1602),
(1122, 3, 3, '1999-01-06 00:00:00', 1187, 1187),
(1123, 3, 3, '1999-06-22 00:00:00', 995, 995),
(1124, 3, 3, '1999-07-14 00:00:00', 892, 892),
(1125, 3, 3, '1999-03-21 00:00:00', 586, 586),
(1126, 3, 1, '1999-04-26 00:00:00', 2403, 7209),
(1127, 3, 1, '1999-05-17 00:00:00', 2074, 6222),
(1128, 3, 8, '1999-04-17 00:00:00', 2358, 7074),
(1129, 3, 8, '1999-03-21 00:00:00', 2260, 6780),
(1130, 3, 8, '1999-10-31 00:00:00', 673, 2019),
(1131, 3, 8, '1999-08-29 00:00:00', 522, 1566),
(1132, 3, 6, '1999-11-04 00:00:00', 2268, 4536),
(1133, 3, 6, '1999-11-24 00:00:00', 2074, 4148),
(1134, 3, 6, '1999-04-12 00:00:00', 1455, 2910),
(1135, 3, 6, '1999-07-14 00:00:00', 738, 1476),
(1136, 3, 13, '1999-01-10 00:00:00', 648, 0),
(1140, 3, 10, '1999-08-04 00:00:00', 1359, 0),
(1141, 3, 10, '1999-06-04 00:00:00', 1004, 0),
(1142, 3, 10, '1999-06-20 00:00:00', 619, 0),
(1143, 3, 10, '1999-08-23 00:00:00', 610, 0),
(1144, 3, 14, '1999-07-03 00:00:00', 2282, 6846),
(1145, 3, 14, '1999-06-07 00:00:00', 1268, 3804),
(1147, 3, 4, '1999-04-19 00:00:00', 2420, 0),
(1148, 3, 4, '1999-02-15 00:00:00', 2146, 0),
(1149, 3, 4, '1999-04-02 00:00:00', 1802, 0),
(1150, 3, 4, '1999-01-08 00:00:00', 1691, 0),
(1151, 3, 4, '1999-03-09 00:00:00', 1684, 0),
(1152, 3, 4, '1999-06-29 00:00:00', 1580, 0),
(1153, 3, 5, '1999-07-08 00:00:00', 2323, 4646),
(1154, 3, 5, '1999-12-13 00:00:00', 2205, 4410),
(1155, 3, 5, '1999-08-19 00:00:00', 1519, 3038),
(1156, 3, 5, '1999-08-17 00:00:00', 622, 1244),
(1157, 3, 5, '1999-10-04 00:00:00', 505, 1010),
(1159, 3, 5, '1999-03-29 00:00:00', 288, 576),
(1160, 9, 11, '1999-05-15 00:00:00', 2371, 2371),
(1161, 9, 11, '1999-11-04 00:00:00', 1231, 1231),
(1162, 9, 12, '1999-02-13 00:00:00', 2371, 4742),
(1163, 9, 12, '1999-01-06 00:00:00', 2257, 4514),
(1164, 9, 12, '1999-10-06 00:00:00', 2140, 4280),
(1165, 9, 12, '1999-05-22 00:00:00', 2071, 4142),
(1166, 9, 12, '1999-04-25 00:00:00', 667, 1334),
(1167, 9, 9, '1999-05-16 00:00:00', 2357, 2357),
(1168, 9, 9, '1999-03-15 00:00:00', 2217, 2217),
(1169, 9, 9, '1999-06-16 00:00:00', 2066, 2066),
(1170, 9, 9, '1999-04-01 00:00:00', 705, 705),
(1171, 9, 9, '1999-08-05 00:00:00', 253, 253),
(1172, 9, 7, '1999-12-15 00:00:00', 2437, 0),
(1173, 9, 7, '1999-05-11 00:00:00', 1949, 0),
(1174, 9, 7, '1999-01-22 00:00:00', 1940, 0),
(1175, 9, 7, '1999-02-20 00:00:00', 1746, 0),
(1176, 9, 7, '1999-08-29 00:00:00', 585, 0),
(1177, 9, 3, '1999-04-10 00:00:00', 2452, 2452),
(1178, 9, 3, '1999-07-30 00:00:00', 2412, 2412),
(1179, 9, 3, '1999-01-11 00:00:00', 2205, 2205),
(1180, 9, 3, '1999-02-02 00:00:00', 2024, 2024),
(1181, 9, 3, '1999-09-21 00:00:00', 1999, 1999),
(1182, 9, 3, '1999-08-02 00:00:00', 1927, 1927),
(1183, 9, 3, '1999-07-05 00:00:00', 1050, 1050),
(1184, 9, 3, '1999-04-04 00:00:00', 851, 851),
(1186, 9, 1, '1999-09-27 00:00:00', 2035, 6105),
(1187, 9, 1, '1999-12-16 00:00:00', 1809, 5427),
(1188, 9, 1, '1999-01-15 00:00:00', 1457, 4371),
(1189, 9, 1, '1999-09-13 00:00:00', 1411, 4233),
(1190, 9, 1, '1999-05-19 00:00:00', 1352, 4056),
(1191, 9, 1, '1999-04-14 00:00:00', 258, 774),
(1192, 9, 8, '1999-09-14 00:00:00', 1077, 3231),
(1193, 9, 8, '1999-11-23 00:00:00', 1036, 3108),
(1194, 9, 8, '1999-11-27 00:00:00', 713, 2139),
(1195, 9, 8, '1999-07-19 00:00:00', 640, 1920),
(1197, 9, 6, '1999-01-24 00:00:00', 1728, 3456),
(1198, 9, 6, '1999-07-23 00:00:00', 1675, 3350),
(1199, 9, 6, '1999-03-25 00:00:00', 1604, 3208),
(1200, 9, 6, '1999-05-18 00:00:00', 1515, 3030),
(1201, 9, 6, '1999-11-20 00:00:00', 866, 1732),
(1202, 9, 13, '1999-01-19 00:00:00', 2459, 0),
(1203, 9, 13, '1999-10-05 00:00:00', 2378, 0),
(1204, 9, 13, '1999-08-26 00:00:00', 2179, 0),
(1205, 9, 13, '1999-07-18 00:00:00', 921, 0),
(1206, 9, 13, '1999-09-10 00:00:00', 897, 0),
(1207, 9, 13, '1999-09-21 00:00:00', 860, 0),
(1208, 9, 13, '1999-05-07 00:00:00', 591, 0),
(1214, 9, 10, '1999-08-19 00:00:00', 2279, 0),
(1215, 9, 10, '1999-03-25 00:00:00', 933, 0),
(1216, 9, 14, '1999-05-28 00:00:00', 1459, 4377),
(1217, 9, 14, '1999-04-05 00:00:00', 686, 2058),
(1219, 9, 4, '1999-01-06 00:00:00', 2398, 0),
(1220, 9, 4, '1999-10-01 00:00:00', 1560, 0),
(1221, 9, 4, '1999-03-02 00:00:00', 961, 0),
(1222, 9, 4, '1999-01-29 00:00:00', 580, 0),
(1223, 9, 5, '1999-11-01 00:00:00', 1957, 3914),
(1224, 9, 5, '1999-09-11 00:00:00', 1534, 3068),
(1225, 9, 5, '1999-10-04 00:00:00', 1473, 2946),
(1226, 9, 5, '1999-08-17 00:00:00', 877, 1754),
(1227, 9, 5, '1999-03-31 00:00:00', 832, 1664),
(1228, 4, 11, '1999-11-10 00:00:00', 1933, 1933),
(1229, 4, 11, '1999-03-14 00:00:00', 1605, 1605),
(1230, 4, 11, '1999-01-02 00:00:00', 1142, 1142),
(1231, 4, 11, '1999-01-02 00:00:00', 772, 772),
(1232, 4, 12, '1999-03-07 00:00:00', 1183, 2366),
(1233, 4, 12, '1999-10-09 00:00:00', 1178, 2356),
(1234, 4, 9, '1999-06-10 00:00:00', 2007, 2007),
(1235, 4, 9, '1999-10-03 00:00:00', 1239, 1239),
(1236, 4, 9, '1999-02-26 00:00:00', 861, 861),
(1237, 4, 7, '1999-04-24 00:00:00', 2132, 0),
(1238, 4, 7, '1999-06-27 00:00:00', 1778, 0),
(1239, 4, 7, '1999-05-14 00:00:00', 1223, 0),
(1240, 4, 7, '1999-06-06 00:00:00', 505, 0),
(1241, 4, 3, '1999-09-19 00:00:00', 1959, 1959),
(1242, 4, 3, '1999-01-13 00:00:00', 1723, 1723),
(1243, 4, 3, '1999-08-15 00:00:00', 1069, 1069),
(1244, 4, 3, '1999-07-21 00:00:00', 963, 963),
(1245, 4, 1, '1999-11-25 00:00:00', 2255, 6765),
(1246, 4, 1, '1999-07-13 00:00:00', 1964, 5892),
(1247, 4, 1, '1999-08-03 00:00:00', 892, 2676),
(1249, 4, 8, '1999-11-24 00:00:00', 2313, 6939),
(1250, 4, 8, '1999-07-21 00:00:00', 2064, 6192),
(1251, 4, 8, '1999-07-30 00:00:00', 1622, 4866),
(1252, 4, 8, '1999-03-29 00:00:00', 979, 2937),
(1253, 4, 6, '1999-04-19 00:00:00', 2462, 4924),
(1254, 4, 6, '1999-05-27 00:00:00', 2195, 4390),
(1255, 4, 6, '1999-12-08 00:00:00', 1292, 2584),
(1256, 4, 6, '1999-03-26 00:00:00', 1007, 2014),
(1257, 4, 6, '1999-12-26 00:00:00', 673, 1346),
(1258, 4, 6, '1999-07-11 00:00:00', 610, 1220),
(1259, 4, 6, '1999-01-28 00:00:00', 553, 1106),
(1261, 4, 6, '1999-07-19 00:00:00', 262, 524),
(1262, 4, 13, '1999-03-19 00:00:00', 2019, 0),
(1263, 4, 13, '1999-08-05 00:00:00', 1972, 0),
(1264, 4, 13, '1999-07-25 00:00:00', 1454, 0),
(1265, 4, 13, '1999-12-07 00:00:00', 1438, 0),
(1266, 4, 13, '1999-07-09 00:00:00', 743, 0),
(1271, 4, 10, '1999-05-31 00:00:00', 2240, 0),
(1272, 4, 10, '1999-10-19 00:00:00', 2159, 0),
(1273, 4, 10, '1999-08-03 00:00:00', 1914, 0),
(1274, 4, 10, '1999-06-28 00:00:00', 1005, 0),
(1275, 4, 10, '1999-03-06 00:00:00', 839, 0),
(1276, 4, 10, '1999-03-17 00:00:00', 711, 0),
(1277, 4, 14, '1999-09-08 00:00:00', 1924, 5772),
(1278, 4, 14, '1999-01-02 00:00:00', 1668, 5004),
(1279, 4, 4, '1999-06-27 00:00:00', 1515, 0),
(1280, 4, 4, '1999-03-29 00:00:00', 997, 0),
(1281, 4, 4, '1999-06-03 00:00:00', 805, 0),
(1282, 4, 4, '1999-05-23 00:00:00', 775, 0),
(1284, 4, 5, '1999-05-17 00:00:00', 2269, 4538),
(1285, 4, 5, '1999-11-30 00:00:00', 1377, 2754);
INSERT INTO `ventas` (`idventas`, `Cod Vendedor`, `Cod Producto`, `Fecha`, `Kilos`, `total`) VALUES
(1287, 2, 11, '1999-07-08 00:00:00', 2498, 2498),
(1288, 2, 11, '1999-04-07 00:00:00', 2144, 2144),
(1289, 2, 11, '1999-12-13 00:00:00', 1525, 1525),
(1290, 2, 11, '1999-04-18 00:00:00', 1339, 1339),
(1291, 2, 11, '1999-09-28 00:00:00', 887, 887),
(1293, 2, 12, '1999-05-08 00:00:00', 1649, 3298),
(1294, 2, 12, '1999-06-28 00:00:00', 1381, 2762),
(1295, 2, 12, '1999-10-09 00:00:00', 1339, 2678),
(1296, 2, 12, '1999-08-29 00:00:00', 1054, 2108),
(1297, 2, 12, '1999-09-29 00:00:00', 1009, 2018),
(1298, 2, 9, '1999-09-17 00:00:00', 2270, 2270),
(1299, 2, 9, '1999-02-27 00:00:00', 1980, 1980),
(1300, 2, 7, '1999-12-02 00:00:00', 2494, 0),
(1301, 2, 7, '1999-04-06 00:00:00', 1621, 0),
(1302, 2, 7, '1999-04-15 00:00:00', 621, 0),
(1303, 2, 7, '1999-11-08 00:00:00', 253, 0),
(1304, 2, 3, '1999-01-05 00:00:00', 2330, 2330),
(1305, 2, 3, '1999-09-29 00:00:00', 2241, 2241),
(1306, 2, 3, '1999-12-03 00:00:00', 957, 957),
(1307, 2, 3, '1999-03-07 00:00:00', 880, 880),
(1308, 2, 3, '1999-10-25 00:00:00', 789, 789),
(1309, 2, 3, '1999-09-11 00:00:00', 714, 714),
(1310, 2, 3, '1999-04-10 00:00:00', 674, 674),
(1311, 2, 1, '1999-10-25 00:00:00', 1580, 4740),
(1312, 2, 8, '1999-10-24 00:00:00', 1743, 5229),
(1313, 2, 8, '1999-04-19 00:00:00', 1606, 4818),
(1314, 2, 8, '1999-09-28 00:00:00', 1523, 4569),
(1315, 2, 8, '1999-07-18 00:00:00', 885, 2655),
(1316, 2, 8, '1999-08-16 00:00:00', 671, 2013),
(1318, 2, 6, '1999-02-19 00:00:00', 2118, 4236),
(1319, 2, 6, '1999-11-21 00:00:00', 1484, 2968),
(1320, 2, 6, '1999-06-16 00:00:00', 1164, 2328),
(1321, 2, 6, '1999-08-21 00:00:00', 770, 1540),
(1322, 2, 13, '1999-02-26 00:00:00', 1326, 0),
(1323, 2, 13, '1999-08-20 00:00:00', 958, 0),
(1332, 2, 10, '1999-11-18 00:00:00', 2159, 0),
(1333, 2, 14, '1999-06-28 00:00:00', 2496, 7488),
(1334, 2, 14, '1999-02-06 00:00:00', 2295, 6885),
(1335, 2, 14, '1999-05-28 00:00:00', 2129, 6387),
(1336, 2, 14, '1999-07-10 00:00:00', 2106, 6318),
(1337, 2, 14, '1999-10-22 00:00:00', 1459, 4377),
(1338, 2, 4, '1999-06-17 00:00:00', 2177, 0),
(1339, 2, 4, '1999-08-18 00:00:00', 2133, 0),
(1340, 2, 4, '1999-07-12 00:00:00', 1432, 0),
(1341, 2, 4, '1999-07-04 00:00:00', 1376, 0),
(1342, 2, 4, '1999-02-06 00:00:00', 870, 0),
(1343, 2, 4, '1999-11-13 00:00:00', 666, 0),
(1344, 2, 4, '1999-04-22 00:00:00', 266, 0),
(1345, 2, 5, '1999-05-28 00:00:00', 2396, 4792),
(1346, 2, 5, '1999-03-27 00:00:00', 2162, 4324),
(1347, 2, 5, '1999-11-03 00:00:00', 2057, 4114),
(1348, 2, 5, '1999-06-29 00:00:00', 1992, 3984),
(1349, 2, 5, '1999-01-30 00:00:00', 1896, 3792),
(1350, 2, 5, '1999-09-07 00:00:00', 1499, 2998),
(1351, 2, 5, '1999-08-17 00:00:00', 1252, 2504),
(1354, 8, 11, '1999-03-31 00:00:00', 2332, 2332),
(1355, 8, 11, '1999-07-08 00:00:00', 2009, 2009),
(1356, 8, 11, '1999-05-23 00:00:00', 1768, 1768),
(1357, 8, 11, '1999-04-20 00:00:00', 1415, 1415),
(1358, 8, 11, '1999-11-26 00:00:00', 1016, 1016),
(1359, 8, 11, '1999-05-29 00:00:00', 940, 940),
(1360, 8, 11, '1999-06-19 00:00:00', 883, 883),
(1362, 8, 12, '1999-09-01 00:00:00', 1895, 3790),
(1363, 8, 12, '1999-11-23 00:00:00', 1732, 3464),
(1364, 8, 12, '1999-10-04 00:00:00', 1189, 2378),
(1365, 8, 12, '1999-10-06 00:00:00', 956, 1912),
(1367, 8, 9, '1999-01-07 00:00:00', 2068, 2068),
(1368, 8, 9, '1999-10-12 00:00:00', 1972, 1972),
(1369, 8, 9, '1999-02-26 00:00:00', 1495, 1495),
(1370, 8, 9, '1999-06-29 00:00:00', 1026, 1026),
(1371, 8, 7, '1999-02-28 00:00:00', 2361, 0),
(1372, 8, 7, '1999-07-17 00:00:00', 1515, 0),
(1373, 8, 7, '1999-10-30 00:00:00', 1090, 0),
(1374, 8, 7, '1999-11-26 00:00:00', 1085, 0),
(1375, 8, 7, '1999-01-10 00:00:00', 267, 0),
(1376, 8, 3, '1999-02-15 00:00:00', 1773, 1773),
(1377, 8, 3, '1999-05-07 00:00:00', 1763, 1763),
(1378, 8, 3, '1999-12-17 00:00:00', 1307, 1307),
(1379, 8, 3, '1999-05-05 00:00:00', 1011, 1011),
(1381, 8, 3, '1999-11-20 00:00:00', 271, 271),
(1382, 8, 1, '1999-10-24 00:00:00', 1487, 4461),
(1383, 8, 1, '1999-07-24 00:00:00', 1434, 4302),
(1384, 8, 8, '1999-11-22 00:00:00', 2155, 6465),
(1385, 8, 8, '1999-04-27 00:00:00', 2068, 6204),
(1386, 8, 8, '1999-02-03 00:00:00', 1070, 3210),
(1387, 8, 8, '1999-06-01 00:00:00', 778, 2334),
(1388, 8, 8, '1999-05-10 00:00:00', 551, 1653),
(1389, 8, 6, '1999-09-09 00:00:00', 2407, 4814),
(1390, 8, 6, '1999-06-04 00:00:00', 2397, 4794),
(1391, 8, 6, '1999-06-24 00:00:00', 1181, 2362),
(1392, 8, 6, '1999-12-17 00:00:00', 1049, 2098),
(1393, 8, 13, '1999-02-20 00:00:00', 1070, 0),
(1394, 8, 13, '1999-06-21 00:00:00', 894, 0),
(1398, 8, 10, '1999-11-23 00:00:00', 2371, 0),
(1399, 8, 10, '1999-07-27 00:00:00', 2085, 0),
(1400, 8, 10, '1999-09-23 00:00:00', 1071, 0),
(1401, 8, 10, '1999-06-24 00:00:00', 684, 0),
(1402, 8, 10, '1999-06-22 00:00:00', 683, 0),
(1403, 8, 10, '1999-08-03 00:00:00', 615, 0),
(1405, 8, 14, '1999-09-22 00:00:00', 2042, 6126),
(1406, 8, 14, '1999-12-26 00:00:00', 1654, 4962),
(1407, 8, 14, '1999-06-07 00:00:00', 1565, 4695),
(1408, 8, 14, '1999-08-26 00:00:00', 1468, 4404),
(1409, 8, 14, '1999-08-13 00:00:00', 859, 2577),
(1410, 8, 4, '1999-02-06 00:00:00', 2242, 0),
(1411, 8, 4, '1999-06-24 00:00:00', 1456, 0),
(1412, 8, 4, '1999-02-06 00:00:00', 715, 0),
(1413, 8, 4, '1999-05-15 00:00:00', 553, 0),
(1414, 8, 4, '1999-04-17 00:00:00', 268, 0),
(1415, 8, 5, '1999-11-10 00:00:00', 2246, 4492),
(1416, 8, 5, '1999-07-21 00:00:00', 2214, 4428),
(1417, 8, 5, '1999-06-06 00:00:00', 2122, 4244),
(1418, 8, 5, '1999-10-06 00:00:00', 1961, 3922),
(1419, 8, 5, '1999-08-12 00:00:00', 1418, 2836),
(1420, 10, 11, '1999-01-20 00:00:00', 2429, 2429),
(1421, 10, 11, '1999-07-22 00:00:00', 2229, 2229),
(1422, 10, 11, '1999-09-10 00:00:00', 2174, 2174),
(1423, 10, 11, '1999-10-29 00:00:00', 2003, 2003),
(1424, 10, 11, '1999-03-12 00:00:00', 1124, 1124),
(1425, 10, 12, '1999-07-08 00:00:00', 1373, 2746),
(1426, 10, 12, '1999-03-23 00:00:00', 276, 552),
(1427, 10, 9, '1999-08-11 00:00:00', 2268, 2268),
(1428, 10, 7, '1999-04-08 00:00:00', 1834, 0),
(1429, 10, 7, '1999-04-20 00:00:00', 755, 0),
(1430, 10, 3, '1999-01-21 00:00:00', 2484, 2484),
(1431, 10, 3, '1999-01-14 00:00:00', 2479, 2479),
(1432, 10, 3, '1999-02-24 00:00:00', 1972, 1972),
(1433, 10, 3, '1999-09-22 00:00:00', 1620, 1620),
(1434, 10, 3, '1999-07-21 00:00:00', 824, 824),
(1436, 10, 1, '1999-02-01 00:00:00', 2119, 6357),
(1437, 10, 1, '1999-06-03 00:00:00', 1826, 5478),
(1438, 10, 1, '1999-08-29 00:00:00', 1605, 4815),
(1439, 10, 1, '1999-06-29 00:00:00', 1244, 3732),
(1440, 10, 1, '1999-02-01 00:00:00', 951, 2853),
(1441, 10, 1, '1999-07-30 00:00:00', 892, 2676),
(1442, 10, 8, '1999-07-23 00:00:00', 1992, 5976),
(1443, 10, 8, '1999-07-19 00:00:00', 1800, 5400),
(1444, 10, 8, '1999-04-22 00:00:00', 978, 2934),
(1445, 10, 8, '1999-08-29 00:00:00', 842, 2526),
(1447, 10, 6, '1999-01-17 00:00:00', 1971, 3942),
(1448, 10, 6, '1999-11-05 00:00:00', 1913, 3826),
(1449, 10, 6, '1999-10-24 00:00:00', 1167, 2334),
(1450, 10, 6, '1999-01-31 00:00:00', 569, 1138),
(1452, 10, 13, '1999-10-18 00:00:00', 2328, 0),
(1453, 10, 13, '1999-06-19 00:00:00', 1236, 0),
(1454, 10, 13, '1999-01-24 00:00:00', 1077, 0),
(1455, 10, 13, '1999-01-09 00:00:00', 700, 0),
(1456, 10, 13, '1999-02-14 00:00:00', 647, 0),
(1457, 10, 13, '1999-04-23 00:00:00', 635, 0),
(1465, 10, 10, '1999-06-09 00:00:00', 2329, 0),
(1466, 10, 10, '1999-02-14 00:00:00', 2227, 0),
(1467, 10, 10, '1999-05-23 00:00:00', 843, 0),
(1469, 10, 14, '1999-12-12 00:00:00', 2111, 6333),
(1470, 10, 14, '1999-07-20 00:00:00', 1662, 4986),
(1471, 10, 14, '1999-10-09 00:00:00', 812, 2436),
(1472, 10, 14, '1999-11-14 00:00:00', 700, 2100),
(1473, 10, 14, '1999-12-29 00:00:00', 548, 1644),
(1474, 10, 4, '1999-10-01 00:00:00', 1829, 0),
(1475, 10, 4, '1999-07-04 00:00:00', 1692, 0),
(1476, 10, 5, '1999-05-19 00:00:00', 2167, 4334),
(1477, 10, 5, '1999-09-04 00:00:00', 1926, 3852),
(1478, 10, 5, '1999-07-02 00:00:00', 1923, 3846),
(1479, 10, 5, '1999-08-06 00:00:00', 1240, 2480),
(1480, 5, 11, '1999-10-06 00:00:00', 2360, 2360),
(1481, 5, 11, '1999-08-05 00:00:00', 2131, 2131),
(1482, 5, 11, '1999-03-02 00:00:00', 1372, 1372),
(1483, 5, 12, '1999-12-21 00:00:00', 2269, 4538),
(1484, 5, 12, '1999-04-15 00:00:00', 2041, 4082),
(1485, 5, 12, '1999-11-20 00:00:00', 1682, 3364),
(1486, 5, 12, '1999-06-16 00:00:00', 843, 1686),
(1487, 5, 9, '1999-08-30 00:00:00', 2067, 2067),
(1488, 5, 9, '1999-12-14 00:00:00', 2026, 2026),
(1489, 5, 9, '1999-01-18 00:00:00', 1354, 1354),
(1490, 5, 9, '1999-03-09 00:00:00', 1280, 1280),
(1491, 5, 7, '1999-11-11 00:00:00', 1808, 0),
(1492, 5, 7, '1999-04-08 00:00:00', 1803, 0),
(1493, 5, 7, '1999-04-26 00:00:00', 1769, 0),
(1494, 5, 7, '1999-08-29 00:00:00', 999, 0),
(1495, 5, 7, '1999-10-08 00:00:00', 927, 0),
(1496, 5, 7, '1999-07-29 00:00:00', 825, 0),
(1497, 5, 7, '1999-08-05 00:00:00', 632, 0),
(1498, 5, 3, '1999-08-07 00:00:00', 2423, 2423),
(1499, 5, 3, '1999-04-20 00:00:00', 1972, 1972),
(1500, 5, 3, '1999-08-22 00:00:00', 1151, 1151),
(1501, 5, 3, '1999-09-26 00:00:00', 894, 894),
(1502, 5, 3, '1999-12-26 00:00:00', 525, 525),
(1503, 5, 3, '1999-04-26 00:00:00', 508, 508),
(1504, 5, 1, '1999-03-01 00:00:00', 2284, 6852),
(1505, 5, 1, '1999-05-26 00:00:00', 2116, 6348),
(1506, 5, 1, '1999-10-07 00:00:00', 1732, 5196),
(1507, 5, 1, '1999-09-12 00:00:00', 1621, 4863),
(1508, 5, 1, '1999-03-03 00:00:00', 851, 2553),
(1509, 5, 8, '1999-11-11 00:00:00', 2313, 6939),
(1510, 5, 8, '1999-08-08 00:00:00', 1187, 3561),
(1511, 5, 8, '1999-11-27 00:00:00', 960, 2880),
(1512, 5, 6, '1999-10-17 00:00:00', 2464, 4928),
(1513, 5, 6, '1999-07-24 00:00:00', 1766, 3532),
(1514, 5, 6, '1999-01-16 00:00:00', 1234, 2468),
(1515, 5, 6, '1999-11-25 00:00:00', 904, 1808),
(1516, 5, 6, '1999-03-09 00:00:00', 711, 1422),
(1517, 5, 6, '1999-01-27 00:00:00', 697, 1394),
(1518, 5, 6, '1999-10-20 00:00:00', 654, 1308),
(1519, 5, 13, '1999-02-27 00:00:00', 1880, 0),
(1520, 5, 13, '1999-03-14 00:00:00', 1849, 0),
(1521, 5, 13, '1999-06-16 00:00:00', 1340, 0),
(1522, 5, 13, '1999-07-11 00:00:00', 715, 0),
(1523, 5, 13, '1999-10-10 00:00:00', 659, 0),
(1524, 5, 13, '1999-12-07 00:00:00', 262, 0),
(1527, 5, 10, '1999-08-23 00:00:00', 2247, 0),
(1528, 5, 10, '1999-09-08 00:00:00', 2227, 0),
(1529, 5, 10, '1999-04-14 00:00:00', 2226, 0),
(1530, 5, 10, '1999-11-05 00:00:00', 2180, 0),
(1531, 5, 10, '1999-07-14 00:00:00', 1683, 0),
(1532, 5, 10, '1999-09-07 00:00:00', 1486, 0),
(1533, 5, 10, '1999-11-10 00:00:00', 1471, 0),
(1534, 5, 10, '1999-10-18 00:00:00', 957, 0),
(1535, 5, 10, '1999-05-30 00:00:00', 836, 0),
(1536, 5, 10, '1999-09-06 00:00:00', 793, 0),
(1538, 5, 14, '1999-03-31 00:00:00', 2460, 7380),
(1539, 5, 14, '1999-10-13 00:00:00', 2053, 6159),
(1540, 5, 14, '1999-07-16 00:00:00', 1883, 5649),
(1541, 5, 14, '1999-01-10 00:00:00', 1205, 3615),
(1542, 5, 14, '1999-03-09 00:00:00', 872, 2616),
(1543, 5, 14, '1999-05-17 00:00:00', 821, 2463),
(1544, 5, 4, '1999-01-16 00:00:00', 1929, 0),
(1545, 5, 4, '1999-02-20 00:00:00', 1516, 0),
(1546, 5, 4, '1999-06-28 00:00:00', 1361, 0),
(1547, 5, 4, '1999-01-26 00:00:00', 1318, 0),
(1548, 5, 4, '1999-11-10 00:00:00', 1124, 0),
(1549, 5, 4, '1999-08-15 00:00:00', 684, 0),
(1550, 5, 4, '1999-07-30 00:00:00', 606, 0),
(1552, 5, 5, '1999-06-16 00:00:00', 1962, 3924),
(1553, 5, 5, '1999-11-07 00:00:00', 1211, 2422),
(1554, 5, 5, '1999-08-13 00:00:00', 592, 1184),
(1555, 12, 11, '1999-03-03 00:00:00', 1222, 1222),
(1556, 12, 12, '1999-06-13 00:00:00', 2143, 4286),
(1557, 12, 12, '1999-02-27 00:00:00', 1155, 2310),
(1558, 12, 12, '1999-07-18 00:00:00', 586, 1172),
(1559, 12, 12, '1999-04-13 00:00:00', 508, 1016),
(1560, 12, 9, '1999-06-15 00:00:00', 2092, 2092),
(1561, 12, 9, '1999-03-10 00:00:00', 1150, 1150),
(1562, 12, 9, '1999-01-09 00:00:00', 809, 809),
(1563, 12, 9, '1999-05-20 00:00:00', 660, 660),
(1567, 12, 7, '1999-12-15 00:00:00', 2392, 0),
(1568, 12, 7, '1999-09-28 00:00:00', 1652, 0),
(1569, 12, 7, '1999-06-12 00:00:00', 1332, 0),
(1570, 12, 7, '1999-08-09 00:00:00', 1000, 0),
(1571, 12, 3, '1999-04-05 00:00:00', 2092, 2092),
(1573, 12, 3, '1999-06-10 00:00:00', 279, 279),
(1574, 12, 1, '1999-10-28 00:00:00', 2249, 6747),
(1575, 12, 1, '1999-11-12 00:00:00', 1575, 4725),
(1576, 12, 1, '1999-10-12 00:00:00', 831, 2493),
(1577, 12, 1, '1999-12-02 00:00:00', 537, 1611),
(1578, 12, 8, '1999-09-13 00:00:00', 2244, 6732),
(1579, 12, 8, '1999-03-13 00:00:00', 1980, 5940),
(1580, 12, 8, '1999-04-06 00:00:00', 1712, 5136),
(1581, 12, 8, '1999-12-14 00:00:00', 1206, 3618),
(1582, 12, 6, '1999-06-28 00:00:00', 2403, 4806),
(1583, 12, 6, '1999-07-04 00:00:00', 2255, 4510),
(1584, 12, 6, '1999-05-19 00:00:00', 2201, 4402),
(1585, 12, 6, '1999-05-08 00:00:00', 2124, 4248),
(1586, 12, 6, '1999-12-24 00:00:00', 2114, 4228),
(1587, 12, 6, '1999-12-25 00:00:00', 1702, 3404),
(1588, 12, 6, '1999-04-06 00:00:00', 1451, 2902),
(1589, 12, 6, '1999-01-08 00:00:00', 1417, 2834),
(1590, 12, 6, '1999-02-20 00:00:00', 283, 566),
(1591, 12, 13, '1999-03-12 00:00:00', 2070, 0),
(1592, 12, 13, '1999-06-11 00:00:00', 1482, 0),
(1593, 12, 13, '1999-10-22 00:00:00', 1188, 0),
(1594, 12, 13, '1999-10-26 00:00:00', 918, 0),
(1604, 12, 10, '1999-03-15 00:00:00', 1581, 0),
(1605, 12, 10, '1999-08-11 00:00:00', 1411, 0),
(1606, 12, 10, '1999-12-06 00:00:00', 259, 0),
(1607, 12, 14, '1999-08-21 00:00:00', 2399, 7197),
(1608, 12, 14, '1999-06-06 00:00:00', 2010, 6030),
(1609, 12, 14, '1999-07-02 00:00:00', 1607, 4821),
(1610, 12, 14, '1999-06-19 00:00:00', 1338, 4014),
(1611, 12, 14, '1999-04-18 00:00:00', 1317, 3951),
(1612, 12, 4, '1999-11-17 00:00:00', 2181, 0),
(1613, 12, 4, '1999-09-11 00:00:00', 2084, 0),
(1614, 12, 4, '1999-02-26 00:00:00', 1874, 0),
(1615, 12, 4, '1999-04-26 00:00:00', 1326, 0),
(1616, 12, 4, '1999-04-14 00:00:00', 1277, 0),
(1617, 12, 4, '1999-04-03 00:00:00', 572, 0),
(1619, 12, 5, '1999-03-07 00:00:00', 2241, 4482),
(1620, 12, 5, '1999-03-09 00:00:00', 2118, 4236),
(1621, 12, 5, '1999-07-03 00:00:00', 1600, 3200),
(1623, 13, 11, '1999-03-21 00:00:00', 1653, 1653),
(1624, 13, 11, '1999-09-09 00:00:00', 1519, 1519),
(1625, 13, 11, '1999-12-12 00:00:00', 581, 581),
(1627, 13, 12, '1999-05-25 00:00:00', 2451, 4902),
(1628, 13, 12, '1999-05-23 00:00:00', 1192, 2384),
(1629, 13, 9, '1999-07-05 00:00:00', 2160, 2160),
(1630, 13, 9, '1999-05-24 00:00:00', 2154, 2154),
(1631, 13, 9, '1999-09-06 00:00:00', 1802, 1802),
(1632, 13, 9, '1999-12-11 00:00:00', 930, 930),
(1633, 13, 9, '1999-06-09 00:00:00', 652, 652),
(1635, 13, 7, '1999-09-17 00:00:00', 2395, 0),
(1636, 13, 7, '1999-01-20 00:00:00', 1776, 0),
(1637, 13, 7, '1999-04-03 00:00:00', 1655, 0),
(1638, 13, 7, '1999-01-18 00:00:00', 1119, 0),
(1639, 13, 7, '1999-01-27 00:00:00', 830, 0),
(1640, 13, 7, '1999-02-16 00:00:00', 279, 0),
(1641, 13, 7, '1999-08-29 00:00:00', 262, 0),
(1642, 13, 3, '1999-01-18 00:00:00', 2061, 2061),
(1643, 13, 3, '1999-08-10 00:00:00', 1621, 1621),
(1644, 13, 3, '1999-04-26 00:00:00', 1362, 1362),
(1645, 13, 3, '1999-01-15 00:00:00', 1342, 1342),
(1646, 13, 3, '1999-03-19 00:00:00', 1053, 1053),
(1648, 13, 1, '1999-02-14 00:00:00', 1970, 5910),
(1649, 13, 1, '1999-10-21 00:00:00', 735, 2205),
(1651, 13, 8, '1999-01-05 00:00:00', 2322, 6966),
(1652, 13, 8, '1999-07-25 00:00:00', 1944, 5832),
(1654, 13, 6, '1999-07-08 00:00:00', 2498, 4996),
(1655, 13, 6, '1999-05-25 00:00:00', 2207, 4414),
(1656, 13, 6, '1999-02-08 00:00:00', 1958, 3916),
(1657, 13, 6, '1999-03-10 00:00:00', 1276, 2552),
(1658, 13, 6, '1999-07-27 00:00:00', 1274, 2548),
(1659, 13, 6, '1999-06-29 00:00:00', 654, 1308),
(1660, 13, 6, '1999-07-23 00:00:00', 285, 570),
(1661, 13, 13, '1999-02-12 00:00:00', 757, 0),
(1664, 13, 10, '1999-07-10 00:00:00', 2440, 0),
(1665, 13, 10, '1999-03-19 00:00:00', 2181, 0),
(1666, 13, 10, '1999-10-13 00:00:00', 718, 0),
(1667, 13, 14, '1999-12-14 00:00:00', 1954, 5862),
(1668, 13, 14, '1999-05-15 00:00:00', 1682, 5046),
(1669, 13, 14, '1999-02-01 00:00:00', 1593, 4779),
(1670, 13, 14, '1999-03-04 00:00:00', 1557, 4671),
(1671, 13, 14, '1999-06-08 00:00:00', 1533, 4599),
(1672, 13, 14, '1999-11-27 00:00:00', 899, 2697),
(1673, 13, 14, '1999-12-16 00:00:00', 820, 2460),
(1674, 13, 14, '1999-04-10 00:00:00', 803, 2409),
(1677, 13, 4, '1999-06-19 00:00:00', 732, 0),
(1678, 13, 4, '1999-06-04 00:00:00', 569, 0),
(1679, 13, 5, '1999-06-25 00:00:00', 1214, 2428),
(1680, 11, 11, '2000-11-17 00:00:00', 2155, 2155),
(1681, 11, 11, '2000-05-23 00:00:00', 1955, 1955),
(1682, 11, 11, '2000-06-18 00:00:00', 1954, 1954),
(1683, 11, 11, '2000-08-14 00:00:00', 764, 764),
(1684, 11, 11, '2000-03-09 00:00:00', 695, 695),
(1685, 11, 11, '2000-03-12 00:00:00', 529, 529),
(1687, 11, 12, '2000-12-14 00:00:00', 2419, 4838),
(1688, 11, 12, '2000-03-10 00:00:00', 2049, 4098),
(1689, 11, 12, '2000-04-24 00:00:00', 1920, 3840),
(1690, 11, 12, '2000-07-12 00:00:00', 1490, 2980),
(1691, 11, 12, '2000-10-14 00:00:00', 1291, 2582),
(1692, 11, 12, '2000-05-30 00:00:00', 1180, 2360),
(1693, 11, 12, '2000-05-25 00:00:00', 689, 1378),
(1695, 11, 9, '2000-01-09 00:00:00', 2423, 2423),
(1696, 11, 9, '2000-11-01 00:00:00', 2176, 2176),
(1697, 11, 9, '2000-04-26 00:00:00', 2059, 2059),
(1698, 11, 9, '2000-06-08 00:00:00', 1792, 1792),
(1699, 11, 9, '2000-08-10 00:00:00', 1349, 1349),
(1700, 11, 7, '2000-02-03 00:00:00', 955, 0),
(1702, 11, 3, '2000-08-18 00:00:00', 1112, 1112),
(1703, 11, 3, '2000-09-26 00:00:00', 878, 878),
(1704, 11, 1, '2000-08-18 00:00:00', 1063, 3189),
(1705, 11, 8, '2000-05-23 00:00:00', 2465, 7395),
(1706, 11, 8, '2000-05-07 00:00:00', 1982, 5946),
(1707, 11, 8, '2000-04-29 00:00:00', 1590, 4770),
(1708, 11, 8, '2000-09-15 00:00:00', 1573, 4719),
(1709, 11, 8, '2000-11-15 00:00:00', 1439, 4317),
(1710, 11, 8, '2000-04-16 00:00:00', 1065, 3195),
(1711, 11, 8, '2000-12-16 00:00:00', 825, 2475),
(1712, 11, 6, '2000-04-14 00:00:00', 2423, 4846),
(1713, 11, 6, '2000-04-06 00:00:00', 1851, 3702),
(1714, 11, 6, '2000-12-03 00:00:00', 1135, 2270),
(1715, 11, 6, '2000-01-03 00:00:00', 1103, 2206),
(1725, 11, 10, '2000-06-27 00:00:00', 1401, 0),
(1726, 11, 10, '2000-11-05 00:00:00', 521, 0),
(1728, 11, 14, '2000-12-10 00:00:00', 1674, 5022),
(1729, 11, 14, '2000-03-31 00:00:00', 1647, 4941),
(1730, 11, 4, '2000-03-06 00:00:00', 1484, 0),
(1731, 11, 4, '2000-06-26 00:00:00', 1274, 0),
(1732, 11, 5, '2000-07-08 00:00:00', 2280, 4560),
(1733, 11, 5, '2000-02-20 00:00:00', 2184, 4368),
(1734, 11, 5, '2000-01-03 00:00:00', 2166, 4332),
(1735, 11, 5, '2000-05-25 00:00:00', 1762, 3524),
(1736, 11, 5, '2000-10-20 00:00:00', 1103, 2206),
(1738, 6, 11, '2000-07-29 00:00:00', 1060, 1060),
(1739, 6, 11, '2000-04-30 00:00:00', 1078, 1078),
(1740, 6, 11, '2000-01-09 00:00:00', 818, 818),
(1741, 6, 11, '2000-04-18 00:00:00', 1479, 1479),
(1742, 6, 11, '2000-07-18 00:00:00', 2346, 2346),
(1743, 6, 11, '2000-10-15 00:00:00', 2403, 2403),
(1744, 6, 11, '2000-10-31 00:00:00', 2404, 2404),
(1745, 6, 12, '2000-05-31 00:00:00', 1687, 3374),
(1746, 6, 12, '2000-12-02 00:00:00', 1992, 3984),
(1747, 6, 9, '2000-07-04 00:00:00', 1601, 1601),
(1748, 6, 9, '2000-04-26 00:00:00', 2018, 2018),
(1749, 6, 7, '2000-06-26 00:00:00', 1445, 0),
(1750, 6, 7, '2000-08-30 00:00:00', 1732, 0),
(1751, 6, 7, '2000-09-30 00:00:00', 2099, 0),
(1752, 6, 7, '2000-10-08 00:00:00', 842, 0),
(1753, 6, 3, '2000-09-28 00:00:00', 1496, 1496),
(1754, 6, 3, '2000-03-19 00:00:00', 1811, 1811),
(1755, 6, 3, '2000-07-03 00:00:00', 1968, 1968),
(1756, 6, 3, '2000-06-25 00:00:00', 2153, 2153),
(1757, 6, 3, '2000-10-08 00:00:00', 851, 851),
(1758, 6, 1, '2000-05-07 00:00:00', 1848, 5544),
(1759, 6, 1, '2000-04-11 00:00:00', 2195, 6585),
(1760, 6, 1, '2000-01-02 00:00:00', 2239, 6717),
(1761, 6, 1, '2000-02-15 00:00:00', 2479, 7437),
(1762, 6, 8, '2000-08-23 00:00:00', 1537, 4611),
(1763, 6, 8, '2000-01-27 00:00:00', 1643, 4929),
(1764, 6, 8, '2000-07-16 00:00:00', 1647, 4941),
(1765, 6, 6, '2000-12-15 00:00:00', 1356, 2712),
(1766, 6, 6, '2000-06-13 00:00:00', 1394, 2788),
(1768, 6, 6, '2000-01-19 00:00:00', 1664, 3328),
(1769, 6, 6, '2000-02-27 00:00:00', 1974, 3948),
(1770, 6, 13, '2000-07-02 00:00:00', 1151, 0),
(1771, 6, 13, '2000-01-31 00:00:00', 1195, 0),
(1772, 6, 13, '2000-04-15 00:00:00', 1763, 0),
(1773, 6, 13, '2000-02-22 00:00:00', 692, 0),
(1774, 6, 13, '2000-11-08 00:00:00', 1998, 0),
(1775, 6, 13, '2000-10-25 00:00:00', 2450, 0),
(1781, 6, 10, '2000-12-11 00:00:00', 1608, 0),
(1782, 6, 10, '2000-05-08 00:00:00', 667, 0),
(1783, 6, 10, '2000-11-04 00:00:00', 549, 0),
(1784, 6, 14, '2000-07-08 00:00:00', 1988, 5964),
(1785, 6, 14, '2000-08-19 00:00:00', 2217, 6651),
(1786, 6, 14, '2000-11-06 00:00:00', 2331, 6993),
(1787, 6, 14, '2000-05-03 00:00:00', 861, 2583),
(1788, 6, 4, '2000-04-15 00:00:00', 977, 0),
(1789, 6, 4, '2000-11-18 00:00:00', 276, 0),
(1790, 6, 5, '2000-11-02 00:00:00', 1150, 2300),
(1791, 6, 5, '2000-06-11 00:00:00', 1643, 3286),
(1793, 1, 11, '2000-08-11 00:00:00', 2299, 2299),
(1794, 1, 11, '2000-05-03 00:00:00', 2015, 2015),
(1795, 1, 11, '2000-01-12 00:00:00', 1689, 1689),
(1796, 1, 11, '2000-03-20 00:00:00', 1532, 1532),
(1797, 1, 11, '2000-08-01 00:00:00', 1303, 1303),
(1798, 1, 11, '2000-05-18 00:00:00', 634, 634),
(1804, 1, 9, '2000-02-15 00:00:00', 1383, 1383),
(1805, 1, 9, '2000-12-26 00:00:00', 1290, 1290),
(1806, 1, 9, '2000-06-08 00:00:00', 1041, 1041),
(1807, 1, 7, '2000-12-17 00:00:00', 1901, 0),
(1809, 1, 3, '2000-09-13 00:00:00', 2229, 2229),
(1810, 1, 3, '2000-02-19 00:00:00', 1734, 1734),
(1811, 1, 3, '2000-08-29 00:00:00', 1271, 1271),
(1812, 1, 3, '2000-04-19 00:00:00', 1043, 1043),
(1813, 1, 3, '2000-03-14 00:00:00', 633, 633),
(1814, 1, 1, '2000-09-18 00:00:00', 2030, 6090),
(1815, 1, 1, '2000-05-09 00:00:00', 1204, 3612),
(1816, 1, 1, '2000-08-27 00:00:00', 918, 2754),
(1818, 1, 8, '2000-01-07 00:00:00', 2407, 7221),
(1819, 1, 8, '2000-09-01 00:00:00', 1589, 4767),
(1820, 1, 8, '2000-10-09 00:00:00', 1505, 4515),
(1821, 1, 8, '2000-11-27 00:00:00', 1297, 3891),
(1822, 1, 8, '2000-06-11 00:00:00', 848, 2544),
(1826, 1, 6, '2000-05-31 00:00:00', 2287, 4574),
(1827, 1, 6, '2000-01-25 00:00:00', 2239, 4478),
(1828, 1, 6, '2000-02-19 00:00:00', 1248, 2496),
(1829, 1, 6, '2000-12-18 00:00:00', 1170, 2340),
(1830, 1, 13, '2000-11-12 00:00:00', 2369, 0),
(1831, 1, 13, '2000-01-02 00:00:00', 2191, 0),
(1832, 1, 13, '2000-12-20 00:00:00', 1721, 0),
(1833, 1, 13, '2000-01-28 00:00:00', 1657, 0),
(1834, 1, 13, '2000-02-15 00:00:00', 574, 0),
(1841, 1, 10, '2000-10-04 00:00:00', 1802, 0),
(1842, 1, 10, '2000-06-05 00:00:00', 1636, 0),
(1843, 1, 14, '2000-11-17 00:00:00', 1452, 4356),
(1845, 1, 14, '2000-09-14 00:00:00', 279, 837),
(1846, 1, 4, '2000-09-07 00:00:00', 2043, 0),
(1847, 1, 4, '2000-04-18 00:00:00', 1819, 0),
(1848, 1, 4, '2000-09-26 00:00:00', 1776, 0),
(1849, 1, 4, '2000-07-08 00:00:00', 1669, 0),
(1850, 1, 4, '2000-08-06 00:00:00', 1554, 0),
(1851, 1, 4, '2000-09-26 00:00:00', 1472, 0),
(1852, 1, 4, '2000-06-10 00:00:00', 1384, 0),
(1853, 1, 4, '2000-07-09 00:00:00', 812, 0),
(1854, 1, 4, '2000-10-17 00:00:00', 561, 0),
(1856, 1, 5, '2000-04-03 00:00:00', 1913, 3826),
(1857, 1, 5, '2000-07-08 00:00:00', 1174, 2348),
(1858, 1, 5, '2000-12-14 00:00:00', 691, 1382),
(1861, 7, 11, '2000-06-19 00:00:00', 2184, 2184),
(1862, 7, 11, '2000-12-26 00:00:00', 2129, 2129),
(1863, 7, 11, '2000-09-04 00:00:00', 2092, 2092),
(1864, 7, 11, '2000-02-03 00:00:00', 1915, 1915),
(1865, 7, 12, '2000-01-21 00:00:00', 2248, 4496),
(1866, 7, 12, '2000-12-30 00:00:00', 1828, 3656),
(1867, 7, 12, '2000-04-09 00:00:00', 1656, 3312),
(1868, 7, 12, '2000-09-04 00:00:00', 1548, 3096),
(1869, 7, 12, '2000-07-06 00:00:00', 779, 1558),
(1870, 7, 12, '2000-04-18 00:00:00', 676, 1352),
(1871, 7, 12, '2000-04-20 00:00:00', 279, 558),
(1872, 7, 9, '2000-08-14 00:00:00', 2464, 2464),
(1873, 7, 9, '2000-06-29 00:00:00', 1051, 1051),
(1874, 7, 7, '2000-12-07 00:00:00', 2174, 0),
(1875, 7, 7, '2000-07-02 00:00:00', 1945, 0),
(1877, 7, 3, '2000-03-06 00:00:00', 1131, 1131),
(1878, 7, 3, '2000-08-08 00:00:00', 1086, 1086),
(1879, 7, 3, '2000-03-05 00:00:00', 1058, 1058),
(1880, 7, 3, '2000-03-27 00:00:00', 871, 871),
(1881, 7, 3, '2000-12-16 00:00:00', 782, 782),
(1882, 7, 3, '2000-02-27 00:00:00', 649, 649),
(1883, 7, 1, '2000-06-26 00:00:00', 2176, 6528),
(1884, 7, 1, '2000-11-13 00:00:00', 1989, 5967),
(1885, 7, 1, '2000-09-07 00:00:00', 1416, 4248),
(1886, 7, 1, '2000-07-06 00:00:00', 1147, 3441),
(1887, 7, 1, '2000-01-09 00:00:00', 1115, 3345),
(1888, 7, 1, '2000-02-26 00:00:00', 986, 2958),
(1889, 7, 8, '2000-03-01 00:00:00', 1683, 5049),
(1890, 7, 8, '2000-04-05 00:00:00', 663, 1989),
(1891, 7, 8, '2000-11-13 00:00:00', 639, 1917),
(1892, 7, 6, '2000-03-20 00:00:00', 2408, 4816),
(1893, 7, 6, '2000-09-25 00:00:00', 2083, 4166),
(1894, 7, 6, '2000-11-14 00:00:00', 1476, 2952),
(1895, 7, 6, '2000-10-15 00:00:00', 1014, 2028),
(1896, 7, 13, '2000-05-28 00:00:00', 2094, 0),
(1897, 7, 13, '2000-11-24 00:00:00', 2074, 0),
(1898, 7, 13, '2000-08-22 00:00:00', 892, 0),
(1901, 7, 10, '2000-04-30 00:00:00', 2231, 0),
(1902, 7, 10, '2000-11-07 00:00:00', 2103, 0),
(1903, 7, 10, '2000-05-02 00:00:00', 1725, 0),
(1904, 7, 10, '2000-03-25 00:00:00', 1635, 0),
(1905, 7, 10, '2000-05-12 00:00:00', 922, 0),
(1906, 7, 10, '2000-02-27 00:00:00', 534, 0),
(1907, 7, 14, '2000-11-23 00:00:00', 2290, 6870),
(1908, 7, 14, '2000-05-02 00:00:00', 1145, 3435),
(1909, 7, 14, '2000-12-13 00:00:00', 1100, 3300),
(1910, 7, 14, '2000-12-11 00:00:00', 1096, 3288),
(1911, 7, 14, '2000-06-18 00:00:00', 1063, 3189),
(1912, 7, 14, '2000-02-19 00:00:00', 275, 825),
(1913, 7, 4, '2000-07-30 00:00:00', 2219, 0),
(1914, 7, 4, '2000-08-02 00:00:00', 2155, 0),
(1915, 7, 4, '2000-09-15 00:00:00', 1592, 0),
(1916, 7, 4, '2000-05-20 00:00:00', 1362, 0),
(1917, 7, 4, '2000-10-08 00:00:00', 1357, 0),
(1918, 7, 4, '2000-08-27 00:00:00', 1189, 0),
(1919, 7, 4, '2000-08-26 00:00:00', 899, 0),
(1920, 7, 4, '2000-09-09 00:00:00', 779, 0),
(1921, 7, 5, '2000-06-30 00:00:00', 1087, 2174),
(1924, 3, 11, '2000-04-27 00:00:00', 2225, 2225),
(1925, 3, 11, '2000-07-01 00:00:00', 1657, 1657),
(1926, 3, 11, '2000-12-12 00:00:00', 1498, 1498),
(1927, 3, 12, '2000-02-24 00:00:00', 2381, 4762),
(1928, 3, 12, '2000-02-03 00:00:00', 2233, 4466),
(1929, 3, 12, '2000-07-18 00:00:00', 1371, 2742),
(1930, 3, 12, '2000-10-31 00:00:00', 766, 1532),
(1932, 3, 9, '2000-04-08 00:00:00', 1550, 1550),
(1933, 3, 7, '2000-02-12 00:00:00', 2421, 0),
(1934, 3, 7, '2000-07-03 00:00:00', 2243, 0),
(1935, 3, 7, '2000-10-22 00:00:00', 1461, 0),
(1936, 3, 7, '2000-04-20 00:00:00', 1079, 0),
(1939, 3, 3, '2000-04-08 00:00:00', 1958, 1958),
(1940, 3, 3, '2000-09-07 00:00:00', 1742, 1742),
(1941, 3, 3, '2000-05-08 00:00:00', 1240, 1240),
(1942, 3, 3, '2000-03-16 00:00:00', 738, 738),
(1943, 3, 3, '2000-12-16 00:00:00', 710, 710),
(1945, 3, 1, '2000-08-23 00:00:00', 1873, 5619),
(1946, 3, 1, '2000-10-14 00:00:00', 554, 1662),
(1947, 3, 8, '2000-05-15 00:00:00', 1721, 5163),
(1948, 3, 8, '2000-02-03 00:00:00', 1628, 4884),
(1949, 3, 8, '2000-03-03 00:00:00', 1551, 4653),
(1950, 3, 8, '2000-07-19 00:00:00', 720, 2160),
(1951, 3, 8, '2000-07-01 00:00:00', 682, 2046),
(1952, 3, 8, '2000-08-01 00:00:00', 652, 1956),
(1953, 3, 8, '2000-11-25 00:00:00', 614, 1842),
(1956, 3, 6, '2000-10-10 00:00:00', 2086, 4172),
(1957, 3, 6, '2000-06-27 00:00:00', 2071, 4142),
(1958, 3, 6, '2000-12-29 00:00:00', 1872, 3744),
(1959, 3, 6, '2000-07-16 00:00:00', 1646, 3292),
(1960, 3, 6, '2000-01-23 00:00:00', 1246, 2492),
(1961, 3, 6, '2000-02-11 00:00:00', 1080, 2160),
(1962, 3, 13, '2000-01-31 00:00:00', 2292, 0),
(1963, 3, 13, '2000-10-06 00:00:00', 1807, 0),
(1964, 3, 13, '2000-11-10 00:00:00', 1708, 0),
(1970, 3, 10, '2000-08-03 00:00:00', 2260, 0),
(1971, 3, 10, '2000-08-03 00:00:00', 1786, 0),
(1972, 3, 10, '2000-10-21 00:00:00', 1050, 0),
(1973, 3, 10, '2000-04-18 00:00:00', 977, 0),
(1975, 3, 14, '2000-01-05 00:00:00', 2379, 7137),
(1976, 3, 14, '2000-10-27 00:00:00', 1909, 5727),
(1977, 3, 14, '2000-10-03 00:00:00', 1677, 5031),
(1978, 3, 14, '2000-07-21 00:00:00', 1649, 4947),
(1979, 3, 14, '2000-06-24 00:00:00', 1028, 3084),
(1980, 3, 4, '2000-04-18 00:00:00', 1061, 0),
(1981, 3, 4, '2000-07-19 00:00:00', 927, 0),
(1982, 3, 5, '2000-04-04 00:00:00', 1933, 3866),
(1983, 3, 5, '2000-09-08 00:00:00', 1681, 3362),
(1984, 3, 5, '2000-10-24 00:00:00', 1636, 3272),
(1985, 3, 5, '2000-06-18 00:00:00', 767, 1534),
(1986, 9, 11, '2000-07-19 00:00:00', 1859, 1859),
(1987, 9, 11, '2000-11-09 00:00:00', 1551, 1551),
(1988, 9, 11, '2000-06-20 00:00:00', 927, 927),
(1989, 9, 12, '2000-08-19 00:00:00', 2186, 4372),
(1990, 9, 12, '2000-04-24 00:00:00', 1654, 3308),
(1991, 9, 12, '2000-12-13 00:00:00', 1035, 2070),
(1992, 9, 9, '2000-04-30 00:00:00', 2104, 2104),
(1993, 9, 9, '2000-03-28 00:00:00', 1843, 1843),
(1994, 9, 9, '2000-04-01 00:00:00', 1286, 1286),
(1995, 9, 9, '2000-01-17 00:00:00', 922, 922),
(1996, 9, 7, '2000-02-01 00:00:00', 2121, 0),
(1997, 9, 7, '2000-11-16 00:00:00', 1034, 0),
(1998, 9, 7, '2000-12-11 00:00:00', 779, 0),
(1999, 9, 3, '2000-09-09 00:00:00', 2389, 2389),
(2000, 9, 3, '2000-04-03 00:00:00', 1859, 1859),
(2001, 9, 3, '2000-06-30 00:00:00', 1700, 1700),
(2002, 9, 3, '2000-02-11 00:00:00', 806, 806),
(2003, 9, 3, '2000-09-23 00:00:00', 562, 562),
(2006, 9, 1, '2000-02-05 00:00:00', 2490, 7470),
(2007, 9, 1, '2000-06-14 00:00:00', 2466, 7398),
(2008, 9, 1, '2000-09-09 00:00:00', 2207, 6621),
(2009, 9, 1, '2000-06-07 00:00:00', 1870, 5610),
(2010, 9, 8, '2000-02-07 00:00:00', 2246, 6738),
(2011, 9, 8, '2000-05-23 00:00:00', 1872, 5616),
(2012, 9, 8, '2000-02-15 00:00:00', 1705, 5115),
(2013, 9, 8, '2000-06-17 00:00:00', 1460, 4380),
(2014, 9, 8, '2000-10-26 00:00:00', 1424, 4272),
(2015, 9, 8, '2000-04-26 00:00:00', 1421, 4263),
(2016, 9, 6, '2000-11-10 00:00:00', 2466, 4932),
(2017, 9, 6, '2000-02-06 00:00:00', 2325, 4650),
(2018, 9, 6, '2000-12-19 00:00:00', 1836, 3672),
(2019, 9, 6, '2000-04-02 00:00:00', 1629, 3258),
(2020, 9, 6, '2000-11-09 00:00:00', 1486, 2972),
(2021, 9, 6, '2000-05-18 00:00:00', 1123, 2246),
(2022, 9, 6, '2000-03-04 00:00:00', 727, 1454),
(2026, 9, 10, '2000-06-15 00:00:00', 2358, 0),
(2027, 9, 10, '2000-12-31 00:00:00', 1406, 0),
(2028, 9, 10, '2000-08-24 00:00:00', 1390, 0),
(2029, 9, 10, '2000-11-26 00:00:00', 1240, 0),
(2030, 9, 14, '2000-07-28 00:00:00', 2427, 7281),
(2031, 9, 4, '2000-05-25 00:00:00', 1530, 0),
(2032, 9, 4, '2000-12-24 00:00:00', 795, 0),
(2034, 9, 5, '2000-06-11 00:00:00', 1651, 3302),
(2035, 9, 5, '2000-04-07 00:00:00', 1423, 2846),
(2036, 9, 5, '2000-01-15 00:00:00', 1098, 2196),
(2037, 4, 11, '2000-09-22 00:00:00', 2225, 2225),
(2038, 4, 11, '2000-06-20 00:00:00', 1758, 1758),
(2039, 4, 11, '2000-08-15 00:00:00', 1572, 1572),
(2040, 4, 11, '2000-03-11 00:00:00', 876, 876),
(2041, 4, 12, '2000-08-30 00:00:00', 2377, 4754),
(2042, 4, 12, '2000-03-06 00:00:00', 2360, 4720),
(2043, 4, 12, '2000-02-14 00:00:00', 2294, 4588),
(2044, 4, 12, '2000-06-14 00:00:00', 2108, 4216),
(2045, 4, 12, '2000-04-05 00:00:00', 2072, 4144),
(2046, 4, 12, '2000-02-04 00:00:00', 1972, 3944),
(2047, 4, 12, '2000-05-30 00:00:00', 1889, 3778),
(2048, 4, 12, '2000-10-07 00:00:00', 1663, 3326),
(2049, 4, 12, '2000-08-04 00:00:00', 1577, 3154),
(2050, 4, 12, '2000-10-09 00:00:00', 947, 1894),
(2051, 4, 9, '2000-10-13 00:00:00', 2305, 2305),
(2052, 4, 9, '2000-06-21 00:00:00', 1465, 1465),
(2053, 4, 9, '2000-03-04 00:00:00', 1236, 1236),
(2054, 4, 9, '2000-03-21 00:00:00', 1119, 1119),
(2055, 4, 9, '2000-07-03 00:00:00', 921, 921),
(2056, 4, 9, '2000-07-08 00:00:00', 626, 626),
(2058, 4, 7, '2000-09-22 00:00:00', 2391, 0),
(2059, 4, 7, '2000-05-24 00:00:00', 2245, 0),
(2060, 4, 7, '2000-11-20 00:00:00', 2031, 0),
(2061, 4, 7, '2000-08-14 00:00:00', 1936, 0),
(2062, 4, 7, '2000-07-30 00:00:00', 1712, 0),
(2063, 4, 7, '2000-01-16 00:00:00', 1089, 0),
(2064, 4, 3, '2000-11-25 00:00:00', 2133, 2133),
(2065, 4, 3, '2000-08-08 00:00:00', 885, 885),
(2066, 4, 3, '2000-03-27 00:00:00', 719, 719),
(2067, 4, 3, '2000-06-22 00:00:00', 296, 296),
(2068, 4, 1, '2000-04-09 00:00:00', 2038, 6114),
(2069, 4, 1, '2000-05-23 00:00:00', 1661, 4983),
(2070, 4, 1, '2000-03-14 00:00:00', 1302, 3906),
(2072, 4, 1, '2000-05-19 00:00:00', 274, 822),
(2073, 4, 8, '2000-05-05 00:00:00', 1142, 3426),
(2074, 4, 8, '2000-12-24 00:00:00', 707, 2121),
(2075, 4, 8, '2000-01-15 00:00:00', 651, 1953),
(2076, 4, 8, '2000-04-22 00:00:00', 561, 1683),
(2077, 4, 6, '2000-03-09 00:00:00', 1031, 2062),
(2078, 4, 6, '2000-06-30 00:00:00', 809, 1618),
(2079, 4, 6, '2000-05-13 00:00:00', 788, 1576),
(2080, 4, 13, '2000-07-17 00:00:00', 1529, 0),
(2081, 4, 13, '2000-01-11 00:00:00', 1441, 0),
(2082, 4, 13, '2000-06-09 00:00:00', 1169, 0),
(2083, 4, 13, '2000-04-08 00:00:00', 1094, 0),
(2084, 4, 13, '2000-12-24 00:00:00', 972, 0),
(2085, 4, 13, '2000-12-03 00:00:00', 586, 0),
(2090, 4, 10, '2000-01-25 00:00:00', 1385, 0),
(2091, 4, 10, '2000-08-28 00:00:00', 1318, 0),
(2092, 4, 10, '2000-06-16 00:00:00', 1073, 0),
(2093, 4, 10, '2000-02-16 00:00:00', 870, 0),
(2094, 4, 14, '2000-04-21 00:00:00', 1387, 4161),
(2095, 4, 14, '2000-07-19 00:00:00', 685, 2055),
(2096, 4, 4, '2000-02-03 00:00:00', 1713, 0),
(2097, 4, 4, '2000-08-30 00:00:00', 1594, 0),
(2098, 4, 4, '2000-10-26 00:00:00', 1018, 0),
(2099, 4, 4, '2000-04-19 00:00:00', 880, 0),
(2100, 4, 5, '2000-08-05 00:00:00', 2479, 4958),
(2101, 4, 5, '2000-09-13 00:00:00', 2264, 4528),
(2102, 4, 5, '2000-07-20 00:00:00', 954, 1908),
(2103, 2, 11, '2000-11-20 00:00:00', 1446, 1446),
(2104, 2, 11, '2000-02-14 00:00:00', 1385, 1385),
(2105, 2, 11, '2000-03-23 00:00:00', 938, 938),
(2106, 2, 11, '2000-08-03 00:00:00', 888, 888),
(2107, 2, 12, '2000-01-06 00:00:00', 1379, 2758),
(2108, 2, 12, '2000-10-20 00:00:00', 722, 1444),
(2109, 2, 9, '2000-03-05 00:00:00', 1752, 1752),
(2110, 2, 9, '2000-06-11 00:00:00', 981, 981),
(2111, 2, 9, '2000-09-09 00:00:00', 915, 915),
(2112, 2, 9, '2000-03-04 00:00:00', 727, 727),
(2114, 2, 9, '2000-01-20 00:00:00', 274, 274),
(2115, 2, 7, '2000-08-30 00:00:00', 1890, 0),
(2116, 2, 7, '2000-09-12 00:00:00', 907, 0),
(2117, 2, 7, '2000-02-12 00:00:00', 740, 0),
(2118, 2, 7, '2000-05-28 00:00:00', 663, 0),
(2119, 2, 7, '2000-03-13 00:00:00', 614, 0),
(2120, 2, 3, '2000-03-04 00:00:00', 2486, 2486),
(2121, 2, 3, '2000-02-16 00:00:00', 2236, 2236),
(2122, 2, 3, '2000-06-11 00:00:00', 2122, 2122),
(2123, 2, 3, '2000-08-24 00:00:00', 1910, 1910),
(2124, 2, 3, '2000-04-01 00:00:00', 1862, 1862),
(2125, 2, 3, '2000-09-15 00:00:00', 1659, 1659),
(2126, 2, 3, '2000-01-08 00:00:00', 1623, 1623),
(2127, 2, 3, '2000-07-27 00:00:00', 1472, 1472),
(2128, 2, 3, '2000-09-13 00:00:00', 1282, 1282),
(2129, 2, 3, '2000-06-13 00:00:00', 861, 861),
(2130, 2, 1, '2000-08-09 00:00:00', 2450, 7350),
(2131, 2, 1, '2000-03-19 00:00:00', 1575, 4725),
(2132, 2, 1, '2000-12-14 00:00:00', 1407, 4221),
(2133, 2, 1, '2000-03-17 00:00:00', 848, 2544),
(2134, 2, 1, '2000-02-28 00:00:00', 796, 2388),
(2136, 2, 8, '2000-12-19 00:00:00', 1829, 5487),
(2137, 2, 8, '2000-01-09 00:00:00', 778, 2334),
(2138, 2, 8, '2000-10-26 00:00:00', 657, 1971),
(2139, 2, 8, '2000-03-09 00:00:00', 505, 1515),
(2141, 2, 6, '2000-09-18 00:00:00', 1888, 3776),
(2142, 2, 6, '2000-01-11 00:00:00', 1877, 3754),
(2143, 2, 6, '2000-03-26 00:00:00', 1437, 2874),
(2144, 2, 6, '2000-07-28 00:00:00', 756, 1512),
(2145, 2, 13, '2000-02-02 00:00:00', 2276, 0),
(2146, 2, 13, '2000-04-22 00:00:00', 1106, 0),
(2151, 2, 10, '2000-01-18 00:00:00', 1747, 0),
(2152, 2, 10, '2000-08-29 00:00:00', 880, 0),
(2153, 2, 10, '2000-02-21 00:00:00', 555, 0),
(2154, 2, 10, '2000-02-23 00:00:00', 506, 0),
(2155, 2, 14, '2000-06-18 00:00:00', 1893, 5679),
(2156, 2, 14, '2000-05-10 00:00:00', 1646, 4938),
(2157, 2, 14, '2000-02-16 00:00:00', 1115, 3345),
(2158, 2, 14, '2000-05-17 00:00:00', 1046, 3138),
(2159, 2, 14, '2000-12-18 00:00:00', 847, 2541),
(2160, 2, 14, '2000-04-09 00:00:00', 603, 1809),
(2161, 2, 4, '2000-03-22 00:00:00', 1895, 0),
(2162, 2, 4, '2000-07-12 00:00:00', 952, 0),
(2163, 2, 4, '2000-05-22 00:00:00', 925, 0),
(2164, 2, 4, '2000-04-07 00:00:00', 808, 0),
(2165, 2, 4, '2000-06-07 00:00:00', 737, 0),
(2167, 2, 5, '2000-11-20 00:00:00', 1741, 3482),
(2168, 2, 5, '2000-08-15 00:00:00', 684, 1368),
(2170, 8, 11, '2000-02-15 00:00:00', 2319, 2319),
(2171, 8, 11, '2000-02-01 00:00:00', 1831, 1831),
(2172, 8, 11, '2000-07-20 00:00:00', 900, 900),
(2173, 8, 11, '2000-09-09 00:00:00', 527, 527),
(2174, 8, 11, '2000-04-28 00:00:00', 507, 507),
(2176, 8, 12, '2000-02-23 00:00:00', 1775, 3550),
(2179, 8, 9, '2000-10-23 00:00:00', 1839, 1839),
(2180, 8, 9, '2000-10-25 00:00:00', 945, 945),
(2181, 8, 9, '2000-11-13 00:00:00', 558, 558),
(2182, 8, 7, '2000-06-02 00:00:00', 1689, 0),
(2183, 8, 7, '2000-08-08 00:00:00', 1608, 0),
(2184, 8, 7, '2000-09-17 00:00:00', 748, 0),
(2185, 8, 7, '2000-12-10 00:00:00', 673, 0),
(2186, 8, 7, '2000-06-25 00:00:00', 509, 0),
(2187, 8, 3, '2000-08-03 00:00:00', 1650, 1650),
(2188, 8, 1, '2000-01-11 00:00:00', 1815, 5445),
(2189, 8, 1, '2000-02-17 00:00:00', 1762, 5286),
(2190, 8, 1, '2000-10-26 00:00:00', 1409, 4227),
(2191, 8, 1, '2000-12-30 00:00:00', 1108, 3324),
(2192, 8, 8, '2000-08-03 00:00:00', 1998, 5994),
(2194, 8, 6, '2000-02-03 00:00:00', 2460, 4920),
(2195, 8, 6, '2000-07-04 00:00:00', 1120, 2240),
(2196, 8, 6, '2000-05-28 00:00:00', 898, 1796),
(2197, 8, 6, '2000-12-10 00:00:00', 789, 1578),
(2198, 8, 13, '2000-10-17 00:00:00', 2426, 0),
(2199, 8, 13, '2000-05-21 00:00:00', 2331, 0),
(2200, 8, 13, '2000-08-18 00:00:00', 1293, 0),
(2201, 8, 13, '2000-12-08 00:00:00', 859, 0),
(2202, 8, 13, '2000-05-05 00:00:00', 663, 0),
(2203, 8, 13, '2000-09-04 00:00:00', 283, 0),
(2209, 8, 10, '2000-10-17 00:00:00', 1397, 0),
(2210, 8, 10, '2000-04-28 00:00:00', 1045, 0),
(2211, 8, 10, '2000-06-29 00:00:00', 975, 0),
(2213, 8, 10, '2000-03-22 00:00:00', 254, 0),
(2214, 8, 14, '2000-12-01 00:00:00', 2297, 6891),
(2215, 8, 14, '2000-08-25 00:00:00', 2152, 6456),
(2216, 8, 14, '2000-06-08 00:00:00', 1578, 4734),
(2217, 8, 14, '2000-01-03 00:00:00', 1224, 3672),
(2218, 8, 4, '2000-10-22 00:00:00', 2186, 0),
(2219, 8, 4, '2000-12-27 00:00:00', 1988, 0),
(2220, 8, 4, '2000-12-12 00:00:00', 1694, 0),
(2223, 8, 5, '2000-06-21 00:00:00', 2498, 4996),
(2224, 8, 5, '2000-09-09 00:00:00', 2497, 4994),
(2225, 8, 5, '2000-10-12 00:00:00', 2198, 4396),
(2226, 8, 5, '2000-03-07 00:00:00', 2078, 4156),
(2227, 8, 5, '2000-05-11 00:00:00', 2001, 4002),
(2229, 10, 11, '2000-03-24 00:00:00', 2361, 2361),
(2230, 10, 11, '2000-08-10 00:00:00', 1902, 1902),
(2231, 10, 12, '2000-07-30 00:00:00', 1986, 3972),
(2232, 10, 12, '2000-12-09 00:00:00', 1496, 2992),
(2234, 10, 9, '2000-02-20 00:00:00', 1431, 1431),
(2235, 10, 7, '2000-03-25 00:00:00', 2114, 0),
(2236, 10, 7, '2000-09-25 00:00:00', 1867, 0),
(2237, 10, 7, '2000-12-22 00:00:00', 1480, 0),
(2238, 10, 7, '2000-07-08 00:00:00', 1101, 0),
(2239, 10, 7, '2000-08-28 00:00:00', 1018, 0),
(2240, 10, 7, '2000-04-25 00:00:00', 556, 0),
(2242, 10, 3, '2000-09-14 00:00:00', 1875, 1875),
(2243, 10, 3, '2000-01-02 00:00:00', 532, 532),
(2244, 10, 1, '2000-01-08 00:00:00', 2111, 6333),
(2245, 10, 1, '2000-02-04 00:00:00', 1446, 4338),
(2246, 10, 1, '2000-01-16 00:00:00', 691, 2073),
(2248, 10, 8, '2000-05-23 00:00:00', 2218, 6654),
(2249, 10, 8, '2000-06-19 00:00:00', 2050, 6150),
(2250, 10, 8, '2000-08-01 00:00:00', 1328, 3984),
(2251, 10, 8, '2000-07-12 00:00:00', 525, 1575),
(2252, 10, 6, '2000-07-22 00:00:00', 1629, 3258),
(2253, 10, 6, '2000-08-10 00:00:00', 1400, 2800),
(2254, 10, 6, '2000-06-13 00:00:00', 965, 1930),
(2257, 10, 13, '2000-10-23 00:00:00', 2033, 0),
(2258, 10, 13, '2000-12-10 00:00:00', 1472, 0),
(2259, 10, 13, '2000-10-25 00:00:00', 1339, 0),
(2260, 10, 13, '2000-12-31 00:00:00', 1293, 0),
(2261, 10, 13, '2000-08-25 00:00:00', 984, 0),
(2268, 10, 10, '2000-06-27 00:00:00', 2481, 0),
(2269, 10, 10, '2000-12-15 00:00:00', 2268, 0),
(2270, 10, 10, '2000-08-24 00:00:00', 2020, 0),
(2271, 10, 10, '2000-08-01 00:00:00', 1985, 0),
(2272, 10, 10, '2000-08-01 00:00:00', 1945, 0),
(2273, 10, 14, '2000-09-22 00:00:00', 2252, 6756),
(2274, 10, 14, '2000-03-19 00:00:00', 2180, 6540),
(2275, 10, 14, '2000-07-11 00:00:00', 2130, 6390),
(2276, 10, 14, '2000-03-04 00:00:00', 2113, 6339),
(2277, 10, 14, '2000-03-28 00:00:00', 1820, 5460),
(2278, 10, 14, '2000-09-16 00:00:00', 1494, 4482),
(2279, 10, 14, '2000-12-23 00:00:00', 1472, 4416),
(2280, 10, 14, '2000-02-04 00:00:00', 1224, 3672),
(2281, 10, 14, '2000-05-22 00:00:00', 1049, 3147),
(2282, 10, 14, '2000-03-04 00:00:00', 783, 2349),
(2283, 10, 14, '2000-10-31 00:00:00', 562, 1686),
(2285, 10, 4, '2000-08-30 00:00:00', 947, 0),
(2287, 10, 5, '2000-06-04 00:00:00', 2482, 4964),
(2288, 10, 5, '2000-04-27 00:00:00', 1344, 2688),
(2289, 10, 5, '2000-08-28 00:00:00', 1106, 2212),
(2290, 5, 11, '2000-02-03 00:00:00', 1855, 1855),
(2291, 5, 11, '2000-04-24 00:00:00', 1467, 1467),
(2292, 5, 11, '2000-06-09 00:00:00', 1300, 1300),
(2293, 5, 11, '2000-03-01 00:00:00', 969, 969),
(2294, 5, 11, '2000-12-29 00:00:00', 918, 918),
(2295, 5, 11, '2000-02-09 00:00:00', 841, 841),
(2296, 5, 11, '2000-10-26 00:00:00', 733, 733),
(2298, 5, 12, '2000-02-15 00:00:00', 1421, 2842),
(2299, 5, 12, '2000-08-27 00:00:00', 915, 1830),
(2300, 5, 12, '2000-04-07 00:00:00', 502, 1004),
(2302, 5, 9, '2000-05-19 00:00:00', 2430, 2430),
(2303, 5, 9, '2000-12-02 00:00:00', 1543, 1543),
(2304, 5, 9, '2000-04-30 00:00:00', 668, 668),
(2305, 5, 9, '2000-04-24 00:00:00', 581, 581),
(2306, 5, 7, '2000-08-21 00:00:00', 1981, 0),
(2307, 5, 7, '2000-08-24 00:00:00', 1804, 0),
(2308, 5, 7, '2000-06-15 00:00:00', 1322, 0),
(2309, 5, 7, '2000-09-17 00:00:00', 619, 0),
(2310, 5, 3, '2000-07-28 00:00:00', 2171, 2171),
(2311, 5, 3, '2000-05-14 00:00:00', 1975, 1975),
(2312, 5, 3, '2000-11-08 00:00:00', 1845, 1845),
(2313, 5, 3, '2000-02-19 00:00:00', 1671, 1671),
(2314, 5, 3, '2000-05-31 00:00:00', 1300, 1300),
(2315, 5, 3, '2000-03-24 00:00:00', 1129, 1129),
(2316, 5, 3, '2000-11-12 00:00:00', 753, 753),
(2317, 5, 3, '2000-11-02 00:00:00', 670, 670),
(2318, 5, 1, '2000-11-09 00:00:00', 2243, 6729),
(2319, 5, 1, '2000-11-12 00:00:00', 2083, 6249),
(2320, 5, 1, '2000-09-23 00:00:00', 1480, 4440),
(2321, 5, 1, '2000-07-24 00:00:00', 1128, 3384),
(2322, 5, 1, '2000-12-11 00:00:00', 1078, 3234),
(2323, 5, 1, '2000-04-27 00:00:00', 607, 1821),
(2324, 5, 1, '2000-09-15 00:00:00', 564, 1692),
(2325, 5, 8, '2000-05-08 00:00:00', 2352, 7056),
(2326, 5, 6, '2000-02-24 00:00:00', 2432, 4864),
(2327, 5, 6, '2000-02-12 00:00:00', 2200, 4400),
(2328, 5, 6, '2000-11-10 00:00:00', 2150, 4300),
(2329, 5, 6, '2000-10-21 00:00:00', 1720, 3440),
(2330, 5, 6, '2000-09-25 00:00:00', 1691, 3382),
(2331, 5, 6, '2000-04-17 00:00:00', 1472, 2944),
(2332, 5, 13, '2000-02-10 00:00:00', 1771, 0),
(2333, 5, 13, '2000-05-04 00:00:00', 1240, 0),
(2341, 5, 10, '2000-11-06 00:00:00', 2421, 0),
(2342, 5, 10, '2000-10-17 00:00:00', 1655, 0),
(2343, 5, 10, '2000-11-05 00:00:00', 1466, 0),
(2344, 5, 10, '2000-06-09 00:00:00', 657, 0),
(2345, 5, 10, '2000-12-12 00:00:00', 626, 0),
(2346, 5, 14, '2000-12-28 00:00:00', 2444, 7332),
(2347, 5, 14, '2000-02-18 00:00:00', 1969, 5907),
(2348, 5, 14, '2000-12-01 00:00:00', 1260, 3780),
(2349, 5, 14, '2000-08-23 00:00:00', 1124, 3372),
(2350, 5, 14, '2000-10-21 00:00:00', 1116, 3348),
(2351, 5, 14, '2000-04-09 00:00:00', 867, 2601),
(2352, 5, 14, '2000-09-19 00:00:00', 733, 2199),
(2353, 5, 14, '2000-04-08 00:00:00', 656, 1968),
(2354, 5, 4, '2000-06-20 00:00:00', 1390, 0),
(2355, 5, 4, '2000-07-30 00:00:00', 1297, 0),
(2356, 5, 4, '2000-09-25 00:00:00', 295, 0),
(2357, 5, 5, '2000-12-11 00:00:00', 2249, 4498),
(2358, 5, 5, '2000-04-02 00:00:00', 1472, 2944),
(2359, 5, 5, '2000-12-01 00:00:00', 1462, 2924),
(2360, 5, 5, '2000-03-09 00:00:00', 1342, 2684),
(2361, 5, 5, '2000-07-04 00:00:00', 632, 1264),
(2362, 5, 5, '2000-06-26 00:00:00', 276, 552),
(2363, 12, 11, '2000-09-29 00:00:00', 2030, 2030),
(2364, 12, 11, '2000-12-01 00:00:00', 1441, 1441),
(2365, 12, 11, '2000-12-28 00:00:00', 1383, 1383),
(2366, 12, 11, '2000-01-11 00:00:00', 914, 914),
(2367, 12, 12, '2000-02-21 00:00:00', 1648, 3296),
(2368, 12, 12, '2000-05-08 00:00:00', 1367, 2734),
(2369, 12, 12, '2000-10-27 00:00:00', 1171, 2342),
(2370, 12, 12, '2000-07-11 00:00:00', 1120, 2240),
(2371, 12, 12, '2000-06-17 00:00:00', 1105, 2210),
(2372, 12, 12, '2000-09-23 00:00:00', 1027, 2054),
(2373, 12, 12, '2000-09-25 00:00:00', 818, 1636),
(2374, 12, 9, '2000-06-21 00:00:00', 829, 829),
(2375, 12, 9, '2000-03-06 00:00:00', 525, 525),
(2377, 12, 7, '2000-05-01 00:00:00', 2166, 0),
(2378, 12, 7, '2000-11-23 00:00:00', 1727, 0),
(2379, 12, 7, '2000-05-18 00:00:00', 1645, 0),
(2380, 12, 7, '2000-08-28 00:00:00', 1062, 0),
(2381, 12, 7, '2000-08-19 00:00:00', 554, 0),
(2383, 12, 3, '2000-08-16 00:00:00', 2473, 2473),
(2384, 12, 3, '2000-02-22 00:00:00', 2396, 2396),
(2385, 12, 3, '2000-01-05 00:00:00', 2333, 2333),
(2386, 12, 3, '2000-09-23 00:00:00', 2240, 2240),
(2387, 12, 3, '2000-05-23 00:00:00', 2058, 2058),
(2388, 12, 3, '2000-06-06 00:00:00', 1675, 1675),
(2389, 12, 3, '2000-08-19 00:00:00', 1292, 1292),
(2390, 12, 3, '2000-12-30 00:00:00', 1144, 1144),
(2391, 12, 3, '2000-02-04 00:00:00', 261, 261),
(2392, 12, 1, '2000-05-01 00:00:00', 700, 2100),
(2393, 12, 8, '2000-03-06 00:00:00', 1945, 5835),
(2394, 12, 8, '2000-03-12 00:00:00', 1529, 4587),
(2395, 12, 8, '2000-12-20 00:00:00', 1527, 4581),
(2396, 12, 8, '2000-12-14 00:00:00', 1258, 3774),
(2397, 12, 8, '2000-07-05 00:00:00', 994, 2982),
(2398, 12, 8, '2000-04-27 00:00:00', 932, 2796),
(2399, 12, 6, '2000-06-08 00:00:00', 2095, 4190),
(2400, 12, 6, '2000-10-15 00:00:00', 1923, 3846),
(2401, 12, 6, '2000-11-24 00:00:00', 1489, 2978),
(2402, 12, 6, '2000-02-26 00:00:00', 1237, 2474),
(2403, 12, 13, '2000-04-26 00:00:00', 2046, 0),
(2404, 12, 13, '2000-03-06 00:00:00', 1542, 0),
(2405, 12, 13, '2000-01-22 00:00:00', 784, 0),
(2415, 12, 10, '2000-02-24 00:00:00', 1409, 0),
(2416, 12, 10, '2000-08-05 00:00:00', 1326, 0),
(2417, 12, 10, '2000-12-27 00:00:00', 278, 0),
(2418, 12, 14, '2000-02-19 00:00:00', 2475, 7425),
(2419, 12, 14, '2000-12-14 00:00:00', 2324, 6972),
(2420, 12, 14, '2000-01-05 00:00:00', 2019, 6057),
(2421, 12, 14, '2000-05-21 00:00:00', 1976, 5928),
(2422, 12, 14, '2000-07-13 00:00:00', 787, 2361),
(2424, 12, 4, '2000-12-24 00:00:00', 664, 0),
(2426, 12, 5, '2000-06-01 00:00:00', 2181, 4362),
(2427, 12, 5, '2000-01-29 00:00:00', 2026, 4052),
(2428, 12, 5, '2000-07-29 00:00:00', 2025, 4050),
(2429, 12, 5, '2000-05-18 00:00:00', 1940, 3880),
(2430, 12, 5, '2000-08-05 00:00:00', 1849, 3698),
(2431, 12, 5, '2000-10-28 00:00:00', 1777, 3554),
(2432, 12, 5, '2000-05-19 00:00:00', 1633, 3266),
(2433, 13, 11, '2000-08-16 00:00:00', 1584, 1584),
(2434, 13, 11, '2000-02-22 00:00:00', 1110, 1110),
(2435, 13, 12, '2000-12-21 00:00:00', 2438, 4876),
(2436, 13, 12, '2000-08-14 00:00:00', 1932, 3864),
(2437, 13, 12, '2000-02-13 00:00:00', 1876, 3752),
(2438, 13, 12, '2000-05-06 00:00:00', 1774, 3548),
(2439, 13, 12, '2000-09-26 00:00:00', 1172, 2344),
(2440, 13, 12, '2000-10-17 00:00:00', 978, 1956),
(2442, 13, 9, '2000-10-16 00:00:00', 1987, 1987),
(2443, 13, 9, '2000-06-08 00:00:00', 1970, 1970),
(2444, 13, 9, '2000-08-22 00:00:00', 1377, 1377),
(2445, 13, 9, '2000-10-28 00:00:00', 1328, 1328),
(2446, 13, 9, '2000-10-03 00:00:00', 582, 582),
(2449, 13, 7, '2000-12-11 00:00:00', 2247, 0),
(2450, 13, 7, '2000-11-08 00:00:00', 1939, 0),
(2451, 13, 7, '2000-04-16 00:00:00', 1611, 0),
(2452, 13, 7, '2000-12-08 00:00:00', 1118, 0),
(2453, 13, 7, '2000-10-10 00:00:00', 509, 0),
(2454, 13, 3, '2000-07-14 00:00:00', 2453, 2453),
(2455, 13, 3, '2000-11-04 00:00:00', 2107, 2107),
(2456, 13, 3, '2000-09-12 00:00:00', 2047, 2047),
(2457, 13, 3, '2000-12-11 00:00:00', 1785, 1785),
(2458, 13, 3, '2000-07-28 00:00:00', 623, 623),
(2460, 13, 1, '2000-05-05 00:00:00', 2368, 7104),
(2461, 13, 1, '2000-01-13 00:00:00', 2212, 6636),
(2462, 13, 1, '2000-02-21 00:00:00', 2212, 6636),
(2463, 13, 1, '2000-06-22 00:00:00', 1209, 3627),
(2464, 13, 1, '2000-02-11 00:00:00', 588, 1764),
(2466, 13, 8, '2000-10-05 00:00:00', 1780, 5340),
(2467, 13, 6, '2000-09-06 00:00:00', 2427, 4854),
(2468, 13, 6, '2000-11-11 00:00:00', 2298, 4596),
(2469, 13, 6, '2000-11-10 00:00:00', 1724, 3448),
(2470, 13, 6, '2000-08-12 00:00:00', 1072, 2144),
(2471, 13, 6, '2000-08-01 00:00:00', 939, 1878),
(2472, 13, 6, '2000-05-27 00:00:00', 743, 1486),
(2473, 13, 13, '2000-07-11 00:00:00', 2455, 0),
(2474, 13, 13, '2000-08-24 00:00:00', 2304, 0),
(2475, 13, 13, '2000-03-14 00:00:00', 1973, 0),
(2476, 13, 13, '2000-04-09 00:00:00', 1930, 0),
(2477, 13, 13, '2000-08-19 00:00:00', 1894, 0),
(2478, 13, 13, '2000-09-12 00:00:00', 1291, 0),
(2485, 13, 10, '2000-12-26 00:00:00', 1699, 0),
(2486, 13, 10, '2000-03-31 00:00:00', 691, 0),
(2487, 13, 14, '2000-11-26 00:00:00', 1941, 5823),
(2488, 13, 14, '2000-04-08 00:00:00', 1930, 5790),
(2489, 13, 14, '2000-01-19 00:00:00', 1705, 5115),
(2490, 13, 14, '2000-05-09 00:00:00', 1328, 3984),
(2491, 13, 14, '2000-12-27 00:00:00', 946, 2838),
(2492, 13, 14, '2000-09-01 00:00:00', 843, 2529),
(2493, 13, 4, '2000-08-14 00:00:00', 2247, 0),
(2494, 13, 4, '2000-01-17 00:00:00', 2010, 0),
(2495, 13, 4, '2000-06-12 00:00:00', 1933, 0),
(2496, 13, 4, '2000-10-04 00:00:00', 1837, 0),
(2497, 13, 4, '2000-07-30 00:00:00', 1479, 0),
(2498, 13, 4, '2000-05-10 00:00:00', 908, 0),
(2499, 13, 5, '2000-04-17 00:00:00', 2229, 4458),
(2500, 13, 5, '2000-04-09 00:00:00', 1792, 3584),
(2501, 13, 5, '2000-07-07 00:00:00', 1495, 2990),
(2502, 13, 5, '2000-07-02 00:00:00', 1377, 2754),
(2503, 13, 5, '2000-08-21 00:00:00', 744, 1488),
(2504, 13, 5, '2000-09-23 00:00:00', 296, 592),
(2505, 11, 11, '2001-06-23 00:00:00', 1967, 1967),
(2506, 11, 11, '2001-09-13 00:00:00', 613, 613),
(2507, 11, 11, '2001-05-30 00:00:00', 522, 522),
(2508, 11, 12, '2001-03-16 00:00:00', 2309, 4618),
(2509, 11, 12, '2001-11-15 00:00:00', 1761, 3522),
(2510, 11, 12, '2001-06-14 00:00:00', 1686, 3372),
(2511, 11, 12, '2001-06-05 00:00:00', 1603, 3206),
(2512, 11, 12, '2001-09-22 00:00:00', 1424, 2848),
(2513, 11, 12, '2001-10-23 00:00:00', 1408, 2816),
(2514, 11, 12, '2001-07-27 00:00:00', 1205, 2410),
(2515, 11, 12, '2001-07-09 00:00:00', 1195, 2390),
(2516, 11, 12, '2001-02-07 00:00:00', 822, 1644),
(2517, 11, 12, '2001-04-17 00:00:00', 618, 1236),
(2518, 11, 12, '2001-05-28 00:00:00', 547, 1094),
(2520, 11, 9, '2001-04-28 00:00:00', 2491, 2491),
(2521, 11, 9, '2001-09-25 00:00:00', 2205, 2205),
(2522, 11, 9, '2001-03-09 00:00:00', 1340, 1340),
(2523, 11, 9, '2001-04-08 00:00:00', 545, 545),
(2524, 11, 7, '2001-02-18 00:00:00', 1887, 0),
(2525, 11, 7, '2001-04-22 00:00:00', 1562, 0),
(2526, 11, 7, '2001-07-10 00:00:00', 1128, 0),
(2527, 11, 3, '2001-01-30 00:00:00', 2143, 2143),
(2528, 11, 3, '2001-10-16 00:00:00', 2083, 2083),
(2529, 11, 3, '2001-03-09 00:00:00', 2008, 2008),
(2530, 11, 3, '2001-08-22 00:00:00', 1364, 1364),
(2531, 11, 3, '2001-06-27 00:00:00', 1115, 1115),
(2532, 11, 3, '2001-04-23 00:00:00', 602, 602),
(2534, 11, 1, '2001-10-09 00:00:00', 2451, 7353),
(2535, 11, 1, '2001-08-07 00:00:00', 833, 2499),
(2536, 11, 8, '2001-09-20 00:00:00', 1356, 4068),
(2537, 11, 8, '2001-12-30 00:00:00', 1155, 3465),
(2539, 11, 6, '2001-01-28 00:00:00', 2483, 4966),
(2540, 11, 6, '2001-04-25 00:00:00', 1983, 3966),
(2541, 11, 6, '2001-08-05 00:00:00', 1273, 2546),
(2542, 11, 6, '2001-05-08 00:00:00', 992, 1984),
(2543, 11, 13, '2001-09-24 00:00:00', 1727, 0),
(2544, 11, 13, '2001-02-15 00:00:00', 1388, 0),
(2545, 11, 13, '2001-04-14 00:00:00', 1142, 0),
(2546, 11, 13, '2001-08-20 00:00:00', 1091, 0),
(2547, 11, 13, '2001-11-22 00:00:00', 595, 0),
(2555, 11, 10, '2001-11-12 00:00:00', 2292, 0),
(2556, 11, 10, '2001-09-23 00:00:00', 2141, 0),
(2557, 11, 10, '2001-09-01 00:00:00', 1634, 0),
(2558, 11, 10, '2001-11-04 00:00:00', 1535, 0),
(2559, 11, 10, '2001-02-19 00:00:00', 1331, 0),
(2560, 11, 10, '2001-02-17 00:00:00', 882, 0),
(2561, 11, 10, '2001-09-17 00:00:00', 879, 0),
(2563, 11, 14, '2001-04-28 00:00:00', 1250, 3750),
(2564, 11, 14, '2001-06-20 00:00:00', 775, 2325);
INSERT INTO `ventas` (`idventas`, `Cod Vendedor`, `Cod Producto`, `Fecha`, `Kilos`, `total`) VALUES
(2565, 11, 14, '2001-04-18 00:00:00', 551, 1653),
(2566, 11, 5, '2001-12-30 00:00:00', 2098, 4196),
(2567, 11, 5, '2001-05-02 00:00:00', 2048, 4096),
(2568, 11, 5, '2001-11-17 00:00:00', 1617, 3234),
(2569, 11, 5, '2001-04-03 00:00:00', 1289, 2578),
(2570, 11, 5, '2001-02-28 00:00:00', 564, 1128),
(2571, 6, 11, '2001-11-01 00:00:00', 2444, 2444),
(2572, 6, 11, '2001-07-26 00:00:00', 1844, 1844),
(2573, 6, 11, '2001-12-23 00:00:00', 1437, 1437),
(2574, 6, 12, '2001-01-20 00:00:00', 2198, 4396),
(2575, 6, 12, '2001-10-19 00:00:00', 2096, 4192),
(2576, 6, 12, '2001-01-22 00:00:00', 1471, 2942),
(2577, 6, 12, '2001-05-21 00:00:00', 1419, 2838),
(2578, 6, 12, '2001-07-26 00:00:00', 1400, 2800),
(2579, 6, 12, '2001-10-30 00:00:00', 1171, 2342),
(2580, 6, 12, '2001-12-11 00:00:00', 1138, 2276),
(2581, 6, 12, '2001-09-04 00:00:00', 282, 564),
(2582, 6, 9, '2001-02-08 00:00:00', 2274, 2274),
(2583, 6, 9, '2001-03-22 00:00:00', 1996, 1996),
(2584, 6, 9, '2001-11-14 00:00:00', 1995, 1995),
(2585, 6, 9, '2001-07-28 00:00:00', 253, 253),
(2586, 6, 7, '2001-02-04 00:00:00', 2495, 0),
(2587, 6, 7, '2001-04-03 00:00:00', 2257, 0),
(2588, 6, 7, '2001-04-21 00:00:00', 1815, 0),
(2589, 6, 7, '2001-09-09 00:00:00', 1601, 0),
(2590, 6, 7, '2001-07-31 00:00:00', 1316, 0),
(2591, 6, 7, '2001-08-31 00:00:00', 965, 0),
(2593, 6, 3, '2001-08-16 00:00:00', 1549, 1549),
(2594, 6, 3, '2001-08-29 00:00:00', 1187, 1187),
(2596, 6, 1, '2001-10-17 00:00:00', 1880, 5640),
(2597, 6, 1, '2001-07-10 00:00:00', 1665, 4995),
(2598, 6, 1, '2001-10-29 00:00:00', 1560, 4680),
(2599, 6, 1, '2001-01-31 00:00:00', 716, 2148),
(2601, 6, 8, '2001-03-28 00:00:00', 2439, 7317),
(2602, 6, 8, '2001-03-25 00:00:00', 1671, 5013),
(2603, 6, 8, '2001-01-17 00:00:00', 891, 2673),
(2604, 6, 8, '2001-06-17 00:00:00', 659, 1977),
(2605, 6, 6, '2001-07-24 00:00:00', 2120, 4240),
(2606, 6, 6, '2001-06-29 00:00:00', 1686, 3372),
(2607, 6, 6, '2001-07-14 00:00:00', 1627, 3254),
(2608, 6, 6, '2001-12-28 00:00:00', 1324, 2648),
(2609, 6, 6, '2001-11-28 00:00:00', 801, 1602),
(2610, 6, 6, '2001-07-23 00:00:00', 788, 1576),
(2612, 6, 6, '2001-02-10 00:00:00', 286, 572),
(2613, 6, 13, '2001-12-05 00:00:00', 2240, 0),
(2614, 6, 13, '2001-05-29 00:00:00', 2049, 0),
(2615, 6, 13, '2001-03-13 00:00:00', 1178, 0),
(2616, 6, 13, '2001-09-08 00:00:00', 901, 0),
(2623, 6, 10, '2001-02-08 00:00:00', 2278, 0),
(2624, 6, 10, '2001-01-02 00:00:00', 1019, 0),
(2625, 6, 10, '2001-07-31 00:00:00', 773, 0),
(2626, 6, 10, '2001-12-13 00:00:00', 639, 0),
(2627, 6, 14, '2001-07-22 00:00:00', 1814, 5442),
(2628, 6, 14, '2001-05-03 00:00:00', 971, 2913),
(2629, 6, 4, '2001-07-07 00:00:00', 2206, 0),
(2630, 6, 4, '2001-12-30 00:00:00', 1547, 0),
(2631, 6, 4, '2001-01-03 00:00:00', 1279, 0),
(2632, 6, 4, '2001-02-10 00:00:00', 802, 0),
(2633, 6, 4, '2001-11-11 00:00:00', 567, 0),
(2635, 6, 5, '2001-09-20 00:00:00', 1912, 3824),
(2636, 6, 5, '2001-05-08 00:00:00', 1734, 3468),
(2637, 6, 5, '2001-04-21 00:00:00', 1733, 3466),
(2638, 6, 5, '2001-12-02 00:00:00', 1543, 3086),
(2639, 6, 5, '2001-11-06 00:00:00', 873, 1746),
(2640, 6, 5, '2001-08-22 00:00:00', 869, 1738),
(2641, 6, 5, '2001-03-23 00:00:00', 643, 1286),
(2642, 1, 11, '2001-01-13 00:00:00', 2203, 2203),
(2643, 1, 11, '2001-03-22 00:00:00', 2093, 2093),
(2644, 1, 11, '2001-05-23 00:00:00', 1777, 1777),
(2645, 1, 11, '2001-12-20 00:00:00', 1492, 1492),
(2646, 1, 11, '2001-04-03 00:00:00', 1285, 1285),
(2647, 1, 11, '2001-04-08 00:00:00', 1121, 1121),
(2648, 1, 11, '2001-05-09 00:00:00', 878, 878),
(2650, 1, 12, '2001-08-20 00:00:00', 2115, 4230),
(2651, 1, 12, '2001-01-02 00:00:00', 875, 1750),
(2652, 1, 12, '2001-07-18 00:00:00', 295, 590),
(2653, 1, 9, '2001-09-26 00:00:00', 2123, 2123),
(2654, 1, 9, '2001-11-30 00:00:00', 2064, 2064),
(2655, 1, 9, '2001-07-24 00:00:00', 1962, 1962),
(2656, 1, 9, '2001-07-15 00:00:00', 1006, 1006),
(2657, 1, 9, '2001-03-30 00:00:00', 725, 725),
(2660, 1, 7, '2001-08-24 00:00:00', 2006, 0),
(2661, 1, 7, '2001-01-04 00:00:00', 1508, 0),
(2662, 1, 7, '2001-01-12 00:00:00', 1090, 0),
(2664, 1, 3, '2001-04-11 00:00:00', 1696, 1696),
(2665, 1, 3, '2001-11-24 00:00:00', 1229, 1229),
(2667, 1, 1, '2001-06-30 00:00:00', 2122, 6366),
(2668, 1, 1, '2001-05-18 00:00:00', 1875, 5625),
(2669, 1, 1, '2001-10-10 00:00:00', 1488, 4464),
(2670, 1, 1, '2001-11-12 00:00:00', 1147, 3441),
(2671, 1, 1, '2001-11-29 00:00:00', 1033, 3099),
(2672, 1, 1, '2001-06-11 00:00:00', 785, 2355),
(2674, 1, 8, '2001-07-26 00:00:00', 1840, 5520),
(2675, 1, 8, '2001-12-22 00:00:00', 1824, 5472),
(2676, 1, 8, '2001-07-03 00:00:00', 1363, 4089),
(2677, 1, 8, '2001-06-06 00:00:00', 774, 2322),
(2678, 1, 6, '2001-02-25 00:00:00', 1411, 2822),
(2679, 1, 6, '2001-11-16 00:00:00', 1355, 2710),
(2680, 1, 6, '2001-04-14 00:00:00', 1164, 2328),
(2681, 1, 6, '2001-01-16 00:00:00', 564, 1128),
(2682, 1, 13, '2001-08-23 00:00:00', 1630, 0),
(2683, 1, 13, '2001-12-15 00:00:00', 936, 0),
(2687, 1, 10, '2001-06-07 00:00:00', 1705, 0),
(2688, 1, 10, '2001-10-18 00:00:00', 1536, 0),
(2689, 1, 10, '2001-01-23 00:00:00', 1270, 0),
(2690, 1, 14, '2001-09-15 00:00:00', 1663, 4989),
(2692, 1, 14, '2001-06-05 00:00:00', 281, 843),
(2693, 1, 4, '2001-02-01 00:00:00', 1704, 0),
(2694, 1, 4, '2001-05-03 00:00:00', 1636, 0),
(2695, 1, 4, '2001-06-14 00:00:00', 1134, 0),
(2696, 1, 4, '2001-06-17 00:00:00', 1036, 0),
(2698, 1, 5, '2001-09-27 00:00:00', 2179, 4358),
(2699, 1, 5, '2001-12-29 00:00:00', 2081, 4162),
(2700, 1, 5, '2001-06-12 00:00:00', 1985, 3970),
(2701, 1, 5, '2001-07-21 00:00:00', 1606, 3212),
(2702, 7, 11, '2001-03-10 00:00:00', 1862, 1862),
(2703, 7, 11, '2001-12-07 00:00:00', 1447, 1447),
(2704, 7, 11, '2001-05-14 00:00:00', 618, 618),
(2705, 7, 12, '2001-08-13 00:00:00', 2328, 4656),
(2706, 7, 12, '2001-08-31 00:00:00', 1559, 3118),
(2707, 7, 9, '2001-03-02 00:00:00', 1734, 1734),
(2708, 7, 9, '2001-12-30 00:00:00', 1020, 1020),
(2709, 7, 9, '2001-01-14 00:00:00', 592, 592),
(2710, 7, 7, '2001-04-21 00:00:00', 1733, 0),
(2711, 7, 7, '2001-05-19 00:00:00', 1537, 0),
(2712, 7, 7, '2001-02-21 00:00:00', 1354, 0),
(2713, 7, 7, '2001-02-01 00:00:00', 1115, 0),
(2714, 7, 7, '2001-06-08 00:00:00', 573, 0),
(2715, 7, 7, '2001-04-22 00:00:00', 254, 0),
(2716, 7, 3, '2001-04-15 00:00:00', 2318, 2318),
(2717, 7, 3, '2001-03-13 00:00:00', 1909, 1909),
(2718, 7, 3, '2001-01-12 00:00:00', 1710, 1710),
(2721, 7, 1, '2001-04-28 00:00:00', 1838, 5514),
(2722, 7, 1, '2001-07-11 00:00:00', 1811, 5433),
(2723, 7, 1, '2001-03-06 00:00:00', 1415, 4245),
(2725, 7, 8, '2001-07-01 00:00:00', 2460, 7380),
(2726, 7, 8, '2001-08-01 00:00:00', 1716, 5148),
(2727, 7, 6, '2001-05-01 00:00:00', 2339, 4678),
(2728, 7, 6, '2001-02-04 00:00:00', 2314, 4628),
(2729, 7, 6, '2001-10-08 00:00:00', 2267, 4534),
(2730, 7, 6, '2001-01-03 00:00:00', 575, 1150),
(2731, 7, 6, '2001-05-13 00:00:00', 261, 522),
(2732, 7, 13, '2001-04-04 00:00:00', 2186, 0),
(2733, 7, 13, '2001-04-30 00:00:00', 1559, 0),
(2734, 7, 13, '2001-02-23 00:00:00', 1340, 0),
(2735, 7, 13, '2001-09-21 00:00:00', 1296, 0),
(2736, 7, 13, '2001-04-14 00:00:00', 1037, 0),
(2738, 7, 10, '2001-01-07 00:00:00', 925, 0),
(2740, 7, 14, '2001-05-12 00:00:00', 2418, 7254),
(2741, 7, 14, '2001-01-04 00:00:00', 1933, 5799),
(2742, 7, 14, '2001-03-23 00:00:00', 1754, 5262),
(2743, 7, 14, '2001-02-23 00:00:00', 1509, 4527),
(2744, 7, 14, '2001-04-09 00:00:00', 910, 2730),
(2745, 7, 14, '2001-10-04 00:00:00', 596, 1788),
(2746, 7, 4, '2001-01-21 00:00:00', 2201, 0),
(2747, 7, 4, '2001-08-13 00:00:00', 2087, 0),
(2748, 7, 4, '2001-04-15 00:00:00', 1985, 0),
(2749, 7, 4, '2001-12-24 00:00:00', 1693, 0),
(2750, 7, 4, '2001-11-27 00:00:00', 1536, 0),
(2751, 7, 4, '2001-09-08 00:00:00', 1189, 0),
(2752, 7, 4, '2001-07-29 00:00:00', 970, 0),
(2753, 7, 4, '2001-06-29 00:00:00', 913, 0),
(2755, 7, 5, '2001-08-27 00:00:00', 2062, 4124),
(2756, 7, 5, '2001-11-09 00:00:00', 1512, 3024),
(2757, 7, 5, '2001-06-10 00:00:00', 1164, 2328),
(2758, 7, 5, '2001-08-07 00:00:00', 628, 1256),
(2759, 3, 11, '2001-05-12 00:00:00', 1589, 1589),
(2760, 3, 11, '2001-01-18 00:00:00', 924, 924),
(2761, 3, 12, '2001-12-03 00:00:00', 1600, 3200),
(2762, 3, 12, '2001-03-31 00:00:00', 1550, 3100),
(2763, 3, 12, '2001-09-15 00:00:00', 1346, 2692),
(2764, 3, 12, '2001-05-16 00:00:00', 980, 1960),
(2765, 3, 12, '2001-06-28 00:00:00', 898, 1796),
(2766, 3, 12, '2001-01-20 00:00:00', 620, 1240),
(2768, 3, 9, '2001-09-11 00:00:00', 2267, 2267),
(2769, 3, 9, '2001-09-28 00:00:00', 1798, 1798),
(2770, 3, 7, '2001-12-12 00:00:00', 2280, 0),
(2771, 3, 7, '2001-12-02 00:00:00', 1940, 0),
(2772, 3, 7, '2001-09-25 00:00:00', 1851, 0),
(2773, 3, 7, '2001-06-06 00:00:00', 1700, 0),
(2774, 3, 7, '2001-11-28 00:00:00', 1165, 0),
(2775, 3, 7, '2001-08-28 00:00:00', 904, 0),
(2776, 3, 7, '2001-09-04 00:00:00', 700, 0),
(2777, 3, 7, '2001-10-26 00:00:00', 561, 0),
(2779, 3, 3, '2001-04-23 00:00:00', 985, 985),
(2780, 3, 1, '2001-02-10 00:00:00', 2386, 7158),
(2781, 3, 1, '2001-10-02 00:00:00', 1410, 4230),
(2782, 3, 1, '2001-03-18 00:00:00', 1161, 3483),
(2783, 3, 1, '2001-04-10 00:00:00', 1073, 3219),
(2784, 3, 1, '2001-09-19 00:00:00', 906, 2718),
(2785, 3, 1, '2001-05-02 00:00:00', 617, 1851),
(2786, 3, 8, '2001-10-29 00:00:00', 1677, 5031),
(2787, 3, 8, '2001-09-15 00:00:00', 1540, 4620),
(2788, 3, 8, '2001-10-27 00:00:00', 1201, 3603),
(2789, 3, 8, '2001-02-23 00:00:00', 972, 2916),
(2790, 3, 8, '2001-07-27 00:00:00', 749, 2247),
(2791, 3, 8, '2001-05-19 00:00:00', 733, 2199),
(2792, 3, 6, '2001-01-10 00:00:00', 1959, 3918),
(2793, 3, 6, '2001-12-22 00:00:00', 1154, 2308),
(2794, 3, 6, '2001-08-09 00:00:00', 1063, 2126),
(2795, 3, 6, '2001-05-22 00:00:00', 1051, 2102),
(2796, 3, 6, '2001-03-30 00:00:00', 548, 1096),
(2798, 3, 13, '2001-12-04 00:00:00', 2442, 0),
(2799, 3, 13, '2001-12-13 00:00:00', 2092, 0),
(2800, 3, 13, '2001-03-03 00:00:00', 1842, 0),
(2801, 3, 13, '2001-03-25 00:00:00', 1462, 0),
(2802, 3, 13, '2001-04-24 00:00:00', 1247, 0),
(2803, 3, 13, '2001-06-19 00:00:00', 632, 0),
(2804, 3, 13, '2001-04-28 00:00:00', 597, 0),
(2805, 3, 13, '2001-12-29 00:00:00', 276, 0),
(2808, 3, 10, '2001-06-18 00:00:00', 1568, 0),
(2809, 3, 14, '2001-12-08 00:00:00', 2247, 6741),
(2810, 3, 14, '2001-08-25 00:00:00', 2244, 6732),
(2811, 3, 14, '2001-03-01 00:00:00', 2225, 6675),
(2812, 3, 14, '2001-01-08 00:00:00', 1783, 5349),
(2813, 3, 14, '2001-02-25 00:00:00', 1685, 5055),
(2814, 3, 14, '2001-02-15 00:00:00', 1669, 5007),
(2815, 3, 14, '2001-11-20 00:00:00', 1647, 4941),
(2816, 3, 4, '2001-10-16 00:00:00', 2035, 0),
(2817, 3, 4, '2001-07-20 00:00:00', 1973, 0),
(2818, 3, 4, '2001-09-15 00:00:00', 1506, 0),
(2819, 3, 4, '2001-11-13 00:00:00', 1426, 0),
(2820, 3, 4, '2001-11-23 00:00:00', 682, 0),
(2821, 3, 4, '2001-04-15 00:00:00', 578, 0),
(2822, 3, 5, '2001-07-30 00:00:00', 2094, 4188),
(2823, 3, 5, '2001-05-11 00:00:00', 1342, 2684),
(2824, 3, 5, '2001-06-14 00:00:00', 1219, 2438),
(2825, 3, 5, '2001-01-30 00:00:00', 989, 1978),
(2826, 3, 5, '2001-04-19 00:00:00', 875, 1750),
(2827, 9, 11, '2001-06-06 00:00:00', 2484, 2484),
(2828, 9, 11, '2001-07-21 00:00:00', 2370, 2370),
(2829, 9, 11, '2001-12-21 00:00:00', 1791, 1791),
(2830, 9, 11, '2001-06-11 00:00:00', 1786, 1786),
(2831, 9, 11, '2001-09-21 00:00:00', 1188, 1188),
(2832, 9, 12, '2001-10-10 00:00:00', 913, 1826),
(2833, 9, 9, '2001-03-11 00:00:00', 2156, 2156),
(2834, 9, 9, '2001-03-16 00:00:00', 1908, 1908),
(2835, 9, 9, '2001-01-18 00:00:00', 1810, 1810),
(2836, 9, 9, '2001-12-31 00:00:00', 1401, 1401),
(2837, 9, 9, '2001-05-06 00:00:00', 1000, 1000),
(2838, 9, 9, '2001-04-17 00:00:00', 703, 703),
(2839, 9, 9, '2001-10-11 00:00:00', 630, 630),
(2840, 9, 9, '2001-12-20 00:00:00', 590, 590),
(2842, 9, 7, '2001-04-28 00:00:00', 2039, 0),
(2843, 9, 7, '2001-10-07 00:00:00', 2022, 0),
(2845, 9, 3, '2001-09-10 00:00:00', 1916, 1916),
(2846, 9, 3, '2001-05-25 00:00:00', 1329, 1329),
(2847, 9, 3, '2001-02-12 00:00:00', 917, 917),
(2848, 9, 1, '2001-10-26 00:00:00', 1032, 3096),
(2849, 9, 1, '2001-04-15 00:00:00', 995, 2985),
(2851, 9, 8, '2001-10-07 00:00:00', 2428, 7284),
(2852, 9, 8, '2001-08-12 00:00:00', 1781, 5343),
(2853, 9, 8, '2001-01-30 00:00:00', 1525, 4575),
(2854, 9, 8, '2001-01-14 00:00:00', 1206, 3618),
(2855, 9, 6, '2001-08-11 00:00:00', 904, 1808),
(2857, 9, 13, '2001-04-20 00:00:00', 1970, 0),
(2858, 9, 13, '2001-08-29 00:00:00', 1638, 0),
(2859, 9, 13, '2001-04-03 00:00:00', 1383, 0),
(2860, 9, 13, '2001-01-05 00:00:00', 717, 0),
(2865, 9, 10, '2001-12-27 00:00:00', 1907, 0),
(2866, 9, 10, '2001-02-21 00:00:00', 1358, 0),
(2867, 9, 10, '2001-03-11 00:00:00', 633, 0),
(2868, 9, 14, '2001-04-25 00:00:00', 1829, 5487),
(2869, 9, 14, '2001-02-27 00:00:00', 1401, 4203),
(2870, 9, 4, '2001-09-08 00:00:00', 2379, 0),
(2871, 9, 4, '2001-02-12 00:00:00', 1575, 0),
(2872, 9, 4, '2001-08-07 00:00:00', 1063, 0),
(2873, 9, 4, '2001-08-13 00:00:00', 600, 0),
(2874, 9, 4, '2001-01-02 00:00:00', 595, 0),
(2875, 9, 4, '2001-10-01 00:00:00', 545, 0),
(2876, 9, 5, '2001-03-09 00:00:00', 2328, 4656),
(2877, 9, 5, '2001-09-10 00:00:00', 1871, 3742),
(2878, 9, 5, '2001-12-11 00:00:00', 1238, 2476),
(2879, 9, 5, '2001-06-09 00:00:00', 1206, 2412),
(2880, 9, 5, '2001-05-31 00:00:00', 1089, 2178),
(2881, 9, 5, '2001-09-07 00:00:00', 299, 598),
(2882, 4, 11, '2001-02-08 00:00:00', 2300, 2300),
(2883, 4, 11, '2001-06-30 00:00:00', 2275, 2275),
(2884, 4, 11, '2001-07-06 00:00:00', 2204, 2204),
(2885, 4, 11, '2001-08-06 00:00:00', 2143, 2143),
(2886, 4, 11, '2001-01-31 00:00:00', 1941, 1941),
(2887, 4, 11, '2001-04-18 00:00:00', 1649, 1649),
(2888, 4, 11, '2001-09-18 00:00:00', 1429, 1429),
(2889, 4, 11, '2001-06-22 00:00:00', 1124, 1124),
(2890, 4, 11, '2001-10-04 00:00:00', 1049, 1049),
(2891, 4, 11, '2001-01-07 00:00:00', 919, 919),
(2892, 4, 12, '2001-06-19 00:00:00', 2415, 4830),
(2893, 4, 12, '2001-07-29 00:00:00', 2337, 4674),
(2894, 4, 12, '2001-04-20 00:00:00', 1401, 2802),
(2895, 4, 12, '2001-11-11 00:00:00', 1328, 2656),
(2896, 4, 12, '2001-03-08 00:00:00', 1116, 2232),
(2897, 4, 9, '2001-09-24 00:00:00', 1214, 1214),
(2898, 4, 9, '2001-07-19 00:00:00', 1138, 1138),
(2899, 4, 7, '2001-02-08 00:00:00', 1408, 0),
(2900, 4, 7, '2001-07-27 00:00:00', 871, 0),
(2902, 4, 3, '2001-07-04 00:00:00', 2353, 2353),
(2903, 4, 3, '2001-03-12 00:00:00', 2343, 2343),
(2904, 4, 3, '2001-06-10 00:00:00', 2064, 2064),
(2905, 4, 3, '2001-02-15 00:00:00', 1946, 1946),
(2906, 4, 3, '2001-05-22 00:00:00', 1745, 1745),
(2907, 4, 3, '2001-07-10 00:00:00', 1701, 1701),
(2908, 4, 3, '2001-04-22 00:00:00', 1076, 1076),
(2909, 4, 3, '2001-02-23 00:00:00', 1015, 1015),
(2910, 4, 3, '2001-10-06 00:00:00', 857, 857),
(2911, 4, 3, '2001-07-11 00:00:00', 853, 853),
(2913, 4, 1, '2001-01-17 00:00:00', 2326, 6978),
(2914, 4, 1, '2001-01-13 00:00:00', 1905, 5715),
(2915, 4, 1, '2001-07-10 00:00:00', 1893, 5679),
(2916, 4, 1, '2001-07-03 00:00:00', 1804, 5412),
(2917, 4, 1, '2001-04-01 00:00:00', 1668, 5004),
(2918, 4, 1, '2001-07-17 00:00:00', 811, 2433),
(2919, 4, 1, '2001-02-10 00:00:00', 749, 2247),
(2920, 4, 1, '2001-04-09 00:00:00', 279, 837),
(2921, 4, 1, '2001-01-15 00:00:00', 261, 783),
(2922, 4, 8, '2001-06-28 00:00:00', 2347, 7041),
(2923, 4, 8, '2001-05-28 00:00:00', 1202, 3606),
(2924, 4, 8, '2001-04-17 00:00:00', 866, 2598),
(2926, 4, 6, '2001-04-23 00:00:00', 2476, 4952),
(2927, 4, 6, '2001-01-18 00:00:00', 1635, 3270),
(2928, 4, 6, '2001-02-03 00:00:00', 1609, 3218),
(2929, 4, 6, '2001-08-03 00:00:00', 1466, 2932),
(2930, 4, 13, '2001-09-28 00:00:00', 2162, 0),
(2931, 4, 13, '2001-01-26 00:00:00', 2133, 0),
(2932, 4, 13, '2001-06-28 00:00:00', 1944, 0),
(2933, 4, 13, '2001-04-19 00:00:00', 1098, 0),
(2934, 4, 13, '2001-08-18 00:00:00', 1071, 0),
(2938, 4, 10, '2001-04-30 00:00:00', 1920, 0),
(2939, 4, 10, '2001-04-26 00:00:00', 1854, 0),
(2940, 4, 10, '2001-06-14 00:00:00', 1404, 0),
(2941, 4, 10, '2001-02-20 00:00:00', 1269, 0),
(2942, 4, 10, '2001-07-29 00:00:00', 1150, 0),
(2943, 4, 10, '2001-08-23 00:00:00', 857, 0),
(2944, 4, 14, '2001-09-20 00:00:00', 2413, 7239),
(2945, 4, 14, '2001-01-06 00:00:00', 2290, 6870),
(2946, 4, 14, '2001-05-28 00:00:00', 2124, 6372),
(2947, 4, 14, '2001-08-04 00:00:00', 1935, 5805),
(2948, 4, 14, '2001-11-21 00:00:00', 1510, 4530),
(2949, 4, 14, '2001-05-22 00:00:00', 1380, 4140),
(2950, 4, 14, '2001-04-26 00:00:00', 1260, 3780),
(2951, 4, 14, '2001-07-10 00:00:00', 814, 2442),
(2952, 4, 14, '2001-12-06 00:00:00', 740, 2220),
(2953, 4, 14, '2001-07-17 00:00:00', 666, 1998),
(2954, 4, 4, '2001-01-04 00:00:00', 2409, 0),
(2955, 4, 4, '2001-08-21 00:00:00', 2226, 0),
(2956, 4, 4, '2001-06-08 00:00:00', 1356, 0),
(2957, 4, 4, '2001-02-14 00:00:00', 1324, 0),
(2958, 4, 4, '2001-08-16 00:00:00', 708, 0),
(2959, 4, 5, '2001-08-05 00:00:00', 2306, 4612),
(2960, 4, 5, '2001-05-20 00:00:00', 1685, 3370),
(2961, 4, 5, '2001-10-29 00:00:00', 1206, 2412),
(2962, 4, 5, '2001-03-03 00:00:00', 1132, 2264),
(2963, 4, 5, '2001-11-13 00:00:00', 920, 1840),
(2966, 2, 12, '2001-06-23 00:00:00', 2291, 4582),
(2967, 2, 12, '2001-04-14 00:00:00', 2026, 4052),
(2968, 2, 12, '2001-07-04 00:00:00', 1919, 3838),
(2969, 2, 12, '2001-06-04 00:00:00', 964, 1928),
(2970, 2, 9, '2001-10-16 00:00:00', 2393, 2393),
(2971, 2, 9, '2001-04-21 00:00:00', 2386, 2386),
(2972, 2, 9, '2001-03-10 00:00:00', 1914, 1914),
(2973, 2, 9, '2001-05-05 00:00:00', 1839, 1839),
(2974, 2, 9, '2001-08-14 00:00:00', 1791, 1791),
(2975, 2, 7, '2001-01-16 00:00:00', 2295, 0),
(2976, 2, 7, '2001-02-17 00:00:00', 1459, 0),
(2977, 2, 7, '2001-07-17 00:00:00', 1084, 0),
(2978, 2, 7, '2001-12-10 00:00:00', 990, 0),
(2979, 2, 7, '2001-05-17 00:00:00', 802, 0),
(2980, 2, 3, '2001-12-03 00:00:00', 2492, 2492),
(2981, 2, 3, '2001-07-24 00:00:00', 2408, 2408),
(2982, 2, 3, '2001-06-03 00:00:00', 2317, 2317),
(2983, 2, 3, '2001-04-17 00:00:00', 2011, 2011),
(2985, 2, 1, '2001-11-22 00:00:00', 2415, 7245),
(2986, 2, 1, '2001-10-24 00:00:00', 2361, 7083),
(2987, 2, 1, '2001-09-04 00:00:00', 1429, 4287),
(2988, 2, 1, '2001-06-05 00:00:00', 1408, 4224),
(2989, 2, 1, '2001-07-14 00:00:00', 1306, 3918),
(2990, 2, 1, '2001-07-19 00:00:00', 1070, 3210),
(2991, 2, 1, '2001-02-12 00:00:00', 555, 1665),
(2993, 2, 8, '2001-06-18 00:00:00', 2493, 7479),
(2994, 2, 8, '2001-11-05 00:00:00', 2491, 7473),
(2995, 2, 8, '2001-12-29 00:00:00', 1687, 5061),
(2996, 2, 8, '2001-07-01 00:00:00', 1270, 3810),
(2997, 2, 8, '2001-02-01 00:00:00', 954, 2862),
(2998, 2, 8, '2001-10-24 00:00:00', 724, 2172),
(2999, 2, 8, '2001-01-27 00:00:00', 688, 2064),
(3000, 2, 6, '2001-09-29 00:00:00', 1805, 3610),
(3001, 2, 6, '2001-04-30 00:00:00', 1540, 3080),
(3002, 2, 6, '2001-12-21 00:00:00', 862, 1724),
(3003, 2, 6, '2001-11-23 00:00:00', 581, 1162),
(3004, 2, 13, '2001-08-13 00:00:00', 1358, 0),
(3005, 2, 13, '2001-05-08 00:00:00', 1206, 0),
(3006, 2, 13, '2001-10-09 00:00:00', 674, 0),
(3007, 2, 13, '2001-11-02 00:00:00', 606, 0),
(3012, 2, 10, '2001-05-17 00:00:00', 1915, 0),
(3013, 2, 10, '2001-12-20 00:00:00', 523, 0),
(3015, 2, 14, '2001-03-31 00:00:00', 1219, 3657),
(3016, 2, 14, '2001-06-04 00:00:00', 1081, 3243),
(3017, 2, 14, '2001-06-30 00:00:00', 1051, 3153),
(3019, 2, 4, '2001-12-31 00:00:00', 1559, 0),
(3020, 2, 4, '2001-07-01 00:00:00', 1558, 0),
(3021, 2, 4, '2001-12-07 00:00:00', 645, 0),
(3022, 2, 5, '2001-10-27 00:00:00', 1990, 3980),
(3023, 2, 5, '2001-07-04 00:00:00', 1725, 3450),
(3024, 2, 5, '2001-08-17 00:00:00', 1710, 3420),
(3025, 2, 5, '2001-04-25 00:00:00', 1644, 3288),
(3026, 2, 5, '2001-07-09 00:00:00', 1457, 2914),
(3027, 2, 5, '2001-07-31 00:00:00', 1309, 2618),
(3028, 2, 5, '2001-12-29 00:00:00', 1195, 2390),
(3030, 8, 11, '2001-09-22 00:00:00', 1978, 1978),
(3031, 8, 11, '2001-02-13 00:00:00', 1866, 1866),
(3032, 8, 11, '2001-12-02 00:00:00', 1254, 1254),
(3033, 8, 11, '2001-01-10 00:00:00', 1050, 1050),
(3034, 8, 11, '2001-04-03 00:00:00', 1009, 1009),
(3035, 8, 11, '2001-03-28 00:00:00', 755, 755),
(3036, 8, 11, '2001-12-30 00:00:00', 754, 754),
(3037, 8, 12, '2001-07-08 00:00:00', 2357, 4714),
(3038, 8, 12, '2001-12-21 00:00:00', 2033, 4066),
(3039, 8, 12, '2001-10-09 00:00:00', 1998, 3996),
(3040, 8, 12, '2001-10-30 00:00:00', 1662, 3324),
(3041, 8, 12, '2001-03-30 00:00:00', 1494, 2988),
(3042, 8, 12, '2001-05-14 00:00:00', 1472, 2944),
(3044, 8, 9, '2001-04-17 00:00:00', 1329, 1329),
(3045, 8, 9, '2001-07-04 00:00:00', 1145, 1145),
(3046, 8, 7, '2001-11-18 00:00:00', 2283, 0),
(3047, 8, 3, '2001-12-22 00:00:00', 2339, 2339),
(3048, 8, 3, '2001-12-20 00:00:00', 2171, 2171),
(3049, 8, 3, '2001-03-03 00:00:00', 1627, 1627),
(3050, 8, 3, '2001-02-14 00:00:00', 1529, 1529),
(3051, 8, 1, '2001-03-30 00:00:00', 2364, 7092),
(3052, 8, 1, '2001-07-26 00:00:00', 1706, 5118),
(3053, 8, 1, '2001-12-13 00:00:00', 1462, 4386),
(3054, 8, 1, '2001-08-20 00:00:00', 1403, 4209),
(3056, 8, 8, '2001-02-19 00:00:00', 1597, 4791),
(3057, 8, 8, '2001-02-17 00:00:00', 1204, 3612),
(3058, 8, 6, '2001-02-01 00:00:00', 2173, 4346),
(3059, 8, 6, '2001-06-07 00:00:00', 1855, 3710),
(3060, 8, 6, '2001-01-07 00:00:00', 799, 1598),
(3062, 8, 13, '2001-03-14 00:00:00', 1745, 0),
(3063, 8, 13, '2001-04-05 00:00:00', 1163, 0),
(3064, 8, 13, '2001-06-01 00:00:00', 927, 0),
(3065, 8, 13, '2001-12-09 00:00:00', 743, 0),
(3066, 8, 13, '2001-08-01 00:00:00', 575, 0),
(3067, 8, 13, '2001-10-18 00:00:00', 253, 0),
(3074, 8, 10, '2001-05-02 00:00:00', 1948, 0),
(3075, 8, 10, '2001-03-25 00:00:00', 1935, 0),
(3077, 8, 14, '2001-04-29 00:00:00', 1437, 4311),
(3078, 8, 14, '2001-09-30 00:00:00', 1201, 3603),
(3079, 8, 14, '2001-06-20 00:00:00', 726, 2178),
(3080, 8, 4, '2001-09-28 00:00:00', 2488, 0),
(3081, 8, 4, '2001-08-24 00:00:00', 2409, 0),
(3082, 8, 4, '2001-04-02 00:00:00', 1693, 0),
(3083, 8, 4, '2001-06-17 00:00:00', 1222, 0),
(3084, 8, 4, '2001-07-01 00:00:00', 1221, 0),
(3085, 8, 4, '2001-09-26 00:00:00', 988, 0),
(3086, 8, 4, '2001-01-28 00:00:00', 523, 0),
(3087, 8, 5, '2001-03-25 00:00:00', 2115, 4230),
(3088, 8, 5, '2001-01-02 00:00:00', 1979, 3958),
(3089, 8, 5, '2001-10-13 00:00:00', 1586, 3172),
(3090, 8, 5, '2001-11-09 00:00:00', 1459, 2918),
(3091, 8, 5, '2001-02-28 00:00:00', 1451, 2902),
(3092, 8, 5, '2001-11-20 00:00:00', 1347, 2694),
(3093, 8, 5, '2001-01-15 00:00:00', 1153, 2306),
(3095, 10, 11, '2001-12-06 00:00:00', 2064, 2064),
(3096, 10, 12, '2001-05-28 00:00:00', 2366, 4732),
(3097, 10, 12, '2001-03-16 00:00:00', 1977, 3954),
(3098, 10, 12, '2001-03-09 00:00:00', 1825, 3650),
(3099, 10, 12, '2001-12-09 00:00:00', 1742, 3484),
(3100, 10, 12, '2001-07-10 00:00:00', 596, 1192),
(3101, 10, 9, '2001-11-17 00:00:00', 2336, 2336),
(3102, 10, 9, '2001-06-30 00:00:00', 1183, 1183),
(3103, 10, 9, '2001-04-08 00:00:00', 1121, 1121),
(3104, 10, 9, '2001-05-27 00:00:00', 885, 885),
(3105, 10, 9, '2001-02-03 00:00:00', 664, 664),
(3106, 10, 7, '2001-01-23 00:00:00', 2116, 0),
(3107, 10, 7, '2001-04-04 00:00:00', 1522, 0),
(3108, 10, 7, '2001-05-08 00:00:00', 1286, 0),
(3109, 10, 7, '2001-10-13 00:00:00', 1136, 0),
(3110, 10, 7, '2001-08-08 00:00:00', 532, 0),
(3111, 10, 3, '2001-03-03 00:00:00', 1015, 1015),
(3112, 10, 3, '2001-07-05 00:00:00', 996, 996),
(3113, 10, 1, '2001-10-04 00:00:00', 2171, 6513),
(3114, 10, 1, '2001-11-17 00:00:00', 1367, 4101),
(3115, 10, 8, '2001-04-08 00:00:00', 2381, 7143),
(3116, 10, 8, '2001-09-27 00:00:00', 2232, 6696),
(3117, 10, 8, '2001-04-29 00:00:00', 1638, 4914),
(3118, 10, 8, '2001-01-25 00:00:00', 974, 2922),
(3119, 10, 8, '2001-04-19 00:00:00', 547, 1641),
(3120, 10, 6, '2001-09-11 00:00:00', 749, 1498),
(3121, 10, 6, '2001-03-13 00:00:00', 735, 1470),
(3123, 10, 13, '2001-04-16 00:00:00', 1509, 0),
(3124, 10, 13, '2001-07-31 00:00:00', 650, 0),
(3125, 10, 13, '2001-08-26 00:00:00', 559, 0),
(3129, 10, 10, '2001-01-02 00:00:00', 2142, 0),
(3130, 10, 10, '2001-08-08 00:00:00', 2022, 0),
(3131, 10, 10, '2001-08-07 00:00:00', 2000, 0),
(3132, 10, 10, '2001-04-13 00:00:00', 1439, 0),
(3133, 10, 10, '2001-11-17 00:00:00', 1425, 0),
(3134, 10, 10, '2001-04-16 00:00:00', 832, 0),
(3135, 10, 10, '2001-11-14 00:00:00', 593, 0),
(3137, 10, 14, '2001-05-24 00:00:00', 1442, 4326),
(3138, 10, 14, '2001-03-12 00:00:00', 1267, 3801),
(3139, 10, 4, '2001-05-05 00:00:00', 2296, 0),
(3140, 10, 4, '2001-11-29 00:00:00', 908, 0),
(3142, 10, 5, '2001-12-05 00:00:00', 1735, 3470),
(3143, 10, 5, '2001-06-12 00:00:00', 1654, 3308),
(3144, 10, 5, '2001-03-15 00:00:00', 1610, 3220),
(3145, 10, 5, '2001-08-21 00:00:00', 1446, 2892),
(3146, 10, 5, '2001-04-29 00:00:00', 286, 572),
(3147, 5, 11, '2001-07-14 00:00:00', 1982, 1982),
(3148, 5, 11, '2001-09-02 00:00:00', 1623, 1623),
(3149, 5, 11, '2001-04-20 00:00:00', 1294, 1294),
(3150, 5, 11, '2001-10-07 00:00:00', 941, 941),
(3151, 5, 12, '2001-04-04 00:00:00', 2261, 4522),
(3152, 5, 12, '2001-12-26 00:00:00', 926, 1852),
(3153, 5, 12, '2001-05-01 00:00:00', 787, 1574),
(3154, 5, 12, '2001-12-14 00:00:00', 504, 1008),
(3155, 5, 9, '2001-08-23 00:00:00', 1787, 1787),
(3156, 5, 9, '2001-08-16 00:00:00', 1447, 1447),
(3157, 5, 9, '2001-10-02 00:00:00', 1384, 1384),
(3158, 5, 9, '2001-12-17 00:00:00', 1189, 1189),
(3159, 5, 9, '2001-10-18 00:00:00', 1168, 1168),
(3160, 5, 9, '2001-11-08 00:00:00', 814, 814),
(3161, 5, 9, '2001-07-07 00:00:00', 751, 751),
(3162, 5, 9, '2001-07-24 00:00:00', 691, 691),
(3163, 5, 9, '2001-09-08 00:00:00', 615, 615),
(3164, 5, 7, '2001-07-21 00:00:00', 2385, 0),
(3165, 5, 7, '2001-03-09 00:00:00', 1703, 0),
(3166, 5, 3, '2001-01-04 00:00:00', 2403, 2403),
(3167, 5, 3, '2001-07-18 00:00:00', 2167, 2167),
(3168, 5, 1, '2001-05-31 00:00:00', 2377, 7131),
(3169, 5, 1, '2001-01-29 00:00:00', 2010, 6030),
(3170, 5, 1, '2001-12-02 00:00:00', 1814, 5442),
(3171, 5, 1, '2001-04-08 00:00:00', 1444, 4332),
(3173, 5, 8, '2001-07-30 00:00:00', 1665, 4995),
(3174, 5, 8, '2001-01-12 00:00:00', 1654, 4962),
(3175, 5, 8, '2001-10-27 00:00:00', 813, 2439),
(3176, 5, 8, '2001-07-05 00:00:00', 802, 2406),
(3177, 5, 6, '2001-05-23 00:00:00', 1998, 3996),
(3178, 5, 6, '2001-10-29 00:00:00', 1339, 2678),
(3179, 5, 6, '2001-11-09 00:00:00', 1275, 2550),
(3180, 5, 6, '2001-07-09 00:00:00', 1069, 2138),
(3181, 5, 6, '2001-12-31 00:00:00', 645, 1290),
(3182, 5, 6, '2001-05-07 00:00:00', 543, 1086),
(3183, 5, 13, '2001-09-08 00:00:00', 2012, 0),
(3184, 5, 13, '2001-06-27 00:00:00', 1876, 0),
(3185, 5, 13, '2001-09-21 00:00:00', 1484, 0),
(3186, 5, 13, '2001-12-15 00:00:00', 963, 0),
(3187, 5, 13, '2001-01-23 00:00:00', 611, 0),
(3188, 5, 13, '2001-08-17 00:00:00', 516, 0),
(3192, 5, 10, '2001-10-08 00:00:00', 2032, 0),
(3193, 5, 14, '2001-03-22 00:00:00', 2369, 7107),
(3194, 5, 14, '2001-08-25 00:00:00', 2221, 6663),
(3195, 5, 14, '2001-12-16 00:00:00', 2083, 6249),
(3196, 5, 14, '2001-07-07 00:00:00', 1706, 5118),
(3197, 5, 14, '2001-02-09 00:00:00', 1269, 3807),
(3198, 5, 14, '2001-12-19 00:00:00', 581, 1743),
(3199, 5, 4, '2001-04-04 00:00:00', 1980, 0),
(3200, 5, 4, '2001-11-21 00:00:00', 1757, 0),
(3201, 5, 4, '2001-05-17 00:00:00', 1006, 0),
(3202, 5, 5, '2001-10-28 00:00:00', 1715, 3430),
(3203, 5, 5, '2001-08-08 00:00:00', 1113, 2226),
(3204, 5, 5, '2001-05-16 00:00:00', 576, 1152),
(3205, 5, 5, '2001-11-21 00:00:00', 510, 1020),
(3206, 12, 11, '2001-05-20 00:00:00', 2318, 2318),
(3207, 12, 11, '2001-05-28 00:00:00', 1613, 1613),
(3208, 12, 11, '2001-06-01 00:00:00', 1584, 1584),
(3209, 12, 11, '2001-06-13 00:00:00', 1495, 1495),
(3210, 12, 12, '2001-03-14 00:00:00', 2137, 4274),
(3211, 12, 12, '2001-07-14 00:00:00', 2096, 4192),
(3212, 12, 12, '2001-05-13 00:00:00', 1937, 3874),
(3213, 12, 12, '2001-09-22 00:00:00', 1428, 2856),
(3214, 12, 12, '2001-02-15 00:00:00', 1398, 2796),
(3215, 12, 12, '2001-11-24 00:00:00', 776, 1552),
(3216, 12, 12, '2001-06-26 00:00:00', 669, 1338),
(3217, 12, 12, '2001-03-31 00:00:00', 624, 1248),
(3218, 12, 12, '2001-07-26 00:00:00', 534, 1068),
(3219, 12, 9, '2001-06-07 00:00:00', 1987, 1987),
(3220, 12, 9, '2001-05-21 00:00:00', 1740, 1740),
(3221, 12, 9, '2001-02-24 00:00:00', 1506, 1506),
(3222, 12, 9, '2001-05-29 00:00:00', 936, 936),
(3223, 12, 9, '2001-11-22 00:00:00', 827, 827),
(3224, 12, 7, '2001-01-26 00:00:00', 1994, 0),
(3225, 12, 7, '2001-05-21 00:00:00', 1590, 0),
(3226, 12, 7, '2001-06-06 00:00:00', 947, 0),
(3227, 12, 7, '2001-07-09 00:00:00', 742, 0),
(3228, 12, 7, '2001-05-10 00:00:00', 735, 0),
(3229, 12, 7, '2001-10-08 00:00:00', 733, 0),
(3231, 12, 7, '2001-09-21 00:00:00', 269, 0),
(3232, 12, 3, '2001-01-08 00:00:00', 2191, 2191),
(3233, 12, 3, '2001-04-05 00:00:00', 1511, 1511),
(3234, 12, 1, '2001-06-26 00:00:00', 2168, 6504),
(3235, 12, 1, '2001-04-12 00:00:00', 1629, 4887),
(3236, 12, 1, '2001-07-04 00:00:00', 1518, 4554),
(3237, 12, 1, '2001-05-27 00:00:00', 1030, 3090),
(3238, 12, 1, '2001-10-14 00:00:00', 508, 1524),
(3239, 12, 8, '2001-02-10 00:00:00', 1261, 3783),
(3240, 12, 8, '2001-12-13 00:00:00', 1209, 3627),
(3241, 12, 6, '2001-08-17 00:00:00', 530, 1060),
(3243, 12, 13, '2001-07-10 00:00:00', 581, 0),
(3254, 12, 10, '2001-03-11 00:00:00', 1577, 0),
(3255, 12, 10, '2001-12-30 00:00:00', 1273, 0),
(3256, 12, 10, '2001-09-18 00:00:00', 1273, 0),
(3257, 12, 10, '2001-11-10 00:00:00', 889, 0),
(3258, 12, 10, '2001-01-03 00:00:00', 264, 0),
(3259, 12, 14, '2001-08-17 00:00:00', 1284, 3852),
(3260, 12, 14, '2001-05-31 00:00:00', 1269, 3807),
(3261, 12, 14, '2001-08-31 00:00:00', 681, 2043),
(3262, 12, 4, '2001-08-14 00:00:00', 2433, 0),
(3263, 12, 4, '2001-06-23 00:00:00', 2328, 0),
(3264, 12, 4, '2001-12-31 00:00:00', 795, 0),
(3265, 12, 4, '2001-12-04 00:00:00', 777, 0),
(3266, 12, 4, '2001-07-31 00:00:00', 597, 0),
(3267, 12, 5, '2001-05-29 00:00:00', 2282, 4564),
(3268, 12, 5, '2001-08-05 00:00:00', 2135, 4270),
(3269, 12, 5, '2001-03-10 00:00:00', 1826, 3652),
(3270, 12, 5, '2001-09-18 00:00:00', 1648, 3296),
(3271, 12, 5, '2001-12-09 00:00:00', 1535, 3070),
(3272, 12, 5, '2001-06-06 00:00:00', 1518, 3036),
(3273, 12, 5, '2001-03-03 00:00:00', 1065, 2130),
(3274, 12, 5, '2001-11-21 00:00:00', 840, 1680),
(3275, 12, 5, '2001-07-21 00:00:00', 783, 1566),
(3276, 12, 5, '2001-10-11 00:00:00', 637, 1274),
(3277, 13, 11, '2001-11-16 00:00:00', 2493, 2493),
(3278, 13, 11, '2001-11-05 00:00:00', 269, 269),
(3279, 13, 12, '2001-06-14 00:00:00', 1384, 2768),
(3280, 13, 12, '2001-11-16 00:00:00', 938, 1876),
(3281, 13, 12, '2001-04-22 00:00:00', 906, 1812),
(3282, 13, 9, '2001-04-26 00:00:00', 1384, 1384),
(3284, 13, 7, '2001-04-30 00:00:00', 1760, 0),
(3285, 13, 7, '2001-07-08 00:00:00', 1221, 0),
(3286, 13, 7, '2001-08-06 00:00:00', 1204, 0),
(3287, 13, 7, '2001-12-25 00:00:00', 1181, 0),
(3288, 13, 7, '2001-09-30 00:00:00', 1061, 0),
(3289, 13, 7, '2001-07-12 00:00:00', 584, 0),
(3292, 13, 3, '2001-05-07 00:00:00', 1546, 1546),
(3293, 13, 3, '2001-10-04 00:00:00', 1072, 1072),
(3294, 13, 3, '2001-10-14 00:00:00', 825, 825),
(3295, 13, 3, '2001-05-01 00:00:00', 796, 796),
(3296, 13, 1, '2001-04-18 00:00:00', 2055, 6165),
(3297, 13, 1, '2001-04-05 00:00:00', 1405, 4215),
(3298, 13, 1, '2001-01-18 00:00:00', 1255, 3765),
(3299, 13, 1, '2001-09-07 00:00:00', 1151, 3453),
(3300, 13, 1, '2001-01-10 00:00:00', 1008, 3024),
(3301, 13, 1, '2001-10-04 00:00:00', 654, 1962),
(3302, 13, 8, '2001-05-08 00:00:00', 1983, 5949),
(3303, 13, 8, '2001-03-18 00:00:00', 1348, 4044),
(3304, 13, 8, '2001-05-03 00:00:00', 555, 1665),
(3307, 13, 6, '2001-01-21 00:00:00', 2213, 4426),
(3308, 13, 6, '2001-03-24 00:00:00', 2110, 4220),
(3309, 13, 6, '2001-12-28 00:00:00', 614, 1228),
(3310, 13, 6, '2001-03-30 00:00:00', 274, 548),
(3311, 13, 13, '2001-01-19 00:00:00', 2113, 0),
(3312, 13, 13, '2001-12-17 00:00:00', 1766, 0),
(3313, 13, 13, '2001-05-03 00:00:00', 1251, 0),
(3314, 13, 13, '2001-08-18 00:00:00', 663, 0),
(3318, 13, 10, '2001-03-13 00:00:00', 2290, 0),
(3319, 13, 10, '2001-10-14 00:00:00', 1869, 0),
(3320, 13, 10, '2001-01-18 00:00:00', 1795, 0),
(3321, 13, 10, '2001-08-21 00:00:00', 1658, 0),
(3322, 13, 10, '2001-12-05 00:00:00', 1219, 0),
(3323, 13, 10, '2001-05-07 00:00:00', 1213, 0),
(3324, 13, 10, '2001-07-26 00:00:00', 1117, 0),
(3325, 13, 10, '2001-03-21 00:00:00', 288, 0),
(3326, 13, 14, '2001-01-19 00:00:00', 1829, 5487),
(3327, 13, 14, '2001-08-19 00:00:00', 1326, 3978),
(3328, 13, 14, '2001-07-11 00:00:00', 1099, 3297),
(3329, 13, 14, '2001-08-31 00:00:00', 1064, 3192),
(3330, 13, 14, '2001-08-12 00:00:00', 932, 2796),
(3331, 13, 14, '2001-03-19 00:00:00', 823, 2469),
(3332, 13, 14, '2001-06-14 00:00:00', 667, 2001),
(3333, 13, 14, '2001-07-06 00:00:00', 653, 1959),
(3334, 13, 14, '2001-08-29 00:00:00', 637, 1911),
(3335, 13, 14, '2001-10-27 00:00:00', 632, 1896),
(3337, 13, 4, '2001-12-29 00:00:00', 2491, 0),
(3338, 13, 4, '2001-10-04 00:00:00', 2429, 0),
(3339, 13, 4, '2001-08-13 00:00:00', 2425, 0),
(3340, 13, 4, '2001-04-27 00:00:00', 2357, 0),
(3341, 13, 4, '2001-07-31 00:00:00', 2167, 0),
(3342, 13, 4, '2001-09-18 00:00:00', 1073, 0),
(3343, 13, 4, '2001-09-10 00:00:00', 808, 0),
(3344, 13, 4, '2001-11-08 00:00:00', 597, 0),
(3345, 13, 5, '2001-08-27 00:00:00', 1716, 3432),
(3346, 13, 5, '2001-10-01 00:00:00', 854, 1708),
(3347, 13, 5, '2001-12-21 00:00:00', 584, 1168),
(3348, 11, 11, '2002-05-01 00:00:00', 2491, 2491),
(3349, 11, 11, '2002-12-25 00:00:00', 2400, 2400),
(3351, 11, 12, '2002-01-04 00:00:00', 1745, 3490),
(3352, 11, 12, '2002-05-20 00:00:00', 1043, 2086),
(3353, 11, 9, '2002-02-17 00:00:00', 2006, 2006),
(3354, 11, 9, '2002-10-30 00:00:00', 1981, 1981),
(3355, 11, 9, '2002-02-14 00:00:00', 1609, 1609),
(3356, 11, 9, '2002-09-27 00:00:00', 1515, 1515),
(3357, 11, 9, '2002-05-09 00:00:00', 1241, 1241),
(3358, 11, 9, '2002-07-14 00:00:00', 1017, 1017),
(3359, 11, 7, '2002-02-28 00:00:00', 2146, 0),
(3360, 11, 7, '2002-11-03 00:00:00', 2129, 0),
(3361, 11, 7, '2002-11-01 00:00:00', 1364, 0),
(3362, 11, 3, '2002-08-04 00:00:00', 2450, 2450),
(3363, 11, 3, '2002-05-19 00:00:00', 2449, 2449),
(3364, 11, 3, '2002-09-08 00:00:00', 1896, 1896),
(3365, 11, 3, '2002-04-10 00:00:00', 1881, 1881),
(3366, 11, 3, '2002-09-09 00:00:00', 1806, 1806),
(3367, 11, 3, '2002-03-14 00:00:00', 1639, 1639),
(3368, 11, 3, '2002-11-21 00:00:00', 1291, 1291),
(3369, 11, 3, '2002-05-21 00:00:00', 1242, 1242),
(3370, 11, 3, '2002-10-09 00:00:00', 1092, 1092),
(3371, 11, 3, '2002-02-09 00:00:00', 542, 542),
(3372, 11, 3, '2002-11-19 00:00:00', 519, 519),
(3373, 11, 1, '2002-10-22 00:00:00', 2295, 6885),
(3374, 11, 1, '2002-04-18 00:00:00', 2177, 6531),
(3375, 11, 1, '2002-07-30 00:00:00', 1846, 5538),
(3376, 11, 1, '2002-09-06 00:00:00', 1654, 4962),
(3377, 11, 1, '2002-11-13 00:00:00', 1466, 4398),
(3378, 11, 1, '2002-03-10 00:00:00', 1198, 3594),
(3379, 11, 1, '2002-02-01 00:00:00', 769, 2307),
(3380, 11, 1, '2002-07-24 00:00:00', 596, 1788),
(3381, 11, 1, '2002-11-28 00:00:00', 573, 1719),
(3382, 11, 8, '2002-08-05 00:00:00', 2201, 6603),
(3383, 11, 8, '2002-08-16 00:00:00', 1956, 5868),
(3384, 11, 8, '2002-01-21 00:00:00', 1767, 5301),
(3385, 11, 8, '2002-09-09 00:00:00', 1118, 3354),
(3386, 11, 6, '2002-01-17 00:00:00', 2491, 4982),
(3387, 11, 6, '2002-05-03 00:00:00', 2127, 4254),
(3388, 11, 6, '2002-11-09 00:00:00', 1905, 3810),
(3389, 11, 6, '2002-10-10 00:00:00', 1732, 3464),
(3390, 11, 6, '2002-08-05 00:00:00', 1631, 3262),
(3391, 11, 6, '2002-07-18 00:00:00', 1106, 2212),
(3392, 11, 6, '2002-04-14 00:00:00', 533, 1066),
(3395, 11, 13, '2002-07-23 00:00:00', 2202, 0),
(3396, 11, 13, '2002-11-09 00:00:00', 1995, 0),
(3397, 11, 13, '2002-09-09 00:00:00', 879, 0),
(3398, 11, 13, '2002-10-28 00:00:00', 792, 0),
(3400, 11, 13, '2002-03-17 00:00:00', 262, 0),
(3404, 11, 10, '2002-01-19 00:00:00', 2390, 0),
(3405, 11, 10, '2002-12-11 00:00:00', 1636, 0),
(3406, 11, 10, '2002-05-25 00:00:00', 1419, 0),
(3407, 11, 10, '2002-06-02 00:00:00', 669, 0),
(3408, 11, 10, '2002-02-21 00:00:00', 628, 0),
(3409, 11, 14, '2002-07-14 00:00:00', 2440, 7320),
(3410, 11, 14, '2002-06-06 00:00:00', 1649, 4947),
(3411, 11, 14, '2002-10-31 00:00:00', 720, 2160),
(3412, 11, 4, '2002-12-31 00:00:00', 2210, 0),
(3413, 11, 4, '2002-04-17 00:00:00', 2141, 0),
(3414, 11, 4, '2002-09-06 00:00:00', 1560, 0),
(3415, 11, 4, '2002-02-18 00:00:00', 1415, 0),
(3416, 11, 4, '2002-09-19 00:00:00', 1127, 0),
(3417, 11, 5, '2002-08-20 00:00:00', 2165, 4330),
(3418, 11, 5, '2002-12-28 00:00:00', 1719, 3438),
(3419, 11, 5, '2002-11-29 00:00:00', 1471, 2942),
(3420, 11, 5, '2002-12-09 00:00:00', 978, 1956),
(3422, 6, 11, '2002-12-15 00:00:00', 2312, 2312),
(3423, 6, 11, '2002-05-24 00:00:00', 1499, 1499),
(3424, 6, 12, '2002-01-09 00:00:00', 2405, 4810),
(3425, 6, 12, '2002-09-23 00:00:00', 1683, 3366),
(3426, 6, 12, '2002-05-14 00:00:00', 1177, 2354),
(3427, 6, 12, '2002-12-21 00:00:00', 1105, 2210),
(3430, 6, 12, '2002-09-23 00:00:00', 294, 588),
(3431, 6, 12, '2002-05-04 00:00:00', 262, 524),
(3432, 6, 9, '2002-07-30 00:00:00', 2290, 2290),
(3433, 6, 9, '2002-04-01 00:00:00', 1897, 1897),
(3434, 6, 9, '2002-11-20 00:00:00', 1775, 1775),
(3435, 6, 9, '2002-08-09 00:00:00', 1452, 1452),
(3436, 6, 9, '2002-07-15 00:00:00', 1418, 1418),
(3437, 6, 9, '2002-06-17 00:00:00', 939, 939),
(3438, 6, 9, '2002-04-12 00:00:00', 902, 902),
(3439, 6, 7, '2002-10-18 00:00:00', 2430, 0),
(3440, 6, 7, '2002-01-08 00:00:00', 2153, 0),
(3441, 6, 7, '2002-05-14 00:00:00', 1795, 0),
(3442, 6, 7, '2002-10-13 00:00:00', 1458, 0),
(3443, 6, 7, '2002-09-24 00:00:00', 1456, 0),
(3444, 6, 7, '2002-04-13 00:00:00', 1030, 0),
(3445, 6, 7, '2002-04-22 00:00:00', 605, 0),
(3446, 6, 3, '2002-03-04 00:00:00', 1541, 1541),
(3447, 6, 3, '2002-06-01 00:00:00', 1410, 1410),
(3448, 6, 3, '2002-09-02 00:00:00', 1383, 1383),
(3449, 6, 3, '2002-06-21 00:00:00', 1290, 1290),
(3450, 6, 3, '2002-10-10 00:00:00', 631, 631),
(3451, 6, 1, '2002-09-08 00:00:00', 1947, 5841),
(3452, 6, 1, '2002-07-31 00:00:00', 1886, 5658),
(3453, 6, 1, '2002-03-26 00:00:00', 1854, 5562),
(3454, 6, 1, '2002-11-02 00:00:00', 847, 2541),
(3455, 6, 1, '2002-03-13 00:00:00', 778, 2334),
(3456, 6, 8, '2002-09-15 00:00:00', 2024, 6072),
(3457, 6, 8, '2002-07-26 00:00:00', 1915, 5745),
(3458, 6, 6, '2002-01-22 00:00:00', 2122, 4244),
(3459, 6, 6, '2002-01-30 00:00:00', 1048, 2096),
(3460, 6, 6, '2002-02-07 00:00:00', 988, 1976),
(3461, 6, 6, '2002-03-18 00:00:00', 638, 1276),
(3462, 6, 6, '2002-04-04 00:00:00', 521, 1042),
(3463, 6, 6, '2002-12-03 00:00:00', 259, 518),
(3464, 6, 13, '2002-02-11 00:00:00', 2268, 0),
(3465, 6, 13, '2002-09-14 00:00:00', 1865, 0),
(3466, 6, 13, '2002-06-26 00:00:00', 1692, 0),
(3467, 6, 13, '2002-06-10 00:00:00', 1479, 0),
(3468, 6, 13, '2002-02-06 00:00:00', 1468, 0),
(3473, 6, 10, '2002-04-23 00:00:00', 2445, 0),
(3474, 6, 10, '2002-09-08 00:00:00', 1811, 0),
(3475, 6, 10, '2002-03-07 00:00:00', 1158, 0),
(3477, 6, 14, '2002-08-30 00:00:00', 1453, 4359),
(3478, 6, 14, '2002-01-15 00:00:00', 1347, 4041),
(3479, 6, 14, '2002-01-20 00:00:00', 1058, 3174),
(3480, 6, 14, '2002-02-19 00:00:00', 830, 2490),
(3481, 6, 4, '2002-08-23 00:00:00', 2397, 0),
(3482, 6, 4, '2002-04-08 00:00:00', 1488, 0),
(3483, 6, 4, '2002-04-13 00:00:00', 1448, 0),
(3484, 6, 4, '2002-08-13 00:00:00', 1415, 0),
(3485, 6, 4, '2002-02-01 00:00:00', 1189, 0),
(3486, 6, 4, '2002-08-10 00:00:00', 843, 0),
(3488, 6, 5, '2002-10-04 00:00:00', 2038, 4076),
(3490, 1, 11, '2002-05-05 00:00:00', 1750, 1750),
(3491, 1, 11, '2002-02-25 00:00:00', 1019, 1019),
(3492, 1, 11, '2002-05-16 00:00:00', 816, 816),
(3493, 1, 11, '2002-03-04 00:00:00', 539, 539),
(3494, 1, 11, '2002-10-01 00:00:00', 289, 289),
(3495, 1, 12, '2002-01-17 00:00:00', 2100, 4200),
(3496, 1, 12, '2002-04-24 00:00:00', 1679, 3358),
(3497, 1, 12, '2002-01-25 00:00:00', 1635, 3270),
(3498, 1, 12, '2002-07-15 00:00:00', 1271, 2542),
(3499, 1, 12, '2002-05-20 00:00:00', 552, 1104),
(3501, 1, 9, '2002-04-07 00:00:00', 2287, 2287),
(3502, 1, 9, '2002-09-16 00:00:00', 1987, 1987),
(3503, 1, 9, '2002-10-04 00:00:00', 1822, 1822),
(3504, 1, 9, '2002-11-06 00:00:00', 1483, 1483),
(3505, 1, 9, '2002-02-27 00:00:00', 1278, 1278),
(3506, 1, 9, '2002-10-15 00:00:00', 1025, 1025),
(3507, 1, 7, '2002-03-05 00:00:00', 2332, 0),
(3508, 1, 7, '2002-12-30 00:00:00', 2292, 0),
(3509, 1, 7, '2002-07-23 00:00:00', 1686, 0),
(3510, 1, 7, '2002-11-03 00:00:00', 1010, 0),
(3511, 1, 7, '2002-06-20 00:00:00', 726, 0),
(3513, 1, 3, '2002-07-20 00:00:00', 2372, 2372),
(3514, 1, 3, '2002-06-15 00:00:00', 773, 773),
(3515, 1, 1, '2002-02-22 00:00:00', 2058, 6174),
(3516, 1, 1, '2002-01-07 00:00:00', 2055, 6165),
(3517, 1, 1, '2002-02-25 00:00:00', 1366, 4098),
(3518, 1, 1, '2002-04-27 00:00:00', 961, 2883),
(3519, 1, 1, '2002-11-26 00:00:00', 878, 2634),
(3520, 1, 1, '2002-08-13 00:00:00', 569, 1707),
(3522, 1, 8, '2002-02-27 00:00:00', 2432, 7296),
(3523, 1, 8, '2002-04-29 00:00:00', 2398, 7194),
(3524, 1, 8, '2002-11-14 00:00:00', 1734, 5202),
(3525, 1, 8, '2002-03-21 00:00:00', 1378, 4134),
(3526, 1, 6, '2002-12-09 00:00:00', 2377, 4754),
(3527, 1, 6, '2002-10-20 00:00:00', 1920, 3840),
(3528, 1, 6, '2002-12-11 00:00:00', 1796, 3592),
(3529, 1, 13, '2002-06-12 00:00:00', 2056, 0),
(3530, 1, 13, '2002-10-16 00:00:00', 1381, 0),
(3531, 1, 13, '2002-02-26 00:00:00', 1290, 0),
(3532, 1, 13, '2002-05-11 00:00:00', 1139, 0),
(3533, 1, 13, '2002-03-07 00:00:00', 1121, 0),
(3534, 1, 13, '2002-09-05 00:00:00', 1111, 0),
(3543, 1, 10, '2002-08-26 00:00:00', 1570, 0),
(3544, 1, 14, '2002-02-01 00:00:00', 1452, 4356),
(3545, 1, 14, '2002-01-02 00:00:00', 1131, 3393),
(3546, 1, 14, '2002-03-08 00:00:00', 862, 2586),
(3547, 1, 14, '2002-05-06 00:00:00', 740, 2220),
(3548, 1, 14, '2002-01-07 00:00:00', 539, 1617),
(3549, 1, 14, '2002-01-10 00:00:00', 537, 1611),
(3550, 1, 4, '2002-04-08 00:00:00', 2045, 0),
(3551, 1, 4, '2002-02-22 00:00:00', 1296, 0),
(3553, 1, 5, '2002-12-16 00:00:00', 2116, 4232),
(3554, 1, 5, '2002-10-05 00:00:00', 1994, 3988),
(3555, 1, 5, '2002-12-16 00:00:00', 1623, 3246),
(3556, 7, 11, '2002-11-03 00:00:00', 1598, 1598),
(3557, 7, 11, '2002-03-28 00:00:00', 1024, 1024),
(3558, 7, 12, '2002-04-24 00:00:00', 1334, 2668),
(3559, 7, 12, '2002-05-07 00:00:00', 1049, 2098),
(3560, 7, 12, '2002-05-25 00:00:00', 708, 1416),
(3561, 7, 9, '2002-08-12 00:00:00', 745, 745),
(3562, 7, 9, '2002-07-16 00:00:00', 686, 686),
(3563, 7, 9, '2002-05-01 00:00:00', 628, 628),
(3564, 7, 9, '2002-07-08 00:00:00', 620, 620),
(3565, 7, 7, '2002-07-15 00:00:00', 2461, 0),
(3566, 7, 7, '2002-04-14 00:00:00', 2328, 0),
(3567, 7, 7, '2002-11-04 00:00:00', 2045, 0),
(3568, 7, 7, '2002-07-18 00:00:00', 1573, 0),
(3569, 7, 7, '2002-10-29 00:00:00', 1552, 0),
(3570, 7, 7, '2002-11-09 00:00:00', 1284, 0),
(3571, 7, 7, '2002-10-25 00:00:00', 1128, 0),
(3572, 7, 7, '2002-06-01 00:00:00', 921, 0),
(3573, 7, 3, '2002-10-05 00:00:00', 1419, 1419),
(3574, 7, 3, '2002-04-03 00:00:00', 897, 897),
(3575, 7, 3, '2002-07-10 00:00:00', 759, 759),
(3576, 7, 1, '2002-06-20 00:00:00', 2359, 7077),
(3577, 7, 1, '2002-03-29 00:00:00', 1726, 5178),
(3578, 7, 1, '2002-12-08 00:00:00', 1384, 4152),
(3579, 7, 1, '2002-11-17 00:00:00', 1274, 3822),
(3580, 7, 1, '2002-11-03 00:00:00', 877, 2631),
(3581, 7, 8, '2002-11-24 00:00:00', 2495, 7485),
(3582, 7, 8, '2002-02-16 00:00:00', 2174, 6522),
(3583, 7, 8, '2002-11-18 00:00:00', 1618, 4854),
(3584, 7, 8, '2002-10-13 00:00:00', 1230, 3690),
(3585, 7, 8, '2002-09-12 00:00:00', 952, 2856),
(3586, 7, 6, '2002-09-21 00:00:00', 1780, 3560),
(3587, 7, 6, '2002-04-07 00:00:00', 699, 1398),
(3588, 7, 6, '2002-07-13 00:00:00', 672, 1344),
(3591, 7, 13, '2002-02-05 00:00:00', 2191, 0),
(3592, 7, 13, '2002-11-03 00:00:00', 2020, 0),
(3593, 7, 13, '2002-07-02 00:00:00', 1857, 0),
(3594, 7, 13, '2002-12-02 00:00:00', 576, 0),
(3600, 7, 10, '2002-09-07 00:00:00', 1054, 0),
(3601, 7, 10, '2002-01-09 00:00:00', 920, 0),
(3602, 7, 14, '2002-07-10 00:00:00', 1819, 5457),
(3603, 7, 14, '2002-05-26 00:00:00', 1742, 5226),
(3604, 7, 14, '2002-09-21 00:00:00', 1199, 3597),
(3605, 7, 14, '2002-11-16 00:00:00', 816, 2448),
(3606, 7, 4, '2002-02-24 00:00:00', 2236, 0),
(3607, 7, 4, '2002-07-11 00:00:00', 2135, 0),
(3608, 7, 4, '2002-08-30 00:00:00', 2051, 0),
(3609, 7, 4, '2002-04-01 00:00:00', 727, 0),
(3611, 7, 5, '2002-01-02 00:00:00', 2039, 4078),
(3612, 7, 5, '2002-08-23 00:00:00', 1885, 3770),
(3613, 7, 5, '2002-06-05 00:00:00', 1563, 3126),
(3614, 3, 11, '2002-01-22 00:00:00', 2180, 2180),
(3615, 3, 11, '2002-05-14 00:00:00', 1973, 1973),
(3616, 3, 11, '2002-02-28 00:00:00', 1424, 1424),
(3617, 3, 11, '2002-11-11 00:00:00', 513, 513),
(3618, 3, 12, '2002-03-23 00:00:00', 1777, 3554),
(3619, 3, 12, '2002-04-27 00:00:00', 1646, 3292),
(3620, 3, 12, '2002-12-24 00:00:00', 1544, 3088),
(3621, 3, 12, '2002-11-15 00:00:00', 1401, 2802),
(3622, 3, 12, '2002-01-20 00:00:00', 659, 1318),
(3623, 3, 12, '2002-12-12 00:00:00', 625, 1250),
(3624, 3, 9, '2002-09-22 00:00:00', 2477, 2477),
(3625, 3, 9, '2002-03-13 00:00:00', 2375, 2375),
(3626, 3, 9, '2002-01-17 00:00:00', 1816, 1816),
(3627, 3, 9, '2002-04-23 00:00:00', 1508, 1508),
(3628, 3, 9, '2002-09-21 00:00:00', 884, 884),
(3630, 3, 7, '2002-03-22 00:00:00', 2278, 0),
(3631, 3, 7, '2002-11-07 00:00:00', 603, 0),
(3632, 3, 7, '2002-10-02 00:00:00', 521, 0),
(3634, 3, 7, '2002-10-05 00:00:00', 279, 0),
(3635, 3, 3, '2002-04-10 00:00:00', 2333, 2333),
(3636, 3, 3, '2002-11-22 00:00:00', 1988, 1988),
(3637, 3, 3, '2002-01-27 00:00:00', 1865, 1865),
(3638, 3, 3, '2002-06-26 00:00:00', 1562, 1562),
(3639, 3, 3, '2002-09-17 00:00:00', 1492, 1492),
(3640, 3, 1, '2002-01-23 00:00:00', 2500, 7500),
(3641, 3, 1, '2002-01-17 00:00:00', 1922, 5766),
(3642, 3, 1, '2002-06-29 00:00:00', 1611, 4833),
(3643, 3, 1, '2002-07-31 00:00:00', 1075, 3225),
(3644, 3, 8, '2002-04-17 00:00:00', 1648, 4944),
(3645, 3, 8, '2002-11-22 00:00:00', 1570, 4710),
(3646, 3, 8, '2002-02-27 00:00:00', 1270, 3810),
(3649, 3, 6, '2002-09-14 00:00:00', 2340, 4680),
(3650, 3, 6, '2002-09-09 00:00:00', 2163, 4326),
(3651, 3, 6, '2002-05-14 00:00:00', 1331, 2662),
(3652, 3, 6, '2002-09-10 00:00:00', 869, 1738),
(3653, 3, 13, '2002-11-01 00:00:00', 2153, 0),
(3654, 3, 13, '2002-09-06 00:00:00', 1853, 0),
(3655, 3, 13, '2002-07-27 00:00:00', 1629, 0),
(3656, 3, 13, '2002-10-26 00:00:00', 1455, 0),
(3661, 3, 10, '2002-12-16 00:00:00', 2445, 0),
(3662, 3, 10, '2002-07-04 00:00:00', 2305, 0),
(3663, 3, 10, '2002-11-30 00:00:00', 2111, 0),
(3664, 3, 10, '2002-05-17 00:00:00', 1841, 0),
(3666, 3, 14, '2002-07-29 00:00:00', 1806, 5418),
(3667, 3, 14, '2002-11-26 00:00:00', 1532, 4596),
(3668, 3, 14, '2002-05-30 00:00:00', 295, 885),
(3669, 3, 4, '2002-05-06 00:00:00', 2478, 0),
(3670, 3, 4, '2002-06-08 00:00:00', 2212, 0),
(3671, 3, 4, '2002-01-18 00:00:00', 1559, 0),
(3672, 3, 4, '2002-08-09 00:00:00', 1361, 0),
(3673, 3, 4, '2002-11-18 00:00:00', 1204, 0),
(3675, 3, 5, '2002-02-02 00:00:00', 1535, 3070),
(3676, 3, 5, '2002-07-24 00:00:00', 1185, 2370),
(3677, 3, 5, '2002-08-17 00:00:00', 284, 568),
(3678, 9, 11, '2002-10-04 00:00:00', 2162, 2162),
(3679, 9, 11, '2002-11-25 00:00:00', 835, 835),
(3680, 9, 11, '2002-09-15 00:00:00', 833, 833),
(3681, 9, 11, '2002-12-03 00:00:00', 735, 735),
(3682, 9, 12, '2002-03-28 00:00:00', 2400, 4800),
(3683, 9, 12, '2002-12-12 00:00:00', 1750, 3500),
(3684, 9, 12, '2002-06-22 00:00:00', 1465, 2930),
(3685, 9, 12, '2002-03-10 00:00:00', 1242, 2484),
(3686, 9, 12, '2002-04-23 00:00:00', 1151, 2302),
(3687, 9, 12, '2002-08-06 00:00:00', 896, 1792),
(3688, 9, 9, '2002-06-30 00:00:00', 1929, 1929),
(3689, 9, 9, '2002-10-24 00:00:00', 1757, 1757),
(3690, 9, 9, '2002-12-26 00:00:00', 1535, 1535),
(3691, 9, 9, '2002-12-19 00:00:00', 1265, 1265),
(3692, 9, 9, '2002-05-20 00:00:00', 591, 591),
(3693, 9, 7, '2002-05-29 00:00:00', 2193, 0),
(3694, 9, 3, '2002-10-16 00:00:00', 2420, 2420),
(3695, 9, 3, '2002-07-20 00:00:00', 1699, 1699),
(3696, 9, 1, '2002-11-05 00:00:00', 2482, 7446),
(3697, 9, 1, '2002-07-14 00:00:00', 1693, 5079),
(3698, 9, 1, '2002-09-07 00:00:00', 1367, 4101),
(3699, 9, 1, '2002-10-01 00:00:00', 1356, 4068),
(3700, 9, 1, '2002-04-11 00:00:00', 554, 1662),
(3701, 9, 8, '2002-01-04 00:00:00', 2064, 6192),
(3702, 9, 8, '2002-03-12 00:00:00', 1759, 5277),
(3703, 9, 8, '2002-05-31 00:00:00', 1355, 4065),
(3704, 9, 8, '2002-05-20 00:00:00', 1082, 3246),
(3705, 9, 6, '2002-10-22 00:00:00', 2374, 4748),
(3706, 9, 6, '2002-05-16 00:00:00', 1839, 3678),
(3707, 9, 6, '2002-12-05 00:00:00', 987, 1974),
(3708, 9, 6, '2002-01-03 00:00:00', 545, 1090),
(3709, 9, 13, '2002-11-16 00:00:00', 2477, 0),
(3710, 9, 13, '2002-12-19 00:00:00', 2180, 0),
(3711, 9, 13, '2002-12-24 00:00:00', 871, 0),
(3712, 9, 13, '2002-05-18 00:00:00', 605, 0),
(3713, 9, 13, '2002-10-24 00:00:00', 558, 0),
(3715, 9, 13, '2002-01-18 00:00:00', 256, 0),
(3719, 9, 10, '2002-01-25 00:00:00', 2278, 0),
(3720, 9, 10, '2002-02-11 00:00:00', 2136, 0),
(3721, 9, 10, '2002-04-25 00:00:00', 2111, 0),
(3722, 9, 10, '2002-01-12 00:00:00', 2025, 0),
(3723, 9, 10, '2002-11-13 00:00:00', 1236, 0),
(3724, 9, 10, '2002-01-11 00:00:00', 1093, 0),
(3725, 9, 10, '2002-09-02 00:00:00', 951, 0),
(3726, 9, 10, '2002-03-18 00:00:00', 743, 0),
(3727, 9, 10, '2002-04-09 00:00:00', 566, 0),
(3730, 9, 4, '2002-08-06 00:00:00', 2496, 0),
(3731, 9, 4, '2002-06-26 00:00:00', 2211, 0),
(3732, 9, 4, '2002-08-25 00:00:00', 1671, 0),
(3733, 9, 4, '2002-07-21 00:00:00', 1415, 0),
(3734, 9, 4, '2002-04-21 00:00:00', 668, 0),
(3735, 9, 4, '2002-06-04 00:00:00', 630, 0),
(3736, 9, 5, '2002-05-29 00:00:00', 2422, 4844),
(3737, 9, 5, '2002-08-18 00:00:00', 1866, 3732),
(3738, 9, 5, '2002-10-05 00:00:00', 920, 1840),
(3739, 9, 5, '2002-11-10 00:00:00', 546, 1092),
(3740, 9, 5, '2002-03-27 00:00:00', 536, 1072),
(3741, 4, 11, '2002-11-25 00:00:00', 2484, 2484),
(3742, 4, 11, '2002-09-15 00:00:00', 2447, 2447),
(3743, 4, 11, '2002-12-22 00:00:00', 1978, 1978),
(3744, 4, 11, '2002-07-28 00:00:00', 1460, 1460),
(3745, 4, 12, '2002-05-05 00:00:00', 2196, 4392),
(3746, 4, 12, '2002-08-05 00:00:00', 1079, 2158),
(3747, 4, 12, '2002-07-12 00:00:00', 884, 1768),
(3748, 4, 9, '2002-03-15 00:00:00', 2033, 2033),
(3749, 4, 9, '2002-07-29 00:00:00', 1709, 1709),
(3750, 4, 9, '2002-07-01 00:00:00', 1702, 1702),
(3751, 4, 9, '2002-12-17 00:00:00', 1485, 1485),
(3752, 4, 9, '2002-10-27 00:00:00', 943, 943),
(3753, 4, 9, '2002-12-23 00:00:00', 783, 783),
(3754, 4, 7, '2002-09-29 00:00:00', 2472, 0),
(3755, 4, 7, '2002-02-08 00:00:00', 692, 0),
(3756, 4, 3, '2002-01-15 00:00:00', 2425, 2425),
(3757, 4, 3, '2002-06-05 00:00:00', 1713, 1713),
(3758, 4, 3, '2002-01-22 00:00:00', 1278, 1278),
(3759, 4, 3, '2002-12-04 00:00:00', 815, 815),
(3761, 4, 1, '2002-11-05 00:00:00', 2423, 7269),
(3762, 4, 1, '2002-07-31 00:00:00', 1930, 5790),
(3763, 4, 1, '2002-01-11 00:00:00', 1829, 5487),
(3764, 4, 1, '2002-12-03 00:00:00', 1311, 3933),
(3765, 4, 1, '2002-05-19 00:00:00', 731, 2193),
(3766, 4, 8, '2002-08-20 00:00:00', 1704, 5112),
(3767, 4, 8, '2002-05-19 00:00:00', 1407, 4221),
(3768, 4, 8, '2002-08-24 00:00:00', 1285, 3855),
(3769, 4, 8, '2002-03-07 00:00:00', 1035, 3105),
(3770, 4, 8, '2002-05-10 00:00:00', 698, 2094),
(3772, 4, 6, '2002-04-29 00:00:00', 2472, 4944),
(3773, 4, 6, '2002-07-31 00:00:00', 2396, 4792),
(3774, 4, 6, '2002-07-18 00:00:00', 1926, 3852),
(3775, 4, 6, '2002-10-14 00:00:00', 1356, 2712),
(3776, 4, 6, '2002-02-13 00:00:00', 883, 1766),
(3777, 4, 6, '2002-08-25 00:00:00', 615, 1230),
(3778, 4, 13, '2002-10-01 00:00:00', 2354, 0),
(3779, 4, 13, '2002-08-16 00:00:00', 1945, 0),
(3780, 4, 13, '2002-12-24 00:00:00', 1515, 0),
(3781, 4, 13, '2002-08-08 00:00:00', 1333, 0),
(3782, 4, 13, '2002-07-31 00:00:00', 579, 0),
(3787, 4, 10, '2002-03-10 00:00:00', 2021, 0),
(3788, 4, 10, '2002-10-08 00:00:00', 2011, 0),
(3789, 4, 10, '2002-02-01 00:00:00', 1198, 0),
(3790, 4, 10, '2002-10-23 00:00:00', 1112, 0),
(3791, 4, 10, '2002-10-25 00:00:00', 788, 0),
(3794, 4, 14, '2002-04-04 00:00:00', 1486, 4458),
(3795, 4, 14, '2002-10-18 00:00:00', 524, 1572),
(3797, 4, 4, '2002-09-11 00:00:00', 962, 0),
(3798, 4, 4, '2002-08-24 00:00:00', 900, 0),
(3799, 4, 5, '2002-11-07 00:00:00', 1644, 3288),
(3800, 4, 5, '2002-03-15 00:00:00', 1117, 2234),
(3801, 4, 5, '2002-12-26 00:00:00', 1022, 2044),
(3802, 4, 5, '2002-03-15 00:00:00', 694, 1388),
(3803, 2, 11, '2002-12-31 00:00:00', 2108, 2108);
INSERT INTO `ventas` (`idventas`, `Cod Vendedor`, `Cod Producto`, `Fecha`, `Kilos`, `total`) VALUES
(3804, 2, 11, '2002-04-22 00:00:00', 1534, 1534),
(3805, 2, 11, '2002-04-09 00:00:00', 554, 554),
(3806, 2, 12, '2002-10-23 00:00:00', 2255, 4510),
(3807, 2, 12, '2002-01-05 00:00:00', 2142, 4284),
(3808, 2, 12, '2002-10-16 00:00:00', 1949, 3898),
(3809, 2, 12, '2002-05-30 00:00:00', 1022, 2044),
(3810, 2, 12, '2002-06-04 00:00:00', 940, 1880),
(3811, 2, 12, '2002-10-11 00:00:00', 572, 1144),
(3812, 2, 12, '2002-10-25 00:00:00', 507, 1014),
(3813, 2, 9, '2002-07-30 00:00:00', 2059, 2059),
(3814, 2, 9, '2002-02-27 00:00:00', 1768, 1768),
(3815, 2, 9, '2002-04-13 00:00:00', 595, 595),
(3816, 2, 7, '2002-11-06 00:00:00', 2489, 0),
(3817, 2, 7, '2002-04-08 00:00:00', 2470, 0),
(3818, 2, 7, '2002-09-03 00:00:00', 2195, 0),
(3819, 2, 7, '2002-10-20 00:00:00', 2014, 0),
(3820, 2, 7, '2002-02-21 00:00:00', 1639, 0),
(3821, 2, 7, '2002-12-12 00:00:00', 1621, 0),
(3822, 2, 7, '2002-04-21 00:00:00', 1599, 0),
(3823, 2, 7, '2002-06-12 00:00:00', 1017, 0),
(3824, 2, 3, '2002-10-10 00:00:00', 2091, 2091),
(3825, 2, 3, '2002-09-08 00:00:00', 2052, 2052),
(3826, 2, 3, '2002-01-10 00:00:00', 1948, 1948),
(3827, 2, 3, '2002-06-30 00:00:00', 1742, 1742),
(3828, 2, 3, '2002-07-14 00:00:00', 1733, 1733),
(3829, 2, 3, '2002-06-21 00:00:00', 1335, 1335),
(3830, 2, 3, '2002-08-22 00:00:00', 1329, 1329),
(3831, 2, 3, '2002-12-21 00:00:00', 1135, 1135),
(3832, 2, 3, '2002-08-11 00:00:00', 971, 971),
(3834, 2, 1, '2002-01-25 00:00:00', 2198, 6594),
(3835, 2, 1, '2002-07-03 00:00:00', 1957, 5871),
(3836, 2, 1, '2002-12-10 00:00:00', 971, 2913),
(3838, 2, 8, '2002-05-18 00:00:00', 2432, 7296),
(3839, 2, 8, '2002-05-19 00:00:00', 2356, 7068),
(3840, 2, 8, '2002-06-11 00:00:00', 541, 1623),
(3841, 2, 6, '2002-05-18 00:00:00', 2412, 4824),
(3842, 2, 6, '2002-09-24 00:00:00', 2178, 4356),
(3843, 2, 6, '2002-08-25 00:00:00', 2091, 4182),
(3844, 2, 13, '2002-04-18 00:00:00', 2441, 0),
(3845, 2, 13, '2002-08-18 00:00:00', 2009, 0),
(3846, 2, 13, '2002-05-05 00:00:00', 1942, 0),
(3847, 2, 13, '2002-05-07 00:00:00', 1926, 0),
(3848, 2, 13, '2002-09-20 00:00:00', 1690, 0),
(3849, 2, 13, '2002-11-08 00:00:00', 750, 0),
(3856, 2, 10, '2002-08-17 00:00:00', 1335, 0),
(3857, 2, 10, '2002-02-22 00:00:00', 1134, 0),
(3859, 2, 14, '2002-05-21 00:00:00', 2402, 7206),
(3860, 2, 14, '2002-07-31 00:00:00', 2275, 6825),
(3861, 2, 14, '2002-02-13 00:00:00', 1732, 5196),
(3862, 2, 14, '2002-07-18 00:00:00', 258, 774),
(3863, 2, 4, '2002-01-08 00:00:00', 2274, 0),
(3864, 2, 4, '2002-10-29 00:00:00', 2160, 0),
(3865, 2, 4, '2002-08-11 00:00:00', 1177, 0),
(3867, 2, 4, '2002-12-23 00:00:00', 273, 0),
(3868, 2, 5, '2002-07-25 00:00:00', 1797, 3594),
(3869, 2, 5, '2002-03-13 00:00:00', 1394, 2788),
(3870, 8, 11, '2002-05-16 00:00:00', 1442, 1442),
(3871, 8, 11, '2002-10-09 00:00:00', 1305, 1305),
(3872, 8, 11, '2002-11-25 00:00:00', 1247, 1247),
(3873, 8, 11, '2002-07-14 00:00:00', 721, 721),
(3875, 8, 12, '2002-07-12 00:00:00', 2018, 4036),
(3876, 8, 12, '2002-11-12 00:00:00', 2006, 4012),
(3877, 8, 12, '2002-12-14 00:00:00', 840, 1680),
(3878, 8, 12, '2002-04-30 00:00:00', 555, 1110),
(3880, 8, 9, '2002-04-27 00:00:00', 2070, 2070),
(3881, 8, 9, '2002-05-11 00:00:00', 1960, 1960),
(3882, 8, 9, '2002-05-16 00:00:00', 1492, 1492),
(3883, 8, 9, '2002-03-29 00:00:00', 1325, 1325),
(3884, 8, 9, '2002-03-25 00:00:00', 1321, 1321),
(3885, 8, 9, '2002-01-09 00:00:00', 914, 914),
(3886, 8, 7, '2002-03-29 00:00:00', 1501, 0),
(3887, 8, 7, '2002-04-19 00:00:00', 997, 0),
(3889, 8, 3, '2002-09-23 00:00:00', 748, 748),
(3890, 8, 1, '2002-06-24 00:00:00', 2287, 6861),
(3891, 8, 1, '2002-09-20 00:00:00', 1784, 5352),
(3892, 8, 1, '2002-07-23 00:00:00', 1446, 4338),
(3893, 8, 1, '2002-06-11 00:00:00', 1309, 3927),
(3894, 8, 1, '2002-05-02 00:00:00', 1028, 3084),
(3895, 8, 8, '2002-09-29 00:00:00', 1781, 5343),
(3896, 8, 6, '2002-03-09 00:00:00', 1981, 3962),
(3897, 8, 6, '2002-12-22 00:00:00', 1618, 3236),
(3898, 8, 13, '2002-04-06 00:00:00', 2314, 0),
(3899, 8, 13, '2002-12-13 00:00:00', 2243, 0),
(3900, 8, 13, '2002-01-19 00:00:00', 1496, 0),
(3901, 8, 13, '2002-05-10 00:00:00', 1231, 0),
(3902, 8, 13, '2002-09-13 00:00:00', 794, 0),
(3903, 8, 13, '2002-02-02 00:00:00', 295, 0),
(3908, 8, 10, '2002-07-17 00:00:00', 1728, 0),
(3909, 8, 10, '2002-03-27 00:00:00', 1715, 0),
(3910, 8, 10, '2002-08-30 00:00:00', 1229, 0),
(3911, 8, 14, '2002-02-15 00:00:00', 670, 2010),
(3913, 8, 4, '2002-07-20 00:00:00', 2168, 0),
(3914, 8, 4, '2002-03-30 00:00:00', 657, 0),
(3916, 8, 5, '2002-07-01 00:00:00', 2090, 4180),
(3917, 8, 5, '2002-07-15 00:00:00', 1788, 3576),
(3918, 8, 5, '2002-09-11 00:00:00', 1584, 3168),
(3919, 8, 5, '2002-04-08 00:00:00', 1341, 2682),
(3920, 8, 5, '2002-09-17 00:00:00', 681, 1362),
(3921, 10, 11, '2002-06-13 00:00:00', 2453, 2453),
(3922, 10, 11, '2002-09-29 00:00:00', 2151, 2151),
(3923, 10, 11, '2002-06-02 00:00:00', 2042, 2042),
(3924, 10, 11, '2002-07-09 00:00:00', 1799, 1799),
(3925, 10, 11, '2002-08-17 00:00:00', 1505, 1505),
(3926, 10, 11, '2002-05-02 00:00:00', 1481, 1481),
(3927, 10, 12, '2002-02-21 00:00:00', 1732, 3464),
(3928, 10, 12, '2002-12-23 00:00:00', 1543, 3086),
(3929, 10, 12, '2002-05-23 00:00:00', 1228, 2456),
(3930, 10, 12, '2002-07-05 00:00:00', 892, 1784),
(3931, 10, 12, '2002-04-24 00:00:00', 252, 504),
(3932, 10, 9, '2002-09-20 00:00:00', 2215, 2215),
(3933, 10, 9, '2002-11-16 00:00:00', 1653, 1653),
(3934, 10, 9, '2002-01-05 00:00:00', 504, 504),
(3935, 10, 7, '2002-01-06 00:00:00', 2349, 0),
(3936, 10, 7, '2002-03-02 00:00:00', 2176, 0),
(3937, 10, 7, '2002-03-30 00:00:00', 1463, 0),
(3938, 10, 7, '2002-04-03 00:00:00', 1410, 0),
(3939, 10, 7, '2002-03-05 00:00:00', 624, 0),
(3940, 10, 3, '2002-12-28 00:00:00', 2303, 2303),
(3941, 10, 3, '2002-01-05 00:00:00', 1751, 1751),
(3942, 10, 3, '2002-07-28 00:00:00', 1580, 1580),
(3943, 10, 3, '2002-10-12 00:00:00', 1537, 1537),
(3944, 10, 3, '2002-07-03 00:00:00', 863, 863),
(3946, 10, 1, '2002-01-30 00:00:00', 1490, 4470),
(3947, 10, 1, '2002-09-27 00:00:00', 858, 2574),
(3948, 10, 1, '2002-08-02 00:00:00', 592, 1776),
(3950, 10, 8, '2002-03-14 00:00:00', 1855, 5565),
(3951, 10, 8, '2002-05-24 00:00:00', 925, 2775),
(3952, 10, 8, '2002-06-11 00:00:00', 903, 2709),
(3953, 10, 6, '2002-09-17 00:00:00', 2447, 4894),
(3954, 10, 6, '2002-09-07 00:00:00', 2030, 4060),
(3955, 10, 6, '2002-10-17 00:00:00', 1916, 3832),
(3956, 10, 6, '2002-08-21 00:00:00', 1234, 2468),
(3957, 10, 6, '2002-12-03 00:00:00', 733, 1466),
(3959, 10, 13, '2002-08-29 00:00:00', 651, 0),
(3960, 10, 13, '2002-08-14 00:00:00', 588, 0),
(3964, 10, 10, '2002-11-27 00:00:00', 2483, 0),
(3965, 10, 10, '2002-04-07 00:00:00', 2336, 0),
(3966, 10, 10, '2002-07-16 00:00:00', 1862, 0),
(3967, 10, 10, '2002-02-25 00:00:00', 1221, 0),
(3968, 10, 10, '2002-04-09 00:00:00', 1110, 0),
(3969, 10, 10, '2002-03-05 00:00:00', 997, 0),
(3970, 10, 10, '2002-12-20 00:00:00', 688, 0),
(3971, 10, 10, '2002-01-29 00:00:00', 537, 0),
(3972, 10, 14, '2002-04-12 00:00:00', 2306, 6918),
(3973, 10, 14, '2002-12-16 00:00:00', 944, 2832),
(3974, 10, 14, '2002-02-03 00:00:00', 701, 2103),
(3975, 10, 4, '2002-06-18 00:00:00', 783, 0),
(3976, 10, 4, '2002-07-21 00:00:00', 755, 0),
(3978, 10, 5, '2002-04-07 00:00:00', 2216, 4432),
(3979, 10, 5, '2002-12-17 00:00:00', 1996, 3992),
(3980, 10, 5, '2002-09-20 00:00:00', 1993, 3986),
(3981, 5, 11, '2002-12-27 00:00:00', 1749, 1749),
(3982, 5, 11, '2002-07-10 00:00:00', 1279, 1279),
(3983, 5, 12, '2002-09-30 00:00:00', 2002, 4004),
(3984, 5, 12, '2002-05-01 00:00:00', 1661, 3322),
(3985, 5, 9, '2002-06-13 00:00:00', 801, 801),
(3986, 5, 9, '2002-04-29 00:00:00', 782, 782),
(3987, 5, 9, '2002-08-31 00:00:00', 265, 265),
(3988, 5, 7, '2002-03-28 00:00:00', 2461, 0),
(3989, 5, 7, '2002-09-21 00:00:00', 2295, 0),
(3990, 5, 7, '2002-09-14 00:00:00', 1999, 0),
(3991, 5, 7, '2002-10-26 00:00:00', 1590, 0),
(3992, 5, 7, '2002-01-13 00:00:00', 934, 0),
(3993, 5, 7, '2002-06-23 00:00:00', 865, 0),
(3994, 5, 3, '2002-10-09 00:00:00', 2354, 2354),
(3995, 5, 3, '2002-05-24 00:00:00', 2008, 2008),
(3996, 5, 3, '2002-01-02 00:00:00', 1413, 1413),
(3997, 5, 1, '2002-02-04 00:00:00', 2447, 7341),
(3998, 5, 1, '2002-12-10 00:00:00', 1755, 5265),
(3999, 5, 1, '2002-12-14 00:00:00', 1471, 4413),
(4001, 5, 8, '2002-06-18 00:00:00', 2499, 7497),
(4002, 5, 8, '2002-04-22 00:00:00', 1940, 5820),
(4003, 5, 8, '2002-06-06 00:00:00', 1647, 4941),
(4004, 5, 8, '2002-03-10 00:00:00', 1111, 3333),
(4005, 5, 8, '2002-07-05 00:00:00', 810, 2430),
(4006, 5, 8, '2002-04-19 00:00:00', 720, 2160),
(4007, 5, 6, '2002-11-02 00:00:00', 2478, 4956),
(4008, 5, 6, '2002-10-01 00:00:00', 2198, 4396),
(4009, 5, 6, '2002-12-27 00:00:00', 1791, 3582),
(4010, 5, 6, '2002-07-12 00:00:00', 1690, 3380),
(4011, 5, 6, '2002-05-15 00:00:00', 1567, 3134),
(4012, 5, 6, '2002-05-01 00:00:00', 1200, 2400),
(4013, 5, 6, '2002-10-14 00:00:00', 1163, 2326),
(4014, 5, 6, '2002-08-27 00:00:00', 929, 1858),
(4015, 5, 13, '2002-08-22 00:00:00', 1779, 0),
(4016, 5, 13, '2002-09-29 00:00:00', 1536, 0),
(4017, 5, 13, '2002-05-02 00:00:00', 1527, 0),
(4018, 5, 13, '2002-05-14 00:00:00', 1384, 0),
(4019, 5, 13, '2002-11-07 00:00:00', 1217, 0),
(4020, 5, 13, '2002-12-07 00:00:00', 254, 0),
(4027, 5, 10, '2002-05-12 00:00:00', 1486, 0),
(4028, 5, 10, '2002-01-27 00:00:00', 533, 0),
(4029, 5, 14, '2002-08-15 00:00:00', 2242, 6726),
(4030, 5, 14, '2002-09-25 00:00:00', 2236, 6708),
(4031, 5, 14, '2002-11-02 00:00:00', 1529, 4587),
(4032, 5, 14, '2002-08-28 00:00:00', 611, 1833),
(4033, 5, 14, '2002-03-22 00:00:00', 521, 1563),
(4035, 5, 4, '2002-03-29 00:00:00', 1588, 0),
(4036, 5, 4, '2002-06-26 00:00:00', 1157, 0),
(4037, 5, 4, '2002-12-09 00:00:00', 964, 0),
(4038, 5, 4, '2002-03-24 00:00:00', 865, 0),
(4040, 5, 5, '2002-02-17 00:00:00', 699, 1398),
(4041, 12, 11, '2002-04-19 00:00:00', 1987, 1987),
(4042, 12, 11, '2002-03-16 00:00:00', 1869, 1869),
(4043, 12, 11, '2002-03-05 00:00:00', 1415, 1415),
(4044, 12, 11, '2002-06-08 00:00:00', 1259, 1259),
(4045, 12, 11, '2002-09-04 00:00:00', 843, 843),
(4046, 12, 11, '2002-04-20 00:00:00', 675, 675),
(4047, 12, 12, '2002-11-04 00:00:00', 2060, 4120),
(4048, 12, 12, '2002-03-19 00:00:00', 1688, 3376),
(4049, 12, 12, '2002-12-20 00:00:00', 1401, 2802),
(4050, 12, 12, '2002-12-04 00:00:00', 1357, 2714),
(4051, 12, 12, '2002-07-02 00:00:00', 1253, 2506),
(4052, 12, 12, '2002-03-31 00:00:00', 1101, 2202),
(4053, 12, 12, '2002-03-05 00:00:00', 1050, 2100),
(4054, 12, 9, '2002-07-22 00:00:00', 1754, 1754),
(4055, 12, 9, '2002-03-04 00:00:00', 948, 948),
(4056, 12, 7, '2002-06-15 00:00:00', 1615, 0),
(4057, 12, 7, '2002-11-20 00:00:00', 1246, 0),
(4058, 12, 7, '2002-04-29 00:00:00', 748, 0),
(4059, 12, 7, '2002-03-07 00:00:00', 725, 0),
(4060, 12, 7, '2002-05-02 00:00:00', 541, 0),
(4062, 12, 3, '2002-02-14 00:00:00', 1863, 1863),
(4063, 12, 3, '2002-09-07 00:00:00', 1824, 1824),
(4064, 12, 3, '2002-05-12 00:00:00', 1227, 1227),
(4065, 12, 3, '2002-11-29 00:00:00', 742, 742),
(4066, 12, 1, '2002-05-21 00:00:00', 1937, 5811),
(4067, 12, 1, '2002-08-17 00:00:00', 1379, 4137),
(4070, 12, 8, '2002-06-22 00:00:00', 2245, 6735),
(4071, 12, 8, '2002-08-05 00:00:00', 2074, 6222),
(4072, 12, 8, '2002-08-11 00:00:00', 1474, 4422),
(4073, 12, 8, '2002-12-17 00:00:00', 1473, 4419),
(4074, 12, 8, '2002-05-07 00:00:00', 1152, 3456),
(4075, 12, 8, '2002-09-01 00:00:00', 963, 2889),
(4076, 12, 8, '2002-06-05 00:00:00', 849, 2547),
(4077, 12, 8, '2002-10-19 00:00:00', 794, 2382),
(4078, 12, 6, '2002-09-12 00:00:00', 1775, 3550),
(4079, 12, 6, '2002-05-13 00:00:00', 1648, 3296),
(4080, 12, 6, '2002-10-14 00:00:00', 1332, 2664),
(4081, 12, 6, '2002-09-19 00:00:00', 822, 1644),
(4082, 12, 6, '2002-08-13 00:00:00', 752, 1504),
(4083, 12, 6, '2002-04-18 00:00:00', 257, 514),
(4084, 12, 13, '2002-01-13 00:00:00', 2221, 0),
(4085, 12, 13, '2002-08-12 00:00:00', 1764, 0),
(4086, 12, 13, '2002-03-21 00:00:00', 1453, 0),
(4087, 12, 13, '2002-04-24 00:00:00', 671, 0),
(4090, 12, 10, '2002-06-30 00:00:00', 2279, 0),
(4091, 12, 10, '2002-02-05 00:00:00', 2142, 0),
(4092, 12, 10, '2002-09-27 00:00:00', 1234, 0),
(4094, 12, 14, '2002-12-01 00:00:00', 1855, 5565),
(4095, 12, 14, '2002-04-17 00:00:00', 1410, 4230),
(4096, 12, 14, '2002-12-28 00:00:00', 931, 2793),
(4097, 12, 4, '2002-08-27 00:00:00', 1948, 0),
(4098, 12, 4, '2002-09-09 00:00:00', 1798, 0),
(4099, 12, 5, '2002-02-24 00:00:00', 2477, 4954),
(4100, 12, 5, '2002-04-11 00:00:00', 2233, 4466),
(4101, 12, 5, '2002-03-15 00:00:00', 2168, 4336),
(4102, 12, 5, '2002-06-21 00:00:00', 2156, 4312),
(4103, 12, 5, '2002-05-11 00:00:00', 1949, 3898),
(4104, 12, 5, '2002-06-06 00:00:00', 1729, 3458),
(4105, 12, 5, '2002-10-28 00:00:00', 1199, 2398),
(4106, 13, 11, '2002-07-15 00:00:00', 884, 884),
(4107, 13, 12, '2002-02-14 00:00:00', 2193, 4386),
(4108, 13, 12, '2002-02-17 00:00:00', 618, 1236),
(4109, 13, 12, '2002-07-25 00:00:00', 576, 1152),
(4110, 13, 12, '2002-04-18 00:00:00', 521, 1042),
(4111, 13, 9, '2002-09-14 00:00:00', 2242, 2242),
(4112, 13, 9, '2002-02-04 00:00:00', 1791, 1791),
(4113, 13, 9, '2002-11-27 00:00:00', 1294, 1294),
(4114, 13, 9, '2002-02-23 00:00:00', 1170, 1170),
(4115, 13, 9, '2002-11-16 00:00:00', 1035, 1035),
(4116, 13, 9, '2002-04-12 00:00:00', 786, 786),
(4117, 13, 7, '2002-06-29 00:00:00', 1700, 0),
(4118, 13, 7, '2002-09-14 00:00:00', 1574, 0),
(4119, 13, 7, '2002-06-13 00:00:00', 644, 0),
(4120, 13, 7, '2002-03-05 00:00:00', 541, 0),
(4121, 13, 3, '2002-06-22 00:00:00', 2026, 2026),
(4122, 13, 3, '2002-09-27 00:00:00', 2024, 2024),
(4123, 13, 3, '2002-03-27 00:00:00', 576, 576),
(4125, 13, 8, '2002-08-17 00:00:00', 2472, 7416),
(4126, 13, 8, '2002-12-28 00:00:00', 2442, 7326),
(4127, 13, 8, '2002-10-03 00:00:00', 2341, 7023),
(4128, 13, 8, '2002-04-04 00:00:00', 1722, 5166),
(4129, 13, 8, '2002-04-10 00:00:00', 501, 1503),
(4130, 13, 6, '2002-10-29 00:00:00', 1804, 3608),
(4131, 13, 6, '2002-01-11 00:00:00', 833, 1666),
(4132, 13, 13, '2002-11-30 00:00:00', 1639, 0),
(4133, 13, 13, '2002-03-24 00:00:00', 1542, 0),
(4134, 13, 13, '2002-02-24 00:00:00', 934, 0),
(4139, 13, 10, '2002-05-15 00:00:00', 1846, 0),
(4140, 13, 10, '2002-05-18 00:00:00', 1550, 0),
(4141, 13, 10, '2002-02-28 00:00:00', 1061, 0),
(4142, 13, 10, '2002-02-21 00:00:00', 694, 0),
(4144, 13, 14, '2002-08-20 00:00:00', 2308, 6924),
(4145, 13, 14, '2002-11-16 00:00:00', 2186, 6558),
(4146, 13, 14, '2002-09-01 00:00:00', 2145, 6435),
(4147, 13, 14, '2002-07-11 00:00:00', 2036, 6108),
(4148, 13, 14, '2002-12-02 00:00:00', 1986, 5958),
(4149, 13, 14, '2002-09-03 00:00:00', 1477, 4431),
(4150, 13, 14, '2002-08-02 00:00:00', 1094, 3282),
(4151, 13, 14, '2002-12-05 00:00:00', 708, 2124),
(4152, 13, 14, '2002-10-22 00:00:00', 529, 1587),
(4153, 13, 4, '2002-11-29 00:00:00', 1237, 0),
(4154, 13, 4, '2002-06-02 00:00:00', 863, 0),
(4155, 13, 4, '2002-05-07 00:00:00', 611, 0),
(4156, 13, 4, '2002-07-29 00:00:00', 562, 0),
(4157, 13, 4, '2002-01-05 00:00:00', 552, 0),
(4160, 13, 5, '2002-01-20 00:00:00', 1628, 3256),
(4161, 13, 5, '2002-08-14 00:00:00', 1383, 2766),
(4162, 13, 5, '2002-09-22 00:00:00', 947, 1894),
(4163, 13, 5, '2002-01-21 00:00:00', 907, 1814),
(4165, 11, 11, '2003-01-26 00:00:00', 1270, 1270),
(4167, 11, 12, '2003-05-26 00:00:00', 1806, 3612),
(4168, 11, 12, '2003-06-02 00:00:00', 678, 1356),
(4169, 11, 12, '2003-08-30 00:00:00', 545, 1090),
(4171, 11, 9, '2003-10-06 00:00:00', 2157, 2157),
(4172, 11, 9, '2003-10-11 00:00:00', 1733, 1733),
(4173, 11, 9, '2003-12-13 00:00:00', 1588, 1588),
(4174, 11, 9, '2003-03-18 00:00:00', 807, 807),
(4175, 11, 7, '2003-06-28 00:00:00', 1728, 0),
(4176, 11, 7, '2003-01-02 00:00:00', 989, 0),
(4177, 11, 3, '2003-07-30 00:00:00', 2105, 2105),
(4178, 11, 3, '2003-09-01 00:00:00', 1417, 1417),
(4179, 11, 3, '2003-07-21 00:00:00', 1066, 1066),
(4180, 11, 3, '2003-09-09 00:00:00', 960, 960),
(4181, 11, 1, '2003-01-03 00:00:00', 2374, 7122),
(4182, 11, 1, '2003-10-11 00:00:00', 2320, 6960),
(4183, 11, 1, '2003-04-19 00:00:00', 1897, 5691),
(4184, 11, 1, '2003-12-16 00:00:00', 1841, 5523),
(4185, 11, 1, '2003-01-21 00:00:00', 1309, 3927),
(4186, 11, 1, '2003-09-28 00:00:00', 1284, 3852),
(4187, 11, 1, '2003-09-03 00:00:00', 848, 2544),
(4188, 11, 1, '2003-03-19 00:00:00', 649, 1947),
(4189, 11, 8, '2003-05-06 00:00:00', 2444, 7332),
(4190, 11, 8, '2003-09-17 00:00:00', 2366, 7098),
(4191, 11, 8, '2003-01-26 00:00:00', 1970, 5910),
(4192, 11, 8, '2003-06-08 00:00:00', 1575, 4725),
(4194, 11, 6, '2003-06-22 00:00:00', 2036, 4072),
(4195, 11, 6, '2003-07-28 00:00:00', 1421, 2842),
(4196, 11, 6, '2003-02-07 00:00:00', 1315, 2630),
(4197, 11, 6, '2003-06-26 00:00:00', 563, 1126),
(4198, 11, 13, '2003-06-20 00:00:00', 1482, 0),
(4199, 11, 13, '2003-11-16 00:00:00', 1435, 0),
(4200, 11, 13, '2003-05-13 00:00:00', 1294, 0),
(4201, 11, 13, '2003-10-28 00:00:00', 698, 0),
(4203, 11, 13, '2003-06-30 00:00:00', 262, 0),
(4205, 11, 10, '2003-11-07 00:00:00', 2309, 0),
(4206, 11, 10, '2003-10-20 00:00:00', 869, 0),
(4208, 11, 14, '2003-05-09 00:00:00', 1903, 5709),
(4209, 11, 14, '2003-10-22 00:00:00', 1790, 5370),
(4210, 11, 14, '2003-12-29 00:00:00', 715, 2145),
(4211, 11, 14, '2003-01-13 00:00:00', 582, 1746),
(4213, 11, 4, '2003-01-18 00:00:00', 2398, 0),
(4214, 11, 4, '2003-11-05 00:00:00', 1930, 0),
(4215, 11, 4, '2003-10-19 00:00:00', 732, 0),
(4216, 11, 5, '2003-11-18 00:00:00', 1990, 3980),
(4217, 11, 5, '2003-06-21 00:00:00', 1916, 3832),
(4218, 11, 5, '2003-12-13 00:00:00', 1389, 2778),
(4219, 11, 5, '2003-11-23 00:00:00', 1367, 2734),
(4221, 6, 11, '2003-03-27 00:00:00', 1925, 1925),
(4222, 6, 12, '2003-12-23 00:00:00', 1541, 3082),
(4223, 6, 12, '2003-02-13 00:00:00', 1037, 2074),
(4224, 6, 12, '2003-02-04 00:00:00', 863, 1726),
(4226, 6, 9, '2003-10-24 00:00:00', 2362, 2362),
(4227, 6, 9, '2003-01-10 00:00:00', 2356, 2356),
(4228, 6, 9, '2003-12-21 00:00:00', 1375, 1375),
(4229, 6, 9, '2003-01-25 00:00:00', 1345, 1345),
(4231, 6, 9, '2003-12-05 00:00:00', 268, 268),
(4232, 6, 7, '2003-05-09 00:00:00', 1871, 0),
(4233, 6, 7, '2003-09-24 00:00:00', 1567, 0),
(4234, 6, 7, '2003-03-19 00:00:00', 1542, 0),
(4235, 6, 7, '2003-02-12 00:00:00', 855, 0),
(4236, 6, 3, '2003-09-11 00:00:00', 2205, 2205),
(4237, 6, 3, '2003-06-23 00:00:00', 1990, 1990),
(4238, 6, 3, '2003-09-18 00:00:00', 1739, 1739),
(4239, 6, 3, '2003-07-31 00:00:00', 1575, 1575),
(4240, 6, 3, '2003-04-18 00:00:00', 954, 954),
(4241, 6, 3, '2003-08-27 00:00:00', 596, 596),
(4242, 6, 1, '2003-08-24 00:00:00', 2120, 6360),
(4243, 6, 1, '2003-08-13 00:00:00', 2086, 6258),
(4244, 6, 1, '2003-12-02 00:00:00', 1471, 4413),
(4245, 6, 1, '2003-01-21 00:00:00', 994, 2982),
(4246, 6, 8, '2003-04-19 00:00:00', 905, 2715),
(4247, 6, 8, '2003-12-15 00:00:00', 525, 1575),
(4248, 6, 6, '2003-05-31 00:00:00', 2149, 4298),
(4249, 6, 6, '2003-07-14 00:00:00', 1965, 3930),
(4250, 6, 6, '2003-01-13 00:00:00', 1689, 3378),
(4251, 6, 6, '2003-07-26 00:00:00', 1117, 2234),
(4253, 6, 13, '2003-02-25 00:00:00', 2202, 0),
(4254, 6, 13, '2003-12-04 00:00:00', 2092, 0),
(4255, 6, 13, '2003-05-09 00:00:00', 1288, 0),
(4256, 6, 13, '2003-09-30 00:00:00', 1124, 0),
(4257, 6, 13, '2003-11-10 00:00:00', 836, 0),
(4266, 6, 10, '2003-03-22 00:00:00', 2148, 0),
(4267, 6, 10, '2003-08-16 00:00:00', 1141, 0),
(4268, 6, 10, '2003-01-09 00:00:00', 1007, 0),
(4269, 6, 10, '2003-12-03 00:00:00', 867, 0),
(4270, 6, 10, '2003-08-07 00:00:00', 824, 0),
(4271, 6, 10, '2003-04-05 00:00:00', 745, 0),
(4273, 6, 14, '2003-04-01 00:00:00', 2320, 6960),
(4274, 6, 14, '2003-08-08 00:00:00', 1620, 4860),
(4275, 6, 14, '2003-03-25 00:00:00', 1102, 3306),
(4276, 6, 14, '2003-10-01 00:00:00', 827, 2481),
(4277, 6, 14, '2003-08-04 00:00:00', 725, 2175),
(4278, 6, 4, '2003-11-07 00:00:00', 2455, 0),
(4279, 6, 4, '2003-01-11 00:00:00', 2286, 0),
(4280, 6, 4, '2003-03-12 00:00:00', 2185, 0),
(4281, 6, 4, '2003-07-11 00:00:00', 2141, 0),
(4282, 6, 4, '2003-02-01 00:00:00', 1132, 0),
(4284, 6, 5, '2003-07-27 00:00:00', 2377, 4754),
(4285, 6, 5, '2003-07-09 00:00:00', 2195, 4390),
(4288, 1, 11, '2003-04-17 00:00:00', 2476, 2476),
(4289, 1, 11, '2003-01-07 00:00:00', 2461, 2461),
(4290, 1, 11, '2003-11-19 00:00:00', 2072, 2072),
(4291, 1, 11, '2003-07-01 00:00:00', 1391, 1391),
(4293, 1, 12, '2003-09-04 00:00:00', 2340, 4680),
(4294, 1, 12, '2003-03-10 00:00:00', 1716, 3432),
(4295, 1, 12, '2003-12-08 00:00:00', 1644, 3288),
(4296, 1, 12, '2003-06-22 00:00:00', 1599, 3198),
(4298, 1, 9, '2003-06-13 00:00:00', 2491, 2491),
(4299, 1, 9, '2003-01-18 00:00:00', 2328, 2328),
(4300, 1, 9, '2003-01-30 00:00:00', 1913, 1913),
(4301, 1, 9, '2003-11-16 00:00:00', 1866, 1866),
(4302, 1, 9, '2003-02-06 00:00:00', 1353, 1353),
(4303, 1, 9, '2003-12-19 00:00:00', 1133, 1133),
(4304, 1, 9, '2003-12-01 00:00:00', 1108, 1108),
(4305, 1, 9, '2003-01-14 00:00:00', 565, 565),
(4306, 1, 7, '2003-10-02 00:00:00', 1596, 0),
(4307, 1, 7, '2003-01-27 00:00:00', 1389, 0),
(4308, 1, 7, '2003-07-15 00:00:00', 1246, 0),
(4309, 1, 7, '2003-07-01 00:00:00', 853, 0),
(4310, 1, 3, '2003-03-24 00:00:00', 2447, 2447),
(4311, 1, 3, '2003-01-07 00:00:00', 2297, 2297),
(4312, 1, 3, '2003-03-26 00:00:00', 2092, 2092),
(4313, 1, 3, '2003-11-01 00:00:00', 1996, 1996),
(4314, 1, 3, '2003-12-27 00:00:00', 1867, 1867),
(4315, 1, 3, '2003-02-19 00:00:00', 1478, 1478),
(4316, 1, 1, '2003-09-20 00:00:00', 2374, 7122),
(4317, 1, 1, '2003-02-19 00:00:00', 1260, 3780),
(4318, 1, 8, '2003-09-24 00:00:00', 2445, 7335),
(4319, 1, 8, '2003-04-24 00:00:00', 956, 2868),
(4320, 1, 6, '2003-05-08 00:00:00', 2133, 4266),
(4321, 1, 6, '2003-10-25 00:00:00', 2132, 4264),
(4322, 1, 6, '2003-04-21 00:00:00', 1990, 3980),
(4323, 1, 6, '2003-05-17 00:00:00', 1710, 3420),
(4324, 1, 6, '2003-10-26 00:00:00', 1435, 2870),
(4325, 1, 6, '2003-04-19 00:00:00', 789, 1578),
(4326, 1, 6, '2003-04-11 00:00:00', 589, 1178),
(4328, 1, 13, '2003-05-17 00:00:00', 2475, 0),
(4329, 1, 13, '2003-12-23 00:00:00', 1694, 0),
(4330, 1, 13, '2003-10-05 00:00:00', 1514, 0),
(4331, 1, 13, '2003-12-20 00:00:00', 1068, 0),
(4332, 1, 13, '2003-04-11 00:00:00', 1049, 0),
(4340, 1, 10, '2003-09-04 00:00:00', 1948, 0),
(4341, 1, 10, '2003-03-29 00:00:00', 1824, 0),
(4342, 1, 10, '2003-12-29 00:00:00', 677, 0),
(4343, 1, 14, '2003-11-26 00:00:00', 1922, 5766),
(4344, 1, 14, '2003-09-11 00:00:00', 1781, 5343),
(4345, 1, 14, '2003-04-02 00:00:00', 1360, 4080),
(4346, 1, 14, '2003-09-07 00:00:00', 1163, 3489),
(4347, 1, 14, '2003-09-05 00:00:00', 931, 2793),
(4348, 1, 14, '2003-09-08 00:00:00', 768, 2304),
(4349, 1, 14, '2003-06-22 00:00:00', 591, 1773),
(4350, 1, 4, '2003-08-16 00:00:00', 1885, 0),
(4351, 1, 4, '2003-01-09 00:00:00', 1353, 0),
(4352, 1, 4, '2003-12-10 00:00:00', 870, 0),
(4353, 1, 5, '2003-03-21 00:00:00', 1493, 2986),
(4354, 1, 5, '2003-07-13 00:00:00', 1380, 2760),
(4355, 1, 5, '2003-03-31 00:00:00', 1181, 2362),
(4356, 1, 5, '2003-04-07 00:00:00', 987, 1974),
(4357, 1, 5, '2003-01-25 00:00:00', 732, 1464),
(4358, 1, 5, '2003-09-12 00:00:00', 516, 1032),
(4359, 7, 11, '2003-11-23 00:00:00', 2357, 2357),
(4360, 7, 11, '2003-11-02 00:00:00', 712, 712),
(4361, 7, 12, '2003-10-05 00:00:00', 659, 1318),
(4362, 7, 9, '2003-10-07 00:00:00', 2291, 2291),
(4363, 7, 9, '2003-11-03 00:00:00', 1987, 1987),
(4364, 7, 9, '2003-04-24 00:00:00', 715, 715),
(4366, 7, 7, '2003-07-26 00:00:00', 2392, 0),
(4367, 7, 7, '2003-12-15 00:00:00', 2150, 0),
(4368, 7, 7, '2003-04-28 00:00:00', 1012, 0),
(4369, 7, 7, '2003-11-10 00:00:00', 916, 0),
(4370, 7, 3, '2003-08-14 00:00:00', 2494, 2494),
(4371, 7, 3, '2003-05-30 00:00:00', 2002, 2002),
(4372, 7, 3, '2003-03-17 00:00:00', 1987, 1987),
(4373, 7, 3, '2003-05-14 00:00:00', 1307, 1307),
(4374, 7, 3, '2003-04-23 00:00:00', 1256, 1256),
(4375, 7, 3, '2003-09-19 00:00:00', 1127, 1127),
(4376, 7, 1, '2003-06-27 00:00:00', 1179, 3537),
(4377, 7, 1, '2003-10-12 00:00:00', 1059, 3177),
(4378, 7, 1, '2003-09-15 00:00:00', 744, 2232),
(4379, 7, 8, '2003-11-14 00:00:00', 2260, 6780),
(4380, 7, 8, '2003-01-28 00:00:00', 1923, 5769),
(4381, 7, 8, '2003-11-27 00:00:00', 1866, 5598),
(4382, 7, 8, '2003-03-11 00:00:00', 1746, 5238),
(4383, 7, 8, '2003-07-05 00:00:00', 1092, 3276),
(4384, 7, 8, '2003-08-26 00:00:00', 1035, 3105),
(4385, 7, 8, '2003-03-06 00:00:00', 509, 1527),
(4386, 7, 8, '2003-01-26 00:00:00', 298, 894),
(4387, 7, 6, '2003-03-28 00:00:00', 2041, 4082),
(4388, 7, 6, '2003-12-05 00:00:00', 1125, 2250),
(4389, 7, 6, '2003-07-19 00:00:00', 776, 1552),
(4392, 7, 13, '2003-01-09 00:00:00', 2474, 0),
(4393, 7, 13, '2003-05-05 00:00:00', 2427, 0),
(4394, 7, 13, '2003-05-29 00:00:00', 2331, 0),
(4395, 7, 13, '2003-08-01 00:00:00', 1994, 0),
(4396, 7, 13, '2003-03-20 00:00:00', 674, 0),
(4407, 7, 10, '2003-02-20 00:00:00', 2070, 0),
(4408, 7, 10, '2003-12-23 00:00:00', 1889, 0),
(4409, 7, 10, '2003-12-30 00:00:00', 1682, 0),
(4410, 7, 10, '2003-02-01 00:00:00', 880, 0),
(4411, 7, 10, '2003-07-18 00:00:00', 865, 0),
(4412, 7, 10, '2003-10-09 00:00:00', 843, 0),
(4413, 7, 14, '2003-03-03 00:00:00', 1912, 5736),
(4414, 7, 14, '2003-06-25 00:00:00', 686, 2058),
(4415, 7, 4, '2003-01-02 00:00:00', 2495, 0),
(4416, 7, 4, '2003-09-23 00:00:00', 1948, 0),
(4417, 7, 4, '2003-06-17 00:00:00', 1897, 0),
(4418, 7, 4, '2003-03-26 00:00:00', 1789, 0),
(4419, 7, 4, '2003-05-09 00:00:00', 1413, 0),
(4421, 7, 5, '2003-02-23 00:00:00', 985, 1970),
(4422, 7, 5, '2003-08-14 00:00:00', 711, 1422),
(4424, 3, 11, '2003-06-04 00:00:00', 1047, 1047),
(4425, 3, 11, '2003-11-17 00:00:00', 1038, 1038),
(4428, 3, 12, '2003-07-09 00:00:00', 2443, 4886),
(4429, 3, 12, '2003-05-23 00:00:00', 2310, 4620),
(4430, 3, 12, '2003-02-10 00:00:00', 1568, 3136),
(4431, 3, 12, '2003-10-23 00:00:00', 1441, 2882),
(4433, 3, 9, '2003-06-20 00:00:00', 2425, 2425),
(4434, 3, 9, '2003-11-04 00:00:00', 2296, 2296),
(4435, 3, 9, '2003-09-29 00:00:00', 2012, 2012),
(4436, 3, 9, '2003-09-19 00:00:00', 1932, 1932),
(4437, 3, 9, '2003-03-17 00:00:00', 1810, 1810),
(4438, 3, 9, '2003-11-06 00:00:00', 1736, 1736),
(4439, 3, 9, '2003-03-31 00:00:00', 1604, 1604),
(4440, 3, 7, '2003-09-12 00:00:00', 2424, 0),
(4441, 3, 7, '2003-01-21 00:00:00', 1454, 0),
(4443, 3, 3, '2003-10-08 00:00:00', 2247, 2247),
(4444, 3, 3, '2003-12-21 00:00:00', 2130, 2130),
(4445, 3, 3, '2003-01-03 00:00:00', 1020, 1020),
(4446, 3, 3, '2003-04-05 00:00:00', 815, 815),
(4447, 3, 3, '2003-01-06 00:00:00', 627, 627),
(4448, 3, 1, '2003-02-25 00:00:00', 2387, 7161),
(4449, 3, 1, '2003-07-06 00:00:00', 2167, 6501),
(4450, 3, 1, '2003-08-09 00:00:00', 557, 1671),
(4451, 3, 1, '2003-05-06 00:00:00', 276, 828),
(4452, 3, 8, '2003-08-29 00:00:00', 2081, 6243),
(4453, 3, 8, '2003-02-24 00:00:00', 1916, 5748),
(4454, 3, 8, '2003-12-26 00:00:00', 1785, 5355),
(4455, 3, 8, '2003-05-01 00:00:00', 1383, 4149),
(4456, 3, 8, '2003-08-17 00:00:00', 1326, 3978),
(4457, 3, 8, '2003-11-15 00:00:00', 1067, 3201),
(4458, 3, 8, '2003-02-03 00:00:00', 268, 804),
(4459, 3, 6, '2003-03-31 00:00:00', 2460, 4920),
(4460, 3, 6, '2003-11-26 00:00:00', 2400, 4800),
(4461, 3, 6, '2003-10-21 00:00:00', 1464, 2928),
(4462, 3, 6, '2003-10-08 00:00:00', 1016, 2032),
(4464, 3, 13, '2003-02-26 00:00:00', 2310, 0),
(4465, 3, 13, '2003-11-25 00:00:00', 1746, 0),
(4466, 3, 13, '2003-01-03 00:00:00', 978, 0),
(4467, 3, 13, '2003-01-02 00:00:00', 657, 0),
(4476, 3, 10, '2003-02-05 00:00:00', 1907, 0),
(4477, 3, 14, '2003-05-08 00:00:00', 2380, 7140),
(4478, 3, 14, '2003-05-01 00:00:00', 2368, 7104),
(4479, 3, 14, '2003-07-11 00:00:00', 2278, 6834),
(4480, 3, 14, '2003-03-30 00:00:00', 2118, 6354),
(4481, 3, 14, '2003-04-19 00:00:00', 1738, 5214),
(4482, 3, 14, '2003-04-11 00:00:00', 504, 1512),
(4483, 3, 4, '2003-01-10 00:00:00', 2328, 0),
(4484, 3, 4, '2003-03-13 00:00:00', 1941, 0),
(4485, 3, 4, '2003-08-05 00:00:00', 1664, 0),
(4486, 3, 5, '2003-05-08 00:00:00', 2306, 4612),
(4487, 3, 5, '2003-04-11 00:00:00', 2292, 4584),
(4488, 3, 5, '2003-11-24 00:00:00', 293, 586),
(4489, 9, 11, '2003-04-17 00:00:00', 2217, 2217),
(4490, 9, 11, '2003-05-21 00:00:00', 1654, 1654),
(4491, 9, 11, '2003-06-12 00:00:00', 1357, 1357),
(4492, 9, 12, '2003-05-28 00:00:00', 2344, 4688),
(4493, 9, 12, '2003-04-27 00:00:00', 1363, 2726),
(4494, 9, 12, '2003-01-15 00:00:00', 811, 1622),
(4495, 9, 9, '2003-01-04 00:00:00', 1790, 1790),
(4496, 9, 9, '2003-10-14 00:00:00', 1665, 1665),
(4497, 9, 9, '2003-08-02 00:00:00', 1309, 1309),
(4498, 9, 9, '2003-09-05 00:00:00', 1242, 1242),
(4500, 9, 7, '2003-01-14 00:00:00', 2157, 0),
(4501, 9, 7, '2003-09-14 00:00:00', 2089, 0),
(4502, 9, 7, '2003-02-05 00:00:00', 910, 0),
(4504, 9, 3, '2003-04-16 00:00:00', 2149, 2149),
(4505, 9, 3, '2003-09-23 00:00:00', 1491, 1491),
(4506, 9, 3, '2003-08-31 00:00:00', 1358, 1358),
(4507, 9, 3, '2003-11-10 00:00:00', 1207, 1207),
(4508, 9, 3, '2003-04-07 00:00:00', 728, 728),
(4509, 9, 3, '2003-08-12 00:00:00', 259, 259),
(4510, 9, 1, '2003-05-17 00:00:00', 1805, 5415),
(4511, 9, 1, '2003-01-12 00:00:00', 1251, 3753),
(4512, 9, 8, '2003-07-18 00:00:00', 2368, 7104),
(4513, 9, 8, '2003-12-16 00:00:00', 1624, 4872),
(4514, 9, 8, '2003-01-28 00:00:00', 1379, 4137),
(4515, 9, 8, '2003-05-10 00:00:00', 828, 2484),
(4517, 9, 6, '2003-03-16 00:00:00', 1032, 2064),
(4518, 9, 6, '2003-10-19 00:00:00', 830, 1660),
(4519, 9, 13, '2003-08-28 00:00:00', 2376, 0),
(4520, 9, 13, '2003-04-13 00:00:00', 2106, 0),
(4521, 9, 13, '2003-04-22 00:00:00', 2001, 0),
(4522, 9, 13, '2003-07-15 00:00:00', 1949, 0),
(4523, 9, 13, '2003-09-15 00:00:00', 1578, 0),
(4524, 9, 13, '2003-05-04 00:00:00', 1302, 0),
(4525, 9, 13, '2003-09-30 00:00:00', 797, 0),
(4526, 9, 13, '2003-12-03 00:00:00', 768, 0),
(4534, 9, 10, '2003-11-20 00:00:00', 2168, 0),
(4535, 9, 10, '2003-10-19 00:00:00', 1402, 0),
(4536, 9, 10, '2003-12-26 00:00:00', 991, 0),
(4538, 9, 14, '2003-09-27 00:00:00', 2477, 7431),
(4539, 9, 14, '2003-03-24 00:00:00', 2469, 7407),
(4540, 9, 14, '2003-02-09 00:00:00', 1796, 5388),
(4541, 9, 14, '2003-03-18 00:00:00', 1410, 4230),
(4542, 9, 4, '2003-11-07 00:00:00', 1935, 0),
(4543, 9, 4, '2003-11-04 00:00:00', 1844, 0),
(4544, 9, 4, '2003-06-01 00:00:00', 1825, 0),
(4545, 9, 4, '2003-02-27 00:00:00', 819, 0),
(4547, 9, 5, '2003-04-10 00:00:00', 2393, 4786),
(4548, 9, 5, '2003-03-20 00:00:00', 1891, 3782),
(4549, 9, 5, '2003-10-10 00:00:00', 1004, 2008),
(4550, 4, 11, '2003-04-12 00:00:00', 2433, 2433),
(4551, 4, 11, '2003-08-13 00:00:00', 765, 765),
(4552, 4, 11, '2003-12-26 00:00:00', 727, 727),
(4553, 4, 12, '2003-03-03 00:00:00', 2215, 4430),
(4554, 4, 12, '2003-02-08 00:00:00', 1980, 3960),
(4555, 4, 12, '2003-10-15 00:00:00', 1926, 3852),
(4556, 4, 12, '2003-08-17 00:00:00', 1566, 3132),
(4557, 4, 9, '2003-03-09 00:00:00', 2471, 2471),
(4558, 4, 9, '2003-11-18 00:00:00', 1368, 1368),
(4559, 4, 9, '2003-10-20 00:00:00', 1231, 1231),
(4560, 4, 9, '2003-12-30 00:00:00', 967, 967),
(4561, 4, 9, '2003-04-11 00:00:00', 648, 648),
(4562, 4, 7, '2003-10-23 00:00:00', 2367, 0),
(4563, 4, 7, '2003-05-23 00:00:00', 1282, 0),
(4564, 4, 7, '2003-02-10 00:00:00', 1209, 0),
(4565, 4, 7, '2003-05-03 00:00:00', 946, 0),
(4566, 4, 7, '2003-01-20 00:00:00', 630, 0),
(4567, 4, 7, '2003-03-20 00:00:00', 268, 0),
(4568, 4, 3, '2003-12-06 00:00:00', 2413, 2413),
(4569, 4, 3, '2003-06-05 00:00:00', 1911, 1911),
(4570, 4, 3, '2003-12-19 00:00:00', 1809, 1809),
(4571, 4, 3, '2003-05-09 00:00:00', 1400, 1400),
(4572, 4, 3, '2003-06-29 00:00:00', 1131, 1131),
(4573, 4, 3, '2003-01-14 00:00:00', 1122, 1122),
(4574, 4, 1, '2003-04-25 00:00:00', 2123, 6369),
(4575, 4, 1, '2003-08-14 00:00:00', 1988, 5964),
(4576, 4, 1, '2003-11-05 00:00:00', 885, 2655),
(4578, 4, 8, '2003-02-27 00:00:00', 1398, 4194),
(4579, 4, 8, '2003-09-29 00:00:00', 1193, 3579),
(4580, 4, 8, '2003-03-08 00:00:00', 1082, 3246),
(4581, 4, 6, '2003-11-22 00:00:00', 1452, 2904),
(4582, 4, 6, '2003-03-21 00:00:00', 1421, 2842),
(4583, 4, 6, '2003-12-15 00:00:00', 1152, 2304),
(4584, 4, 13, '2003-11-10 00:00:00', 2123, 0),
(4585, 4, 13, '2003-07-07 00:00:00', 1959, 0),
(4586, 4, 13, '2003-11-24 00:00:00', 1532, 0),
(4587, 4, 13, '2003-08-08 00:00:00', 1088, 0),
(4588, 4, 13, '2003-07-08 00:00:00', 1052, 0),
(4594, 4, 10, '2003-03-05 00:00:00', 580, 0),
(4597, 4, 14, '2003-10-24 00:00:00', 1654, 4962),
(4598, 4, 14, '2003-04-18 00:00:00', 1176, 3528),
(4599, 4, 14, '2003-12-10 00:00:00', 526, 1578),
(4600, 4, 4, '2003-03-25 00:00:00', 2064, 0),
(4601, 4, 4, '2003-12-21 00:00:00', 1830, 0),
(4602, 4, 4, '2003-01-29 00:00:00', 1142, 0),
(4603, 4, 5, '2003-05-01 00:00:00', 2455, 4910),
(4604, 4, 5, '2003-07-09 00:00:00', 2218, 4436),
(4605, 4, 5, '2003-06-30 00:00:00', 1873, 3746),
(4606, 4, 5, '2003-05-02 00:00:00', 1800, 3600),
(4608, 2, 11, '2003-06-20 00:00:00', 2358, 2358),
(4609, 2, 11, '2003-10-21 00:00:00', 1572, 1572),
(4610, 2, 11, '2003-02-17 00:00:00', 815, 815),
(4611, 2, 11, '2003-10-09 00:00:00', 630, 630),
(4612, 2, 12, '2003-06-23 00:00:00', 2207, 4414),
(4613, 2, 12, '2003-04-16 00:00:00', 2195, 4390),
(4614, 2, 12, '2003-07-11 00:00:00', 539, 1078),
(4616, 2, 9, '2003-01-08 00:00:00', 2155, 2155),
(4618, 2, 7, '2003-01-27 00:00:00', 2290, 0),
(4619, 2, 7, '2003-05-02 00:00:00', 2056, 0),
(4620, 2, 7, '2003-08-09 00:00:00', 1682, 0),
(4621, 2, 7, '2003-02-01 00:00:00', 1643, 0),
(4622, 2, 7, '2003-01-16 00:00:00', 1046, 0),
(4623, 2, 3, '2003-08-25 00:00:00', 2136, 2136),
(4624, 2, 3, '2003-09-23 00:00:00', 2091, 2091),
(4625, 2, 3, '2003-05-15 00:00:00', 1824, 1824),
(4626, 2, 3, '2003-12-19 00:00:00', 1185, 1185),
(4627, 2, 3, '2003-10-13 00:00:00', 1025, 1025),
(4628, 2, 3, '2003-02-25 00:00:00', 608, 608),
(4629, 2, 3, '2003-09-17 00:00:00', 295, 295),
(4630, 2, 1, '2003-01-14 00:00:00', 2218, 6654),
(4631, 2, 1, '2003-12-15 00:00:00', 2015, 6045),
(4632, 2, 1, '2003-12-18 00:00:00', 1927, 5781),
(4633, 2, 1, '2003-02-16 00:00:00', 865, 2595),
(4634, 2, 1, '2003-11-21 00:00:00', 739, 2217),
(4635, 2, 8, '2003-10-22 00:00:00', 1863, 5589),
(4636, 2, 8, '2003-08-22 00:00:00', 1557, 4671),
(4637, 2, 8, '2003-05-24 00:00:00', 1199, 3597),
(4638, 2, 8, '2003-08-30 00:00:00', 818, 2454),
(4639, 2, 8, '2003-06-30 00:00:00', 756, 2268),
(4640, 2, 8, '2003-01-05 00:00:00', 510, 1530),
(4642, 2, 6, '2003-10-14 00:00:00', 1784, 3568),
(4643, 2, 6, '2003-04-24 00:00:00', 1622, 3244),
(4644, 2, 6, '2003-04-12 00:00:00', 1419, 2838),
(4645, 2, 6, '2003-07-20 00:00:00', 666, 1332),
(4646, 2, 13, '2003-03-14 00:00:00', 1567, 0),
(4647, 2, 13, '2003-05-02 00:00:00', 823, 0),
(4650, 2, 10, '2003-06-12 00:00:00', 1569, 0),
(4651, 2, 14, '2003-06-10 00:00:00', 2330, 6990),
(4652, 2, 14, '2003-02-28 00:00:00', 1927, 5781),
(4653, 2, 14, '2003-02-15 00:00:00', 1032, 3096),
(4654, 2, 14, '2003-10-05 00:00:00', 720, 2160),
(4655, 2, 14, '2003-08-04 00:00:00', 608, 1824),
(4656, 2, 4, '2003-10-24 00:00:00', 2444, 0),
(4657, 2, 4, '2003-10-15 00:00:00', 2134, 0),
(4658, 2, 4, '2003-10-06 00:00:00', 1960, 0),
(4659, 2, 4, '2003-12-11 00:00:00', 1821, 0),
(4660, 2, 4, '2003-08-19 00:00:00', 1444, 0),
(4661, 2, 4, '2003-10-20 00:00:00', 1238, 0),
(4662, 2, 4, '2003-08-26 00:00:00', 957, 0),
(4663, 2, 5, '2003-08-04 00:00:00', 2390, 4780),
(4664, 2, 5, '2003-01-06 00:00:00', 1755, 3510),
(4665, 2, 5, '2003-02-18 00:00:00', 840, 1680),
(4666, 8, 11, '2003-04-29 00:00:00', 1883, 1883),
(4667, 8, 11, '2003-03-24 00:00:00', 1645, 1645),
(4669, 8, 12, '2003-07-28 00:00:00', 2226, 4452),
(4670, 8, 12, '2003-09-23 00:00:00', 1863, 3726),
(4671, 8, 12, '2003-12-06 00:00:00', 1591, 3182),
(4672, 8, 12, '2003-11-30 00:00:00', 266, 532),
(4673, 8, 9, '2003-03-24 00:00:00', 2265, 2265),
(4674, 8, 9, '2003-05-28 00:00:00', 1553, 1553),
(4675, 8, 9, '2003-08-15 00:00:00', 1279, 1279),
(4676, 8, 9, '2003-12-23 00:00:00', 990, 990),
(4677, 8, 9, '2003-05-10 00:00:00', 783, 783),
(4678, 8, 9, '2003-10-10 00:00:00', 607, 607),
(4680, 8, 7, '2003-02-04 00:00:00', 1597, 0),
(4681, 8, 7, '2003-08-27 00:00:00', 1149, 0),
(4684, 8, 3, '2003-01-23 00:00:00', 1790, 1790),
(4685, 8, 3, '2003-02-12 00:00:00', 1509, 1509),
(4686, 8, 3, '2003-10-13 00:00:00', 1135, 1135),
(4687, 8, 3, '2003-06-24 00:00:00', 976, 976),
(4688, 8, 1, '2003-04-11 00:00:00', 2480, 7440),
(4689, 8, 1, '2003-06-07 00:00:00', 1913, 5739),
(4690, 8, 1, '2003-06-06 00:00:00', 1833, 5499),
(4691, 8, 1, '2003-08-11 00:00:00', 1531, 4593),
(4692, 8, 1, '2003-03-23 00:00:00', 1414, 4242),
(4693, 8, 1, '2003-09-20 00:00:00', 587, 1761),
(4695, 8, 8, '2003-02-10 00:00:00', 2419, 7257),
(4696, 8, 8, '2003-08-23 00:00:00', 2294, 6882),
(4697, 8, 8, '2003-05-07 00:00:00', 767, 2301),
(4698, 8, 13, '2003-05-10 00:00:00', 2455, 0),
(4699, 8, 13, '2003-09-02 00:00:00', 2274, 0),
(4700, 8, 13, '2003-03-01 00:00:00', 2194, 0),
(4701, 8, 13, '2003-10-05 00:00:00', 1741, 0),
(4702, 8, 13, '2003-04-13 00:00:00', 1502, 0),
(4703, 8, 13, '2003-09-28 00:00:00', 1481, 0),
(4704, 8, 13, '2003-10-26 00:00:00', 1115, 0),
(4705, 8, 13, '2003-07-18 00:00:00', 900, 0),
(4714, 8, 10, '2003-08-15 00:00:00', 2079, 0),
(4715, 8, 10, '2003-09-15 00:00:00', 1745, 0),
(4716, 8, 10, '2003-04-26 00:00:00', 1389, 0),
(4717, 8, 10, '2003-03-27 00:00:00', 1256, 0),
(4718, 8, 10, '2003-01-26 00:00:00', 978, 0),
(4719, 8, 14, '2003-08-04 00:00:00', 2393, 7179),
(4720, 8, 14, '2003-06-25 00:00:00', 2315, 6945),
(4721, 8, 14, '2003-12-23 00:00:00', 1677, 5031),
(4722, 8, 14, '2003-07-28 00:00:00', 568, 1704),
(4724, 8, 4, '2003-05-07 00:00:00', 2311, 0),
(4725, 8, 4, '2003-04-07 00:00:00', 2187, 0),
(4726, 8, 4, '2003-02-06 00:00:00', 1464, 0),
(4727, 8, 4, '2003-07-31 00:00:00', 1201, 0),
(4728, 8, 5, '2003-01-25 00:00:00', 2154, 4308),
(4729, 8, 5, '2003-08-12 00:00:00', 1244, 2488),
(4730, 8, 5, '2003-04-18 00:00:00', 793, 1586),
(4731, 8, 5, '2003-01-03 00:00:00', 787, 1574),
(4732, 8, 5, '2003-10-20 00:00:00', 691, 1382),
(4733, 10, 11, '2003-12-07 00:00:00', 1628, 1628),
(4734, 10, 11, '2003-04-02 00:00:00', 1111, 1111),
(4735, 10, 11, '2003-01-23 00:00:00', 991, 991),
(4736, 10, 11, '2003-04-20 00:00:00', 766, 766),
(4737, 10, 11, '2003-11-17 00:00:00', 696, 696),
(4738, 10, 12, '2003-10-18 00:00:00', 2350, 4700),
(4739, 10, 12, '2003-07-09 00:00:00', 1859, 3718),
(4740, 10, 12, '2003-08-01 00:00:00', 1858, 3716),
(4741, 10, 12, '2003-05-05 00:00:00', 735, 1470),
(4743, 10, 9, '2003-12-12 00:00:00', 2167, 2167),
(4744, 10, 9, '2003-12-30 00:00:00', 2146, 2146),
(4745, 10, 9, '2003-01-04 00:00:00', 682, 682),
(4748, 10, 7, '2003-05-22 00:00:00', 2379, 0),
(4749, 10, 7, '2003-05-13 00:00:00', 1689, 0),
(4750, 10, 7, '2003-05-11 00:00:00', 1661, 0),
(4751, 10, 3, '2003-07-05 00:00:00', 2094, 2094),
(4752, 10, 3, '2003-09-13 00:00:00', 1603, 1603),
(4753, 10, 3, '2003-12-09 00:00:00', 1335, 1335),
(4754, 10, 3, '2003-06-19 00:00:00', 1062, 1062),
(4755, 10, 3, '2003-05-17 00:00:00', 546, 546),
(4758, 10, 1, '2003-06-03 00:00:00', 2471, 7413),
(4759, 10, 1, '2003-09-16 00:00:00', 2391, 7173),
(4760, 10, 1, '2003-06-17 00:00:00', 1750, 5250),
(4761, 10, 1, '2003-09-27 00:00:00', 1562, 4686),
(4762, 10, 1, '2003-08-25 00:00:00', 1260, 3780),
(4763, 10, 1, '2003-09-22 00:00:00', 1072, 3216),
(4764, 10, 1, '2003-12-09 00:00:00', 722, 2166),
(4765, 10, 1, '2003-09-23 00:00:00', 596, 1788),
(4766, 10, 1, '2003-03-12 00:00:00', 567, 1701),
(4768, 10, 8, '2003-04-25 00:00:00', 1501, 4503),
(4769, 10, 8, '2003-03-08 00:00:00', 1421, 4263),
(4770, 10, 8, '2003-08-18 00:00:00', 1158, 3474),
(4771, 10, 8, '2003-05-20 00:00:00', 1028, 3084),
(4772, 10, 8, '2003-01-02 00:00:00', 964, 2892),
(4773, 10, 8, '2003-12-12 00:00:00', 904, 2712),
(4775, 10, 6, '2003-03-06 00:00:00', 2249, 4498),
(4776, 10, 6, '2003-02-27 00:00:00', 2101, 4202),
(4777, 10, 6, '2003-02-01 00:00:00', 1559, 3118),
(4778, 10, 6, '2003-08-31 00:00:00', 1322, 2644),
(4779, 10, 6, '2003-05-21 00:00:00', 1168, 2336),
(4780, 10, 6, '2003-03-30 00:00:00', 744, 1488),
(4782, 10, 13, '2003-03-10 00:00:00', 2326, 0),
(4786, 10, 10, '2003-03-07 00:00:00', 2230, 0),
(4787, 10, 10, '2003-02-14 00:00:00', 1944, 0),
(4788, 10, 10, '2003-08-11 00:00:00', 1840, 0),
(4789, 10, 10, '2003-08-11 00:00:00', 771, 0),
(4790, 10, 14, '2003-02-09 00:00:00', 1751, 5253),
(4791, 10, 14, '2003-12-11 00:00:00', 1495, 4485),
(4792, 10, 14, '2003-06-08 00:00:00', 996, 2988),
(4793, 10, 14, '2003-07-02 00:00:00', 555, 1665),
(4796, 10, 4, '2003-08-13 00:00:00', 1915, 0),
(4797, 10, 4, '2003-10-02 00:00:00', 1228, 0),
(4798, 10, 5, '2003-10-01 00:00:00', 2181, 4362),
(4799, 10, 5, '2003-09-15 00:00:00', 1008, 2016),
(4800, 10, 5, '2003-06-17 00:00:00', 879, 1758),
(4801, 10, 5, '2003-02-06 00:00:00', 611, 1222),
(4803, 5, 11, '2003-01-20 00:00:00', 2393, 2393),
(4804, 5, 11, '2003-09-20 00:00:00', 2226, 2226),
(4805, 5, 11, '2003-08-30 00:00:00', 2009, 2009),
(4806, 5, 11, '2003-10-08 00:00:00', 1919, 1919),
(4807, 5, 11, '2003-02-10 00:00:00', 1517, 1517),
(4808, 5, 11, '2003-06-18 00:00:00', 1396, 1396),
(4809, 5, 12, '2003-03-06 00:00:00', 1905, 3810),
(4810, 5, 12, '2003-01-19 00:00:00', 1688, 3376),
(4811, 5, 12, '2003-05-04 00:00:00', 1289, 2578),
(4812, 5, 12, '2003-09-13 00:00:00', 1265, 2530),
(4813, 5, 12, '2003-08-10 00:00:00', 889, 1778),
(4814, 5, 9, '2003-12-26 00:00:00', 1238, 1238),
(4816, 5, 7, '2003-12-09 00:00:00', 1966, 0),
(4817, 5, 7, '2003-11-10 00:00:00', 1962, 0),
(4818, 5, 7, '2003-03-23 00:00:00', 1694, 0),
(4819, 5, 3, '2003-12-01 00:00:00', 2392, 2392),
(4820, 5, 3, '2003-11-01 00:00:00', 1132, 1132),
(4821, 5, 3, '2003-08-27 00:00:00', 288, 288),
(4822, 5, 1, '2003-05-17 00:00:00', 2281, 6843),
(4823, 5, 1, '2003-11-18 00:00:00', 2168, 6504),
(4824, 5, 1, '2003-09-24 00:00:00', 1696, 5088),
(4825, 5, 1, '2003-12-21 00:00:00', 1381, 4143),
(4826, 5, 1, '2003-02-19 00:00:00', 704, 2112),
(4827, 5, 1, '2003-03-26 00:00:00', 274, 822),
(4828, 5, 8, '2003-04-23 00:00:00', 2126, 6378),
(4829, 5, 8, '2003-02-18 00:00:00', 1829, 5487),
(4830, 5, 8, '2003-09-23 00:00:00', 1015, 3045),
(4831, 5, 8, '2003-08-13 00:00:00', 796, 2388),
(4832, 5, 6, '2003-12-20 00:00:00', 2375, 4750),
(4833, 5, 6, '2003-07-15 00:00:00', 2348, 4696),
(4834, 5, 6, '2003-06-02 00:00:00', 2032, 4064),
(4835, 5, 6, '2003-12-28 00:00:00', 1337, 2674),
(4836, 5, 13, '2003-12-28 00:00:00', 1827, 0),
(4837, 5, 13, '2003-08-28 00:00:00', 643, 0),
(4843, 5, 10, '2003-03-04 00:00:00', 2211, 0),
(4844, 5, 10, '2003-05-15 00:00:00', 2170, 0),
(4847, 5, 14, '2003-09-08 00:00:00', 2436, 7308),
(4848, 5, 14, '2003-07-13 00:00:00', 2271, 6813),
(4849, 5, 14, '2003-02-20 00:00:00', 2103, 6309),
(4850, 5, 14, '2003-10-20 00:00:00', 1968, 5904),
(4851, 5, 14, '2003-06-02 00:00:00', 1668, 5004),
(4852, 5, 14, '2003-05-28 00:00:00', 1599, 4797),
(4853, 5, 14, '2003-03-02 00:00:00', 1368, 4104),
(4854, 5, 14, '2003-01-03 00:00:00', 612, 1836),
(4856, 5, 4, '2003-10-27 00:00:00', 2239, 0),
(4857, 5, 4, '2003-04-04 00:00:00', 2216, 0),
(4858, 5, 4, '2003-12-06 00:00:00', 1888, 0),
(4859, 5, 4, '2003-06-19 00:00:00', 1871, 0),
(4860, 5, 4, '2003-11-21 00:00:00', 1853, 0),
(4861, 5, 4, '2003-10-03 00:00:00', 1050, 0),
(4862, 5, 4, '2003-04-30 00:00:00', 991, 0),
(4864, 5, 5, '2003-03-15 00:00:00', 2249, 4498),
(4865, 5, 5, '2003-02-06 00:00:00', 1977, 3954),
(4866, 5, 5, '2003-10-29 00:00:00', 1814, 3628),
(4867, 5, 5, '2003-10-16 00:00:00', 1560, 3120),
(4868, 5, 5, '2003-07-28 00:00:00', 962, 1924),
(4869, 5, 5, '2003-05-17 00:00:00', 805, 1610),
(4870, 5, 5, '2003-04-02 00:00:00', 523, 1046),
(4871, 12, 11, '2003-11-10 00:00:00', 1689, 1689),
(4872, 12, 11, '2003-10-10 00:00:00', 1042, 1042),
(4873, 12, 11, '2003-12-26 00:00:00', 1013, 1013),
(4874, 12, 11, '2003-04-13 00:00:00', 609, 609),
(4875, 12, 12, '2003-07-26 00:00:00', 2237, 4474),
(4876, 12, 12, '2003-08-21 00:00:00', 1739, 3478),
(4877, 12, 12, '2003-10-09 00:00:00', 1127, 2254),
(4878, 12, 12, '2003-01-08 00:00:00', 516, 1032),
(4879, 12, 12, '2003-02-15 00:00:00', 276, 552),
(4880, 12, 9, '2003-02-16 00:00:00', 2252, 2252),
(4881, 12, 9, '2003-10-08 00:00:00', 1952, 1952),
(4883, 12, 7, '2003-01-22 00:00:00', 2313, 0),
(4884, 12, 7, '2003-03-20 00:00:00', 1457, 0),
(4885, 12, 7, '2003-03-19 00:00:00', 881, 0),
(4886, 12, 7, '2003-12-16 00:00:00', 837, 0),
(4887, 12, 3, '2003-06-25 00:00:00', 1954, 1954),
(4888, 12, 3, '2003-05-23 00:00:00', 1889, 1889),
(4889, 12, 3, '2003-06-16 00:00:00', 1446, 1446),
(4890, 12, 3, '2003-01-17 00:00:00', 1426, 1426),
(4891, 12, 3, '2003-12-04 00:00:00', 535, 535),
(4893, 12, 1, '2003-11-11 00:00:00', 1648, 4944),
(4894, 12, 1, '2003-08-06 00:00:00', 1402, 4206),
(4895, 12, 1, '2003-11-05 00:00:00', 1130, 3390),
(4898, 12, 8, '2003-06-19 00:00:00', 2454, 7362),
(4899, 12, 8, '2003-11-11 00:00:00', 2420, 7260),
(4900, 12, 8, '2003-01-14 00:00:00', 2342, 7026),
(4901, 12, 8, '2003-08-19 00:00:00', 2150, 6450),
(4902, 12, 8, '2003-07-28 00:00:00', 1913, 5739),
(4903, 12, 8, '2003-02-14 00:00:00', 1163, 3489),
(4904, 12, 8, '2003-12-14 00:00:00', 270, 810),
(4905, 12, 6, '2003-04-14 00:00:00', 2164, 4328),
(4906, 12, 6, '2003-08-24 00:00:00', 2095, 4190),
(4907, 12, 6, '2003-12-16 00:00:00', 1557, 3114),
(4908, 12, 6, '2003-07-29 00:00:00', 1471, 2942),
(4909, 12, 6, '2003-09-01 00:00:00', 1249, 2498),
(4911, 12, 13, '2003-01-22 00:00:00', 2268, 0),
(4912, 12, 13, '2003-06-12 00:00:00', 2255, 0),
(4913, 12, 13, '2003-06-07 00:00:00', 1886, 0),
(4914, 12, 13, '2003-05-10 00:00:00', 1547, 0),
(4915, 12, 13, '2003-07-24 00:00:00', 1423, 0),
(4916, 12, 13, '2003-02-06 00:00:00', 1233, 0),
(4917, 12, 13, '2003-11-15 00:00:00', 1113, 0),
(4921, 12, 10, '2003-12-01 00:00:00', 2162, 0),
(4922, 12, 10, '2003-10-18 00:00:00', 1933, 0),
(4923, 12, 10, '2003-06-10 00:00:00', 1631, 0),
(4924, 12, 10, '2003-01-07 00:00:00', 1446, 0),
(4925, 12, 10, '2003-01-16 00:00:00', 1249, 0),
(4926, 12, 10, '2003-04-26 00:00:00', 1076, 0),
(4927, 12, 14, '2003-02-27 00:00:00', 1883, 5649),
(4929, 12, 4, '2003-03-03 00:00:00', 2263, 0),
(4930, 12, 4, '2003-05-01 00:00:00', 2024, 0),
(4931, 12, 4, '2003-06-23 00:00:00', 1995, 0),
(4932, 12, 4, '2003-12-12 00:00:00', 1647, 0),
(4933, 12, 4, '2003-12-11 00:00:00', 647, 0),
(4934, 12, 5, '2003-07-08 00:00:00', 1581, 3162),
(4935, 12, 5, '2003-05-22 00:00:00', 846, 1692),
(4936, 12, 5, '2003-12-02 00:00:00', 285, 570),
(4937, 13, 11, '2003-01-30 00:00:00', 2167, 2167),
(4938, 13, 11, '2003-11-02 00:00:00', 1802, 1802),
(4939, 13, 11, '2003-05-26 00:00:00', 508, 508),
(4940, 13, 12, '2003-02-04 00:00:00', 893, 1786),
(4941, 13, 12, '2003-09-20 00:00:00', 855, 1710),
(4942, 13, 12, '2003-07-01 00:00:00', 686, 1372),
(4943, 13, 12, '2003-06-24 00:00:00', 598, 1196),
(4944, 13, 12, '2003-03-22 00:00:00', 557, 1114),
(4947, 13, 9, '2003-11-18 00:00:00', 1678, 1678),
(4948, 13, 9, '2003-10-06 00:00:00', 997, 997),
(4949, 13, 7, '2003-10-16 00:00:00', 2482, 0),
(4950, 13, 7, '2003-11-30 00:00:00', 2338, 0),
(4951, 13, 7, '2003-12-10 00:00:00', 1916, 0),
(4952, 13, 7, '2003-07-11 00:00:00', 1591, 0),
(4953, 13, 7, '2003-02-23 00:00:00', 1519, 0),
(4954, 13, 7, '2003-03-31 00:00:00', 1017, 0),
(4955, 13, 7, '2003-02-04 00:00:00', 745, 0),
(4956, 13, 7, '2003-07-22 00:00:00', 603, 0),
(4957, 13, 3, '2003-09-07 00:00:00', 2051, 2051),
(4958, 13, 3, '2003-08-03 00:00:00', 1164, 1164),
(4959, 13, 3, '2003-01-26 00:00:00', 1161, 1161),
(4960, 13, 3, '2003-05-28 00:00:00', 1144, 1144),
(4962, 13, 3, '2003-12-21 00:00:00', 285, 285),
(4963, 13, 1, '2003-12-25 00:00:00', 2314, 6942),
(4964, 13, 1, '2003-05-02 00:00:00', 1351, 4053),
(4965, 13, 1, '2003-11-01 00:00:00', 1066, 3198),
(4966, 13, 1, '2003-12-10 00:00:00', 1045, 3135),
(4967, 13, 1, '2003-03-21 00:00:00', 612, 1836),
(4970, 13, 8, '2003-10-27 00:00:00', 1830, 5490),
(4971, 13, 8, '2003-08-13 00:00:00', 1439, 4317),
(4972, 13, 8, '2003-04-29 00:00:00', 728, 2184),
(4973, 13, 8, '2003-09-14 00:00:00', 578, 1734),
(4974, 13, 13, '2003-06-26 00:00:00', 2122, 0),
(4975, 13, 13, '2003-08-18 00:00:00', 1917, 0),
(4976, 13, 13, '2003-05-13 00:00:00', 745, 0),
(4977, 13, 13, '2003-05-26 00:00:00', 739, 0),
(4978, 13, 13, '2003-05-18 00:00:00', 574, 0),
(4983, 13, 10, '2003-04-05 00:00:00', 1860, 0),
(4984, 13, 10, '2003-11-12 00:00:00', 1519, 0),
(4985, 13, 10, '2003-05-29 00:00:00', 1027, 0),
(4986, 13, 14, '2003-06-06 00:00:00', 1857, 5571),
(4987, 13, 14, '2003-08-06 00:00:00', 550, 1650),
(4988, 13, 4, '2003-10-21 00:00:00', 2206, 0),
(4989, 13, 4, '2003-10-29 00:00:00', 2135, 0),
(4990, 13, 4, '2003-05-28 00:00:00', 2090, 0),
(4991, 13, 4, '2003-02-05 00:00:00', 1746, 0),
(4992, 13, 4, '2003-06-26 00:00:00', 1109, 0),
(4993, 13, 4, '2003-07-13 00:00:00', 535, 0),
(4994, 13, 5, '2003-02-20 00:00:00', 2005, 4010),
(4995, 13, 5, '2003-03-22 00:00:00', 1910, 3820),
(4996, 13, 5, '2003-09-18 00:00:00', 1889, 3778),
(4997, 13, 5, '2003-09-04 00:00:00', 1359, 2718),
(4998, 2, 3, '2004-05-12 00:00:00', 251, 251);

-- --------------------------------------------------------

--
-- Table structure for table `verduras`
--

CREATE TABLE `verduras` (
  `id` int NOT NULL,
  `NomProducto` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `verduras`
--

INSERT INTO `verduras` (`id`, `NomProducto`) VALUES
(1, 'Lechugas'),
(2, 'Coles'),
(3, 'Tomates');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `grupos`
--
ALTER TABLE `grupos`
  ADD PRIMARY KEY (`IdGrupo`);

--
-- Indexes for table `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`IdProducto`),
  ADD KEY `FK_productos_grupos_IdGrupo` (`IdGrupo`);

--
-- Indexes for table `productosvendidos`
--
ALTER TABLE `productosvendidos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vendedores`
--
ALTER TABLE `vendedores`
  ADD PRIMARY KEY (`IdVendedor`);

--
-- Indexes for table `ventas`
--
ALTER TABLE `ventas`
  ADD PRIMARY KEY (`idventas`),
  ADD KEY `FK_ventas_productos_IdProducto` (`Cod Producto`),
  ADD KEY `FK_ventas_vendedores_IdVendedor` (`Cod Vendedor`);

--
-- Indexes for table `verduras`
--
ALTER TABLE `verduras`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `grupos`
--
ALTER TABLE `grupos`
  MODIFY `IdGrupo` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `productos`
--
ALTER TABLE `productos`
  MODIFY `IdProducto` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `productosvendidos`
--
ALTER TABLE `productosvendidos`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `vendedores`
--
ALTER TABLE `vendedores`
  MODIFY `IdVendedor` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `ventas`
--
ALTER TABLE `ventas`
  MODIFY `idventas` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4999;

--
-- AUTO_INCREMENT for table `verduras`
--
ALTER TABLE `verduras`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `productos`
--
ALTER TABLE `productos`
  ADD CONSTRAINT `FK_productos_grupos_IdGrupo` FOREIGN KEY (`IdGrupo`) REFERENCES `grupos` (`IdGrupo`);

--
-- Constraints for table `ventas`
--
ALTER TABLE `ventas`
  ADD CONSTRAINT `FK_ventas_productos_IdProducto` FOREIGN KEY (`Cod Producto`) REFERENCES `productos` (`IdProducto`),
  ADD CONSTRAINT `FK_ventas_vendedores_IdVendedor` FOREIGN KEY (`Cod Vendedor`) REFERENCES `vendedores` (`IdVendedor`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
