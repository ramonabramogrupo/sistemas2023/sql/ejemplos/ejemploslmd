﻿USE empresas;

-- 1
-- listar todos los registros de personas

SELECT * FROM personas p;

SELECT p.id,
       p.nombre,
       p.apellidos,
       p.poblacion,
       p.fecha,
       p.dato1,
       p.dato2,
       p.dato3,
       p.fechaTrabaja,
       p.empresa FROM personas p;

-- 2
-- listar todos los registro de empresas

SELECT * FROM empresas e;

SELECT e.codigo,
       e.nombre,
       e.poblacion,
       e.cp,
       e.trabajadores FROM empresas e;

-- 3 
-- listar las tablas de la base de datos

SHOW TABLES;

SHOW TABLES FROM empresas;


-- 4
-- Listar el nombre y la poblacion de las empresas

SELECT e.nombre, e.poblacion FROM empresas e;









