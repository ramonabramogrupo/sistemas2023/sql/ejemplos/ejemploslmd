﻿/* PRACTICA 10 */
/* CONSULTAS DE TOTALES */
USE ciclistas;


-- 1	Numero de ciclistas que hay

-- opcion ideal
SELECT COUNT(*) numero FROM ciclista c;

-- opcion correcta
SELECT COUNT(c.dorsal) numero FROM ciclista c;

-- esta opcion cuenta los ciclistas de los cuales conozco la edad
SELECT COUNT(c.edad) numero FROM ciclista c;

-- 2	Numero de ciclistas que hay del equipo Banesto

-- opcion directa
SELECT 
    count(*) numero 
  FROM ciclista c 
  WHERE c.nomequipo='banesto';

-- utilizando funciones para evitar problemas con 
-- mayusculas y minusculas

-- AYUDA

-- FUNCION LOWER(campo) ==> te devuelve el valor del campo en minusculas
SELECT 
    count(*) numero 
  FROM ciclista c 
  WHERE LOWER(c.nomequipo)='banesto';

-- FUNCION UPPER(campo) ==> te devuelve el valor del campo en mayusculas
  
SELECT 
    count(*) numero 
  FROM ciclista c 
  WHERE UPPER(c.nomequipo)='BANESTO';


-- 3	La edad media de los ciclistas

-- FUNCION DE TOTALES A UTILIZAR
-- AVG(CAMPO) ==> RETORNA EL VALOR MEDIO DE ESE CAMPO

SELECT 
  AVG(c.edad) edadMedia
  FROM ciclista c;

-- 4	La edad media de los de equipo Banesto

SELECT 
    AVG(c.edad) edadMedia 
  FROM ciclista c
  WHERE c.nomequipo='banesto';

-- 5	La edad media de los ciclistas por cada equipo

-- AYUDA
-- COMO LO QUIERO POR EQUIPO NECESITO 
-- GROUP BY

-- opcion directa
SELECT 
    c.nomequipo,
    AVG(c.edad) edadMedia
  FROM ciclista c
  GROUP BY c.nomequipo;

-- quitar decimales
-- utilizar funciones
-- AYUDA
-- TRUNCATE(campo,numero decimales)
-- ROUND(campo,numero decimales)
SELECT 
    c.nomequipo,
    ROUND(AVG(c.edad),2) edadMediaRedondeada,
    TRUNCATE(AVG(c.edad),2) edadMediaTruncada,
    AVG(c.edad) edadMedia
  FROM ciclista c
  GROUP BY c.nomequipo;

-- 6	El numero de ciclistas por equipo	
  
  SELECT 
    c.nomequipo, 
    COUNT(*) numero
  FROM 
    ciclista c 
  GROUP BY 
    c.nomequipo;

-- 7	El número total de puertos

-- contar directamente
SELECT 
    COUNT(*) numeroPuertos 
  FROM puerto p;

-- contar por la clave principal
SELECT 
    COUNT(p.nompuerto) numeroPuertos 
  FROM puerto p;

-- contar el numero de puertos de los cuales
-- conozco el ganador
SELECT 
    COUNT(p.dorsal) numeroPuertos 
  FROM puerto p;

-- 8	El número total de puertos mayores de 1500
SELECT 
    count(*) numeroPuertos
  FROM puerto p
  WHERE p.altura>1500;

-- 9	Listar el nombre de los equipos que tengan más de 4 ciclistas

-- utilizando el alias en el having
SELECT 
    c.nomequipo,
    COUNT(*) numeroCiclistas 
  FROM ciclista c
  GROUP BY c.nomequipo
  HAVING numeroCiclistas>4;

-- utilizando la funcion de totales en el having
-- no necesito mostrar el numero de ciclistas
SELECT 
    c.nomequipo
  FROM ciclista c
  GROUP BY c.nomequipo
  HAVING count(*)>4;

-- QUIERO REALIZAR LA CONSULTA SIN HAVING

-- Consulta C1
-- Numero de ciclistas por equipo
SELECT 
    c.nomequipo,
    count(*) numeroCiclistas 
  FROM ciclista c
  GROUP BY c.nomequipo;

-- ahora pensar que C1 es como una tabla
-- tabla C1(nomequipo,numeroCiclistas)

-- ahora quiero sacar de la tabla C1 los equipos donde numeroCiclistas>4

SELECT 
  * 
  FROM (
        SELECT 
            c.nomequipo,
            count(*) numeroCiclistas 
          FROM ciclista c
          GROUP BY c.nomequipo
  )C1
  WHERE numeroCiclistas>4;

-- puedo crear la vista C1
CREATE OR REPLACE VIEW c1 AS 
SELECT 
    c.nomequipo,
    count(*) numeroCiclistas 
  FROM ciclista c
  GROUP BY c.nomequipo;

-- ahora puedo utilizar C1 como si fuera una tabla (vista)
SELECT 
    * 
  FROM c1
  WHERE numeroCiclistas>4; 

-- 10	Listar el nombre de los equipos que tengan más de 4 ciclistas cuya edad este entre 28 y 32


--  me pregunto
-- ¿de donde saco la edad? ==> de la tabla ciclistas (WHERE)
-- ¿de donde saco que tengan los equipos mas de 4 ciclistas? ==> (GROUP BY + COUNT = HAVING)

-- muestra el nombre de los equipos y el numero de ciclistas
SELECT 
    c.nomequipo,
    COUNT(*) numeroCiclistas 
  FROM ciclista c
  WHERE c.edad BETWEEN 28 AND 32
  GROUP BY c.nomequipo
  HAVING numeroCiclistas>4;

-- si solo quiero mostrar el nombre de los equipos
SELECT 
    c.nomequipo
  FROM ciclista c
  WHERE c.edad BETWEEN 28 AND 32
  GROUP BY c.nomequipo
  HAVING count(*)>4;

-- 11	Indícame el número de etapas que ha ganado cada uno de los ciclistas

SELECT
    e.dorsal,
    count(*) numeroEtapas
  FROM etapa e
  GROUP BY e.dorsal;

-- si me piden el nombre del ciclista

SELECT
    e.dorsal,
    c.nombre,
    count(*) numeroEtapas
  FROM etapa e JOIN ciclista c ON e.dorsal = c.dorsal
  GROUP BY e.dorsal;

-- esta consulta optimizar

-- Consulta C1
-- Saco el dorsal y el numero de etapas que ha ganado
-- cada ciclista
SELECT
    e.dorsal,
    count(*) numeroEtapas
  FROM etapa e
  GROUP BY e.dorsal;

-- Junto (JOIN) la consulta C1 con la tabla ciclista
-- para obtener el nombre

SELECT 
    c1.*,
    c.nombre 
  FROM (
        SELECT
            e.dorsal,
            count(*) numeroEtapas
          FROM etapa e
          GROUP BY e.dorsal
  ) c1 JOIN ciclista c ON c.dorsal=c1.dorsal;

-- 12	Indícame el dorsal de los ciclistas que hayan ganado más de 1 etapa	
  
SELECT 
    e.dorsal
  FROM etapa e
  GROUP BY e.dorsal
  HAVING count(*)>1;

-- si queremos nombre del ciclista junto con el dorsal
-- necesitamos unir las tablas etapa con ciclista
-- ¿JOIN las tablas antes del group by o despues del
-- group by?

-- esta consulta esta sin optimizar
SELECT 
    c.dorsal,
    c.nombre 
  FROM etapa e JOIN ciclista c ON e.dorsal = c.dorsal
  GROUP BY e.dorsal
  HAVING count(*)>1;

-- optimizar

-- la llamo C1
SELECT 
    e.dorsal
  FROM etapa e
  GROUP BY e.dorsal
  HAVING count(*)>1;    

-- termino la consulta
-- C1 JOIN ciclista

SELECT 
    c.dorsal,
    c.nombre 
  FROM (
      SELECT 
        e.dorsal
      FROM etapa e
      GROUP BY e.dorsal
      HAVING count(*)>1
  ) C1 JOIN ciclista c ON c.dorsal=C1.dorsal;
      



