﻿-- MySQL Administrator dump 1.4
--
-- ------------------------------------------------------
-- Server version	5.1.37


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


--
-- Create schema sp
--
DROP DATABASE IF EXISTS sp;
CREATE DATABASE IF NOT EXISTS sp;
USE sp;

--
-- Definition of table `p`
--

DROP TABLE IF EXISTS `p`;
CREATE TABLE `p` (
  `p` varchar(2) NOT NULL,
  `pnombre` varchar(50) DEFAULT NULL,
  `color` varchar(20) DEFAULT NULL,
  `peso` smallint(5) DEFAULT NULL,
  `ciudad` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`p`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `p`
--

/*!40000 ALTER TABLE `p` DISABLE KEYS */;
INSERT INTO `p` (`p`,`pnombre`,`color`,`peso`,`ciudad`) VALUES 
 ('P1','tuerca','verde',12,'París'),
 ('P2','perno','rojo',17,'Londres'),
 ('P3','birlo','azul',17,'Roma'),
 ('P4','birlo','rojo',14,'Londres'),
 ('P5','leva','azul',12,'París'),
 ('P6','engrane','rojo',19,'París');
/*!40000 ALTER TABLE `p` ENABLE KEYS */;


--
-- Definition of table `s`
--

DROP TABLE IF EXISTS `s`;
CREATE TABLE `s` (
  `s` varchar(2) NOT NULL,
  `snombre` varchar(50) DEFAULT NULL,
  `estado` smallint(5) DEFAULT NULL,
  `ciudad` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`s`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `s`
--

/*!40000 ALTER TABLE `s` DISABLE KEYS */;
INSERT INTO `s` (`s`,`snombre`,`estado`,`ciudad`) VALUES 
 ('S1','Salazar',20,'Londres'),
 ('S2','Jaimes',10,'París'),
 ('S3','Bernal',30,'París'),
 ('S4','Corona',20,'Londres'),
 ('S5','Aldana',30,'Atenas');
/*!40000 ALTER TABLE `s` ENABLE KEYS */;


--
-- Definition of table `sp`
--

DROP TABLE IF EXISTS `sp`;
CREATE TABLE `sp` (
  `s` varchar(2) NOT NULL,
  `p` varchar(2) NOT NULL,
  `cant` smallint(5) DEFAULT NULL,
  PRIMARY KEY (`s`,`p`),
  KEY `PSP` (`p`),
  KEY `SSP` (`s`),
  CONSTRAINT `FK_sp_1` FOREIGN KEY (`s`) REFERENCES `s` (`s`),
  CONSTRAINT fkSpP FOREIGN KEY (p) REFERENCES p(p)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sp`
--

/*!40000 ALTER TABLE `sp` DISABLE KEYS */;
INSERT INTO `sp` (`s`,`p`,`cant`) VALUES 
 ('S1','P1',300),
 ('S1','P2',200),
 ('S1','P3',400),
 ('S1','P4',200),
 ('S1','P5',100),
 ('S1','P6',100),
 ('S2','P1',300),
 ('S2','P2',400),
 ('S3','P2',200),
 ('S4','P2',200),
 ('S4','P4',300),
 ('S4','P5',400);
/*!40000 ALTER TABLE `sp` ENABLE KEYS */;




/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
