﻿-- phpMyAdmin SQL Dump
-- version 3.2.0.1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 25-09-2014 a las 16:52:45
-- Versión del servidor: 5.1.37
-- Versión de PHP: 5.3.0

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `al3`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `j`
--

DROP DATABASE IF EXISTS spj;
CREATE DATABASE spj;
USE spj;

DROP TABLE IF EXISTS `j`;
CREATE TABLE IF NOT EXISTS `j` (
  `j` varchar(9) COLLATE utf8_spanish_ci NOT NULL,
  `nomj` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `ciudad` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`j`),
  UNIQUE KEY `j_UNIQUE` (`j`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcar la base de datos para la tabla `j`
--

INSERT INTO `j` (`j`, `nomj`, `ciudad`) VALUES
('J1', 'Edificio1', 'París'),
('J2', 'Edificio2', 'Roma'),
('J3', 'Edificio3', 'Atenas'),
('J4', 'Edificio4', 'Atenas'),
('J5', 'Edificio5', 'Londres'),
('J6', 'Edificio6', 'Madrid'),
('J7', 'Edificio7', 'Londres');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `p`
--

DROP TABLE IF EXISTS `p`;
CREATE TABLE IF NOT EXISTS `p` (
  `P` varchar(9) COLLATE utf8_spanish_ci NOT NULL,
  `nomp` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `color` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `peso` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `ciudad` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`P`),
  UNIQUE KEY `P_UNIQUE` (`P`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcar la base de datos para la tabla `p`
--

INSERT INTO `p` (`P`, `nomp`, `color`, `peso`, `ciudad`) VALUES
('P1', 'MESA', 'ROJO', '12', 'LONDRES'),
('P2', 'SILLA', 'BLANCA', '17', 'PARIS'),
('P3', 'ARMARIO', 'GRIS', '17', 'ROMA'),
('P4', 'ARCHIVADOR', 'ROJO', '14', 'LONDRES'),
('P5', 'PUERTA ', 'BLANCA', '12', 'PARIS'),
('P6', 'LAMPARA', 'AMARILLA', '19', 'LONDRES');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `s`
--

DROP TABLE IF EXISTS `s`;
CREATE TABLE IF NOT EXISTS `s` (
  `s` varchar(9) COLLATE utf8_spanish_ci NOT NULL,
  `noms` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `estado` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `ciudad` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`s`),
  UNIQUE KEY `s_UNIQUE` (`s`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcar la base de datos para la tabla `s`
--

INSERT INTO `s` (`s`, `noms`, `estado`, `ciudad`) VALUES
('S1', 'Smith', '20', 'Londres'),
('S2', 'Jones', '10', 'París'),
('S3', 'Blake', '30', 'París'),
('S4', 'Clark', '20', 'Londres'),
('S5', 'Adams', '30', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `spj`
--

DROP TABLE IF EXISTS `spj`;
CREATE TABLE IF NOT EXISTS `spj` (
  `s` varchar(9) COLLATE utf8_spanish_ci NOT NULL,
  `p` varchar(9) COLLATE utf8_spanish_ci NOT NULL,
  `j` varchar(9) COLLATE utf8_spanish_ci NOT NULL,
  `cant` int(11) DEFAULT NULL,
  PRIMARY KEY (`s`,`p`,`j`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

ALTER TABLE spj
  ADD CONSTRAINT fkSpjS 
    FOREIGN KEY (s) REFERENCES s(s) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT fkSpjP
    FOREIGN KEY (p) REFERENCES p(P) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT fkSpjJ
    FOREIGN KEY (j) REFERENCES j(j) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Volcar la base de datos para la tabla `spj`
--

INSERT INTO `spj` (`s`, `p`, `j`, `cant`) VALUES
('s1', 'p1', 'j1', 200),
('s1', 'p1', 'j4', 200),
('s1', 'p1', 'j5', 700),
('s1', 'p3', 'j2', 20),
('s1', 'p3', 'j3', 30),
('s1', 'p3', 'j4', 40),
('s1', 'p3', 'j5', 50),
('s1', 'p3', 'j6', 400),
('s1', 'p3', 'j7', 70),
('s2', 'p3', 'j1', 400),
('s2', 'p3', 'j2', 200),
('s2', 'p3', 'j3', 200),
('s2', 'p3', 'j4', 500),
('s2', 'p3', 'j5', 600),
('s2', 'p3', 'j7', 800),
('s2', 'p5', 'j2', 100),
('s3', 'p3', 'j1', 200),
('s3', 'p4', 'j2', 500),
('s4', 'p6', 'j3', 300),
('s4', 'p6', 'j7', 300);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
